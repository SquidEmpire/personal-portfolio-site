"use strict";

import * as THREE from './vendor/three.module.js';
import Stats from './vendor/stats.module.js';

import { OrbitControls } from './vendor/OrbitControls.js';
import { RoomEnvironment } from './vendor/RoomEnvironment.js';
import { GLTFLoader } from './vendor/GLTFLoader.js';
import { DRACOLoader } from './vendor/DRACOLoader.js';

import { OutlineEffect } from './vendor/OutlineEffect.js';


import { EffectComposer } from "./vendor/postprocessing/EffectComposer.js";
import { RenderPass } from "./vendor/postprocessing/RenderPass.js";
import { ShaderPass } from "./vendor/postprocessing/ShaderPass.js";
import { FXAAShader } from "./vendor/shaders/FXAAShader.js";

import { CustomOutlinePass } from "./vendor/webgl-outlines/CustomOutlinePass.js";
import FindSurfaces from "./vendor/webgl-outlines/FindSurfaces.js";


var renderer, scene, camera, stats, mixer, controls;
var effect;
var webglOutlineComposer;
var mode = 0; //0 - gradient only, 1 - outline shader, 2 - webgl shader

const origin = new THREE.Vector3(0,0,0);

const clock = new THREE.Clock();

Start();

//https://threejs.org/examples/#webgl_animation_keyframes
//https://threejs.org/examples/webgl_materials_toon.html
//https://github.com/OmarShehata/webgl-outlines

//TODO
//https://threejs-outlines-postprocess.glitch.me/

function Start() {

    document.getElementById("effectSelect").addEventListener("change", (e) => {
        mode = parseInt(e.target.value);
    });

    stats = new Stats();
	document.body.appendChild( stats.dom );
    stats.dom.id = "stats";

    renderer = new THREE.WebGLRenderer( { antialias: false } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight, false );
    renderer.outputEncoding = THREE.sRGBEncoding;

    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.BasicShadowMap;
    renderer.physicallyCorrectLights = true;

    document.body.appendChild( renderer.domElement );

    scene = new THREE.Scene();
    const pmremGenerator = new THREE.PMREMGenerator( renderer );
    scene.background = new THREE.Color( 0xbfe3dd );
    // scene.environment = pmremGenerator.fromScene( new RoomEnvironment( renderer ), 0.04 ).texture;

    AddLights();

    camera = new THREE.PerspectiveCamera( 40, window.innerWidth / window.innerHeight, 1, 100 );
    camera.position.set( 5, 2, 8 );

    controls = new OrbitControls( camera, renderer.domElement );
    controls.target.set( 0, 0.5, 0 );
    controls.update();
    controls.enablePan = false;
    controls.enableDamping = true;



    //webgl outline init
    const depthTexture = new THREE.DepthTexture();
    const renderTarget = new THREE.WebGLRenderTarget(
        window.innerWidth,
        window.innerHeight,
        {
            depthTexture: depthTexture,
            depthBuffer: true,
        }
    );
    // Initial render pass.
    webglOutlineComposer = new EffectComposer(renderer, renderTarget);
    const pass = new RenderPass(scene, camera);
    webglOutlineComposer.addPass(pass);

    // Outline pass.
    const customOutline = new CustomOutlinePass(
        new THREE.Vector2(window.innerWidth, window.innerHeight),
        scene,
        camera
    );
    webglOutlineComposer.addPass(customOutline);

    // Antialias pass.
    const effectFXAA = new ShaderPass(FXAAShader);
    effectFXAA.uniforms["resolution"].value.set(
        1 / window.innerWidth,
        1 / window.innerHeight
    );
    // webglOutlineComposer.addPass(effectFXAA);

    const surfaceFinder = new FindSurfaces();

    const uniforms = customOutline.fsQuad.material.uniforms;
    uniforms.outlineColor.value.set(0x000000);





    const dracoLoader = new DRACOLoader();
    dracoLoader.setDecoderPath( './vendor/libs/draco-gltf/' );

    const loader = new GLTFLoader();
    loader.setDRACOLoader( dracoLoader );
    loader.load( './models/LittlestTokyo.glb', function ( gltf ) {

        const format = ( renderer.capabilities.isWebGL2 ) ? THREE.RedFormat : THREE.LuminanceFormat;
        const colors = new Uint8Array( 2 );
        for ( let c = 0; c <= colors.length; c ++ ) {
            colors[ c ] = ( c / colors.length ) * 256;
        }
        const gradientMap = new THREE.DataTexture( colors, colors.length, 1, format );
        gradientMap.needsUpdate = true;

        const model = gltf.scene;
        model.position.set( 1, 1, 0 );
        model.scale.set( 0.01, 0.01, 0.01 );

        surfaceFinder.surfaceId = 0;

        model.traverse ( function ( child )
        {
            if ( child.isMesh )
            {
                child.castShadow = true;
                child.receiveShadow = true;
                // child.material.metalness = 0;
                // child.metalness = 0;

                let oldMaterial = child.material;
                child.material = new THREE.MeshToonMaterial();

                THREE.MeshBasicMaterial.prototype.copy.call( child.material, oldMaterial );

                child.material.gradientMap = gradientMap;


                const colorsTypedArray = surfaceFinder.getSurfaceIdAttribute(child);
                child.geometry.setAttribute(
                    "color",
                    new THREE.BufferAttribute(colorsTypedArray, 4)
                );
            }
        });

        scene.add( model );

        mixer = new THREE.AnimationMixer( model );
        mixer.clipAction( gltf.animations[ 0 ] ).play();

        effect = new OutlineEffect( renderer );

        customOutline.updateMaxSurfaceId(surfaceFinder.surfaceId + 1);

        Animate();

    }, undefined, function ( e ) {

        console.error( e );

    } );

    window.addEventListener( 'resize', OnWindowResize, false );

    camera.lookAt( origin );
    scene.add(camera);
}

function AddLights() {

    //ambient
    const ambientLight = new THREE.AmbientLight( 0x66adff );
    scene.add( ambientLight );

    //sun
    const sunSphere = new THREE.Mesh(
        new THREE.SphereGeometry( 0.1, 8, 8 ),
        new THREE.MeshBasicMaterial( { color: 0xffffff } )
    );
    scene.add( sunSphere );
    sunSphere.position.set(2, 4, 4);

    const sunLight = new THREE.PointLight( 0xffffff, 4, 0, 0 );
    sunLight.castShadow = true;
    sunLight.shadow.bias = -0.001;
	sunSphere.add( sunLight );

    //neon light
    const neonSphere = new THREE.Mesh(
        new THREE.SphereGeometry( 0.1, 8, 8 ),
        new THREE.MeshBasicMaterial( { color: 0xffaaff } )
    );
    scene.add( neonSphere );
    neonSphere.position.set(1, 0, 0);

    const neonLight = new THREE.PointLight( 0xffaaff, 2, 0, 0 );
    neonLight.castShadow = true;
    neonLight.shadow.bias = -0.001;
	neonSphere.add( neonLight );
}

function OnWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight, false );
}

function Animate() {
    requestAnimationFrame( Animate );
    const delta = clock.getDelta();
    mixer.update( delta );
    controls.update();
    stats.update();
    Render();
}

function Render() {
    switch (mode)
    {
        case 2:
            webglOutlineComposer.render();
            break;
        case 1:
            effect.render( scene, camera );
            break;
        default:
            renderer.render( scene, camera );
    }
}