"use strict";

import * as THREE from './vendor/three.module.js';
import Stats from './vendor/stats.module.js';
import RendererStats from './vendor/threex.rendererstats.js';
import * as BufferGeometryUtils from './vendor/BufferGeometryUtils.js';

import { OutlineEffect } from './vendor/OutlineEffect.js';

import { ColladaLoader } from './vendor/ColladaLoader.js';
import { GLTFLoader } from './vendor/GLTFLoader.js';

import { OrbitControls } from './vendor/OrbitControls.js';

var renderer, scene, camera, stats, rendererStats, controls;
var effect;
const assets = [];
const instances = new Map();

let l1;

class Asset {

    name;
    path;
    type = "File";
    contents;

    constructor( name, path, type ) {
        this.name = name;
        this.path = path;
        this.type = type;
    }

    setContents(contents) {
        this.contents = contents;
    }

    clone() {
        let clone = new Asset(this.name, this.path, this.type);
        clone.setContents(this.contents.clone());
        return clone;
    }

};

Start();

async function Start() {

    stats = new Stats();
	document.body.appendChild( stats.dom );
    stats.dom.id = "stats";

    rendererStats = new RendererStats()
	rendererStats.domElement.id = "renderer-stats";
	document.body.appendChild( rendererStats.domElement )

    THREE.Object3D.DefaultUp = new THREE.Vector3(0,0,1);

    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight, false );
    renderer.outputEncoding = THREE.sRGBEncoding;

    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.BasicShadowMap;
    renderer.physicallyCorrectLights = true;

    document.body.appendChild( renderer.domElement );

    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0xbfe3dd );

    AddLights();

    camera = new THREE.PerspectiveCamera( 40, window.innerWidth / window.innerHeight, 0.1, 100 );
    camera.position.set(-12, -12, 10);

    controls = new OrbitControls( camera, renderer.domElement );
    controls.update();
    controls.enableDamping = true;

    controls.target.set(12, 12, 0);


    const colladaLoader = new ColladaLoader();
    const gltfLoader = new GLTFLoader();

    assets.push( new Asset('brick_lod_1', './models/output/bricks_lod1.gltf', "gltf"));
    assets.push( new Asset('brick_lod_2', './models/output/bricks_lod2.gltf', "gltf"));
    assets.push( new Asset('brick_lod_3', './models/output/bricks_lod3.gltf', "gltf"));
    assets.push( new Asset('brick_lod_4', './models/output/bricks_lod4.gltf', "gltf"));

    assets.push( new Asset('bricksScene', './models/bricksScene.dae', "dae"));

    let assetPromises = [];

    assets.forEach(asset => {
        assetPromises.push(new Promise(function(resolve, reject) {
            if (asset.type === "dae") {
                colladaLoader.load(asset.path, (objectContent) => {
                    OnLoadModel(objectContent);
                    asset.setContents( objectContent.scene );
                    resolve();
                }, undefined, (e) => {
                    reject(e);
                });
            } else {
                gltfLoader.load(asset.path, (objectContent) => {
                    OnLoadModel(objectContent);
                    asset.setContents( objectContent.scene );
                    resolve();
                }, undefined, (e) => {
                    reject(e);
                });
            }
        }.bind(this)));
    });

    await Promise.all(assetPromises);

    window.addEventListener( 'resize', OnWindowResize, false );

    scene.add(camera);

    //prepare the wall lod object
    const lod1 = assets.find(x => x.name === 'brick_lod_1').contents;
    const lod2 = assets.find(x => x.name === 'brick_lod_2').contents;
    const lod3 = assets.find(x => x.name === 'brick_lod_3').contents;
    const lod4 = assets.find(x => x.name === 'brick_lod_4').contents;

    l1 = new THREE.LOD();
    l1.addLevel( lod1.clone(), 0.0 );
    l1.addLevel( lod2.clone(), 10.0 );
    l1.addLevel( lod3.clone(), 20.0 );
    l1.addLevel( lod4.clone(), 50.0 );
    //l1.rotation.x += Math.PI/2;

    instances.set("brick_wall", l1);

    // //clean and replace objects with lods in the scene
    const sceneModel = assets.find(x => x.name == "bricksScene").contents;
    sceneModel.traverse( node => {
        if (node.userData.instanceName && instances.get(node.userData.instanceName)) {
            //spawn an instance object at this pos
            const newLod = instances.get(node.userData.instanceName).clone();
            newLod.applyMatrix4(node.matrix);
            newLod.updateMatrix();
            newLod.scale.divide(sceneModel.scale);            
            node.parent.add(newLod);
        }
    });

    //now disable matrix updates on everything in the scene because they don't move (performance?)
    sceneModel.traverse( node => {
        if ( node.isMesh ) {
            node.matrixAutoUpdate = false;    
        }
    } );
    
    //add the scene
    scene.add(sceneModel);

    //scene.add(l1.clone());

    effect = new OutlineEffect( renderer );

    Animate();
}

function OnLoadModel( model ) {
    const format = ( renderer.capabilities.isWebGL2 ) ? THREE.RedFormat : THREE.LuminanceFormat;
    const colors = new Uint8Array( 2 );
    for ( let c = 0; c <= colors.length; c ++ ) {
        colors[ c ] = ( c / colors.length ) * 256;
    }
    const gradientMap = new THREE.DataTexture( colors, colors.length, 1, format );
    gradientMap.needsUpdate = true;

    model = model.scene;

    // model.scale.set( 0.01, 0.01, 0.01 );

    let coreParent;

    const outlineMaterial = new THREE.LineBasicMaterial({color: 0x000000, linewidth: 1});

    const materialsAndMeshes = new Map(); //map of {material: material, meshes: []}
    const lineSegments = [];
    const cameras = [];

    const finalMeshes = [];

    model.traverse ( function ( child )
    {
        if ( child.isMesh ) {
            child.castShadow = true;
            child.receiveShadow = true;

            let oldMaterial = child.material;

            if (!Array.isArray(oldMaterial))
            {
                let newMaterial;
                if (materialsAndMeshes.get(oldMaterial.name)) {
                    newMaterial = materialsAndMeshes.get(oldMaterial.name).material;
                    materialsAndMeshes.get(oldMaterial.name).meshes.push(child);
                } else {
                    newMaterial = new THREE.MeshToonMaterial();
                    THREE.MeshBasicMaterial.prototype.copy.call( newMaterial, oldMaterial );
                    newMaterial.gradientMap = gradientMap;
                    materialsAndMeshes.set(newMaterial.name, {material: newMaterial, meshes: [child]});
                }
                child.material = newMaterial;
                
            } else {
                //if the mesh has multiple materials, it can't be merged
                for (let i = 0; i < oldMaterial.length; i++) {
                    let newMaterial;
                    if (materialsAndMeshes.get(oldMaterial[i].name)) {
                        newMaterial = materialsAndMeshes.get(oldMaterial[i].name).material;
                    } else {
                        newMaterial = new THREE.MeshToonMaterial();
                        THREE.MeshBasicMaterial.prototype.copy.call( newMaterial, oldMaterial[i] );
                        newMaterial.gradientMap = gradientMap;
                        materialsAndMeshes.set(newMaterial.name, {material: newMaterial, meshes: []});
                    }
                    child.material[i] = newMaterial;
                }
            }
        } else if (child.isLineSegments) {
            child.material.dispose();
            child.material = outlineMaterial;
            lineSegments.push(child);

        } else if (child.isCamera) {
            //sketchup has a camera for every model!
            cameras.push(child);
        }

        if (child.name === "SketchUp") {
            //the sketchup object is the main scene
            //it has a camera and the top level groups.
        }
    });

    const segmentGeometries = [];
    lineSegments.forEach(segment => {
        segment.removeFromParent();
        segmentGeometries.push(segment.geometry);
    });

    let mergedSegmentGeometry = BufferGeometryUtils.mergeBufferGeometries(segmentGeometries, true);
    if (mergedSegmentGeometry) {
        mergedSegmentGeometry = BufferGeometryUtils.mergeVertices(mergedSegmentGeometry);
        mergedSegmentGeometry.computeBoundingSphere();
        
        const newSegmentMesh = new THREE.LineSegments(mergedSegmentGeometry, outlineMaterial);

        for (let i = 0; i < lineSegments.length; i++) {
            lineSegments[i].removeFromParent();
            lineSegments[i].geometry.dispose();
        }

        //mega sus
        // newSegmentMesh.rotation.x -= Math.PI/2;
        finalMeshes.push(newSegmentMesh);
    }
    
    cameras.forEach(cameraInstace => {
        cameraInstace.removeFromParent();
    });

    materialsAndMeshes.forEach(materialAndMeshes => {
        if (materialAndMeshes.meshes.length > 1) {
            const geometries = [];
            for (let i = 0; i < materialAndMeshes.meshes.length; i++) {
                geometries.push(materialAndMeshes.meshes[i].geometry);
            }
            let mergedGeometry = BufferGeometryUtils.mergeBufferGeometries(geometries, true);
            if (mergedGeometry) {
                mergedGeometry = BufferGeometryUtils.mergeVertices(mergedGeometry);
                mergedGeometry.computeBoundingSphere();
                const newMesh = new THREE.Mesh(mergedGeometry, materialAndMeshes.material);
                newMesh.castShadow = true;
                newMesh.receiveShadow = true;
    
                for (let i = 0; i < materialAndMeshes.meshes.length; i++) {
                    materialAndMeshes.meshes[i].removeFromParent();
                    materialAndMeshes.meshes[i].geometry.dispose();
                }

                //mega sus
                // newMesh.rotation.x -= Math.PI/2;
                finalMeshes.push(newMesh);

            } else {
                console.error("failed to merge!");
            }
            
        } else if (materialAndMeshes.meshes.length === 1) {
            finalMeshes.push(materialAndMeshes.meshes[0]);
        }
    });

    //todo also remove garbage?
    for (let i = 0; i < finalMeshes.length; i++) {
        model.add(finalMeshes[i]);
    }
    
    console.log(model);
}

function AddLights() {

    //ambient
    const ambientLight = new THREE.AmbientLight( 0x66adff );
    scene.add( ambientLight );

    //sun
    const sunSphere = new THREE.Mesh(
        new THREE.SphereGeometry( 0.1, 8, 8 ),
        new THREE.MeshBasicMaterial( { color: 0xffffff } )
    );
    scene.add( sunSphere );
    sunSphere.position.set(2, -4, 4);

    const sunLight = new THREE.PointLight( 0xffffff, 4, 0, 0 );
    sunLight.castShadow = true;
    sunLight.shadow.bias = -0.002;
	sunSphere.add( sunLight );
}

function OnWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight, false );
}

function Animate() {
    requestAnimationFrame( Animate );
    controls.update();
    stats.update();
    rendererStats.update(renderer);
    Render();
}

function Render() {
    effect.render( scene, camera );
}