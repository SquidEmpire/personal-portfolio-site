"use strict";

import * as THREE from './vendor/three.module.js';
import Stats from './vendor/stats.module.js';

import { OutlineEffect } from './vendor/OutlineEffect.js';

import { ColladaLoader } from './vendor/ColladaLoader.js';

import { OrbitControls } from './vendor/OrbitControls.js';


var renderer, scene, camera, stats, controls;
var effect;
const assets = [];

let l1, l2, l3, l4;

class Asset {

    name;
    path;
    type = "File";
    contents;

    constructor( name, path, type ) {
        this.name = name;
        this.path = path;
        this.type = type;
    }

    setContents(contents) {
        this.contents = contents;
    }

    clone() {
        let clone = new Asset(this.name, this.path, this.type);
        clone.setContents(this.contents.clone());
        return clone;
    }

};

Start();

async function Start() {

    stats = new Stats();
	document.body.appendChild( stats.dom );
    stats.dom.id = "stats";

    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight, false );
    renderer.outputEncoding = THREE.sRGBEncoding;

    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.BasicShadowMap;
    renderer.physicallyCorrectLights = true;

    document.body.appendChild( renderer.domElement );

    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0xbfe3dd );

    AddLights();

    camera = new THREE.PerspectiveCamera( 40, window.innerWidth / window.innerHeight, 0.1, 100 );
    camera.position.set(-1.6, 0.8, 1.0);

    controls = new OrbitControls( camera, renderer.domElement );
    controls.update();
    // controls.enablePan = false;
    controls.enableDamping = true;

    controls.target.set(1.5, 0.8, -10);


    const loader = new ColladaLoader();

    assets.push( new Asset('brick_lod_1', './models/bricks_lod1.dae'));
    assets.push( new Asset('brick_lod_2', './models/bricks_lod2.dae'));
    assets.push( new Asset('brick_lod_3', './models/bricks_lod3.dae'));
    assets.push( new Asset('brick_lod_4', './models/bricks_lod4.dae'));

    let assetPromises = [];

    assets.forEach(asset => {
        assetPromises.push(new Promise(function(resolve, reject) {
            loader.load(asset.path, (objectContent) => {
                OnLoadModel(objectContent);
                asset.setContents( objectContent.scene );
                //todo: destroy unused stuff?
                resolve();
            }, undefined, (e) => {
                reject(e);
            });
        }.bind(this)));
    });

    await Promise.all(assetPromises);

    window.addEventListener( 'resize', OnWindowResize, false );

    scene.add(camera);

    //add the walls

    const lod1 = assets.find(x => x.name === 'brick_lod_1').contents;
    const lod2 = assets.find(x => x.name === 'brick_lod_2').contents;
    const lod3 = assets.find(x => x.name === 'brick_lod_3').contents;
    const lod4 = assets.find(x => x.name === 'brick_lod_4').contents;

    // l1 = lod1.clone();
    // scene.add(l1);
    // l1.position.set( 0, 0, -1 );

    l1 = new THREE.LOD();
    l1.addLevel( lod1.clone(), 0.0 );
    l1.addLevel( lod2.clone(), 3.0 );
    l1.addLevel( lod3.clone(), 10.0 );
    l1.addLevel( lod4.clone(), 40.0 );
    l1.position.set( 0, 0, -1 );
    scene.add(l1);

    l2 = lod2.clone();
    scene.add(l2);
    l2.position.set( -0.5, 0, -6 );

    l3 = lod3.clone();
    scene.add(l3);
    l3.position.set( -2, 0, -11 );

    l4 = lod4.clone();
    scene.add(l4);
    l4.position.set( -4.3, 0, -16 );

    effect = new OutlineEffect( renderer );

    Animate();
}

function OnLoadModel( collada ) {
    const format = ( renderer.capabilities.isWebGL2 ) ? THREE.RedFormat : THREE.LuminanceFormat;
    const colors = new Uint8Array( 2 );
    for ( let c = 0; c <= colors.length; c ++ ) {
        colors[ c ] = ( c / colors.length ) * 256;
    }
    const gradientMap = new THREE.DataTexture( colors, colors.length, 1, format );
    gradientMap.needsUpdate = true;

    const model = collada.scene;

    model.scale.set( 0.01, 0.01, 0.01 );

    const outlineMaterial = new THREE.LineBasicMaterial({color: 0x000000, linewidth: 10});

    model.traverse ( function ( child )
    {
        if ( child.isMesh ) {
            child.castShadow = true;
            child.receiveShadow = true;

            let oldMaterial = child.material;

            if (!Array.isArray(oldMaterial))
            {
                child.material = new THREE.MeshToonMaterial();
    
                THREE.MeshBasicMaterial.prototype.copy.call( child.material, oldMaterial );
    
                child.material.gradientMap = gradientMap;
            } else {
                //sketchup duplicates materials and applies to the parent group? maybe??
                console.log(child);
                oldMaterial.forEach(material => {
                    material.dispose();
                });
            }
        } else if (child.isLineSegments) {
            child.material.dispose();
            child.material = outlineMaterial;
        }
    });
}

function AddLights() {

    //ambient
    const ambientLight = new THREE.AmbientLight( 0x66adff );
    scene.add( ambientLight );

    //sun
    const sunSphere = new THREE.Mesh(
        new THREE.SphereGeometry( 0.1, 8, 8 ),
        new THREE.MeshBasicMaterial( { color: 0xffffff } )
    );
    scene.add( sunSphere );
    sunSphere.position.set(2, 4, 4);

    const sunLight = new THREE.PointLight( 0xffffff, 4, 0, 0 );
    sunLight.castShadow = true;
    sunLight.shadow.bias = -0.001;
	sunSphere.add( sunLight );
}

function OnWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight, false );
}

function Animate() {
    requestAnimationFrame( Animate );
    controls.update();
    stats.update();

    
    l1.rotation.y -= 0.002;//lod changes rotation axis??
    l2.rotation.z -= 0.002;
    l3.rotation.z -= 0.002;
    l4.rotation.z -= 0.002;


    Render();
}

function Render() {
    effect.render( scene, camera );
    // renderer.render( scene, camera );
}