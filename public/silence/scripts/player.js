import * as THREE from './vendor/three.module.js';
import * as GAME from './game.js';
import { GameObject } from './gameObject.js';

class Player extends GameObject {    
    // global
    controls;
    position;

    //internal
    body;
    head;
    //TODO: pivot is at about "eye" level = e.g. the neck between head and body... it should be at the feet?
    _pivot;
    _downRaycaster;
    _motionRaycaster;
    _motionRaycaster2;
    _interactionRaycaster;

    _motionRaycasterDebugArrow1;
    _motionRaycasterDebugArrow2;

    _positionTemp = new THREE.Vector3();
    _directionTemp = new THREE.Quaternion();
    _leftRightAxis = new THREE.Vector3(0, 1, 0);
    _upDownAxis = new THREE.Vector3(1, 0, 0);
    

    //state
    moveForward = false;
    moveBackward = false;
    lookLeft = false;
    lookRight = false;
    lookUp = false;
    lookDown = false;
    canJump = false;
    
    velocity = new THREE.Vector3();
    direction = new THREE.Vector3();    

    item;

    //settings
    mass = 20;
    bodyHeight = 1.5;
    headSize = 0.3;
    height = this.bodyHeight + this.headSize;
    jumpSpeed = 20;
    movementSpeed = 50;
    slopeClimbFactor = 0.5;
    offsetFromCamera = new THREE.Vector3(0, 0, 0);
    terminalVelocity = -300;
    rotateSpeed = 0.01;

    constructor( controls ) {

        super();
        this.pivot = new THREE.Group();
        this.controls = controls;

        document.addEventListener( 'keydown', this.onKeyDown.bind(this), false );
        document.addEventListener( 'keyup', this.onKeyUp.bind(this), false );

        document.addEventListener( 'mousedown', this.onMouseDown.bind(this), false );
        document.addEventListener( 'mouseup', this.onMouseUp.bind(this), false );

        this._downRaycaster = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3(), 0 );
        this._motionRaycaster = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3(), 0, 0.5 );
        this._motionRaycaster2 = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3(), 0, 0.5 );
        this._interactionRaycaster = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3(), 0, this.height );

        this._downRaycaster.firstHitOnly = true;
        this._motionRaycaster.firstHitOnly = true;
        this._motionRaycaster2.firstHitOnly = true;
        this._interactionRaycaster.firstHitOnly = true;

        let bodyGeometry = new THREE.BoxGeometry( this.bodyHeight*0.42, this.bodyHeight, this.bodyHeight*0.42*0.5 );
        let headGeometry = new THREE.BoxGeometry( this.headSize, this.headSize, this.headSize );
        let bodyMaterial = new THREE.MeshStandardMaterial({emissive: 0xffaa00});
        let headMaterial = new THREE.MeshStandardMaterial({emissive: 0xff6600});
    
        this.body = new THREE.Mesh( bodyGeometry, bodyMaterial );
        this.head = new THREE.Mesh( headGeometry, headMaterial );

        //controls change angle on mouse move which can happen many times per render so we need to reposition every time the controls change
        this.controls.addEventListener( 'change', this.repositionPlayer.bind(this) );

    }

    initialize() {

        this.body.add( this.head );
        this.head.position.set( 0, this.bodyHeight/2 + this.headSize/2, 0 );

        this.pivot.add( this.body );
        this.body.position.copy( this.offsetFromCamera );
        this.body.position.y -= this.bodyHeight / 2;

        this._motionRaycasterDebugArrow1 = new THREE.ArrowHelper( this._motionRaycaster.ray.direction, this._motionRaycaster.ray.origin, 100, 0xffaaaa ); //salmon
        this._motionRaycasterDebugArrow2 = new THREE.ArrowHelper( this._motionRaycaster.ray.direction, this._motionRaycaster2.ray.origin, 100, 0xaaffaa ); //green
        //GAME.Globals.scene.add(this._motionRaycasterDebugArrow1);
        //GAME.Globals.scene.add(this._motionRaycasterDebugArrow2);

        GAME.Globals.scene.add( this.pivot );
        super.initialize();

    }

    interact() {
        //check there's something in front of us we can interact with
        this._interactionRaycaster.setFromCamera( new THREE.Vector2(), this.controls.getObject() );
        //let interactionIntersections = this._interactionRaycaster.intersectObjects( GAME.Globals.interactableObjects );

        //if (interactionIntersections.length && interactionIntersections[0].object.userData.onInteract) {
        //    interactionIntersections[0].object.userData.onInteract();
        //}  
    }

    intertitle() {
        GAME.toggleIntertitle();
    }

    onKeyDown ( event ) {

        switch ( event.key ) {

            case 'ArrowUp':
            case 'w':
                this.lookUp = true;
                break;

            case 'ArrowLeft':
            case 'a':
                this.lookLeft = true;
                break;

            case 'ArrowDown':
            case 's':
                this.lookDown = true;
                break;

            case 'ArrowRight':
            case 'd':
                this.lookRight = true;
                break;

            case ' ':
                if ( this.canJump === true ) this.velocity.y += this.jumpSpeed;
                this.canJump = false;
                break;

            case 'i':
                this.intertitle();

            case 'e':
                this.interact();

        }

    };

    onKeyUp ( event ) {

        switch ( event.key ) {

            case 'ArrowUp':
            case 'w':
                this.lookUp = false;
                break;

            case 'ArrowLeft':
            case 'a':
                this.lookLeft = false;
                break;

            case 'ArrowDown':
            case 's':
                this.lookDown = false;
                break;

            case 'ArrowRight':
            case 'd':
                this.lookRight = false;
                break;

        }
    }

    onMouseDown (e) {
        if (e) {
            switch (e.which) {
                case 1: //left
                this.moveForward = true;
                break;
                case 2: //middle
                break;
                case 3: //right
                this.moveBackward = true;
                break; 
            }
        }
    }

    onMouseUp (e) {
        if (e){ 
            switch (e.which) {
                case 1: //left
                this.moveForward = false;
                break;
                case 2: //middle
                break;
                case 3: //right
                this.moveBackward = false;
                break; 
            }
        }
    }

    onGameTickAlways (delta) {
        if ( this.controls.isLocked === true && GAME.Globals.paused === false )
         {
            //check if using our current Y velocity whether in this tick we are on a solid object and set our velocity accordingly            
            let nextVerticalPosition = this.calculateNextVerticalPosition(delta);

            this.velocity.x -= this.velocity.x * 10.0 * delta;
            this.velocity.z -= this.velocity.z * 10.0 * delta;            

            this.direction.z = Number( this.moveForward ) - Number( this.moveBackward );
            this.direction.x = Number( this.moveRight ) - Number( this.moveLeft );
            this.direction.normalize(); // this ensures consistent movements in all directions

            //perform rotations
            if (this.lookLeft) {
                this.controls.getObject().rotateOnWorldAxis(this._leftRightAxis, this.rotateSpeed);
            } else if (this.lookRight) {
                this.controls.getObject().rotateOnWorldAxis(this._leftRightAxis, -this.rotateSpeed);
            }

            if (this.lookUp) {
                this.controls.getObject().rotateOnAxis(this._upDownAxis, this.rotateSpeed);
            } else if (this.lookDown) {
                this.controls.getObject().rotateOnAxis(this._upDownAxis, -this.rotateSpeed);
            }
            
            //set our velocity
            if ( this.moveForward || this.moveBackward ) this.velocity.z -= this.direction.z * this.movementSpeed * delta;
            if ( this.moveLeft || this.moveRight ) this.velocity.x -= this.direction.x * this.movementSpeed * delta;

            //before we move check the area is free
            this.performMotionRaycasts();            

            //apply movement
            this.controls.moveRight( - this.velocity.x * delta );
            this.controls.moveForward( - this.velocity.z * delta );
            this.controls.getObject().position.y = nextVerticalPosition; 

            //after calculating our position, reposition anything on us
            this.repositionPlayer();
        }
    }

    calculateNextVerticalPosition(delta) {
        
        let bodyWorldPosition = new THREE.Vector3();
        this.body.updateMatrixWorld();
        let nextYPosition = this.controls.getObject().position.y + ( this.velocity.y * delta );

        this.body.getWorldPosition(bodyWorldPosition);
        this._downRaycaster.ray.origin.copy( bodyWorldPosition );
        this._downRaycaster.ray.origin.y += this.height;

        let raycastDirectionVector = new THREE.Vector3(0, -1, 0);

        this._downRaycaster.ray.direction.copy(raycastDirectionVector);
        let downwardIntersections = this._downRaycaster.intersectObjects( GAME.Globals.solidObjects );

        let onObject = false;
           
        for (var i = 0; i < downwardIntersections.length; i++) {
            if (downwardIntersections[i].point.y + this.height + 0.1 >= nextYPosition) {
                this.velocity.y = Math.max( 0, this.velocity.y );
                nextYPosition = downwardIntersections[i].point.y + this.height + 0.1;
                this.canJump = true;
                onObject = true;
                break;
            }    
        }

        if ( onObject === false ) {

            this.velocity.y = Math.max(this.terminalVelocity, this.velocity.y - (GAME.Globals.gravity * this.mass * delta));

            //stop us going below 0
            if ( this.controls.getObject().position.y < this.height ) {

                this.velocity.y = 0;
                this.controls.getObject().position.y = this.height;
                this.canJump = true;

            }
        }

        return nextYPosition;
    }

    performMotionRaycasts() {
        let bodyWorldPosition = new THREE.Vector3();
        this.pivot.getWorldPosition(bodyWorldPosition);
        
        this._motionRaycaster.ray.origin.copy( bodyWorldPosition );
        this._motionRaycaster.ray.origin.y -= this.height;
        //motion raycaster2 is placed above the base raycaster. If we detect a collision in caster 1 but not caster 2 the obstruction is very short and can be "slid" over
        this._motionRaycaster2.ray.origin.copy( bodyWorldPosition );
        this._motionRaycaster2.ray.origin.y -= this.height - this.slopeClimbFactor;

        let raycastDirectionVector1 = new THREE.Vector3( this.direction.x, 0, 0 );
        raycastDirectionVector1.applyQuaternion( this.pivot.quaternion );
        raycastDirectionVector1.normalize();
        this._motionRaycaster.ray.direction.copy(raycastDirectionVector1);
        this._motionRaycaster2.ray.direction.copy(raycastDirectionVector1);    
        let movementXIntersections = this._motionRaycaster.intersectObjects( GAME.Globals.solidObjects );
        let movementXIntersections2 = this._motionRaycaster2.intersectObjects( GAME.Globals.solidObjects );

        let raycastDirectionVector2 = new THREE.Vector3( 0, 0, -this.direction.z );
        raycastDirectionVector2.applyQuaternion( this.pivot.quaternion );
        raycastDirectionVector2.normalize();
        this._motionRaycaster.ray.direction.copy(raycastDirectionVector2);
        this._motionRaycaster2.ray.direction.copy(raycastDirectionVector2);
        let movementZIntersections = this._motionRaycaster.intersectObjects( GAME.Globals.solidObjects );
        let movementZIntersections2 = this._motionRaycaster2.intersectObjects( GAME.Globals.solidObjects );

        if ( movementXIntersections.length > 0 ) {
            if ( movementXIntersections2.length === 0 ) {
                //this.velocity.y = this.jumpSpeed; //slide
                this.controls.getObject().position.y += this.slopeClimbFactor;
            } else {
                this.velocity.x = 0;
            }
        };
        if ( movementZIntersections.length > 0 ) {  
            if ( movementZIntersections2.length === 0 ) {
                //this.velocity.y = this.jumpSpeed; //slide
                this.controls.getObject().position.y += this.slopeClimbFactor;
            } else {
                this.velocity.z = 0;
            }

            this._motionRaycasterDebugArrow1.position.copy( this._motionRaycaster.ray.origin );
            this._motionRaycasterDebugArrow1.setDirection( raycastDirectionVector2 );

            this._motionRaycasterDebugArrow2.position.copy( this._motionRaycaster2.ray.origin );
            this._motionRaycasterDebugArrow2.setDirection( raycastDirectionVector2 );
        } 

        

    }

    repositionPlayer () {
       
        this._positionTemp.copy(this.controls.getObject().position);
        this._directionTemp.copy(this.controls.getObject().quaternion);

        this.pivot.position.copy( this._positionTemp);
        this.pivot.quaternion.copy(this._directionTemp);
        this.pivot.quaternion.x = 0;
        this.pivot.quaternion.z = 0;
        this.pivot.quaternion.normalize();

        this.head.quaternion.copy(this._directionTemp);
        this.head.quaternion.y = 0;
        this.head.quaternion.z = 0;
        this.head.quaternion.normalize();

    }

};

export { Player };