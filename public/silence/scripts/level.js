import * as THREE from './vendor/three.module.js';
import { GameObject } from "./gameObject.js";
import { Globals } from "./game.js";

class Level extends GameObject {

    constructor() {

        super();

    }

    setupLights() {
        //always on ambient light
        Globals.ambientLight = new THREE.AmbientLight( 0x909090, 0.5 );
        Globals.scene.add( Globals.ambientLight );

        //torch
        const spotLight = new THREE.SpotLight( 0xffffff );
        spotLight.position.set(0.2, -0.1, 0);
        spotLight.target.position.set(0.2, 0, -100);
        spotLight.intensity = 8;
        spotLight.decay = 1;
        spotLight.distance = 100;
        spotLight.penumbra = 0.2;
        spotLight.angle = Math.PI/4;
        spotLight.castShadow = true;
        // spotLight.shadow.mapSize.width = 512;
        // spotLight.shadow.mapSize.height = 512;
        // spotLight.shadow.camera.near = 0;
        // spotLight.shadow.camera.far = 50;
        // spotLight.shadow.camera.fov = 15;

        Globals.player.controls.getObject().add( spotLight );
        Globals.player.controls.getObject().add( spotLight.target );
        Globals.torch = spotLight;

        // const spotLightHelper = new THREE.SpotLightHelper( spotLight );
        // Globals.scene.add( spotLightHelper );
        // const helper2 = new THREE.CameraHelper( spotLight.shadow.camera );
        // Globals.scene.add( helper2 );
    }

    setupLevel() {

    }

    initialize() {
        this.setupLights();
        this.setupLevel();
        super.initialize();
    }

    onGameTick(delta) {
        
    }

}

export { Level };