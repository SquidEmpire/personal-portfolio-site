import * as THREE from './vendor/three.module.js';
import { GameObject } from "./gameObject.js";
import { Globals } from "./game.js";

class Cube extends GameObject {

    moving;
    height;
    object;

    speed;
    maxDistanaceFromOrigin = 1;
    returning = false;
    distanceFromOrigin;

    constructor(moving) {

        super();

        this.moving = moving;
        this.speed = Math.random() + 0.2;
        this.distanceFromOrigin = 0;

        const material = new THREE.MeshStandardMaterial( {
            //color: (Math.random()*0xFFFFFF<<0),
            map: Globals.assets.find(x => x.name === 'floor').contents
        } );

        this.height = Math.random() * 2 + 0.5;        
        const geometry = new THREE.BoxGeometry( 2, this.height, 2 );
    
        this.object = new THREE.Mesh( geometry, material );
        this.object.castShadow = true;
        this.object.receiveShadow = true;
    }

    initialize() {
        super.initialize();
        const xPos = (Math.random() * 20) - 10;
        const zPos = (Math.random() * 20) - 10;
        Globals.scene.add( this.object );
        Globals.solidObjects.push( this.object );

        this.object.position.y = this.height * 0.5 + 5;
        this.object.position.x = xPos;
        this.object.position.z = zPos;
    }

    onGameTick(delta) {
        if (this.moving) {
            const modD = this.speed * delta;
            if (this.distanceFromOrigin < 0) {
                this.returning = false;
            } if (this.distanceFromOrigin < this.maxDistanaceFromOrigin && !this.returning) {
                this.object.position.y += modD;
                this.distanceFromOrigin += modD;
            } else if (!this.returning) {
                this.returning = true;
            } else {
                this.object.position.y -= modD;
                this.distanceFromOrigin -= modD;
            }
        }
    }

}

export {Cube};