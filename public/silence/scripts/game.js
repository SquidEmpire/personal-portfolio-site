import * as THREE from './vendor/three.module.js';
import Stats from './vendor/stats.module.js';
import { PointerLockControls } from './vendor/PointerLockControls.js';

import { CustomControls } from './customControls.js';
import { Asset } from './asset.js';
import { Loader } from './loader.js';
import { Player } from './player.js';
import { Crank } from './crank.js';
import { Intertitle } from './intertitle.js';

import { EffectComposer } from './vendor/EffectComposer.js';
import { RenderPass } from './vendor/RenderPass.js';
import { ShaderPass } from './vendor/ShaderPass.js';
import { FilmGrainShader } from './vendor/passes/FilmGrainShader.js';
import { GammaCorrectionShader } from './vendor/passes/GammaCorrectionShader.js';
import { SilenceShader } from './vendor/passes/SilenceShader.js';
import { HorizontalBlurShader } from './vendor/passes/HorizontalBlurShader.js';
import { UnrealBloomPass } from './vendor/UnrealBloomPass.js';
import { Level1 } from './levels/level1.js';

var loader, renderer, composer, controls, stats, crank, intertitle;
var grainShaderInstance, blurShaderInstance;
var lastGrainFrame = 1;
var frameDriftX = 0;
var frameDriftY = 0;
var lastTickTime = performance.now();
var framecap = 1000/24; //(24fps) only for debugging with a limited framerate to simulate using the crank
var lastFrameTime = 0;
var debugmode = false;

var Globals = {
    gameSpeed: 200, //ticks per second    
    solidObjects: [], //in order to raycast on this three.js needs a normal array
    gameObjects: new Map(),
    camera: null,
    scene: null,
    level: null,
    ambientLight: null,
    sun: null,
    torch: null,
    player: null,
    paused: false,
    assets: [],

    silenceShaderInstance: null,

    gravity: 5.0,
}



const getRandomInt = function(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.round(Math.random() * (max - min)) + min;
}



function buildAssetList() {

    //textures
    Globals.assets.push(new Asset('noise', './noise.png', THREE.Texture));
    Globals.assets.push(new Asset('floor', './floor.png', THREE.Texture));
    Globals.assets.push(new Asset('sky', './sky1.png', THREE.Texture));
    Globals.assets.push(new Asset('grain1', './grain/grain1.png', THREE.Texture));
    Globals.assets.push(new Asset('grain2', './grain/grain2.png', THREE.Texture));
    Globals.assets.push(new Asset('grain3', './grain/grain3.png', THREE.Texture));
    Globals.assets.push(new Asset('grain4', './grain/grain4.png', THREE.Texture));
    Globals.assets.push(new Asset('grain5', './grain/grain5.png', THREE.Texture));
    Globals.assets.push(new Asset('grain6', './grain/grain6.png', THREE.Texture));
    Globals.assets.push(new Asset('grain7', './grain/grain7.png', THREE.Texture));
    Globals.assets.push(new Asset('grain8', './grain/grain8.png', THREE.Texture));
    Globals.assets.push(new Asset('grain9', './grain/grain9.png', THREE.Texture));
    Globals.assets.push(new Asset('grain10', './grain/grain10.png', THREE.Texture));
    Globals.assets.push(new Asset('grain11', './grain/grain11.png', THREE.Texture));
    Globals.assets.push(new Asset('maze', './models/maze.glb', "GLTF"));
    //Globals.assets.push(new Asset('outside', './models/outside.glb', "GLTF"));

}

function setupShaders() {
    const renderPass = new RenderPass( Globals.scene, Globals.camera );
    const gammaPass = new ShaderPass( GammaCorrectionShader );
    grainShaderInstance = new ShaderPass( FilmGrainShader );
    Globals.silenceShaderInstance = new ShaderPass(SilenceShader);
    blurShaderInstance = new ShaderPass(HorizontalBlurShader);

    Globals.silenceShaderInstance.uniforms.tGrain1.value = Globals.assets.find(x => x.name === 'grain1').contents;
    Globals.silenceShaderInstance.uniforms.tGrain2.value = Globals.assets.find(x => x.name === 'grain2').contents;
    Globals.silenceShaderInstance.uniforms.tGrain3.value = Globals.assets.find(x => x.name === 'grain3').contents;
    Globals.silenceShaderInstance.uniforms.tGrain4.value = Globals.assets.find(x => x.name === 'grain4').contents;
    Globals.silenceShaderInstance.uniforms.tGrain5.value = Globals.assets.find(x => x.name === 'grain5').contents;
    Globals.silenceShaderInstance.uniforms.tGrain6.value = Globals.assets.find(x => x.name === 'grain6').contents;
    Globals.silenceShaderInstance.uniforms.tGrain7.value = Globals.assets.find(x => x.name === 'grain7').contents;
    Globals.silenceShaderInstance.uniforms.tGrain8.value = Globals.assets.find(x => x.name === 'grain8').contents;
    Globals.silenceShaderInstance.uniforms.tGrain9.value = Globals.assets.find(x => x.name === 'grain9').contents;
    Globals.silenceShaderInstance.uniforms.tGrain10.value = Globals.assets.find(x => x.name === 'grain10').contents;
    Globals.silenceShaderInstance.uniforms.tGrain11.value = Globals.assets.find(x => x.name === 'grain11').contents;
    Globals.silenceShaderInstance.uniforms.u_colour.value = new THREE.Color(0xffffff);

    const bloomPass = new UnrealBloomPass( new THREE.Vector2( window.innerWidth, window.innerHeight ), 1.5, 0.4, 0.85 );
    bloomPass.threshold = 0.4;
    bloomPass.strength = 0.8;
    bloomPass.radius = 0.3;

    composer = new EffectComposer( renderer );
    composer.setSize( window.innerWidth, window.innerHeight );
    composer.setPixelRatio( window.devicePixelRatio );
    composer.addPass( renderPass );
    composer.addPass( gammaPass );
    
    //composer.addPass( grainShaderInstance );
    composer.addPass( bloomPass );
    composer.addPass( Globals.silenceShaderInstance );
    composer.addPass( blurShaderInstance );
    

    
}

init();

async function init() {

    //start the game ticks
    setInterval(tick.bind(this), 1000/Globals.gameSpeed);

    Globals.paused = true;

    //build our asset list
    buildAssetList();

    //load
    let loaderTextElement = document.getElementById( 'loader-description' );
    loader = new Loader(loaderTextElement);
    await loader.loadAssets(Globals.assets);


    //scene setup


    Globals.scene = new THREE.Scene();
    Globals.scene.background = new THREE.Color( 0x7b91e0 );


    //player setup
    
    Globals.camera = new THREE.PerspectiveCamera( 55, window.innerWidth / window.innerHeight, 0.01, 2000 );
    Globals.camera.updateProjectionMatrix();
    Globals.camera.lookAt( 0, 0, 0 );
    Globals.scene.add(Globals.camera);

    crank = new Crank();
    intertitle = new Intertitle();

    controls = new CustomControls( Globals.camera, document.body );

    let blocker = document.getElementById( 'blocker' );
    let instructions = document.getElementById( 'instructions' );
    let bottomControls = document.getElementById('bottom-controls');
    blocker.style.display = 'block';
    instructions.style.removeProperty('display');
    bottomControls.style.display = 'none';

    instructions.addEventListener( 'click', (e) => {

        e.preventDefault();
        e.stopPropagation();
        controls.lock();

    }, false );

    controls.addEventListener( 'lock', () => {

        instructions.style.display = 'none';
        blocker.style.display = 'none';
        bottomControls.style.display = '';
        Globals.paused = false;

    } );

    controls.addEventListener( 'unlock', () => {

        blocker.style.display = 'block';
        instructions.style.display = '';
        bottomControls.style.display = 'none';

        Globals.paused = true;

    } );   

    controls.addEventListener( 'change', (e) => {

        crank.onMouseMove(e);

    } );

    Globals.scene.add( controls.getObject() );

    let player = new Player( controls );
    player.initialize();
    Globals.player = player;

    document.body.addEventListener( 'mousedown', () => {if (!Globals.paused) player.onMouseDown()} );
    document.body.addEventListener( 'mouseup', () => {if (!Globals.paused) player.onMouseUp()} );
    
    //move the player
    controls.getObject().position.x = 0;
    controls.getObject().position.y = 6.8;
    controls.getObject().position.z = 0;
    controls.getObject().lookAt( new THREE.Vector3(0, 6.8, 0) );

    //requires player to be initialised
    await intertitle.initialize();

    // renderer setup

    renderer = new THREE.WebGLRenderer( { antialias: false } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight, false );
    renderer.outputColorSpace = THREE.SRGBColorSpace;
    renderer.shadowMap.enabled = true;
	renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    renderer.shadowCameraNear = 100;
    renderer.shadowCameraFar = 15000;
    document.body.appendChild( renderer.domElement );

    setupShaders();

    // level setup

    Globals.level = new Level1();
    Globals.level.initialize();

    stats = new Stats();
    const topControls = document.getElementById("top-controls");
	topControls.appendChild( stats.dom );
    stats.dom.id = "stats";
    stats.dom.style = "";

    window.addEventListener( 'resize', onWindowResize, false );

    animate();

    const loaderElement = document.getElementById( 'loader' );
    loaderElement.style.display = 'none';
    //const blockerElement = document.getElementById( 'blocker' );
    //blockerElement.style.display = 'none';

    render();
}

function toggleIntertitle() {
    intertitle.toggleVisible();
}

function onWindowResize() {

    Globals.camera.aspect = window.innerWidth / window.innerHeight;
    Globals.camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight, false );

    composer.setSize( window.innerWidth, window.innerHeight );
    composer.setPixelRatio( window.devicePixelRatio );

    intertitle.planeFitPerspectiveCamera();

    // render();

}

function tick() {

    const time = performance.now();
    const delta = ( time - lastTickTime ) / 1000;

    Globals.gameObjects.forEach(object => {
        object.onGameTickAlways(delta);
    });

    if (crank) {
        crank.tick(delta);
    }

    lastTickTime = time;
}

function animate() {

    stats.update();
    crank.draw();
    intertitle.draw();
    if (!Globals.paused && (debugmode || crank.checkRevolved())) { 

        //in debug mode there's a frame limiter to simulate cranking
        if (debugmode) {
            const time = performance.now();
            const delta = ( time - lastFrameTime );
            if (delta > framecap) {
                render();
                lastFrameTime = time;
            } 
        } else {
            render();
        }

    }
    
    requestAnimationFrame( animate );

}

function render() {    
    //renderer.render( Globals.scene, Globals.camera );
    Globals.silenceShaderInstance.uniforms.u_time.value += 0.01;
    if (Math.random() > 0.005) {
        Globals.silenceShaderInstance.uniforms.u_dropFrame.value = false;
    } else {
        Globals.silenceShaderInstance.uniforms.u_dropFrame.value = true;
    }

    if (lastGrainFrame === 11) {
        lastGrainFrame = 1;
    } else {
        lastGrainFrame += 1;
    }       
    Globals.silenceShaderInstance.uniforms.u_grainFrame.value = lastGrainFrame;

    grainShaderInstance.uniforms.noiseOffset.value += 0.03;

    blurShaderInstance.uniforms.h.value = 1 / window.innerWidth;

    composer.render();
}

export { Globals, toggleIntertitle };