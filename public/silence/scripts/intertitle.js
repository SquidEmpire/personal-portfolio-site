import * as THREE from './vendor/three.module.js';
import { GameObject } from "./gameObject.js";
import { Globals } from "./game.js";

class Intertitle extends GameObject {

    pivot;
    textplane;
    plane;

    canvas;
    subtitles;

    visible;
    text;

    constructor() {

        super();
        this.visible = false;
        this.text = `"Ecco un esempio più breve del testo che potrebbe apparire"`;
        this.canvas = document.getElementById("title-canvas");
        this.subtitles = document.getElementById( 'subtitles' ); 

        //these can't be changed once defined
        this.canvas.width = 1024;//window.innerWidth;
        this.canvas.height = 1024;//window.innerHeight;

    }

    async initialize() {

        var font = new FontFace('hobbyhorse', 'url(./fonts/hobbyhorse.regular.ttf)');
        //var f = new FontFace('hobbyhorse', 'url(./fonts/Piacevoli.ttf)');
        await font.load();
        document.fonts.add(font);

        this.pivot = new THREE.Group();
        Globals.player.controls.getObject().add(this.pivot );
        this.pivot.position.z -= 0.1;

        let geometry = new THREE.PlaneGeometry( 1, 1 );
        let material = new THREE.MeshBasicMaterial( { color: 0x000000, side: THREE.FrontSide} );
        
        this.plane = new THREE.Mesh( geometry, material );        
        this.pivot.add( this.plane );
        this.plane.position.z -= 0.2;

        geometry = new THREE.PlaneGeometry( 0.18, 0.18 );
        const texture = new THREE.CanvasTexture( this.canvas );
        material = new THREE.MeshBasicMaterial( { map: texture, side: THREE.FrontSide} );
        
        this.textplane = new THREE.Mesh( geometry, material );
        this.pivot.add( this.textplane );

        this.planeFitPerspectiveCamera();

        this.pivot.visible = false;
        this.subtitles.style.display = "none";

        super.initialize();

    }

    //https://discourse.threejs.org/t/resize-geometry-at-window-resize/37996/2
    planeFitPerspectiveCamera(relativeZ = null) {
        const cameraZ = relativeZ !== null ? relativeZ : Globals.camera.position.z;
        const distance = cameraZ - this.plane.position.z;
        const vFov = Globals.camera.fov * Math.PI / 180;
        const scaleY = 2 * Math.tan(vFov / 2) * distance;
        const scaleX = scaleY * Globals.camera.aspect;
      
        //this.plane.scale.set(scaleX, scaleY, 1);
    }

    //https://stackoverflow.com/a/16599668
    getLines(ctx, text, maxWidth) {
        let words = text.split("\n").reduce((a,b) => a.concat(b));
        words = words.split(" ");            
        let lines = [];
        let currentLine = words[0];
        for (var i = 1; i < words.length; i++) {
            let word = words[i];
            let width = ctx.measureText(currentLine + " " + word).width;
            if (width < maxWidth) {
                currentLine += " " + word;
            } else {
                lines.push(currentLine);
                currentLine = word;
            }
        }
        lines.push(currentLine);
        return lines;
    }

    draw() {
        if (this.visible) {
            
            const ctx = this.canvas.getContext("2d");
            ctx.fillStyle = "black";
            ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);

            ctx.font = `52px hobbyhorse`;
            ctx.textAlign = "start";
            ctx.textBaseline = 'middle';
            ctx.fillStyle = "white";

            const maxWidth = 800;
            let textDrawWidth = this.canvas.width - 128;
            if (textDrawWidth > maxWidth) {textDrawWidth = maxWidth;}

            let textString = this.text;
            const lines = this.getLines(ctx, textString, textDrawWidth);
            
            const lineHeight = 54;
            const baseX = (this.canvas.width/2);
            const baseY = (this.canvas.height/2) - lines.length * 0.5 * lineHeight + 64;
            
            for (let i = 0; i < lines.length; i++) {
                //make the SECOND letter biggerer (first letter is a quotation mark)
                if (i === 0) {
                    ctx.font = `52px hobbyhorse`;
                    let drawnWidthSoFar = ctx.measureText(lines[i][0]).width;
                    ctx.fillText(lines[i][0], baseX - textDrawWidth * 0.5, baseY + lineHeight * i);
                    ctx.font = `68px hobbyhorse`;               
                    ctx.fillText(lines[i][1], baseX + drawnWidthSoFar - textDrawWidth * 0.5, baseY + lineHeight * i);
                    drawnWidthSoFar += ctx.measureText(lines[i][1]).width;
                    ctx.font = `52px hobbyhorse`;
                    ctx.fillText(lines[i].substring(2), baseX + drawnWidthSoFar - textDrawWidth * 0.5, baseY + lineHeight * i);
                } else {                        
                    ctx.fillText(lines[i], baseX - textDrawWidth * 0.5, baseY + lineHeight * i);
                }
                
            }

            this.textplane.material.map.needsUpdate = true;
        }
    }

    toggleVisible() {

               
        if (this.visible) {
            this.visible = false;
            this.pivot.visible = false;
            this.subtitles.style.display = "none";
        } else {
            this.visible = true;
            this.pivot.visible = true;
            this.subtitles.style.display = "block";
        }
        this.planeFitPerspectiveCamera();

    }

    onGameTick(delta) {
        
    }

}

export { Intertitle };