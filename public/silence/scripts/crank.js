import { Globals } from "./game.js";

class Crank {
    
    canvas;
    ctx;
    angle;
    lastAngle;

    force;
    velocity;
    friction = 0.99;
    headAngle;
    headLastAngle;

    timeSinceLastRevolutionsTally;
    revolutionsCounted;
    revolvedDrawn;
    revolvedRendered;
    fps;

    constructor() {
        this.canvas = document.getElementById('the-crank');
        this.ctx = this.canvas.getContext('2d');
        this.angle = 0;
        this.lastAngle = 0;
        this.headAngle = 0;
        this.headLastAngle = 0;
        this.force = 0;
        this.velocity = 0;
        this.revolved = false;
        this.revolvedDrawn = 0;
        this.revolvedRendered = false;
        this. revolutionsCounted = 0;

        this.timeSinceLastRevolutionsTally = 0;
        this.fps = 0;
    }

    checkRevolved() {
        if (this.revolvedRendered) {
            this.revolutionsCounted++;
            this.revolvedRendered = false;
            return true;
        }
        return false;
    }

    onMouseMove(e) {
        const x = e.x;
        const y = e.y;
        
        //the mouse motions get really janky with only small movements so only count if they're big
        if (Math.abs(x) > 10 || Math.abs(y) > 10) 
        {
            const newAngle = -Math.atan2(x, y);
            //todo: don't allow backwards rotation?
            this.angle = newAngle;
        }
              
    }

    tick(delta) {
        if (!Globals.paused)
        {
            if (this.timeSinceLastRevolutionsTally >= 1) {
                this.fps = this.revolutionsCounted;

                this.timeSinceLastRevolutionsTally = 0;
                this.revolutionsCounted = 0;
            } else {
                this.timeSinceLastRevolutionsTally += delta;
            }

            this.velocity += this.force * 0.01;
            // if (this.velocity > this.force) {
            //     this.velocity = this.force;
            // }
            this.force = 0;
            if (this.velocity > 0) {
                this.velocity *= this.friction;
            } else if (this.velocity < 0) {
                this.velocity /= this.friction;
            }

            this.headAngle += this.velocity;
            if (this.headAngle > Math.PI*2)
            {
                this.headAngle -= Math.PI*2;
            }
            if (this.headAngle < -Math.PI*2)
            {
                this.headAngle += Math.PI*2;
            }

            if (this.angle !== this.lastAngle) {

                this.force = Math.abs(Math.abs(this.angle) - Math.abs(this.lastAngle));

                //count any 90 degree rotation as a revolution to 4x the framerate
                /*
                if (this.lastAngle !== this.angle &&
                    (Math.abs(this.angle - this.lastAngle) >= Math.PI) ||
                    (this.lastAngle <= 0 && this.angle > 0) ||
                    (this.lastAngle <= Math.PI / 2 && this.angle > Math.PI / 2) ||
                    (this.lastAngle <= -Math.PI / 2 && this.angle > -Math.PI / 2)
                ) {
                    this.revolvedRendered = true;
                    this.revolvedDrawn = 5;
                }
                */
            }

            if (this.headLastAngle !== this.headAngle &&
                (this.headLastAngle > Math.PI && this.headAngle < Math.PI) ||
                (this.headLastAngle < Math.PI && this.headAngle > Math.PI) ||
                (this.headLastAngle < Math.PI / 2 && this.headAngle > Math.PI / 2) ||
                (this.headLastAngle < Math.PI * 1.5 && this.headAngle > Math.PI * 1.5)
            ) {
                this.revolvedRendered = true;
                this.revolvedDrawn = 5;
            }

            this.lastAngle = this.angle;

            this.headLastAngle = this.headAngle;
        }
    }

    draw() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

        //crank face
        const centerX = this.canvas.width / 2;
        const centerY = this.canvas.height / 2;
        const radius = this.canvas.width / 2;
        
        this.ctx.beginPath();
        this.ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
        this.ctx.fillStyle = 'beige';
        this.ctx.fill();
        this.ctx.lineWidth = 5;
        this.ctx.strokeStyle = 'black';
        this.ctx.stroke();

        //arm
        /*
        this.ctx.beginPath();
        if (this.revolvedDrawn > 0) {
            this.ctx.strokeStyle = 'red';
            this.revolvedDrawn--;
        } else {
            this.ctx.strokeStyle = 'black';
        }
        */
        
        this.ctx.moveTo(centerX, centerY);
        const x = centerX + radius * Math.cos(this.angle);
        const y = centerY + radius * Math.sin(this.angle);
        this.ctx.lineTo(x, y);
        this.ctx.stroke();

        //head
        this.ctx.beginPath();
        const x2 = centerX + radius * Math.cos(this.headAngle);
        const y2 = centerY + radius * Math.sin(this.headAngle);
        this.ctx.arc(x2, y2, 10, 0, 2 * Math.PI, false);

        if (this.revolvedDrawn > 0) {
            this.ctx.fillStyle = 'red';
            this.revolvedDrawn--;
        } else {
            this.ctx.fillStyle = 'black';
        }

        this.ctx.fill();

        //fps info
        this.ctx.font = "12px serif";
        this.ctx.fillStyle = 'black';
        this.ctx.textAlign = "center";
        this.ctx.fillText(`FPS: ${this.fps}`, centerX, 150);
    }
}

export {Crank};