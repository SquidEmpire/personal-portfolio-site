import * as THREE from '../vendor/three.module.js';
import { GameObject } from "../gameObject.js";
import { Globals } from "../game.js";
import { Level } from "../level.js";
import { Gyroscope } from '../vendor/Gyroscope.js';
import { Cube } from '../cube.js';

class Level1 extends Level {

    isOutside = true;

    constructor() {

        super();

    }

    setupLights() {
        
        super.setupLights();

        const sun = new THREE.DirectionalLight( 0xffffff, 4 );
        //sun.position.set(-100, 200, -85);
        //sun.target.position.set(-20, 0, -20);
        sun.position.set(10, 20, 5);
        sun.target.position.set(0, 0, 0);
        sun.castShadow = true;
        sun.shadow.mapSize.width = 512;
        sun.shadow.mapSize.height = 512;
        sun.shadow.camera.near = 5;
        sun.shadow.camera.far = 100;
        sun.shadow.camera.left = -30;
        sun.shadow.camera.right = 30;
        sun.shadow.camera.top = 30;
        sun.shadow.camera.bottom = -30;
        sun.shadow.bias = -0.0002;
        sun.shadow.normalBias = 0.1;
        //Globals.scene.add( sun.target );
        //Globals.scene.add( sun );

        //THE SUN REVOLVES AROUND YOU!!!!
        const gyro = new Gyroscope();
        Globals.player.controls.getObject().add(gyro);
        gyro.add( sun.target );
        gyro.add( sun );

        const helper = new THREE.CameraHelper( sun.shadow.camera );
        //Globals.scene.add( helper );

    }

    setupLevel() {
        //add the maze
        const mazeAsset = Globals.assets.find(x => x.name === 'maze').contents;
        mazeAsset.position.set( 0, 5, 0 );
        Globals.scene.add( mazeAsset );
        Globals.solidObjects.push( mazeAsset );

        //add a bunch of crap to test platforming on
        for (let i = 0 ; i < 10; i++) {
            let cube = new Cube(false);
            cube.initialize();
        }
        for (let i = 0 ; i < 5; i++) {
            let cube = new Cube(true);
            cube.initialize();
        }

        //add sky
        /*
        let skyGeo = new THREE.SphereGeometry(1000, 25, 25);
        let skytexture = Globals.assets.find(x => x.name === 'sky').contents;
        skytexture.wrapS = THREE.RepeatWrapping;
        skytexture.repeat.x = 7;
        let material = new THREE.MeshBasicMaterial({ 
            map: skytexture,
            fog: false
        });
        let sky = new THREE.Mesh(skyGeo, material);
        sky.material.side = THREE.BackSide;
        Globals.scene.add(sky);

        //add outside
        const outsideObject = Globals.assets.find(x => x.name === 'outside').contents;
        outsideObject.position.set( 0, 5, 0 );
        //make the bounding box of the outside scene invisible
        for (let i = 0; i < outsideObject.children.length; i++) {
            if (outsideObject.children[i].name === "bounds") {
                outsideObject.children[i].visible = false;
            }
        }

        Globals.scene.add( outsideObject );
        Globals.solidObjects.push( outsideObject );
        */
    }

    initialize() {
        //in this level we start outside
        super.initialize();
        Globals.ambientLight.intensity = 3.0;
        Globals.torch.visible = false;
        Globals.silenceShaderInstance.uniforms.u_colour.value = new THREE.Color(0xA0CCFF); //blue
    }

    onGameTick(delta) {

        //if we pass z ~= -12 we have moved to inside the maze
        if (Globals.player.pivot.position.z < -12 && this.isOutside) {

            Globals.torch.visible = true;
            Globals.ambientLight.intensity = 1;
            Globals.silenceShaderInstance.uniforms.u_colour.value = new THREE.Color(0xBF0909); //red
            this.isOutside = false;

        } else if (Globals.player.pivot.position.z > -12 && !this.isOutside) {

            Globals.torch.visible = false;
            Globals.ambientLight.intensity = 3.0;
            Globals.silenceShaderInstance.uniforms.u_colour.value = new THREE.Color(0xA0CCFF); //blue
            this.isOutside = true;

        }
        
    }

}

export { Level1 };