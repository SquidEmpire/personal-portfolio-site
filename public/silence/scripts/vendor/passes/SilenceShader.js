const SilenceShader = {

	uniforms: {

		'tDiffuse': { value: null },
        'u_time': { type: "f", value: 0.0 },
        'u_colour': { value: null },
        'u_dropFrame': {value: null},
        'u_grainFrame': {value: 1},
        'u_frameDriftX': {value: 0},
        'u_frameDriftY': {value: 0},

        'tGrain1': {type: "t", value: null},
        'tGrain2': {type: "t", value: null},
        'tGrain3': {type: "t", value: null},
        'tGrain4': {type: "t", value: null},
        'tGrain5': {type: "t", value: null},
        'tGrain6': {type: "t", value: null},
        'tGrain7': {type: "t", value: null},
        'tGrain8': {type: "t", value: null},
        'tGrain9': {type: "t", value: null},
        'tGrain10': {type: "t", value: null},
        'tGrain11': {type: "t", value: null}

	},

	vertexShader: /* glsl */`

		varying vec2 vUv;

		void main() {

			vUv = uv;
			gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );

		}`,

	fragmentShader: /* glsl */`

		uniform sampler2D tDiffuse;
        uniform float u_time;
        uniform vec3 u_colour;
        uniform bool u_dropFrame;
        uniform int u_grainFrame;
        uniform float u_frameDriftX;
        uniform float u_frameDriftY;

        uniform sampler2D tGrain1;
        uniform sampler2D tGrain2;
        uniform sampler2D tGrain3;
        uniform sampler2D tGrain4;
        uniform sampler2D tGrain5;
        uniform sampler2D tGrain6;
        uniform sampler2D tGrain7;
        uniform sampler2D tGrain8;
        uniform sampler2D tGrain9;
        uniform sampler2D tGrain10;
        uniform sampler2D tGrain11;

		varying vec2 vUv;

        highp float rand(vec2 co)
        {
            highp float a = 12.9898;
            highp float b = 78.233;
            highp float c = 43758.5453;
            highp float dt= dot(co.xy ,vec2(a,b));
            highp float sn= mod(dt,3.14);
            return fract(sin(sn) * c);
        }

		void main() {

            float brightness = -0.2 + (rand(vec2(u_time * 2.0, u_time)) * 0.1);
            float contrast = 1.5 + (rand(vec2(u_time * 1.5, u_time)) * 0.8);

            if (u_dropFrame)
            {
                gl_FragColor = vec4(0.0,0.0,0.0,1.0);
                return;
            }

            //shake
            vec2 vUv2 = vUv + ((rand(vec2(u_time * 1.2, u_time)) * 2.0 - 1.0) * 0.002);

            //black-white
			vec4 inp = texture2D( tDiffuse, vUv2 );
            float luminance = (inp.r + inp.g + inp.b) / 3.0;
            vec3 pixelvalue = vec3(luminance, luminance, luminance);

            //brightness / contrast
            pixelvalue.rgb = ((pixelvalue.rgb - 0.5f) * max(contrast, 0.0)) + 0.5f;
            pixelvalue.rgb += brightness;

            //colourise
            pixelvalue = pixelvalue * u_colour + pixelvalue * 0.8 + u_colour * 0.2;
                   

            //vignette
            float r = rand(vec2(u_time, u_time)) * 0.2 + 0.7;
            vec2 uv = vUv * (1.0 - vUv.yx);
            float vig = uv.x * uv.y * (15.0 * r);
            vig = pow(vig, 0.1);

            pixelvalue *= vig;

            //grain
            vec3 grain;
            //shake/scale the grain uv coords to add some more variety
            vec2 vUv3 = vUv2 + ((rand(vec2(u_time * 1.2, u_time)) * 2.0 - 1.0));
            switch (u_grainFrame)
            {
            case 2:
                grain = texture2D( tGrain2, vUv3 ).xyz;
                break;
            case 3:
                grain = texture2D( tGrain3, vUv3 ).xyz;
                break;
            case 4:
                grain = texture2D( tGrain4, vUv3 ).xyz;
                break;
            case 5:
                grain = texture2D( tGrain5, vUv3 ).xyz;
                break;
            case 6:
                grain = texture2D( tGrain6, vUv3 ).xyz;
                break;
            case 7:
                grain = texture2D( tGrain7, vUv3 ).xyz;
                break;
            case 8:
                grain = texture2D( tGrain8, vUv3 ).xyz;
                break;
            case 9:
                grain = texture2D( tGrain9, vUv3 ).xyz;
                break;
            case 10:
                grain = texture2D( tGrain10, vUv3 ).xyz;
                break;
            case 11:
                grain = texture2D( tGrain11, vUv3 ).xyz;
                break;
            case 1:
            default:
                grain = texture2D( tGrain1, vUv3 ).xyz;
            }

            grain *= 0.6f;

            pixelvalue -= grain;
            

            gl_FragColor = vec4(pixelvalue, 1.0);


		}`

};

export { SilenceShader };
