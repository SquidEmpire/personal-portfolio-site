"use strict";

import { Globals } from './stars.js';
import * as THREE from './vendor/three.module.js';
import * as Helper from './helper.js';
import { ImprovedNoise } from './vendor/ImprovedNoise.js';
import { NebulaShader } from './vendor/shaders/NebulaShader.js';

class Nebula {

    name;
    pivot;
    nebulaObject;

    constructor() {

    }

    setPosition(x,y,z) {
        this.pivot.position.set(x, y, z);
        this.nebulaObject.updateMatrix();
    }

    initialise( seed = 0, colour = null, xScale = 500, yScale = xScale, zScale = xScale ) {
        //https://github.com/mrdoob/three.js/blob/master/examples/webgl2_volume_cloud.html

        if (!colour) {
            colour = new THREE.Color( 0x5278B2 );
        }

        this.pivot = new THREE.Group();

        const dataSize = 128;
        const data = new Uint8Array( dataSize * dataSize * dataSize );

        let i = 0;
        const scale = 0.05;
        const perlin = new ImprovedNoise();
        const vector = new THREE.Vector3();

        for ( let z = 0; z < dataSize; z ++ ) {
            for ( let y = 0; y < dataSize; y ++ ) {
                for ( let x = 0; x < dataSize; x ++ ) {

                    const d = 1.0 - vector.set( x, y, z ).subScalar( dataSize / 2 ).divideScalar( dataSize ).length();
                    data[ i ] = ( 128 + 128 * perlin.noise( x * scale / 1.5, y * scale, z * scale / 1.5 ) ) * d * d;
                    i ++;

                }
            }
        }

        const texture = new THREE.Data3DTexture( data, dataSize, dataSize, dataSize );
        texture.format = THREE.RedFormat;
        texture.minFilter = THREE.LinearFilter;
        texture.magFilter = THREE.LinearFilter;
        texture.unpackAlignment = 1;
        texture.needsUpdate = true;

        const vert = NebulaShader.vertexShader;
        const frag = NebulaShader.fragmentShader;

        const geometry = new THREE.BoxGeometry( 1, 1, 1 );
        const material = new THREE.ShaderMaterial( {
            glslVersion: THREE.GLSL3,
            uniforms: {
                base: { value: colour },
                map: { value: texture },
                cameraPos: { value: new THREE.Vector3() },
                threshold: { value: 0.25 },
                opacity: { value: 0.05 },
                range: { value: 0.08 },
                steps: { value: 16 },
                frame: { value: 0 },
                seed2: { value: seed }
            },
            vertexShader: vert,
            fragmentShader: frag,
            side: THREE.BackSide,
            transparent: true,
            depthWrite: false,
        } );

        this.nebulaObject = new THREE.Mesh( geometry, material );

        this.nebulaObject.scale.set(xScale, yScale, zScale);
        this.nebulaObject.matrixAutoUpdate = false; //manually call matrix updates for performance(?)

        this.pivot.add( this.nebulaObject );
        
        this.name = this.pivot.uuid;
        this.pivot.name = "Nebula " + this.name;

        Globals.controls.addEventListener("change", this.updateCameraPosUniforms.bind(this));
        this.updateCameraPosUniforms();
    }

    updateCameraPosUniforms() {
        this.nebulaObject.material.uniforms.cameraPos.value.copy( Globals.camera.position );
    }

    onGameTick( delta ) {

        //this.nebulaObject.material.uniforms.frame.value += 1;

    }

};

export { Nebula };