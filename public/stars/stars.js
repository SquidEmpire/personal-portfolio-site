"use strict";

import * as THREE from './vendor/three.module.js';
import * as OrbitControls from './vendor/OrbitControls.js';
import Stats from './vendor/stats.module.js';
import * as Helper from './helper.js';
import { TWEEN } from './vendor/Tween.js'
import { StarNames } from './names.js';

// import { EffectComposer } from './vendor/EffectComposer.js';
// import { RenderPass } from './vendor/RenderPass.js';
// import { ShaderPass } from './vendor/ShaderPass.js';
// import { GammaCorrectionShader } from './vendor/shaders/GammaCorrectionShader.js';
// import { FXAAShader } from './vendor/shaders/FXAAShader.js';
// import { FilmGrainShader } from './vendor/shaders/FilmGrainShader.js';
// import { FilmShader } from './vendor/shaders/FilmShader.js';
// import { LensDistortionShader } from './vendor/shaders/LensDistortionShader.js';
// import { BadTVShader } from './vendor/shaders/BadTVShader.js';

import { Star } from './star.js';
import { Asset } from './asset.js';
import { Loader } from './loader.js';
import { Nebula } from './nebula.js';

var renderer, stats;
var lastTickTime = performance.now();
var loader;
var starInfoCaption;

const raycaster = new THREE.Raycaster();
const pointer = new THREE.Vector2();

var Globals = {
    seededRandomNumberGenerator: null,
    assets: [],
    gameSpeed: 200, //ticks per second
    gameObjects: new Map(),
    starObjectsForRaycasting: [],
    scene: null,
    camera: null,
    controls: null,
}

const skipIntro = true;
var intro;
const cameraTargetPos = new THREE.Vector3();
const cameraPositionMin = new THREE.Vector3(-1000, -1000, -1000);
const cameraPositionMax = new THREE.Vector3(1000, 1000, 1000);

//shader stuff
// var composer;
// var shaderTime;
// var renderPass, distortPass, grainPass, filmPass, gammaPass, fxaaPass, badTVPass;
// var shaderParams = {
//     enableNoise: true,
//     noiseSpeed: 0.05,
//     noiseIntensity: 0.07,

//     enableDistortion: true,
//     baseIor: 1.00,
//     bandOffset: 0.001,
//     jitterIntensity: 2.0,
//     samples: 7,
//     distortionMode: 'rygcbv',

//     tvDistortion: 3.0,
//     tvDistortion2: 1.0,
//     tvSpeed: 0.3,
//     tvRollSpeed: -0.2
// };

function buildAssetList() {

    Globals.assets.push(new Asset('spikeTexture', './spike2.png', THREE.Texture));

}

function setupShaders() {

    shaderTime = 0;

    // Render Pass Setup
    renderPass = new RenderPass( Globals.scene, Globals.camera );
    grainPass = new ShaderPass( FilmGrainShader );
    filmPass = new ShaderPass( FilmShader );
    gammaPass = new ShaderPass( GammaCorrectionShader );
    fxaaPass = new ShaderPass( FXAAShader );
    distortPass = new ShaderPass( LensDistortionShader );
    distortPass.material.defines.CHROMA_SAMPLES = shaderParams.samples;
    badTVPass = new ShaderPass( BadTVShader );

    composer = new EffectComposer( renderer );
    composer.setSize( window.innerWidth, window.innerHeight );
    composer.setPixelRatio( window.devicePixelRatio );
    composer.addPass( renderPass );
    composer.addPass( gammaPass );
    composer.addPass( fxaaPass );
    composer.addPass( distortPass );
    composer.addPass( grainPass );
    composer.addPass( filmPass );
    composer.addPass( badTVPass );

}

function addBackgroundStars() {
    //background stars should be generated in a spherical shape away from the center
    const geometry = new THREE.BufferGeometry();
    const vertices = [];
    const v3Pos = new THREE.Vector3();
    const minDistance = 6000;
    const maxDistance = 20000;
    for ( let i = 0; i < 10000; i ++ ) {

        v3Pos.x = Globals.seededRandomNumberGenerator.getRandom(-1, 1);
        v3Pos.y = Globals.seededRandomNumberGenerator.getRandom(-1, 1);
        v3Pos.z = Globals.seededRandomNumberGenerator.getRandom(-1, 1);
        v3Pos.normalize();
        v3Pos.multiplyScalar( Globals.seededRandomNumberGenerator.getRandom(minDistance, maxDistance) );
        vertices.push( v3Pos.x, v3Pos.y, v3Pos.z );

    }
    geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );

    const spikeTexture = Globals.assets.find(x => x.name === 'spikeTexture').contents;
    const material = new THREE.PointsMaterial( { 
        size: 200,
        map: spikeTexture,
        blending: THREE.AdditiveBlending,
        depthTest: true,
        depthWrite: false,
        transparent: true
     } );
    
    const particles = new THREE.Points( geometry, material );

    particles.matrixAutoUpdate = false;

    Globals.scene.add(particles);
}

function addStars() {
    //generation parameters
    const starsCount = 64;
    const maxDistXZ = 2000;
    const maxDistY = 150; //y axis is "up/down" -> the galactic plane

    //generate the stars
    for (let i = 0; i < starsCount; i++) {
        const newStar = new Star();

        //bias towards cooler stars by rolling twice and taking the smaller number lol
        const t1 = Globals.seededRandomNumberGenerator.getRandom( 2000, 50000 );
        const t2 = Globals.seededRandomNumberGenerator.getRandom( 2000, 50000 );
        const temperature = t1 < t2 ? t1 : t2;

        //const name = Globals.seededRandomNumberGenerator.getRandomValueFromArray( StarNames );
        const name = `FGC ${Globals.seededRandomNumberGenerator.getRandomInt(1, 1000)}`;

        newStar.initialise(name, temperature);

        const r = maxDistXZ * Globals.seededRandomNumberGenerator.random();
        const theta = Globals.seededRandomNumberGenerator.random() * 2 * Math.PI;
        const x = r * Math.cos(theta);
        const z = r * Math.sin(theta);

        const y = Globals.seededRandomNumberGenerator.getRandom(-maxDistY, maxDistY);

        newStar.setPosition(x, y, z);

        Globals.gameObjects.set( newStar.uuid, newStar );
        Globals.starObjectsForRaycasting.push( newStar.starObject );

        Globals.scene.add(newStar.pivot);
    }
}

function addInterstellarNebulas() {

    const nebulaCount = 3;
    const maxDistXZ = 1000;
    const maxDistY = 150;

    const maxScale = 2400;
    const minScale = 1200;

    for (let i = 0; i < nebulaCount; i++) {
        const newNebula = new Nebula();
        const scale = Globals.seededRandomNumberGenerator.getRandom(minScale, maxScale);
        const seed = Globals.seededRandomNumberGenerator.getRandomInt(0, 100);
        const colour = new THREE.Color( 0x5278B2 );
        //colour.offsetHSL( Helper.getRandom(-0.05, 0.05), 0, 0 );
        newNebula.initialise( seed, colour, scale, scale * 0.6);

        const r = maxDistXZ * Globals.seededRandomNumberGenerator.random();
        const theta = Globals.seededRandomNumberGenerator.random() * 2 * Math.PI;
        const x = r * Math.cos(theta);
        const z = r * Math.sin(theta);

        const y = Globals.seededRandomNumberGenerator.getRandom(-maxDistY, maxDistY);

        newNebula.setPosition(x, y, z);

        Globals.gameObjects.set( newNebula.name, newNebula );
        Globals.scene.add(newNebula.pivot);
    }
}

init();

async function init() {

    //set up the seeded random number gen here
    //this will hopefully be useful later on for save/load? 
    const seed = Helper.getRandomString();
    console.log("using seed: ", seed);
    Globals.seededRandomNumberGenerator = new Helper.RandomNumberGenerator(seed);

    //build our asset list
    buildAssetList();

    //load
    let loaderTextElement = document.getElementById( 'loader-description' );
    loader = new Loader(loaderTextElement);
    await loader.loadAssets(Globals.assets);

    document.addEventListener( 'click', onClick, true );
    window.addEventListener( 'pointermove', onPointerMove );
    starInfoCaption = document.getElementById("star-info");

    //start the game ticks
    setInterval(tick.bind(this), 1000/Globals.gameSpeed);

    Globals.camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 20000 );

    Globals.camera.position.set( 0, 0, 1.5 );

    Globals.scene = new THREE.Scene();
    Globals.scene.background = new THREE.Color( 0x000000 );

    renderer = new THREE.WebGLRenderer( { antialias: false } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight, false );
    renderer.outputEncoding = THREE.sRGBEncoding;

    //prepare shaders
    //setupShaders();

    document.body.appendChild( renderer.domElement );

    stats = new Stats();
	document.body.appendChild( stats.dom );
    stats.dom.id = "stats";

    window.addEventListener( 'resize', onWindowResize, false );

    Globals.camera.lookAt( 0, 0, 0 );
    Globals.scene.add(Globals.camera);

    Globals.controls = new OrbitControls.OrbitControls( Globals.camera, renderer.domElement )
    Globals.controls.listenToKeyEvents( window );
    Globals.controls.maxDistance = skipIntro ? 3000 : 25000;
    Globals.controls.minDistance = 1000;
    Globals.controls.zoomSpeed = 0.5;
    Globals.controls.panSpeed = 0.6;
    Globals.controls.maxPolarAngle = Math.PI * 0.4; //limit orbiting the camera too close to the plane
    Globals.controls.minPolarAngle = 0; 

    Globals.controls.mouseButtons = { LEFT: THREE.MOUSE.PAN, MIDDLE: THREE.MOUSE.DOLLY, RIGHT: THREE.MOUSE.ROTATE };
	Globals.controls.touches = { ONE: THREE.TOUCH.PAN, TWO: THREE.TOUCH.DOLLY_ROTATE };

    Globals.camera.position.set( 25000, 0, 0 );

    Globals.controls.autoRotate = !skipIntro;
    Globals.controls.autoRotateSpeed = 0.1;
    Globals.controls.screenSpacePanning = false;
    intro = !skipIntro;

    Globals.controls.enableDamping = true;
    Globals.controls.dampingFactor = 0.05;

    Globals.controls.addEventListener("end", onCameraMoveEnd.bind(this));

    Globals.controls.update();


    buildSystem();


    //run a single game tick to make sure everthing is all good before we render for the first time
    tick();

    animate();

    const loaderElement = document.getElementById( 'loader' );
    loaderElement.style.display = 'none';
    const blockerElement = document.getElementById( 'blocker' );
    blockerElement.style.display = 'none';


    // setTimeout(() => {        
    //     badTVPass.enabled = false;
    // }, 250);
    
}

function buildSystem() {
    //background stars
    addBackgroundStars();

    //interstellar nebulas
    addInterstellarNebulas();

    //stars
    addStars();
}

function onWindowResize() {

    Globals.camera.aspect = window.innerWidth / window.innerHeight;
    Globals.camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight, false );

    //composer.setSize( width, height );
    //composer.setPixelRatio( window.devicePixelRatio );

    //fxaaPass.material.uniforms.resolution.value.set( 1 / width, 1 / height ).multiplyScalar( 1 / window.devicePixelRatio );

}

function onPointerMove( e ) {
	pointer.x = ( e.clientX / window.innerWidth ) * 2 - 1;
	pointer.y = - ( e.clientY / window.innerHeight ) * 2 + 1;
}

function onClick( e ) {
	raycaster.setFromCamera( pointer, Globals.camera );
	const intersects = raycaster.intersectObjects( Globals.starObjectsForRaycasting );

    if (intersects.length) {
        let intersect = intersects[0].object;
        while (intersect.parent !== null && !intersect.parent.isScene) {
            intersect = intersect.parent;
        }
        const star = Globals.gameObjects.get(intersect.uuid);
        starInfoCaption.innerText = `${star.name} - Class ${star.stellarClass} - ${Math.round(star.solarRadius)} Ru - ${Math.round(star.temperature).toLocaleString()}°`;
        starInfoCaption.style.color = "#"+star.spikeColour.getHexString();

        //le hack to force the css animation to restart
        starInfoCaption.style.display = "none";
        setTimeout(() => {
            starInfoCaption.style.display = "block";
        }, 1);

    } else {
        starInfoCaption.innerText = "";
        starInfoCaption.style.display = "none";
    }
}

function onCameraMoveEnd() {

    cameraTargetPos.copy(Globals.controls.target);
    cameraTargetPos.clamp( cameraPositionMin, cameraPositionMax );

    if (!cameraTargetPos.equals(Globals.controls.target)) {
        new TWEEN.Tween(Globals.controls.target).to({
            x: cameraTargetPos.x,
            y: cameraTargetPos.y,
            z: cameraTargetPos.z
        }, 100).start();
    }
}

function tick() {

    const time = performance.now();
    const delta = ( time - lastTickTime ) / 1000;

    Globals.gameObjects.forEach(object => {
        object.onGameTick(delta);
    });

    if (intro) {
        if (Globals.controls.maxDistance != 3000) {
            Globals.controls.maxDistance -= 10;
        } else {
            intro = false;
            Globals.controls.autoRotate = false;
        }
    }

    lastTickTime = time;
}

function animate() {

    requestAnimationFrame( animate );
    stats.update();
    Globals.controls.update();
    TWEEN.update();
    render();

}

function switchTVShaderOnAndOff() {
    if (badTVPass.enabled) {
        badTVPass.enabled = false;
        setTimeout(switchTVShaderOnAndOff, Helper.getRandom(1000, 10000));
    } else {
        badTVPass.enabled = true;
        shaderParams.tvDistortion = 2.0;
        shaderParams.tvDistortion2 = 0.5;
        shaderParams.tvSpeed = 0.3;
        shaderParams.tvRollSpeed = -0.2;
        setTimeout(switchTVShaderOnAndOff, 200);
    }
}

function render() {    
    renderer.render( Globals.scene, Globals.camera );
    // shaderTime += 0.1;

    // grainPass.enabled = shaderParams.enableNoise;
    // grainPass.material.uniforms.noiseOffset.value += shaderParams.noiseSpeed;
    // grainPass.material.uniforms.intensity.value = shaderParams.noiseIntensity;

    // distortPass.enabled = shaderParams.enableDistortion;
    // distortPass.material.uniforms.baseIor.value = shaderParams.baseIor;
    // distortPass.material.uniforms.bandOffset.value = shaderParams.bandOffset;
    // distortPass.material.uniforms.jitterOffset.value += 0.01;
    // distortPass.material.uniforms.jitterIntensity.value = shaderParams.jitterIntensity;

    // badTVPass.material.uniforms.distortion.value = shaderParams.tvDistortion;
    // badTVPass.material.uniforms.distortion2.value = shaderParams.tvDistortion2;
    // badTVPass.material.uniforms.speed.value = shaderParams.tvSpeed;
    // badTVPass.material.uniforms.rollSpeed.value = shaderParams.tvRollSpeed;
    // badTVPass.material.uniforms.time.value = shaderTime;

    // filmPass.material.uniforms.time.value = shaderTime;

    // composer.render();
}

export { Globals };