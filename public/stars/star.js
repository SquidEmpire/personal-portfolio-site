"use strict";

import { Globals } from './stars.js';
import * as THREE from './vendor/three.module.js';
import * as Helper from './helper.js';

class Star {

    uuid;
    name;
    pivot;

    //https://en.wikipedia.org/wiki/Main_sequence#Sample_parameters
    solarRadius; //bigger star = bigger core and spike
    mass; //our sun = 10 range of ~3 - ~85 (1-100)
    luminosity; //our sun = 10 range of ~3 - ~85 -> brighter star = bigger spike UNUSED
    temperature; //range of 2,000 - 50,000 (k) [~6000 = sun] -> this affects colour
    stellarClass; //class is determined by the other values above

    coreColour; //the star core is almost always white
    spikeColour; //the spikes may have more colour variation

    starObject;
    spikeObject;

    constructor() {
        this.coreColour = new THREE.Color( 0xffffff );
        this.spikeColour = new THREE.Color( 0xffffff );
    }

    setPosition(x,y,z) {
        this.pivot.position.set(x, y, z);
        this.spikeObject.updateMatrix();
        this.starObject.updateMatrix();
    }

    //colours comes from tempreture -> hotter = more blue, cooler = more red
    updateColours() {
        let r = 1;
        let g = 1;
        let b = 1;

        if (this.temperature > 10000) {
            //high temp stars are blue
            const rg = 1 - (this.temperature / 75000);
            r = rg;
            g = rg;
        } else if (this.temperature > 4000) {
            //mid temp stars are yellow and white
            const gb = this.temperature / 10000;
            b = gb;
            g = gb * 2;
        } else {
            //very cool stars are red
            const gb = this.temperature / 4000;
            b = 0.0;
            g = gb * 0.2;

            this.coreColour.setRGB( r, g + 0.5, b + 0.5 );
        }
        
        this.spikeColour.setRGB( r, g, b );
    }

    updateStellarClass() {
        if (this.temperature > 30000) {
            this.stellarClass = "O";
        } else if (this.temperature > 10000) {
            this.stellarClass = "B";
        } else if (this.temperature > 7500) {
            this.stellarClass = "A";
        } else if (this.temperature > 6000) {
            this.stellarClass = "F";
        } else if (this.temperature > 5500) {
            this.stellarClass = "G";
        } else if (this.temperature > 4000) {
            this.stellarClass = "K";
        } else if (this.temperature > 2500) {
            this.stellarClass = "M";
        } else {
            this.stellarClass = "L";
        }
    }

    initialise(name, temperature) {

        //choose the star's properties

        //TEMPERATURE
        this.temperature = temperature;

        //SIZE and MASS
        //the temperature of the star bias the size and mass
        this.mass = this.temperature / 600;
        this.solarRadius = this.temperature / 600;

        this.updateColours();
        this.updateStellarClass();

        //3d stuff
        this.pivot = new THREE.Group();

        const starGeometry = new THREE.SphereGeometry( THREE.MathUtils.clamp( this.solarRadius * 0.25, 5, 15), 32, 16 );
        const starMaterial = new THREE.MeshBasicMaterial({
            color: this.coreColour
        });
        this.starObject = new THREE.Mesh( starGeometry, starMaterial );
        this.pivot.add( this.starObject );

        const spikeTexture = Globals.assets.find(x => x.name === 'spikeTexture').contents;
        const spikeMaterial = new THREE.SpriteMaterial({ 
            map: spikeTexture,
            color: this.spikeColour,
            depthWrite: false,
            rotation: Math.PI / 2.5 //I think the spikes look better with a slight rotation :)
        });

        const spikeSize = THREE.MathUtils.clamp(this.solarRadius * 5, 100, 1000);

        this.spikeObject = new THREE.Sprite( spikeMaterial );
        this.spikeObject.scale.set( spikeSize, spikeSize, 1 );
        this.pivot.add( this.spikeObject );

        this.spikeObject.matrixAutoUpdate = false; //manually call matrix updates for performance(?)
        this.starObject.matrixAutoUpdate = false;

        this.name = name;
        this.pivot.name = "Star " + this.name;
        this.uuid = this.pivot.uuid;
    }

    onGameTick( delta ) {

    }

};

export { Star };