"use strict";

//https://stackoverflow.com/a/47593316
class RandomNumberGenerator {

    seed;
    rand;

    constructor(seed = "apples") {
        this.seed = this.cyrb128(seed);
        this.rand = this.mulberry32(this.seed[0]);
    }

    cyrb128(str) {
        let h1 = 1779033703, h2 = 3144134277,
            h3 = 1013904242, h4 = 2773480762;
        for (let i = 0, k; i < str.length; i++) {
            k = str.charCodeAt(i);
            h1 = h2 ^ Math.imul(h1 ^ k, 597399067);
            h2 = h3 ^ Math.imul(h2 ^ k, 2869860233);
            h3 = h4 ^ Math.imul(h3 ^ k, 951274213);
            h4 = h1 ^ Math.imul(h4 ^ k, 2716044179);
        }
        h1 = Math.imul(h3 ^ (h1 >>> 18), 597399067);
        h2 = Math.imul(h4 ^ (h2 >>> 22), 2869860233);
        h3 = Math.imul(h1 ^ (h3 >>> 17), 951274213);
        h4 = Math.imul(h2 ^ (h4 >>> 19), 2716044179);
        return [(h1^h2^h3^h4)>>>0, (h2^h1)>>>0, (h3^h1)>>>0, (h4^h1)>>>0];
    }

    mulberry32(a) {
        return function() {
          var t = a += 0x6D2B79F5;
          t = Math.imul(t ^ t >>> 15, t | 1);
          t ^= t + Math.imul(t ^ t >>> 7, t | 61);
          return ((t ^ t >>> 14) >>> 0) / 4294967296;
        }
    }

    random() {
        return this.rand();
    }

    getRandom(min = 0, max = 1) {
        return (this.random()  * (max - min)) + min;
    }

    getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.round(this.random() * (max - min)) + min;
    }

    getRandomValueFromArray(array) {
        return array[~~(this.random() * array.length)];
    }

}



//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
const getRandomInt = function(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.round(Math.random() * (max - min)) + min;
}

const getRandom = function(min = 0, max = 1) {
    return (Math.random() * (max - min)) + min;
}

const getRandomBool = function() {
    return Math.random() > 0.5;
}

const getRandomValueFromArray = function(array) {
    return array[~~(Math.random() * array.length)];
}

const getRandomString = function() {
    return (Math.random() + 1).toString(36).substring(2);
}

//https://stackoverflow.com/a/2117523
const getUUID = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

export { RandomNumberGenerator, getRandom, getRandomInt, getRandomBool, getUUID, getRandomString, getRandomValueFromArray };