const StarNames = [
    "Burke's Star", "Neeson's Star", "Bonner's Star", "Lynch's Star", "Todd's Star",
    "Hero", "Apep", "Djoser", "Imhotep", "Traitor",
    "Osiris", "Caranack", "Elam", "Spire", "Penguin",
    "Nemesis", "Eyeshine", "Belt", "Everest", "Fairy",
    "Shakespeare", "Pope", "Nesar", "Solace", "Draconis",
    "Zenith", "Lucifer", "Diable", "Argent", "Kirkas",
    "Nefer", "Sapphire", "Painted", "Grees", "Alver",
    "Rends", "Minister", "Nullarbor", "Nulla", "Huon",
    "Cooke", "Shame", "Caleourus", "Hellenius", "Ramulus",
    "Aurelia", "Argentia", "Xenos", "Carbonicus", "Kirk",
    "Tamanrasset", "Assekrem", "Crim", "Yojimbo", "Zero",
    "Munchausen", "Optambi", "Orzonia", "Micar", "Polm",
    "Gillam's Star", "Cashman's Star", "Law's Star", "Bram's Star", "Karst's Star",
    "Spirius", "Rattus", "Turbico", "Wadi", "Gympie",
    "Centeria", "Expar", "Crab", "Gratus", "Jillam"
];

export { StarNames };