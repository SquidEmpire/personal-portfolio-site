let canvas;

const fontSize = 28;

const sunRadius = 40;

const sinterRadius = 6;
const sinterOrbitDistance = 90;
const sinterOrbitSpeed = 2.5;
const sinterColour = "rgb(255, 130, 0)";
const sinterRingColour = "rgba(255, 130, 0, 0.6)";

const moaRadius = 6;
const moaOrbitDistance = 160;
const moaOrbitSpeed = 2.8;
const moaColour = "rgb(255, 240, 210)";
const moaRingColour = "rgba(255, 240, 210, 0.6)";

const asteroidBeltDistance = 200;
const asteroidBeltSpeed = 0.4;
const asteroidBeltColour = "rgb(120, 110, 110)";

const alverSalverRadius = 30;
const alverSalverOrbitDistance = 260;
const alverSalverOrbitSpeed = 2;
const alverSalverRotationSpeed = 0.9;
const alverRadius = 14;
const salverRadius = 10;
const salverColour = "rgb(100, 255, 230)";
const alverColour = "rgb(240, 250, 255)";
const alverSalverRingColour = "rgba(255, 255, 255, 1)";

const sednosRadius = 6;
const sednosOrbitDistance = 450;
const sednosOrbitSpeed = 0.4;
const sednosColour = "rgb(70, 170, 255)";
const sednosRingColour = "rgba(20, 130, 255, 0.6)";

let centrePosition = {x: 0, y: 0};
let alverSalverCenterAngle = 0;
let sinterAngle = 0;
let moaAngle = 0;
let asteroidBeltOffset = 0;
let alverAngle = 0;
let salverAngle = 0;
let sednosAngle = 0;

function init() {
    canvas = document.getElementById("solar-system-canvas");

    centrePosition.x = canvas.width / 2;
    centrePosition.y = canvas.height / 2;

    setTimeout(update, 16); //60fps
}

function update() {
    setTimeout(update, 16);

    const ctx = canvas.getContext('2d');

    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    ctx.font = `${fontSize}px serif`;
    ctx.lineWidth = 1;
    ctx.setLineDash([]);

    ctx.beginPath();

    //sun
    ctx.fillStyle = 'rgb(255, 255, 180)';
    ctx.arc(centrePosition.x, centrePosition.y, sunRadius, 0, Math.PI * 2, false);
    ctx.fill();
    ctx.fillText("Sun", centrePosition.x - 23, centrePosition.y - sunRadius - fontSize/2);

    ctx.closePath();
    
    drawOrbitRing(ctx, sinterOrbitDistance, sinterRingColour);
    drawOrbitRing(ctx, moaOrbitDistance, moaRingColour);
    drawOrbitRing(ctx, alverSalverOrbitDistance, alverSalverRingColour);
    drawOrbitRing(ctx, sednosOrbitDistance, sednosRingColour);

    ctx.beginPath();

    //calculate alver/salver centre position
    alverSalverCenterAngle += Math.acos( 1 - ((alverSalverOrbitSpeed / alverSalverOrbitDistance)**2 / 2));
    const alverSalverX = centrePosition.x + alverSalverOrbitDistance * Math.cos(alverSalverCenterAngle);
    const alverSalverY = centrePosition.y + alverSalverOrbitDistance * Math.sin(alverSalverCenterAngle);

    //draw alver/salver co-orbit ring
    ctx.arc(alverSalverX, alverSalverY, alverSalverRadius, 0, Math.PI * 2, false);
    ctx.strokeStyle = 'rgba(255, 255, 255, 0.6)';
    ctx.stroke();

    ctx.closePath();
    
    //sinter
    sinterAngle += Math.acos( 1 - ((sinterOrbitSpeed / sinterOrbitDistance)**2 / 2));
    drawPlanet(ctx, sinterAngle, sinterOrbitDistance, sinterRadius, sinterColour, "Sinter");

    //moa
    moaAngle += Math.acos( 1 - ((moaOrbitSpeed / moaOrbitDistance)**2 / 2));
    drawPlanet(ctx, moaAngle, moaOrbitDistance, moaRadius, moaColour, "Moa");

    //the asteroid belt
    ctx.beginPath();

    ctx.arc(centrePosition.x, centrePosition.y, asteroidBeltDistance, 0, Math.PI * 2, false);
    ctx.strokeStyle = asteroidBeltColour;
    ctx.fillStyle = asteroidBeltColour;
    ctx.lineWidth = 4;
    ctx.setLineDash([5, 15]);
    ctx.lineDashOffset = asteroidBeltOffset;
    asteroidBeltOffset -= asteroidBeltSpeed;
    ctx.stroke();

    ctx.fillText("Asteroid belt", centrePosition.x - 70, centrePosition.y - asteroidBeltDistance - fontSize/2);

    ctx.closePath();
    ctx.lineWidth = 1;
    ctx.setLineDash([]);

    //calculate & draw alver
    ctx.beginPath();
    alverAngle += Math.acos( 1 - ((alverSalverRotationSpeed / alverSalverRadius)**2 / 2));
    const alverPositionX = alverSalverX + alverSalverRadius * Math.cos(alverAngle);
    const alverPositionY = alverSalverY + alverSalverRadius * Math.sin(alverAngle);

    ctx.fillStyle = alverColour;
    ctx.arc(alverPositionX, alverPositionY, alverRadius, 0, Math.PI * 2, false);
    ctx.fill();

    ctx.fillText("Alver", alverPositionX - 32, alverPositionY - alverRadius - fontSize/2);

    ctx.closePath();
    ctx.beginPath();

    //calculate & draw salver
    salverAngle = alverAngle + Math.PI;
    const salverPositionX = alverSalverX + alverSalverRadius * Math.cos(salverAngle);
    const salverPositionY = alverSalverY + alverSalverRadius * Math.sin(salverAngle);

    ctx.fillStyle = salverColour;
    ctx.arc(salverPositionX, salverPositionY, salverRadius, 0, Math.PI * 2, false);
    ctx.fill();

    ctx.fillText("Salver", salverPositionX - 32, salverPositionY - salverRadius - fontSize/2);

    ctx.closePath();

    //sednos
    sednosAngle += Math.acos( 1 - ((sednosOrbitSpeed / sednosOrbitDistance)**2 / 2));
    drawPlanet(ctx, sednosAngle, sednosOrbitDistance, sednosRadius, sednosColour, "Sednos");

}

function drawOrbitRing(ctx, orbitDistance, colour) {
    ctx.beginPath();

    ctx.arc(centrePosition.x, centrePosition.y, orbitDistance, 0, Math.PI * 2, false);
    ctx.strokeStyle = colour;
    ctx.stroke();

    ctx.closePath();
}

function drawPlanet(ctx, angle, orbitDistance, radius, colour, name) {
    ctx.beginPath();
    //calculate & draw a planet
    const positionX = centrePosition.x + orbitDistance * Math.cos(angle);
    const positionY = centrePosition.y + orbitDistance * Math.sin(angle);

    ctx.fillStyle = colour;
    ctx.arc(positionX, positionY, radius, 0, Math.PI * 2, false);
    ctx.fill();

    ctx.fillText(name, positionX - 32, positionY - radius - fontSize/2);

    ctx.closePath();
}