let canvas;

const values = [
    {
        name: "None",
        value: 58,
        colour: "#a10000",
        textColour: "#CCECEC"
    },
    {
        name: "Reformed Bohrism",
        value: 38,
        colour: "#ac6100",
        textColour: "#CCDBEE"
    },
    {
        name: "Cavallotto Bohrism",
        value: 2.9,
        colour: "#b09b39",
        textColour: "#D7DCEF"
    },
    {
        name: "THOPS",
        value: 1,
        colour: "#b5cf82",
        textColour: "#EBE6F5"
    },
    {
        name: "Nalomism",
        value: 0.1,
        colour: "#c7ffd1",
        textColour: "#FFF4FD"
    }
];

function init() {
    canvas = document.getElementById("state-of-religion-canvas");
    draw();
}

/*mostly stolen from https://code.tutsplus.com/tutorials/how-to-draw-a-pie-chart-and-doughnut-chart-using-javascript-and-html5-canvas--cms-27197*/

function drawPieSlice(ctx, centerX, centerY, radius, startAngle, endAngle, color ){
    ctx.fillStyle = color;
    ctx.beginPath();
    ctx.moveTo(centerX,centerY);
    ctx.arc(centerX, centerY, radius, startAngle, endAngle);
    ctx.closePath();
    ctx.fill();
}

function draw() {
    const ctx = canvas.getContext("2d");

    let total_value = 0;
    for (let i = 0; i < values.length; i++) {
        total_value += values[i].value;
    }

    let start_angle = -Math.PI/2;
    const halfWidth = canvas.width/2;
    const halfHeight = canvas.height/2;
    const pieRadius = Math.min(halfWidth, halfHeight);
    const halfPieRadius = pieRadius / 2

    for (let i = 0; i < values.length; i++) {
        const val = values[i].value;
        const slice_angle = 2 * Math.PI * val / total_value;

        drawPieSlice(
            ctx,
            halfWidth,
            halfHeight,
            Math.min(halfWidth, halfHeight),
            start_angle,
            start_angle + slice_angle,
            values[i].colour
        );
        start_angle += slice_angle;
    }

    start_angle = -Math.PI/2;
    //labels 
    for (let i = 0; i < values.length; i++) {
        const val = values[i].value;
        const slice_angle = 2 * Math.PI * val / total_value;

        const labelX = halfWidth + halfPieRadius * Math.cos(start_angle + slice_angle/2);
        const labelY = halfHeight + halfPieRadius * Math.sin(start_angle + slice_angle/2) - (i * 20) + 20;
        const segmentValue = 100 * val / total_value;
        ctx.fillStyle = values[i].textColour;
        ctx.font = "14px Arial";
        ctx.fillText(values[i].name + " " + segmentValue + "%", labelX, labelY);

        start_angle += slice_angle;
    }
}