﻿File Type: *.PNG (Portable Network Graphic)
-----------------------------------------------------------------------
Release Date: September 12, 2011 11:25 AM CST
-----------------------------------------------------------------------
Dot Matrix Paper
-----------------------------------------------------------------------
Texture by: James Head (BLUamnEsiac) 
            kintarcomics@gmail.com
            http://blueamnesiac.deviantart.com/
=======================================================================
This texture is freely available for anyone to make use of so long as
it's a non-commercial project.