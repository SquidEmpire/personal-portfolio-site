import {Stat} from "./classes/stat.js";
import {ChildhoodBackground} from "./classes/background_child.js";
import {AdulthoodBackground} from "./classes/background_adult.js";
import {Trait} from "./classes/trait.js";
import {Skill} from "./classes/skill.js";

async function loadListAsType(location, classType) {
    const response = await fetch(location);
    const jsonData = await response.json();
    let result = [];
    jsonData.forEach(jsonElement => {
        result.push(new classType(jsonElement));
    });
    console.log(`loaded ${result.length} ${classType.name}s from ${location.split('/').pop()}`);
    return result;
}

async function loadList(location) {
    const response = await fetch(location);
    const jsonData = await response.json();
    let result = [];
    jsonData.forEach(jsonElement => {
        result.push(jsonElement);
    });
    console.log(`loaded ${result.length} items from ${location.split('/').pop()}`);
    return result;
}

//https://stackoverflow.com/a/56150320
export function jsonMapCapableReplacer(key, value) {
    if(value instanceof Map) {
        return {
            dataType: 'Map',
            value: Array.from(value.entries()),
        };
    } else {
        return value;
    }
}

export function jsonMapCapableReviver(key, value) {
    if(typeof value === 'object' && value !== null) {
        switch(value.dataType) {
            case "Map":
                return new Map(value.value);
            case "AdulthoodBackground":
                return new AdulthoodBackground(value);
            case "ChildhoodBackground":
                return new ChildhoodBackground(value);
            case "Skill":
                return new Skill(value);
            case "Stat":
                return new Stat(value);
            case "Trait":
                return new Trait(value);
        }
    }
    return value;
}

export async function loadStats(logElement) {
    if (logElement) {
        logElement.innerHTML = "Loading stats";
    }
    return await loadListAsType('./js/data/stats.json', Stat);
}

export async function loadChildhoodBackgrounds(logElement) {
    if (logElement) {
        logElement.innerHTML = "Loading childhood backgrounds";
    }
    return await loadListAsType('./js/data/backgrounds_child.json', ChildhoodBackground);
}

export async function loadAdulthoodBackgrounds(logElement, tagList) {
    if (logElement) {
        logElement.innerHTML = "Loading adulthood backgrounds";
    }
    return await loadListAsType('./js/data/backgrounds_adult.json', AdulthoodBackground);
}

export async function loadTraits(logElement) {
    if (logElement) {
        logElement.innerHTML = "Loading traits";
    }
    return await loadListAsType('./js/data/traits.json', Trait);
}

export async function loadSkills(logElement) {
    if (logElement) {
        logElement.innerHTML = "Loading skills";
    }
    return await loadListAsType('./js/data/skills.json', Skill);
}

export async function loadFemaleNames(logElement) {
    if (logElement) {
        logElement.innerHTML = "Loading feminine names"; 
    }
    return await loadList('./js/data/names/names_female.json');
}

export async function loadMaleNames(logElement) {
    if (logElement) {
        logElement.innerHTML = "Loading masculine names";
    }
    return await loadList('./js/data/names/names_male.json');
}

export async function loadSurnames(logElement) {
    if (logElement) {
        logElement.innerHTML = "Loading surnames";
    }
    return await loadList('./js/data/names/surnames.json');
}