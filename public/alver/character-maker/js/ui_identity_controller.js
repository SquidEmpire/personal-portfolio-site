import {GlobalData} from "./maker.js"; 

export function setCharacterImage(character, src) {
    const identityImage = document.getElementById("identity-image-img");
    identityImage.src = src;
    character.image = encodeImage(identityImage);
    updateIdentityInformation(character);
}

//https://stackoverflow.com/a/45266377
function encodeImage(imageElement) {
    let canvas = document.createElement('canvas');
    canvas.height = imageElement.naturalHeight;
    canvas.width = imageElement.naturalWidth;
    let ctx = canvas.getContext('2d');

    ctx.drawImage(imageElement, 0, 0, canvas.width, canvas.height);
    return canvas.toDataURL();
}

export function setRandomName(character, type) {
    const nameElement = document.getElementById("identity-name-field");
    const sNames = GlobalData.surnamesList;
    const surnameDoubleBarrelled = Math.random() > 0.95;
    const useMononym = Math.random() > 0.9899;
    let newName = "";

    if (useMononym) {
        let sIndex = Math.round(Math.random() * (sNames.length - 1));
        newName = sNames[sIndex];
    } else {
        if (type === "m") {
            const mNames = GlobalData.maleNamesList;
            const mIndex = Math.round(Math.random() * (mNames.length - 1));
            newName += mNames[mIndex];
        } else if (type === "f") {
            const fNames = GlobalData.femaleNamesList;
            const fIndex = Math.round(Math.random() * (fNames.length - 1));
            newName += fNames[fIndex];
        }
    
        newName += " ";
    
        const sIndex = Math.round(Math.random() * (sNames.length - 1));    
        if (surnameDoubleBarrelled) {
            const sIndex2 = Math.round(Math.random() * (sNames.length - 1));
            newName += sNames[sIndex].split(' ').join('') + "-" + sNames[sIndex2].split(' ').join('');
        } else {
            newName += sNames[sIndex];
        }
    }

    character.name = newName;
    nameElement.value = newName;
    updateIdentityInformation(character);
}

export function setBackstoryPrompts(character) {
    const backstoryContainer = document.getElementById("identity-description-prompts-container");
    const backstoryLabel = document.getElementById("identity-description-prompts-label");
    const backstoryElement = document.getElementById("identity-backstory");

    backstoryLabel.innerHTML = "Here are some prompts to get you thinking...";

    let backstoryPromptString = "";    
    character.adulthoodBackgrounds.forEach(background => {
        backstoryPromptString += `<p>Formerly you were a ${background.name}. How did you start?</p>`;
        if (background.questions) {
            backstoryPromptString += `<p>${background.questions[0]}</p>`;
        }
        backstoryPromptString += `<p>Did you enjoy the experience? Were you good at it?</p>`;
        if (background.questions) {
            backstoryPromptString += `<p>${background.questions[1]}</p>`;
        }
        backstoryPromptString += `<p>Why did you stop?</p>`;
    });

    backstoryElement.placeholder = 
    `My character was a ${character.childhoodBackground.name} child. When they grew up, they became a ${character.adulthoodBackgrounds[0]?.name}.`;
    
    backstoryContainer.innerHTML = backstoryPromptString;
}

//this runs every time anything is done in the text area... bit overkill?
export function updateIdentityInformation(character) {
    const identitiyNextPageButton = document.getElementById("buttonGoToResult");
    const warning = document.getElementById("identity-backstory-warning");

    if (character.name) {// && character.image && character.backgroundText) {
        identitiyNextPageButton.removeAttribute("disabled");
    } else {
        identitiyNextPageButton.setAttribute('disabled', true);
    }

    //if there's too much text in the description (bless 'em) give a warning
    if (character.backgroundText.length >= 1100) {
        warning.classList.remove("hidden");
    } else {
        warning.classList.add("hidden");
    }

    updateIdentityDisplay(character);
}

export function updateIdentityDisplay(character) {
    const backstoryElement = document.getElementById("identity-backstory");
    const nameElement = document.getElementById("identity-name-field");
    const identityImage = document.getElementById("identity-image-img");    

    identityImage.src = character.image ? character.image : "./images/placeholder.png";
    //setBackstoryPrompts(character);
    backstoryElement.value = character.backgroundText;
    nameElement.value = character.name;
}