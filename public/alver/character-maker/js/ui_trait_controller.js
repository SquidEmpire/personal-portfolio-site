import {GlobalData} from "./maker.js"; 

const minTraits = 3;
const maxTraits = 6;
let traitBalance = 0;

export function updateTraitlist(character) {
    //go through the list of traits, work out if we are able to pick them, and disable if not
    const traits = GlobalData.traitsMap;
    traits.forEach(trait => {
        const traitElement = document.querySelector(`[data-name="${trait.name}"]`);
        const passingConditions = getPassingConditions(character, trait);
        if (passingConditions.length === 0) {
            traitElement.classList.add("disabled");
            //if we're disabling a trait, also remove it from the character if they have it selected
            removeTraitIfActive(character, trait);
        } else {
            traitElement.classList.remove("disabled");
        }
    });
}

export function updateTraitSelection(character) {
    const traits = GlobalData.traitsMap;
    traits.forEach(trait => {
        const traitElement = document.querySelector(`[data-name="${trait.name}"]`);
        traitElement.classList.remove("active");
        for (var i = 0; i < character.traits.length; i++) {
            if (character.traits[i].equals(trait)) {
                traitElement.classList.add("active");
                break;
            }
        }
    });

    const traitSelectionElement = document.querySelector(`#traits-selected span`);
    traitSelectionElement.innerHTML = character.traits.length;
    const traitsBalanceElement = document.querySelector(`#traits-points span`);
    traitsBalanceElement.innerHTML = traitBalance;

    const statsNextPageButton = document.getElementById("buttonGoToIdentity");
    if (character.traits.length >= minTraits && character.traits.length <= maxTraits && traitBalance <= 2) {
        statsNextPageButton.removeAttribute("disabled");
    } else {
        statsNextPageButton.setAttribute('disabled', true);
    }
}

export function selectTrait(character, trait) {    
    //if we already have the trait, remove it instead
    if (removeTraitIfActive(character, trait)) {
        return;
    };
    //add a new trait
    if (character.traits.length < maxTraits) {
        if (!getPassingConditions(character, trait).length) {
            return;
        }
        //we can take this trait!
        character.traits.push(trait);
        //adjust balance
        traitBalance += parseInt(trait.value);
    }    
    
    updateTraitSelection(character);
}

function removeTraitIfActive(character, trait) {
    for (var i = 0; i < character.traits.length; i++) {
        if (character.traits[i].equals(trait)) {
            character.traits.splice(i, 1);
            traitBalance -= parseInt(trait.value);
            updateTraitSelection(character);
            return true;
        }
    }
    return false;
}

//given a trait, return the condition/s that pass, allowing the player to take this trait, otherwise, return an empty list
function getPassingConditions(character, trait) {
    let passingConditions = [];
    trait._conditions.forEach(condition => {
        //if the condition is "any", let it through instantly
        if (condition === "Any") {
            passingConditions.push(condition);
            return;
        }
        //now check if the condition matches the name of a background
        //TODO: probably not nessecary to actually find the BG, just search directly in player BG list?
        const backgrounds = GlobalData.adulthoodBackgroundsMap;
        const matchingBackground = backgrounds.get(condition);
        if (matchingBackground) {
            //we have a matching background, now check if the player has it
            character.adulthoodBackgrounds.forEach(background => {
                if (background.name === matchingBackground.name) {
                    //if we do, move on to the next condition
                    passingConditions.push(condition);
                    return;
                }
            });
        }
        //there are no matching backgrounds, try breaking it into a conditional check (e.g. CH 6+)
        const conditionParts = condition.split(' ');
        if (GlobalData.statsMap.get(conditionParts[0])) {
            const characterStatValue = character.adjustedStats.get(conditionParts[0]);
            const value = conditionParts[1];
            if (conditionParts[2]) {
                //is our matching stat at or greater than the condition value?
                if (conditionParts[2] === "+" && characterStatValue >= value) {
                    passingConditions.push(condition);
                 //is our matching stat at or less than the condition value?
                } else if (conditionParts[2] === "-" && characterStatValue <= value) {
                    passingConditions.push(condition);
                }
            } else {
                //is our matching stat exactly at the condition value?
                passingConditions.push(condition);
            }
        }
    });
    return passingConditions;
}