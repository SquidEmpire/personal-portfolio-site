import {GlobalData} from "./maker.js"; 

const maxStatPoints = 15;
let availableStatPoints = 0;

export function refreshStatsValues(character, statsMap) {    
    character.adjustedStats.forEach((value, abbreviation) => {
        const stat = GlobalData.statsMap.get(abbreviation);
        //TODO: should put a local anchor here instead of document
        const statInputElement = document.querySelector(`[data-name="${abbreviation}"] input`);
        const statLevelElement = document.querySelector(`[data-name="${abbreviation}"] p.stat-control-level`);

        statInputElement.value = value;
        statLevelElement.innerHTML = stat.levels[value];
    });
    const statPointsElement = document.querySelector(`#stats-remaining-points span`);
    statPointsElement.innerHTML = availableStatPoints;

    const statsNextPageButton = document.getElementById("buttonGoToTraits");
    if (availableStatPoints === 0) {
        statsNextPageButton.removeAttribute("disabled");
    } else {
        statsNextPageButton.setAttribute('disabled', true);
    }
}

export function incStat(character, stat) {
    const statRef = stat.abbreviation
    const oldStatValue = character.stats.get(statRef);
    const oldAdjustedStatValue = character.adjustedStats.get(statRef);
    if (oldAdjustedStatValue < 10 && availableStatPoints > 0) {
        character.stats.set(statRef, character.stats.get(statRef) + 1);

        applyChildhoodEffect(character, stat);
        availableStatPoints = availableStatPoints + (oldStatValue - character.stats.get(statRef));
        refreshStatsValues(character);
    }    
}

export function decStat(character, stat) {
    const statRef = stat.abbreviation
    const oldStatValue = character.stats.get(statRef);
    const oldAdjustedStatValue = character.adjustedStats.get(statRef);
    if (oldAdjustedStatValue > 1) {
        character.stats.set(statRef, character.stats.get(statRef) - 1);
        
        applyChildhoodEffect(character, stat);
        availableStatPoints = availableStatPoints + (oldStatValue - character.stats.get(statRef));
        refreshStatsValues(character);
    }    
}

export function applyChildhoodEffects(character) {
    character.stats.forEach((value, abbreviation) => {
        const stat = GlobalData.statsMap.get(abbreviation);
        applyChildhoodEffect(character, stat);   
        
        const statEffectsElement = document.querySelector(`[data-name="${abbreviation}"] .stat-effect`);
        statEffectsElement.innerHTML = character.childhoodBackground.getEffectHtml(stat);
    });    
    
    refreshStatsValues(character);
}

function applyChildhoodEffect(character, stat) {
    const statRef = stat.abbreviation
    const currentStatValue = character.stats.get(statRef);
    const effect = character.childhoodBackground.getStatEffect(stat);
    let adjustedStat = currentStatValue + effect;
    if (adjustedStat > 10) {
        adjustedStat = 10;
    }
    if (adjustedStat < 1) {
        adjustedStat = 1;
    }
    character.adjustedStats.set(statRef, adjustedStat);
}