import {GlobalData} from "./maker.js"; 

const adulthoodActiveBackgroundTagsList = [];

export function refreshAdulthoodFilterTags() {
    const adulthoodBackgroundListFilter = document.getElementById("adulthood-backgrounds-filter-tags");
    adulthoodBackgroundListFilter.innerHTML = "";
    for (let i = 0; i < GlobalData.adulthoodBackgroundTagsList.length; i++) {
        const tag = GlobalData.adulthoodBackgroundTagsList[i];
        const html = `
            <li class="adulthood-backgrounds-filter-tag" data-name="${tag}">${tag}</li>
        `;
        adulthoodBackgroundListFilter.insertAdjacentHTML('beforeend', html);
    }
    
    adulthoodBackgroundListFilter.addEventListener('click', (e) => {
        const tagElement = e.target;
        const tag = e.target.dataset.name;
        if (tag && tag !== "") {
            const index = adulthoodActiveBackgroundTagsList.indexOf(tag);
            if (index === -1) {
                //tag not currently active, make it so
                adulthoodActiveBackgroundTagsList.push(tag);
                tagElement.classList.add("active");
            } else {
                //tag is currently active, deactivate it
                adulthoodActiveBackgroundTagsList.splice(index, 1);
                tagElement.classList.remove("active");
            }
    
            refreshFilteredBackgrounds();
        }
    });
}

export function refreshAdulthoodBackgrounds(character) {
    const adulthoodBackgroundListContainer = document.getElementById("adulthood-backgrounds");
    const modifier = character ? character.adulthoodBackgrounds.length : 1;
    adulthoodBackgroundListContainer.innerHTML = "";
    GlobalData.adulthoodBackgroundsMap.forEach(element => {
        const html = element.getHtml(modifier);
        adulthoodBackgroundListContainer.insertAdjacentHTML('beforeend', html);
    });
}

export function selectChildhoodBackground(character, background) {
    if (background) {
        character.childhoodBackground = background;
    } else {
        character.childhoodBackground = null;
        console.error(`Failed to find background ${background} in loaded background data`);
    }
    refreshSelectedChildBackgrounds(character);
    window.scrollTo({ top: document.body.scrollHeight, behavior: 'smooth' })
}

export function selectAdulthoodBackground(character, background) {
    if (background) {
        //if the player already has the background, remove it
        for (var i = 0; i < character.adulthoodBackgrounds.length; i++) {
            if (character.adulthoodBackgrounds[i].equals(background)) {
                character.adulthoodBackgrounds.splice(i, 1);
                refreshAdulthoodBackgrounds(character);
                refreshSelectedAdultBackgrounds(character);
                return;
            }
        }
        if (character.adulthoodBackgrounds.length === 0) {
            character.adulthoodBackgrounds = [background];
        } else if (character.adulthoodBackgrounds.length > 2) {
            //max backgrounds already selected
            return;
        } else {
            character.adulthoodBackgrounds.push(background);
            refreshAdulthoodBackgrounds(character);
        }        
    } else {
        console.error(`Failed to find background ${background} in loaded background data`);
    }
    refreshSelectedAdultBackgrounds(character);
}

export function refreshSelectedBackgrounds(character) {
    refreshSelectedChildBackgrounds(character);
    refreshSelectedAdultBackgrounds(character);
}

function refreshSelectedChildBackgrounds(character) {
    //deactivate the exisiting active background
    let selectedBackground = document.querySelector(`#childhood-backgrounds .active`); 
    if (selectedBackground) {
        selectedBackground.classList.remove("active");
    }    
    //activate the new background
    if (character.childhoodBackground) {
        const backgroundToSelect = (document.querySelector(`#childhood-backgrounds [data-name="${character.childhoodBackground.name}"]`));
        backgroundToSelect.classList.add("active");
    }

    const nextPageButton = document.getElementById("buttonGoToAdultBackgrounds");
    if (character.childhoodBackground !== null) {
        nextPageButton.removeAttribute("disabled");
    } else {
        nextPageButton.setAttribute('disabled', true);
    }

}

function refreshSelectedAdultBackgrounds(character) {
    //deactivate the exisiting active backgrounds
    let selectedBackgrounds = document.querySelectorAll(`#adulthood-backgrounds .active`);
    selectedBackgrounds.forEach(background => {
        background.classList.remove("active");
    });
    //activate the new backgrounds
    character.adulthoodBackgrounds.forEach(background => {
        const backgroundsToSelect = (document.querySelector(`#adulthood-backgrounds [data-name="${background.name}"]`));
        backgroundsToSelect.classList.add("active");
    });

    const nextPageButton = document.getElementById("buttonGoToStats");
    if (character.adulthoodBackgrounds.length > 0) {
        nextPageButton.removeAttribute("disabled");
    } else {
        nextPageButton.setAttribute('disabled', true);
    }
}

function refreshFilteredBackgrounds() {
    GlobalData.adulthoodBackgroundsMap.forEach(background => {
        const element = document.querySelector(`#adulthood-backgrounds [data-name="${background.name}"]`);
        const backgroundHasActiveFilterTag = adulthoodActiveBackgroundTagsList.every(r => background.tags.includes(r));
        if (backgroundHasActiveFilterTag) {
            element.classList.remove("hidden");
        } else {
            element.classList.add("hidden");
        }
    });
}