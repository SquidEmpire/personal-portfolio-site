var currentPageIndex;
var pages;
var navigatorElement;
var buttonGoToAdultBackgrounds;
var buttonGoToStats;
var buttonGoToTraits;
var buttonGoToIdentity;
var buttonGoToResult;

export function init() {

    currentPageIndex = 0;

    //elements
    //TODO: make this a map with key = index so can use as enum? e.g. Navigator.page.landing;
    pages = [
        document.getElementById("landing"),
        document.getElementById("wizard-child-background"),
        document.getElementById("wizard-adult-background"),
        document.getElementById("wizard-stats"),
        document.getElementById("wizard-traits"),
        document.getElementById("wizard-identity"),
        document.getElementById("sheet")
    ];    
    navigatorElement = document.getElementById("navigator");
    buttonGoToAdultBackgrounds = document.getElementById("buttonGoToAdultBackgrounds");
    buttonGoToStats = document.getElementById("buttonGoToStats");
    buttonGoToTraits = document.getElementById("buttonGoToTraits");
    buttonGoToIdentity = document.getElementById("buttonGoToIdentity");
    buttonGoToResult = document.getElementById("buttonGoToResult");

    //navigation listener
    navigatorElement.addEventListener('click', onNavigation.bind(this));
    //page listeners
    buttonGoToAdultBackgrounds.addEventListener('click', () => {showPage(2)});
    buttonGoToStats.addEventListener('click', () => {showPage(3)});
    buttonGoToTraits.addEventListener('click', () => {showPage(4)});
    buttonGoToIdentity.addEventListener('click', () => {showPage(5)});
    buttonGoToResult.addEventListener('click', () => {showPage(6)});
}

function hideAllPages() {
    pages.forEach(page => {
        page.classList.add("hidden");
    });
}

export function showPage(pageIndex) {
    hideAllPages();
    pages[pageIndex].classList.remove("hidden");
    currentPageIndex = pageIndex;
    window.scrollTo(0, 0);
    refreshNavigation();
}

function refreshNavigation() {
    let navigationElements = navigatorElement.children[0].children;
    for (const item of navigationElements) {
        const navIndex = parseInt(item.dataset.navIndex);
        navIndex === currentPageIndex ? item.classList.add("active") : item.classList.remove("active");
        navIndex <= currentPageIndex ? item.classList.remove("disabled") : item.classList.add("disabled");
    };
}

function onNavigation(e) {
    const navElement = e.composedPath()[e.composedPath().indexOf(e.currentTarget) - 2];
    const navIndex = navElement.dataset.navIndex;
    const disabled = navElement.classList.contains("disabled");
    if (!disabled && navIndex !== undefined) {
        showPage(navIndex);
    }
}