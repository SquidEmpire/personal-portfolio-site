export function getSkillStarString(character, skill) {
    const characterSkill = parseInt(character?.skills.get(skill.name));
    if (characterSkill) {
        return "★".repeat(characterSkill) + "☆".repeat(5-characterSkill);
    } else {
        return "☆".repeat(5);
    } 
}

function getSkillLevelString(character, skill) {
    const characterSkill = parseInt(character?.skills.get(skill.name));
    switch (characterSkill) {
        case 1:
            return `NOVICE. You practise this skill maybe once a month, and you're not terrible at it. +2 to rolls.`;
        case 2:
            return `INITIATE. You routinely practise this skill and are quite good at it. +4 to rolls.`;
        case 3:
            return `COMPETENT. You’ve been practising this skill for years, and know a few extra tricks. +6 to rolls.`;
        case 4:
            return `SKILLED. You’re extremely good at this skill, you could teach others. +10 to rolls.`;
        case 5:
            return `MASTER. You’re one of the best in the world at this skill. You always succeed on rolls!`;
        case 0:
        default:
            return `NO SKILL. You’ve only heard about this skill, or maybe you saw someone do it once or twice.`;
    }     
}

export function hideSkillPopup() {
    const skillPopupElement = document.getElementById("skills-popup");
    skillPopupElement.classList.add("hidden");
}

export function showSkillPopup(character, skill, target, direction) {
    const skillPopupElement = document.getElementById("skills-popup");
    skillPopupElement.classList.remove("hidden");
    if (character) {
        skillPopupElement.innerHTML = `
        <div id="skills-popup-title" class="popup-title">
            <h3>${skill.name}</h3>
            <p>${getSkillStarString(character, skill)}</p>
        </div>
        <p>${getSkillLevelString(character, skill)}</p>
        <p id="skills-popup-description" class="popup-description">${skill.description}</p>
    `;
    } else {
        skillPopupElement.innerHTML = `
        <div id="skills-popup-title class="popup-title"">
            <h3>${skill.name}</h3>
        </div>
        <p id="skills-popup-description" class="popup-description">${skill.description}</p>
    `;
    }
    
    const rect = target.getBoundingClientRect();
    const positionString = direction === "left" ? `calc(${rect.left}px - 30vw)` : `calc(${rect.left}px + 100px)`;
    skillPopupElement.style.top = `calc(${rect.top + window.scrollY}px - 3em)`;
    skillPopupElement.style.left = positionString;
}

export function hideStatPopup() {
    const statPopupElement = document.getElementById("stats-popup");
    statPopupElement.classList.add("hidden");
}

export function showStatPopup(stat, target, direction) {
    const statPopupElement = document.getElementById("stats-popup");
    statPopupElement.classList.remove("hidden");
    statPopupElement.innerHTML = `
        <div id="stats-popup-title" class="popup-title">
            <h3>${stat.abbreviation} - ${stat.name}</h3>
        </div>
        <p id="skills-popup-description" class="popup-description">${stat.description}</p>
    `;

    const rect = target.getBoundingClientRect();
    const positionString = direction === "left" ? `calc(${rect.left}px - 30vw)` : `calc(${rect.left}px + 100px)`;
    statPopupElement.style.top = `calc(${rect.top + window.scrollY}px - 3em)`;
    statPopupElement.style.left = positionString;
}