export class Character {

    name;
    backgroundText;
    image;
    childhoodBackground;
    stats;
    adjustedStats;
    adulthoodBackgrounds;
    traits;
    skills;

    constructor(statsMap) {
        this.name = "";
        this.backgroundText = "";
        this.image = "";
        this.childhoodBackground = null;        
        this.adulthoodBackgrounds = [];
        this.traits = [];
        this.skills = new Map();

        this.stats = new Map();
        this.adjustedStats = new Map();
        //TODO: probably not necessary to do this?
        statsMap.forEach(stat => {
            this.stats.set(stat.abbreviation, 5);
            this.adjustedStats.set(stat.abbreviation, 5);
        });
    }

    calculateSkills() {
        this.skills = new Map();
        const modifier = this.adulthoodBackgrounds.length;
        this.adulthoodBackgrounds.forEach(background => {
            background.effects.forEach(effect => {
                const effectValues = background.getModifiedEffectValues(effect, modifier);
                let existitingValue = this.skills.get(effectValues.target);
                if (existitingValue) {
                    //we already have a skill in this, add to the exisiting value
                    this.skills.set(effectValues.target, existitingValue + effectValues.value);
                } else {
                    this.skills.set(effectValues.target, effectValues.value);
                }
            });
        });
        //do a second pass of the skills and round all the values to whole numbers
        this.skills.forEach((skillValue, skillKey) => {
            this.skills.set(skillKey, Math.round(skillValue));
        });
    }

    deserialise(jsonData) {
        this.name = jsonData.name;
        this.backgroundText = jsonData.backgroundText;
        this.image = jsonData.image;
        this.childhoodBackground = jsonData.childhoodBackground;
        this.stats = jsonData.stats;
        this.adjustedStats = jsonData.adjustedStats;
        this.adulthoodBackgrounds = jsonData.adulthoodBackgrounds;
        this.traits = jsonData.traits;
        this.skills = jsonData.skills;
    }

    toJSON() {
        return {
            name: this.name,
            backgroundText: this.backgroundText,
            image: this.image,
            childhoodBackground: this.childhoodBackground,
            stats: this.stats,
            adjustedStats: this.adjustedStats,
            adulthoodBackgrounds: this.adulthoodBackgrounds,
            traits: this.traits,
            skills: this.skills
        }
    }

}