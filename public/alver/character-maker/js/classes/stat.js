export class Stat {

    name;
    description;
    abbreviation;
    levels;

    constructor(jsonData) {
        this.deserialise(jsonData);
    }

    getHtml() {
        return `
            <div class="stat" data-name="${this.abbreviation}">
                <div class="stat-info">
                    <h3>${this.abbreviation}: ${this.name}</h3>
                    <p class="description">${this.description}</p>
                </div>
                <div class="stat-control">
                    <div class="numberbox stat-control-numberbox">
                        <button class="numberbox-down">-</button>
                        <input class="numberbox-number" type="number" max=10 min=0 readonly="true" value="0">
                        <button class="numberbox-up">+</button>
                    </div>
                    <p class="stat-control-level">${this.levels[0]}</p>
                    <div class="stat-effect">
                    </div>
                </div>
            </div>
        `;
    }

    getResultHtml() {
        return `
            <div class="stat-result" data-name="${this.abbreviation}">
                <h2 class="stat-result-number">${this.name} (${this.abbreviation}): <span>0</span></h2>
                <p class="stat-result-level">${this.levels[0]}</p>
            </div>
        `;
    }

    toJson() {
        return {
            dataType: "Stat",
            name: this.name,
            description: this.description,
            abbreviation: this.abbreviation,
            levels: this.levels
        }
    }

    deserialise(jsonData) {
        this.name = jsonData.name;
        this.description = jsonData.description;
        this.abbreviation = jsonData.abbreviation;
        this.levels = jsonData.levels;
    }

}