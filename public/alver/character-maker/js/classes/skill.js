export class Skill {

    name;
    description;
    stat;
    category;

    constructor(jsonData) {
        this.deserialise(jsonData);
    }

    getHtml() {
        //☆★
        return `
            <div class="skill" data-name="${this.name}">
                <h3>${this.name}</h3>
                <p>${this.category}: <span>${this.stat}</span></p>
                <p class="skill-stars">☆☆☆☆☆</p>
            </div>
        `;
    }

    deserialise(jsonData) {
        this.name = jsonData.name;
        this.description = jsonData.description;
        this.stat = jsonData.stat;
        this.category = jsonData.category;
    }

    toJSON() {
        return {
            dataType: "Skill",
            name: this.name,
            description: this.description,
            stat: this.stat,
            category: this.category
        }
    }

}