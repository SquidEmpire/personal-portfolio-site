export class Trait {

    name;
    description;
    value;
    conditionsShorthandString;
    _conditions;

    constructor(jsonData) {
        this.deserialise(jsonData);
        this._parseConditions();
    }

    equals(trait) {
        return (
            trait.name === this.name &&
            trait.description === this.description &&
            trait.value === this.value &&
            trait.conditionsShorthandString === this.conditionsShorthandString
        );
    }

    _parseConditions() {
        let conditionList = this.conditionsShorthandString.split(',');
        for (let i = 0; i < conditionList.length; i++) {
            conditionList[i] = conditionList[i].trim();
        }
        this._conditions = conditionList;
    }    

    getHtml() {
        let valueClassname = "";
        if (this.value == 0) {
            valueClassname = "trait-neutral";
        } else if (this.value > 0) {
            valueClassname = "trait-positive";
        } else {
            valueClassname = "trait-negative";
        }
        return `
            <div class="trait ${valueClassname}" data-name="${this.name}">
                <h3>${this.name}</h3>
                <p class="description">${this.description}</p>
                <p class="conditions">Conditions: ${this.conditionsShorthandString}</p>
            </div>
        `;
    }

    deserialise(jsonData) {
        this.name = jsonData.name;
        this.description = jsonData.description;
        this.value = jsonData.value;
        this.conditionsShorthandString = jsonData.conditions;
    }

    toJSON() {
        return {
            dataType: "Trait",
            name: this.name,
            description: this.description,
            value: this.value,
            conditions: this.conditionsShorthandString
        }
    }
}