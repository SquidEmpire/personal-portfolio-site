export class ChildhoodBackground {

    name;
    description;
    effects;

    constructor(jsonData) {
        this.deserialise(jsonData);
    }

    getStatEffect(stat) {
        for (let i = 0; i < this.effects.length; i++) {
            const effect = this.effects[i];
            const effectParts = effect.split(" ");
            const statAbv = effectParts[0];
            const statEffect = effectParts[1]
            if (statAbv === stat.abbreviation) {
                return parseInt(statEffect);
            }
        };
        return 0;   
    }

    getEffectHtml(stat) {
        for (let i = 0; i < this.effects.length; i++) {
            const effect = this.effects[i];
            const effectParts = effect.split(" ");
            const statAbv = effectParts[0];
            const statEffect = parseInt(effectParts[1]) >= 0 ? "+" + effectParts[1] : effectParts[1];
            if (statAbv === stat.abbreviation && statEffect !== 0) {
                return `
                    <p>${statEffect} from your ${this.name} childhood</p>
                `;
            }
        }
        return "";
    }

    getEffectsHtml() {
        let result = "";
        for (let i = 0; i < this.effects.length; i++) {
            const effect = this.effects[i];
            const effectParts = effect.split(" ");
            const statAbv = effectParts[0];
            const statEffect = parseInt(effectParts[1]) >= 0 ? "+" + effectParts[1] : effectParts[1];
            result += `<li class="background-effect" data-stat-abbreviation="${statAbv}">${statEffect} ${statAbv}</li>`;
        }
        return result;
    }

    getHtml() {
        return `
            <div class="background background-childhood" data-name="${this.name}">
                <h3>${this.name}</h3>
                <p>${this.description}</p>
                <h4>Effects:</h4>
                <ul>${this.getEffectsHtml()}</ul>
            </div>
        `;
    }

    equals(childhoodBackground) {
        return (
            childhoodBackground.name === this.name &&
            childhoodBackground.description === this.description &&
            childhoodBackground.effects.sort().toString() === this.effects.sort().toString()
        );
    }

    toJSON() {
        return {
            dataType: "ChildhoodBackground",
            name: this.name,
            description: this.description,
            effects: this.effects
        }
    }

    deserialise(jsonData) {
        this.name = jsonData.name;
        this.description = jsonData.description;
        this.effects = jsonData.effects;
    }

}