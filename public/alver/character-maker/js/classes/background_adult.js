export class AdulthoodBackground {

    name;
    description;
    effects;
    questions;
    tags;

    constructor(jsonData) {
        this.deserialise(jsonData);
    }

    getModifiedEffectsListHtml(modifier) {
        modifier = modifier ? modifier : 1;
        let result = "";
        this.effects.forEach(effect => {
            let effectValue = this.getModifiedEffectValues(effect, modifier);
            //effectValues.value = Math.floor(effectValues.value);
            effectValue.value = Math.round((effectValue.value + Number.EPSILON) * 100) / 100;
            if (modifier !== 1) {
                const tilde = (effectValue.value % 1 != 0) ? "~" : "";
                result += `<li class="background-effect background-effect-modified" data-skill-name="${effectValue.target}">${tilde}+${effectValue.value} ${effectValue.target}</li>`;
            } else {
                result += `<li class="background-effect" data-skill-name="${effectValue.target}">+${effectValue.value} ${effectValue.target}</li>`;
            }
        });
        return result;
    }

    getHtml(modifier) {
        const effectsListHtml = this.getModifiedEffectsListHtml(modifier);
        return `
            <div class="background background-childhood" data-name="${this.name}">
                <h3>${this.name}</h3>
                <p>${this.description}</p>
                <ul>Effects:</ul>
                <p>${effectsListHtml}</p>
            </div>
        `;
    }

    getEffectValues(effectString) {
        //this relies on our backgrounds_adults being nicely formatted
        const effectValue = parseInt(effectString.slice(1, 2));
        const effectTarget = effectString.slice(3);
        return {"target": effectTarget, "value": effectValue};
    }

    getModifiedEffectValues(effectString, modifier) {
        let effectValues = this.getEffectValues(effectString);
        effectValues.value = effectValues.value / modifier;
        return effectValues;
    }

    equals(childhoodBackground) {
        return (
            childhoodBackground.name === this.name &&
            childhoodBackground.description === this.description &&
            childhoodBackground.effects.toString() === this.effects.toString()
        );
    }

    deserialise(jsonData) {
        this.name = jsonData.name;
        this.description = jsonData.description;
        this.effects = jsonData.effects;
        this.questions = jsonData.questions;
        this.tags = jsonData.tags;
    }

    toJSON() {
        return {
            dataType: "AdulthoodBackground",
            name: this.name,
            description: this.description,
            effects: this.effects
        }
    }

}