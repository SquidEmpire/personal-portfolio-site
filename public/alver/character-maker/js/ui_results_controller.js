import * as PopupController from "./ui_popup_controller.js"
import {GlobalData} from "./maker.js"; 

export function updateResults(character) {
    resetImage(character);
    resetName(character);
    resetStats(character);
    resetBackstory(character);
    resetTraits(character);
    resetSkills(character);
    PopupController.hideSkillPopup();
}

function resetImage(character) {
    const imageElement = document.getElementById("resultsheet-image");
    if (character.image) {
        imageElement.src = character.image;
    } else {
        imageElement.src = "images/default.png";
    }
}

function resetName(character) {
    const nameElement = document.getElementById("resultsheet-name");
    nameElement.innerHTML = character.name;
}

function resetStats(character) {
    const statElement = document.getElementById("resultsheet-stats");
    character.adjustedStats.forEach((value, abbreviation) => {
        const stat = GlobalData.statsMap.get(abbreviation);
        const statInputElement = statElement.querySelector(`[data-name="${abbreviation}"] h2.stat-result-number span`);
        const statLevelElement = statElement.querySelector(`[data-name="${abbreviation}"] p.stat-result-level`);

        statInputElement.innerHTML = value;
        statLevelElement.innerHTML = stat.levels[value];
    });    
}

function resetBackstory(character) {
    const backstoryElement = document.getElementById("resultsheet-backstory");
    backstoryElement.value = character.backgroundText;
}

function resetTraits(character) {
    const traitsContainer = document.getElementById("resultsheet-traits");
    traitsContainer.innerHTML = "";
    character.traits.forEach(trait => {
        const html = trait.getHtml();
        traitsContainer.insertAdjacentHTML('beforeend', html);
    });
}

function resetSkills(character) {
    const skillsContainerElement = document.getElementById("resultsheet-skills");
    character.calculateSkills();
    GlobalData.skillsMap.forEach(skill => {        
        const skillStarElement = skillsContainerElement.querySelector(`[data-name="${skill.name}"] p.skill-stars`);
        skillStarElement.innerHTML = PopupController.getSkillStarString(character, skill);
    });
}