import {Character} from "./classes/character.js";
import * as Loader from "./loader.js";
import * as Navigator from "./navigator.js"
import * as BackgroundController from "./ui_background_controller.js";
import * as StatsController from "./ui_stats_controller.js";
import * as TraitController from "./ui_trait_controller.js";
import * as IdentityController from "./ui_identity_controller.js";
import * as ResultsController from "./ui_results_controller.js";
import * as PopupController from "./ui_popup_controller.js"

var character;

export var GlobalData = {
    adulthoodBackgroundsMap: new Map(),
    adulthoodBackgroundTagsList: [],
    childhoodBackgroundsMap: new Map(),
    statsMap: new Map(),
    traitsMap: new Map(),
    skillsMap: new Map(),
    femaleNamesList: [],
    maleNamesList: [],
    surnamesList: []
};

async function init() {
    setupNavigator();    
    await loadAllData();
    setupPageData();
}

function setupNavigator() {    
    const buttonNewCharacter = document.getElementById("buttonNewCharacter");
    const inputImportCharacter = document.getElementById("buttonImportCharacter");
    const buttonExportCharacter = document.getElementById("buttonExportCharacter");
    buttonNewCharacter.addEventListener('click', onNewCharacterClick.bind(this));
    inputImportCharacter.addEventListener('change', onImportCharacter.bind(this));
    buttonExportCharacter.addEventListener('click', onExportCharacterClick.bind(this));
    Navigator.init();
}

async function loadAllData() {
    const loaderElement = document.getElementById("loader");
    const loaderElementDescription = document.getElementById("loader-description");

    const startTimeStamp = performance.now();
    console.log(`Starting load...`);
    const stArr = await Loader.loadStats(loaderElementDescription);
    const statsMap = new Map(stArr.map(item => [item.abbreviation, item]));
    const chbArr = await Loader.loadChildhoodBackgrounds(loaderElementDescription);
    const childhoodBackgroundsMap = new Map(chbArr.map(item => [item.name, item]));
    const ahArr = await Loader.loadAdulthoodBackgrounds(loaderElementDescription);
    const adulthoodBackgroundsMap = new Map(ahArr.map(item => [item.name, item]));
    const trArr = await Loader.loadTraits(loaderElementDescription);
    const traitsMap = new Map(trArr.map(item => [item.name, item]));
    const skArr = await Loader.loadSkills(loaderElementDescription);
    //skills are sorted alphabetically for later display
    skArr.sort((a, b) => a.name.localeCompare(b.name));
    const skillsMap = new Map(skArr.map(item => [item.name, item]));
    const femaleNamesList = await Loader.loadFemaleNames(loaderElementDescription);
    const maleNamesList = await Loader.loadMaleNames(loaderElementDescription);
    const surnamesList = await Loader.loadSurnames(loaderElementDescription);

    const adulthoodBackgroundTagsList = [];
    adulthoodBackgroundsMap.forEach(background => {
        for (let i = 0; i < background.tags.length; i++) {
            if(adulthoodBackgroundTagsList.indexOf(background.tags[i]) === -1) {
                adulthoodBackgroundTagsList.push(background.tags[i]);
            }
        }
    });
    console.log(`(Also recorded ${adulthoodBackgroundTagsList.length} unique adulthood background tags)`);

    GlobalData = {
        adulthoodBackgroundsMap: adulthoodBackgroundsMap,
        adulthoodBackgroundTagsList : adulthoodBackgroundTagsList,
        childhoodBackgroundsMap: childhoodBackgroundsMap,
        statsMap: statsMap,
        traitsMap: traitsMap,
        skillsMap: skillsMap,
        femaleNamesList: femaleNamesList,
        maleNamesList: maleNamesList,
        surnamesList: surnamesList
    };
    console.log(`Done loading. Took ${(performance.now() - startTimeStamp).toFixed(2)}ms`);
    loaderElement.classList.add("hidden");
}

//setup all the data on the pages
function setupPageData() {
    //setup childhood background page data
    const childhoodBackgroundListContainer = document.getElementById("childhood-backgrounds");
    GlobalData.childhoodBackgroundsMap.forEach(element => {
        const html = element.getHtml();
        childhoodBackgroundListContainer.insertAdjacentHTML('beforeend', html);
    });
    childhoodBackgroundListContainer.addEventListener('click', (e) => {
        const backgroundName = e.composedPath()[e.composedPath().indexOf(e.currentTarget) - 1]?.dataset.name;
        if (backgroundName) {
            const background = GlobalData.childhoodBackgroundsMap.get(backgroundName);
            BackgroundController.selectChildhoodBackground(character, background);
            StatsController.applyChildhoodEffects(character);
        }
    });
    childhoodBackgroundListContainer.addEventListener('mouseover', (e) => {
        PopupController.hideStatPopup();
        const statAbbreviation = e.target?.dataset.statAbbreviation;
        if (statAbbreviation) {
            const stat = GlobalData.statsMap.get(statAbbreviation);
            if (stat) {
                PopupController.showStatPopup(stat, e.target, "right");
            }
        }
    });
    childhoodBackgroundListContainer.addEventListener('mouseout', (e) => {
        PopupController.hideStatPopup();
    });

    //setup adulthood background page data
    BackgroundController.refreshAdulthoodFilterTags();

    const adulthoodBackgroundListContainer = document.getElementById("adulthood-backgrounds");
    BackgroundController.refreshAdulthoodBackgrounds(character);
    adulthoodBackgroundListContainer.addEventListener('click', (e) => {
        const backgroundName = e.composedPath()[e.composedPath().indexOf(e.currentTarget) - 1]?.dataset.name;
        if (backgroundName) {
            const background = GlobalData.adulthoodBackgroundsMap.get(backgroundName);
            BackgroundController.selectAdulthoodBackground(character, background);
            TraitController.updateTraitlist(character);
            IdentityController.setBackstoryPrompts(character);
        }
    });
    adulthoodBackgroundListContainer.addEventListener('mouseover', (e) => {
        PopupController.hideSkillPopup();
        const skillName = e.target?.dataset.skillName;
        if (skillName) {
            const skill = GlobalData.skillsMap.get(skillName);
            if (skill) {
                PopupController.showSkillPopup(null, skill, e.target, "right");
            }
        }
    });
    adulthoodBackgroundListContainer.addEventListener('mouseout', (e) => {
        PopupController.hideSkillPopup();
    });

    //setup stats page data
    const statsContainer = document.getElementById("stats-container");
    GlobalData.statsMap.forEach(stat => {
        const html = stat.getHtml();
        statsContainer.insertAdjacentHTML('beforeend', html);
        const statPlusButton = statsContainer.querySelector(`[data-name="${stat.abbreviation}"] button.numberbox-up`);
        const statMinusButton = statsContainer.querySelector(`[data-name="${stat.abbreviation}"] button.numberbox-down`);
        statPlusButton.addEventListener('click', e => {
            StatsController.incStat(character, stat);
            TraitController.updateTraitlist(character);
        });
        statMinusButton.addEventListener('click', e => {
            StatsController.decStat(character, stat);
            TraitController.updateTraitlist(character);
        });
    });

    //setup traits page data
    const traitsContainer = document.getElementById("traits-container");
    let positiveTraitList = []; 
    let neautralTraitList = [];
    let negativeTraitList = [];
    GlobalData.traitsMap.forEach(trait => {
        let value = parseInt(trait.value);
        if (value == 0) {
            neautralTraitList.push(trait);
        } else if (value > 0) {
            positiveTraitList.push(trait);
        } else {
            negativeTraitList.push(trait);
        }
    });
    let traitsHtml = `<div class="traits-positive"><h3>Positive Traits</h3>`;
    positiveTraitList.forEach(trait => {traitsHtml += trait.getHtml()});
    traitsHtml += `</div><div class="traits-neutral"><h3>Neutral Traits</h3>`;
    neautralTraitList.forEach(trait => {traitsHtml += trait.getHtml()});
    traitsHtml += `</div><div class="traits-negative"><h3>Negative Traits</h3>`;
    negativeTraitList.forEach(trait => {traitsHtml += trait.getHtml()});
    traitsHtml += `</div>`;
    traitsContainer.innerHTML = traitsHtml;
    traitsContainer.addEventListener('click', (e) => {
        const traitName = e.composedPath()[e.composedPath().indexOf(e.currentTarget) - 2]?.dataset.name;
        if (traitName) {
            const trait = GlobalData.traitsMap.get(traitName);
            TraitController.selectTrait(character, trait);
        }
    });

    //setup identity page data
    const reader = new FileReader();
    const identityImageUploadButton = document.getElementById("identity-image-input");
    reader.onload = e => {
        IdentityController.setCharacterImage(character, e.target.result);
    };
    identityImageUploadButton.addEventListener('change', e => {
        const f = e.target.files[0];
        reader.readAsDataURL(f);
    });
    const showPortaitsButton = document.getElementById("identity-portrait-list-show");
    const portraitListContainer = document.getElementById("portrait-list-container");
    showPortaitsButton.addEventListener('click', e => {
        if (portraitListContainer.classList.contains("hidden")) {
            portraitListContainer.classList.remove("hidden");
        } else {
            portraitListContainer.classList.add("hidden");
        }
    });
    portraitListContainer.addEventListener('click', e => {
        IdentityController.setCharacterImage(character, e.target.src);
    });
    const randomMNameButton = document.getElementById("random-m-name");
    const randomFNameButton = document.getElementById("random-f-name");
    randomMNameButton.addEventListener('click', e => {
        IdentityController.setRandomName(character, "m");
    });
    randomFNameButton.addEventListener('click', e => {
        IdentityController.setRandomName(character, "f");
    });

    const nameElement = document.getElementById("identity-name-field");
    nameElement.addEventListener("input", e => {
        character.name = e.target.value;
        IdentityController.updateIdentityInformation(character);
    });
    const backstoryElement = document.getElementById("identity-backstory");
    backstoryElement.addEventListener("input", e => {
        character.backgroundText = e.target.value;
        IdentityController.updateIdentityInformation(character);
    });
    //this is duplication of a listener (navigator already listens)
    const buttonGoToResult = document.getElementById("buttonGoToResult");
    buttonGoToResult.addEventListener('click', () => {ResultsController.updateResults(character);});

    //setup results page data
    const resultsStatsContainer = document.getElementById("resultsheet-stats");
    GlobalData.statsMap.forEach(stat => {
        const resultHtml = stat.getResultHtml();
        resultsStatsContainer.insertAdjacentHTML('beforeend', resultHtml);
    });
    const skillsContainer = document.getElementById("resultsheet-skills");
    GlobalData.skillsMap.forEach(skill => {
        const html = skill.getHtml();
        skillsContainer.insertAdjacentHTML('beforeend', html);
    });
    skillsContainer.addEventListener('mouseover', (e) => {
        PopupController.hideSkillPopup();
        const skillName = e.composedPath()[e.composedPath().indexOf(e.currentTarget) - 1]?.dataset.name;
        if (skillName) {
            const skill = GlobalData.skillsMap.get(skillName);
            PopupController.showSkillPopup(character, skill, e.composedPath()[e.composedPath().indexOf(e.currentTarget) - 1], "left");
        }
    });
    skillsContainer.addEventListener('mouseout', (e) => {
        PopupController.hideSkillPopup();
    });
}

function onNewCharacterClick() {
    character = new Character(GlobalData.statsMap);
    BackgroundController.refreshSelectedBackgrounds(character);
    StatsController.refreshStatsValues(character);
    TraitController.updateTraitSelection(character);
    IdentityController.updateIdentityInformation(character);
    ResultsController.updateResults(character);
    Navigator.showPage(1);
}

function resetStats() {
    [...character.stats.keys()].forEach((key) => {
        character.stats.set(key, 5);
    });
}

function resetTraits() {
    character.traits = [];
}

function onImportCharacter(e) {
    const inputImportCharacter = document.getElementById("buttonImportCharacter");
    const file = inputImportCharacter.files[0];
    const reader = new FileReader();
    reader.onload = () => {
        const fileContent = JSON.parse(reader.result, Loader.jsonMapCapableReviver);
        character = new Character(GlobalData.statsMap);
        character.deserialise(fileContent);

        BackgroundController.refreshSelectedBackgrounds(character);
        StatsController.refreshStatsValues(character);
        TraitController.updateTraitSelection(character);
        TraitController.updateTraitlist(character);
        IdentityController.updateIdentityInformation(character);
        IdentityController.updateIdentityDisplay(character);
        ResultsController.updateResults(character);
        Navigator.showPage(6);
    };
    reader.readAsText(file);
}

function onExportCharacterClick() {
    function download(content, fileName, contentType) {
        var a = document.createElement("a");
        var file = new Blob([content], {type: contentType});
        a.href = URL.createObjectURL(file);
        a.download = fileName;
        a.click();
    }
    const filename = character.name.replace(/[^a-z0-9]/gi, '_').toLowerCase();
    download(JSON.stringify(character, Loader.jsonMapCapableReplacer, "\t"), `${filename}.json`, 'text/plain');
}

init();

//going to need to find a way to wipe previously set data when proceeding to a page after going back behind it
//otherwise you could set traits and then go back and choose a background that disabled them