var characterSheetCloseButton, characterSheet, characterSheetContent;
var slideInfo, slideImage1, slideImage2;
var characters = new Map();
var nextSlidesIndex = 0;
var slides = [];
var showingSlideOne = true;
var timeout;

function buildCharacterSheet(target) {
    let character = characters.get(target);
    if (!character) {console.error("Clicked on something goofy"); return false}
    let traitsHTML = "";
    for (var i = 0; i < character.traits.length; i++) {
        let trait = character.traits[i];
        traitsHTML += `
            <tr>
                <td>${trait.name}</td>
                <td>${trait.desc}</td>
            </tr>
        `;
    }

    return `   
    
    <div class="character-title">
        <h1>${character.name} - ${character.title}</h1>
    </div>          
    <div class="character-info">                        
        <p>${character.desc}</p>
        <img src="${character.image}" class="jpg" alt='${character.title}'></img>
    </div>
    <div class="character-stats">
        <div class="player-stats-table stats">
            <h3>Stats:</h3>
            <table>
                <tbody>
                    <tr>
                        <td>Fitness</td>
                        <td>${character.stats.fitness}</td>
                    </tr>
                    <tr>
                        <td>Charisma</td>
                        <td>${character.stats.charisma}</td>
                    </tr>
                    <tr>
                        <td>Intelligence</td>
                        <td>${character.stats.intelligence}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="player-stats-table traits">
            <h3>Traits:</h3>
            <table>
                <tbody>
                    ${traitsHTML}
                </tbody>
            </table>
        </div>
        <p><b>Special skills: </b><span>${character.skills}</span></p>
    </div>
    
    `;
}

function showCharacterSheet(e) {
    const target = e.target.id;
    const html = buildCharacterSheet(target);
    if (html) {
        characterSheetContent.innerHTML = html;
        characterSheet.classList.remove('hidden');
    }    
}

function hideCharacterSheet() {
    characterSheet.classList.add('hidden');
}

function buildCharacters(charactersData) {
    charactersData.forEach(character => {
        characters.set(character.id, character);
    });
}

function buildSlides(slidesData) {
    slidesData.forEach(slide => {
        slides.push(slide);
    });
}

function changeSlide() {
    clearTimeout(timeout);
    let slide = slides[nextSlidesIndex];
    let html = `
        <h1>${slide.title}</h1>
        <p>${slide.text}</p>
    `;

    if (showingSlideOne) {
        slideImage2.style.background = `url(./images/grain3.gif) center / cover no-repeat, url(${slide.src}) center / cover no-repeat`;
        slideImage1.classList.add("behind");
        slideImage2.classList.remove('behind');
        showingSlideOne = false;
    } else {
        slideImage1.style.background = `url(./images/grain3.gif) center / cover no-repeat, url(${slide.src}) center / cover no-repeat`;
        slideImage2.classList.add("behind");
        slideImage1.classList.remove('behind');
        showingSlideOne = true;
    }

    slideInfo.innerHTML = html;

    if (nextSlidesIndex < slides.length - 1) {
        nextSlidesIndex++;
    } else {
        nextSlidesIndex = 0;
    }
    timeout = setTimeout(changeSlide, 15000);
}

async function init(sessionNumber) {

    //load the data
    let data = await fetch(`js/data/session${sessionNumber}.json`).then(response => response.json());

    characterSheetCloseButton = document.getElementById("characterSheetClose");
    characterSheet = document.getElementById("characterSheet");
    characterSheetContent = document.getElementById("characterSheetContent");

    slideInfo = document.getElementById("slides-info");
    slideImage1 = document.getElementById("slide-img-1");
    slideImage2 = document.getElementById("slide-img-2");

    characterSheetCloseButton.addEventListener("click", hideCharacterSheet);

    document.getElementById("character-table").addEventListener("click", showCharacterSheet);

    document.getElementById("slides").addEventListener("click", changeSlide);

    buildCharacters(data.characters);
    buildSlides(data.slides);
    changeSlide();
}