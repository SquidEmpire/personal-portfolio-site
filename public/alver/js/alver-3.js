var mountainElement1, mountainElement2, mountainElement3, mountainElement4;
var briefingOverlayElement, characterOverlayElement, rulesOverlayElement, equipmentOverlayElement;
var freeItemsTableElement, payloadItemsTableElement, extraItemsTableElement;
var supplyWeightLabel, supplyCostLabel;
var activeOverlayElement;

var chosenCharacter = null;
var items = new Map();

async function init() {

    mountainElement1 = document.getElementById("mountainLayer1");
    mountainElement2 = document.getElementById("mountainLayer2");
    mountainElement3 = document.getElementById("mountainLayer3");
    mountainElement4 = document.getElementById("mountainLayer4");

    briefingOverlayElement = document.getElementById("briefing");
    characterOverlayElement = document.getElementById("characters");
    rulesOverlayElement = document.getElementById("rules");
    equipmentOverlayElement = document.getElementById("equipment");

    freeItemsTableElement = document.getElementById("js-free-items");
    payloadItemsTableElement = document.getElementById("js-payload-items");
    extraItemsTableElement = document.getElementById("js-extra-items");

    supplyWeightLabel = document.getElementById("supply-weight-label");
    supplyCostLabel = document.getElementById("supply-cost-label");

    activeOverlayElement = null;

    await populateItemTables();

}

function onMenuItemClick(element) {
    onCloseOverlay();
    let targetElement = document.getElementById(element.dataset.targetOverlayId);
    targetElement.classList.remove("overlay-hidden");
    activeOverlayElement = targetElement;
}

function onCloseOverlay() {
    if (activeOverlayElement) {
        activeOverlayElement.classList.add("overlay-hidden");
        activeOverlayElement = null;
    }
}

async function populateItemTables() {
    let data = await fetch(`js/data/session3.json`).then(response => response.json());
    data.items.forEach(item => {
        items.set(item.id, item);
    });
    populateFreeItemsTable();
    populatePayloadItemsTable();
    populateExtraItemsTable();
    updateItemState();
}

function populateFreeItemsTable() {
    let html = "";
    items.forEach(item => {
        if (item.isFree) {
            item.owned += 1;
            html += `
            <tr class="item-row">
                <td onclick="incrementItemCount(this)">${item.name}</td>
                <td onclick="incrementItemCount(this)">${item.weight}kg</td>
                <td onclick="incrementItemCount(this)">$${item.cost}</td>
                <td><input type="number" id="item-${item.id}" name="${item.name}" min="0" max="999" value=${item.owned} onchange="updateItemCount(this)"></td>
            </tr>
            `;
        }
    });
    freeItemsTableElement.innerHTML = html;
}

function populatePayloadItemsTable() {
    let html = "";
    items.forEach(item => {
        if (item.isPayload) {
            html += `
            <tr class="item-row">
                <td onclick="incrementItemCount(this)">${item.name}</td>
                <td onclick="incrementItemCount(this)">${item.weight}kg</td>
                <td><input type="number" id="item-${item.id}" name="${item.name}" min="0" max="1" value=${item.owned} onchange="updateItemCount(this)"></td>
            </tr>
            `;
        }
    });
    payloadItemsTableElement.innerHTML = html;
}

function populateExtraItemsTable() {
    let html = "";
    items.forEach(item => {
        if (!item.isFree && !item.isPayload) {
            html += `
            <tr class="item-row">
                <td onclick="incrementItemCount(this)">${item.name}</td>
                <td onclick="incrementItemCount(this)">${item.weight}kg</td>
                <td onclick="incrementItemCount(this)">$${item.cost}</td>
                <td><input type="number" id="item-${item.id}" name="${item.name}" min="0" max="999" value=${item.owned} onchange="updateItemCount(this)"></td>
            </tr>
            `;
        }
    });
    extraItemsTableElement.innerHTML = html;
}

function incrementItemCount(e) {
    const input = e.parentElement.querySelector("input");
    let value = parseInt(input.value) + 1;
    input.value = value;
    updateItemCount(input);
}

function updateItemCount(element) {
    const id = parseInt(element.id.split("-")[1]);
    let item = items.get(id);
    item.owned = element.value;
    updateItemState();
}

function updateItemState() {
    let totalWeight = 0;
    let totalCost = 0;    
    
    items.forEach(item => {

        let itemWeight = 0;
        let itemCost = 0;

        if (item.owned > 0) {

            itemWeight = item.weight * item.owned;
            if (item.canBeWorn) {
                itemWeight -= item.weight;
            }

            itemCost = item.cost * item.owned;
            if (item.isFree) {
                itemCost -= item.cost;
            }
        }

        totalWeight += itemWeight;
        totalCost += itemCost;

    });

    supplyWeightLabel.innerHTML = totalWeight;
    supplyCostLabel.innerHTML = totalCost;
}