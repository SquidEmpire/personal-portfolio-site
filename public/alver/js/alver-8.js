var extraItemsTableElement;
var activeOverlayElement;

var iceTopElement;
var iceBottomElement;
var iceboxElement;

var items = new Map();

async function init() {

    extraItemsTableElement = document.getElementById("js-extra-items");


    iceTopElement = document.getElementById("ice-top");
    iceBottomElement = document.getElementById("ice-bottom");
    iceboxElement = document.getElementById("icebox");

    activeOverlayElement = null;

    await populateItemTables();

}

function onMenuItemClick(element) {
    onCloseOverlay();
    let targetElement = document.getElementById(element.dataset.targetOverlayId);
    targetElement.classList.remove("overlay-hidden");
    iceTopElement.classList.remove("ice-hidden");
    iceBottomElement.classList.remove("ice-hidden");
    iceboxElement.classList.remove("ice-hidden");
    activeOverlayElement = targetElement;
}

function onCloseOverlay() {
    if (activeOverlayElement) {
        activeOverlayElement.classList.add("overlay-hidden");
        iceTopElement.classList.add("ice-hidden");
        iceBottomElement.classList.add("ice-hidden");
        iceboxElement.classList.add("ice-hidden");
        activeOverlayElement = null;
    }
}

async function populateItemTables() {
    let data = await fetch(`js/data/session8.json`).then(response => response.json());
    data.items.forEach(item => {
        items.set(item.id, item);
    });
    populateExtraItemsTable();
}

function populateExtraItemsTable() {
    let html = "";
    items.forEach(item => {
        if (!item.isFree && !item.isPayload) {
            html += `
            <tr class="item-row">
                <td>
                    <p>${item.name}</p>
                    <p class="item-row-desc">${item.description}</p>
                </td>
                <td>${item.owned}</td>
                <td>${item.weight}kg</td>        
            </tr>
            `;
        }
    });
    extraItemsTableElement.innerHTML = html;
}