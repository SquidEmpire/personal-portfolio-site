let canvas, ctx;
const CLOCK_RATE = 1000/30; //30 ticks a second
let lastTime = 0;

let mapIntroDuration = 1000;
let maptimer = 0;

let mapImage;

//https://easings.net
function easeOutSine(x) {
    return Math.sin((x * Math.PI) / 2);
}
function easeInOutSine(x) {
    return -(Math.cos(Math.PI * x) - 1) / 2;
}


function onResize() {
    var width  = canvas.clientWidth;
    var height = canvas.clientHeight;
    ctx.canvas.width  = width;
    ctx.canvas.height = height;
}

async function loadAssets() {
    mapImage = new Image(); // Using optional size for image
    mapImage.src = "./images/session9/map.png";
    await mapImage.decode();
}

async function init() {
    canvas = document.getElementById('hero-canvas');
    ctx = canvas.getContext('2d');
    await loadAssets();
    requestAnimationFrame(draw);
    setInterval(tick, CLOCK_RATE);
    onResize();
    window.addEventListener('resize', onResize);
}

function tick() {

}

function drawMap() {
    let mapangle, mapx, mapy = 0;
    let mapscale = 1;
    let introAnimprog = (maptimer / mapIntroDuration);
    if (introAnimprog < 1) {
        const easedProg = easeOutSine(introAnimprog);
        mapscale = 1 + easedProg;
        mapx = 800 - (400 * easedProg);
        mapy = 200 - (250 * easedProg);
        mapangle = -0.1 * easedProg;
    } else {
        //once the map is loaded in add some subtle movement to keep it going
        let easedsubtleprog;
        if (maptimer % 2000 >= 1000) {
            easedsubtleprog = easeInOutSine((maptimer % 1000) / 1000);
        } else {
            easedsubtleprog = 1 - easeInOutSine((maptimer % 1000) / 1000);
        }
        mapangle = -0.1 - (easedsubtleprog * 0.01);
        mapscale = 2 + (easedsubtleprog * 0.01);
        mapx = 400 + ((easedsubtleprog - 0.5) * 0.5);
        mapy = -50;
    }
    maptimer++;

    ctx.save();
    ctx.scale(mapscale, mapscale);
    ctx.translate(mapx, mapy);
    ctx.rotate(mapangle);
    ctx.translate(-750,-500); //image is 1500x1000
    ctx.drawImage(mapImage, 0, 0);
    ctx.restore();
}

function drawSmoke() {

}

function draw(time) {
    const delta = time - lastTime;

    ctx.clearRect(0, 0, canvas.width, canvas.height);

    drawMap();

    requestAnimationFrame(draw);

    lastTime = time;
}

window.addEventListener("load", init);