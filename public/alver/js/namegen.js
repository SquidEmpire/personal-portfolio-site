var outputElement;
var mNames;
var fNames;
var sNames;

var forceAlliterative = false;

async function init() {

    fNames = await loadList('./character-maker/js/data/names/names_female.json');
    mNames = await loadList('./character-maker/js/data/names/names_male.json');
    sNames = await loadList('./character-maker/js/data/names/surnames.json');

    outputElement = document.getElementById("identity-name-field");

    document.getElementById("random-m-name").addEventListener('click', (e) => {setRandomName('m')});
    document.getElementById("random-f-name").addEventListener('click', (e) => {setRandomName('f')});
    document.getElementById("random-a-name").addEventListener('click', (e) => {setRandomName('a')});

    const alB = document.getElementById("alliterative-button");
    alB.addEventListener('click', (e) => {forceAlliterative = !forceAlliterative; alB.classList.toggle("active")});
    
}

async function loadList(location) {
    const response = await fetch(location);
    const jsonData = await response.json();
    let result = [];
    jsonData.forEach(jsonElement => {
        result.push(jsonElement);
    });
    console.log(`loaded ${result.length} items from ${location.split('/').pop()}`);
    return result;
}

function setRandomName(type) {
    const surnameDoubleBarrelled = Math.random() > 0.95;
    const useMononym = Math.random() > 0.9899;
    let newName = "";

    if (type === "a") {
        type = (Math.random() > 0.5 ? 'f' : 'm');
    }

    if (useMononym) {
        let sIndex = Math.round(Math.random() * (sNames.length - 1));
        newName = sNames[sIndex];

    } else {

        if (type === "m") {

            const mIndex = Math.round(Math.random() * (mNames.length - 1));
            newName += mNames[mIndex];
    
        } else if (type === "f") {
    
            const fIndex = Math.round(Math.random() * (fNames.length - 1));
            newName += fNames[fIndex];
    
        }
    
        newName += " ";
    
        let sIndex = Math.round(Math.random() * (sNames.length - 1));
    
        let attempts = 0;
        while (forceAlliterative && sNames[sIndex][0] !== newName[0] && attempts < 100) {
            sIndex = Math.round(Math.random() * (sNames.length - 1));
            attempts++;
        }
    
        if (surnameDoubleBarrelled) {
    
            const sIndex2 = Math.round(Math.random() * (sNames.length - 1));
            newName += sNames[sIndex].split(' ').join('') + "-" + sNames[sIndex2].split(' ').join('');
    
        } else {
    
            newName += sNames[sIndex];
    
        }

    }   

    outputElement.value = newName;
}