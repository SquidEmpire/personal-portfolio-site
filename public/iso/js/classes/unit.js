import {Globals, getMapGridTerrainTypeValue, getMapGridValueAtGridCoordinates} from "../core.js";
import * as HELPER from "../helper.js";
import { SelectableGameObject } from "./selectableGameObject.js";
import * as THREE from "../vendor/three.module.js"
import { MoveOrderPathfindOrder } from "./orders/moveOrderPathfindOrder.js";

export class Unit extends SelectableGameObject {

    ring;
    pivot;
    speed = 1;
    type = "unit";
    name = "unit";
    floats = true; //floats on water

    _pathfinder;
    _pathfindRequests

    _targetYPosition;
    _yChangeSpeed = 5000;
    _timeSpentChangingY = 0;
    _sinkDepthInWater = 1.6;
    _unitClosenessThreshold = 2; //units will try to stay this far from each other

    //state
    isMoving = false;

    orderQueue = [];

    constructor() {
        super();
        this.pivot = new THREE.Group();
        this.pivot.name= "unit_pivot";

        const meshGeometry = new THREE.BoxGeometry( 1, 2, 0.5 );
        const meshMaterial = new THREE.MeshLambertMaterial( {color: (Math.random() * 0xffffff)} );
        this.mesh = new THREE.Mesh( meshGeometry, meshMaterial );
        this.mesh.receiveShadow = true;
        this.mesh.castShadow = true;
        this.mesh.position.y += 1;
        this.mesh.name = "unit_mesh";
        this.pivot.add(this.mesh);

        const ringGeometry = new THREE.RingGeometry( 1.1, 1.2, 16 );
        const ringMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff, side: THREE.DoubleSide, depthTest: false } );
        this.ring = new THREE.Mesh( ringGeometry, ringMaterial );
        this.ring.rotation.x = Math.PI / 2;

        this.pivot.add(this.ring);

        this._pathfinder = new PF.AStarFinder({
            allowDiagonal: true,
            dontCrossCorners: true
        });        
    }

    initialize( x = 0, y = 0, z = 0 ) {
        super.initialize();
        Globals.scene.add(this.pivot);
        this.moveTo(x, z, true);
        this.ring.visible = false;
        this.onMoveFinishedBound = this.onMoveFinished.bind(this);
    }

    destroy() {
        super.destroy();
        this.mesh.geometry.dispose();
        this.mesh.material.dispose();
        Globals.scene.remove( this.pivot );
    }

    moveTo(x, z, instant) {
        this.pivot.position.set(x, 0, z);
        this.setY(instant);
    }

    setY(instant = false) {
        const gridValue = getMapGridValueAtGridCoordinates(this.pivot.position.x, this.pivot.position.z, true);
        this._targetYPosition = gridValue.height;
        if (this._targetYPosition < Globals.water.waterLevel - this._sinkDepthInWater) {
            this._targetYPosition = Globals.water.waterLevel - this._sinkDepthInWater;
        }
        if (instant) {
            this.pivot.position.y = this._targetYPosition;
        }

        if (this.isSelected) {
            const currentTerrainType = gridValue.terrainType;
            const name = currentTerrainType.name;
            //console.log(`I'm walkin' on ${name} on `, this.pivot.position);
        }
    }

    issueMoveCommand(x, z, queued = false) {        
        const pathfindOrder = new MoveOrderPathfindOrder(this, x, z, this.onMoveFinishedBound);
        if (!queued) {
            this.orderQueue = [pathfindOrder];
        } else {
            this.orderQueue.push(pathfindOrder);
        }
        if (this.isSelected) {
            Globals.uiController.needsUpdate = true;
        }
    }

    onClicked() {
        this.select();
    }

    onDeclicked() {
        this.deselect();
    }

    select() {
        super.select();
        this.ring.visible = true;
    }

    deselect() {
        super.deselect();
        this.ring.visible = false;
    }

    rightClickedOther(other, x, y, shiftKey) {
        //while this unit is active, something else has been right clicked
        if (other.name === "terrain") {
            this.issueMoveCommand(x, y, shiftKey);
        } else {
            
        }
    }

    onMoveFinished() {
        //this.issueMoveCommand(0,0);
        this.repelFromOtherUnits();
    }

    repelFromOtherUnits() {
        //TODO: it might be better to use an octree or other spatial mapping on this for performance. Seems ok for now though
        Globals.gameObjects.forEach(obj => {
            if (obj.type === "unit" && obj.uuid !== this.uuid) {
                if (this.pivot.position.distanceToSquared(obj.pivot.position) < this._unitClosenessThreshold) {
                    //we're too close to something, so pick a random direction and move a bit
                    const newX = HELPER.getRandom(this.pivot.position.x + this._unitClosenessThreshold, this.pivot.position.x - this._unitClosenessThreshold);
                    const newZ = HELPER.getRandom(this.pivot.position.z + this._unitClosenessThreshold, this.pivot.position.z - this._unitClosenessThreshold);
                    this.issueMoveCommand(newX, newZ, false);
                    return;
                }
            }
        });
    }

    onGameTick(delta) {
        if (this.orderQueue.length) {
            const order = this.orderQueue[0];
            order.performStep(delta, this.speed);
            if (order.isComplete) {
                this.orderQueue.splice(0, 1);
                if (this.isSelected) {
                    Globals.uiController.needsUpdate = true;
                }
                order.onComplete();
            }
        }
        if (this.pivot.position.y != this._targetYPosition) {
            const lerpFactor = this._timeSpentChangingY / this._yChangeSpeed;
            this.pivot.position.y = THREE.MathUtils.lerp(this.pivot.position.y, this._targetYPosition, lerpFactor);
            this._timeSpentChangingY += delta;
        } else {
            this._timeSpentChangingY = 0;
        }
    }

}