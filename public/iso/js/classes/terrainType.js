export class TerrainType {

    id;
    name;
    minPollutedColour;
    midPollutedColour
    maxPollutedColour;

    constructor(id, name, minPollutedColour, midPollutedColour, maxPollutedColour) {
        this.id = id;
        this.name = name;
        this.minPollutedColour = minPollutedColour;
        this.midPollutedColour = midPollutedColour;
        this.maxPollutedColour = maxPollutedColour;
    }

}