
import {Globals, Settings} from "../core.js";
import * as HELPER from "../helper.js";
import { GameObject } from "./gameObject.js";
import * as THREE from '../vendor/three.module.js';
import * as SPEModule from "../vendor/SPE.min.js"

//class for holding and updating SPE particle groups (NOT EMITTERS!)
//TODO: SPE has emitter pools that can be used to recycle particles. That should be used isntead of creating/destroying emitters
//https://squarefeet.github.io/ShaderParticleEngine/docs/api/core_SPE.Group.js.html#line544
export class ParticleController extends GameObject {

    particleGroups = new Map(); //SPE particle groups
    //so three js uses the x/y/z position of meshes to work out their depth when rendering, but shader particles have their positions
    //set by the vertex shader instead, and their partent SPE.Group mesh is at 0,0,0, meaning the water plane shader always renders
    //above any particles. To counter this, an offset in Y is used for all particles, so that they render above the water
    //TODO: it should be possible to do something in the shaders inside SPE to handle the depth of the water?
    particleYOffset = 20;

    constructor() {
        super();        
    }

    initialize() {
        //build the particle groups

        //smoke//
        const smokeTexture = Globals.assets.find(x => x.name === 'smoke').contents;  
        let particleGroupSmoke = new SPEModule.SPE.Group({
            texture: {
                value: smokeTexture
            },
            hasPerspective: false,
            depthTest: true,
            maxParticleCount: 1600,
            blending: THREE.NormalBlending
        });
        particleGroupSmoke.mesh.frustumCulled = false; //this may cause perf issues but I was getting bad culls otherwise
        this.particleGroups.set("smoke", particleGroupSmoke);
        particleGroupSmoke.mesh.position.y = this.particleYOffset;

        Globals.scene.add( particleGroupSmoke.mesh );

        //smog//
        const smogTexture = Globals.assets.find(x => x.name === 'smoke').contents;  
        let particleGroupSmog = new SPEModule.SPE.Group({
            texture: {
                value: smogTexture
            },
            hasPerspective: false,
            depthTest: false,
            maxParticleCount: 200,
            blending: THREE.NormalBlending
        });
        particleGroupSmog.mesh.frustumCulled = false; //this may cause perf issues but I was getting bad culls otherwise
        this.particleGroups.set("smog", particleGroupSmog);
        particleGroupSmog.mesh.position.y = this.particleYOffset;

        Globals.scene.add( particleGroupSmog.mesh );

        //grit//
        const gritTexture = Globals.assets.find(x => x.name === 'rain').contents;  
        let particleGroupGrit = new SPEModule.SPE.Group({
            texture: {
                value: gritTexture
            },
            hasPerspective: false,
            depthTest: false,
            maxParticleCount: 1000,
            fog: false,
            blending: THREE.NormalBlending
        });
        particleGroupGrit.mesh.frustumCulled = false; //this may cause perf issues but I was getting bad culls otherwise
        this.particleGroups.set("grit", particleGroupGrit);
        particleGroupGrit.mesh.position.y = this.particleYOffset;

        Globals.scene.add( particleGroupGrit.mesh );

        //rain//
        const rainTexture = Globals.assets.find(x => x.name === 'rain').contents; 
        let rainParticleGroup = new SPEModule.SPE.Group({
            texture: {
                value: rainTexture
            },
            hasPerspective: false,
            maxParticleCount: 4000,
            blending: THREE.AdditiveBlending,
            fog: false,
            colorize: true
        });
        rainParticleGroup.mesh.frustumCulled = false; //this may cause perf issues but I was getting bad culls otherwise
        this.particleGroups.set("rain", rainParticleGroup);
        rainParticleGroup.mesh.position.y = this.particleYOffset;
        Globals.scene.add( rainParticleGroup.mesh );
        
        super.initialize();
    }

    onGameTick(delta) {
        this.particleGroups.forEach(particleGroup => {
            particleGroup.tick( delta/1000 );
        });
    }

    //convience method shortcut for this.particleGroups.get
    get(key) {
        return this.particleGroups.get(key);
    }

    //hide all particles
    hide() {
        this.particleGroups.forEach(particleGroup => {
            particleGroup.mesh.visible = false;
        });
    }

    //show all particles
    show() {
        this.particleGroups.forEach(particleGroup => {
            particleGroup.mesh.visible = true;
        });
    }
}