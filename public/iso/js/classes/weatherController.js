
import {Globals, getMapGridValueAtGridCoordinates} from "../core.js";
import * as HELPER from "../helper.js";
import { GameObject } from "./gameObject.js";
import * as THREE from '../vendor/three.module.js';
import * as SPEModule from "../vendor/SPE.min.js"

export class WeatherController extends GameObject {

    rainParticleGroup;
    rainEmitter;
    smogParticleGroup;
    smogEmitter;
    gritParticleGroup;
    gritEmitter;

    sun;
    sunDirection = new THREE.Quaternion;

    windStrength = 0;
    windDirection = 0;
    _targetWindStrength = 0;
    _targetWindDirection = 0;

    _baseWindStrength = 0;
    _slowScrollSpeed = 4; //must be a factor of the map height (for lazyness)
    _slowScrollLastColIndex = 0;
    _windUpdateFrequency = 8000;
    _ticksSinceLastWindUpdate = this._windUpdateFrequency; //initially set so that an update triggers instantly

    //normal weather colours
    fogNormalColour = new THREE.Color(0x444444);
    fogNormalDistance = 400;
    sunNormalColour = new THREE.Color(0xAAAAAA);
    sunNormalIntensity = 1;

    //fog weather colours
    fogRainColour = new THREE.Color(0xD2DBE2);
    fogRainDistance = 100;
    sunRainColour = new THREE.Color(0x9E9FA4);
    sunRainIntensity = 0.8;

    //the current correct weather settings based purely the current weather (without pollution effects)
    targetFogColour;
    targetFogDistance;
    targetSunColour;
    targetSunIntensity;   

    //pollution colour modifiers
    fogPollutionColourModifier = new THREE.Color(0x6B5D4D);
    sunPollutionColourModifier = new THREE.Color(0xCEC5A3);
    sunPollutionIntensityModifier = 0.2;

    //pollution colour modified target weather settings
    targetFogColourPollutionAdjusted = new THREE.Color();    
    targetSunColourPollutionAdjusted = new THREE.Color();
    targetRainColourPollutionAdjusted = new THREE.Color();
    targetSunInensityPollutionAdjusted = 1.0;

    

    currentWeatherIndex;
    weatherEnum = [
        "normal",
        "rain"
    ];
    
    weatherChangeDuration = 100000;
    weatherNeedsUpdate = false;
    _timeSpentChangingWeather = 0;

    constructor(sun) {
        super();

        this.sun = sun;

        const sunRotate = new THREE.Euler( 0, 0.5, 0.6, 'XYZ' );
        this.sunDirection.setFromEuler( sunRotate );

        this.rainParticleGroup = Globals.particles.particleGroups.get('rain');
        this.smogParticleGroup = Globals.particles.particleGroups.get('smog');
        this.gritParticleGroup = Globals.particles.particleGroups.get('grit');
    }

    initialize() {
        super.initialize();

        this.rainEmitter = new SPEModule.SPE.Emitter({
            maxAge: { value: 0.5 },
            position: { 
                value: new THREE.Vector3(0, 20, 0),
                spread: new THREE.Vector3( Globals.terrain.width, 0, Globals.terrain.height ),
            },
            size: {
                value: 2.5,
            },
            velocity: {
                value: new THREE.Vector3( 0, -50, 10 ),
            },
            color: {
                value: new THREE.Color(0xFFFF00),
            },
            particleCount: 800,
        });
        
        //TODO: rain particle looks not great for how much it costs, might be better to switch to an animated image overlay?
        this.rainParticleGroup.addEmitter( this.rainEmitter );


        this.smogEmitter = new SPEModule.SPE.Emitter({
            maxAge: { value: 4, spread: 2 },
            position: { 
                value: new THREE.Vector3(0, -Globals.particles.particleYOffset + 5, 0),
                spread: new THREE.Vector3( Globals.terrain.width, 0, Globals.terrain.height ),
            },
            size: {
                value: 1024,
            },
            velocity: {
                value: new THREE.Vector3( 0, 0, 0 ),
                spread: new THREE.Vector3( 1, 1, 1 ),
            },
            angle: {
                value: [0, 0, 0],
                spread: [ 10, 10, 10 ],
            },
            color: {
                value: new THREE.Color(0x635F5B),
            },
            opacity: {
                value: [ 0, 0.5, 0 ]
            },
            particleCount: 200,
        });
        
        this.smogParticleGroup.addEmitter( this.smogEmitter );


        this.gritEmitter = new SPEModule.SPE.Emitter({
            maxAge: { value: 0.5, spread: 0.1 },
            position: { 
                value: new THREE.Vector3(0, -Globals.particles.particleYOffset + 5, 0),
                spread: new THREE.Vector3( Globals.terrain.width, 0, Globals.terrain.height ),
            },
            size: {
                value: 3,
            },
            velocity: {
                value: new THREE.Vector3( 0, 0, -10 ),
                spread: new THREE.Vector3( 1, 1, 20 ),
            },
            wiggle: {
                value: 2,
                spread: 1,
            },
            color: {
                value: new THREE.Color(0xAAA19B),
            },
            opacity: {
                value: [ 0, 0, 0 ]
            },
            particleCount: 1000,
        });        
        this.gritParticleGroup.addEmitter( this.gritEmitter );


        this.rainEmitter.disable();
        this.setWeather("normal");
    }

    setWeather(newWeather) {
        switch (newWeather) {
            case "rain":
                this.currentWeatherIndex = 1;
                this.rainEmitter.enable();
                this.targetFogColour = this.fogRainColour;
                this.targetFogDistance = this.fogRainDistance;
                this.targetSunColour = this.sunRainColour;
                this.targetSunIntensity = this.sunRainIntensity;
                break;
            case "normal":
            default:
                this.currentWeatherIndex = 0;
                this.rainEmitter.disable();
                this.targetFogColour = this.fogNormalColour;
                this.targetFogDistance = this.fogNormalDistance;
                this.targetSunColour = this.sunNormalColour;
                this.targetSunIntensity = this.sunNormalIntensity;
        }
        this.updateWeatherForPollution();
        this.weatherNeedsUpdate = true;
        this._timeSpentChangingWeather = 0;
    }

    //based purely on air pollution levels
    updateWeatherForPollution() {
        this.targetFogColourPollutionAdjusted.copy(this.targetFogColour).lerp(this.fogPollutionColourModifier, Globals.pollution.airPollutionLevel);
        //use the same fog colour for the rain, but a bit brighter
        this.targetRainColourPollutionAdjusted.copy(this.targetFogColourPollutionAdjusted).multiplyScalar(1.5);
        this.setRainColour(this.targetRainColourPollutionAdjusted);
        //sun pollution colour adjustment
        this.targetSunColourPollutionAdjusted.copy(this.targetSunColour).lerp(this.sunPollutionColourModifier, Globals.pollution.airPollutionLevel);
        const sunPollutionIntensity = THREE.MathUtils.lerp(1, this.sunPollutionIntensityModifier, Globals.pollution.airPollutionLevel);
        this.targetSunInensityPollutionAdjusted = Math.min(this.targetSunIntensity, sunPollutionIntensity);

        //smog & grit
        this.setSmogOpacity((Globals.pollution.airPollutionLevel / 10));
        this.setGritOpacity((Globals.pollution.airPollutionLevel / 2));
    }

    setRainColour(colour) {
        //the four values are because the SPE particle instance has 4 colour values for start, middle, middle, and end colours (we use the same for all)
        this.rainEmitter.color.value[0].set(colour);
        this.rainEmitter.color.value[1].set(colour);
        this.rainEmitter.color.value[2].set(colour);
        this.rainEmitter.color.value[3].set(colour);
        this.rainEmitter.color.value = this.rainEmitter.color.value;
    }

    setSmogOpacity(opacity) {
        this.smogEmitter.opacity.value[1] = opacity;
        this.smogEmitter.opacity.value[2] = opacity;
        this.smogEmitter.opacity.value = this.smogEmitter.opacity.value;
    }

    setGritOpacity(opacity) {
        this.gritEmitter.opacity.value[0] = opacity;
        this.gritEmitter.opacity.value[1] = opacity;
        this.gritEmitter.opacity.value[2] = opacity;
        this.gritEmitter.opacity.value[4] = opacity / 2;
        this.gritEmitter.opacity.value = this.gritEmitter.opacity.value;
    }

    onGameTick(delta) {
        //apply weather effects until we reach the expected state
        if (this.weatherNeedsUpdate) {
            let weatherDoneChanging = true;
            let lerpFactor = this._timeSpentChangingWeather / this.weatherChangeDuration;
            //TODO: tween the weather... this would be much easier if we used the tween library but I am hesitant to bring it in

            if (!Globals.scene.fog.color.equals(this.targetFogColourPollutionAdjusted)) {
                Globals.scene.fog.color.lerp(this.targetFogColourPollutionAdjusted, lerpFactor);
                Globals.scene.background.lerp(this.targetFogColourPollutionAdjusted, lerpFactor);
                weatherDoneChanging = false;
            }
            if (Globals.scene.fog.far != this.targetFogDistance) {
                Globals.scene.fog.far = THREE.MathUtils.lerp(Globals.scene.fog.far, this.targetFogDistance, lerpFactor);
                weatherDoneChanging = false;
            }
            if (!this.sun.color.equals(this.targetSunColourPollutionAdjusted)) {
                this.sun.color.lerp(this.targetSunColourPollutionAdjusted, lerpFactor);
                weatherDoneChanging = false;
            }
            if (this.sun.intensity != this.targetSunInensityPollutionAdjusted) {
                this.sun.intensity = THREE.MathUtils.lerp(this.sun.intensity, this.targetSunInensityPollutionAdjusted, lerpFactor);
                weatherDoneChanging = false;
            }

            if (weatherDoneChanging) {
                this.weatherNeedsUpdate = false;
            } else {
                this._timeSpentChangingWeather += delta;
            }   
        }

        //scroll over the terrain slowly and apply wind effects (?)
        for (let i = this._slowScrollLastColIndex; i < (this._slowScrollLastColIndex + this._slowScrollSpeed); i++) {
            for (let j = 0; j < Globals.terrain.height; j++) {
                if (Math.random() > 0.8) {
                    let gridValue = getMapGridValueAtGridCoordinates(j, i);
                    gridValue.needsUpdate = true;
                }
            }
        }
        this._slowScrollLastColIndex += this._slowScrollSpeed;
        if (this._slowScrollLastColIndex >= Globals.terrain.width) {
            this._slowScrollLastColIndex = 0;
        }

        if (this._ticksSinceLastWindUpdate == this._windUpdateFrequency) {
            this._targetWindStrength = THREE.MathUtils.clamp(this._baseWindStrength + HELPER.getRandom(0.008, 0.03), 0.0, 0.1);
            this._targetWindDirection += HELPER.getRandom(-1, 1);
            this._ticksSinceLastWindUpdate = 0;
        } else {
            this._ticksSinceLastWindUpdate++;
        }

        if (this.windDirection != this._targetWindDirection) {
            this.windDirection = THREE.MathUtils.lerp(this.windDirection, this._targetWindDirection, 0.001);
        }
        if (this.windStrength != this._targetWindStrength) {
            this.windStrength = THREE.MathUtils.lerp(this.windStrength, this._targetWindStrength, 0.001);
        }

    }
    
}