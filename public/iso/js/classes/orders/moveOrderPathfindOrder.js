import {Globals, worldCoordinatesToGridCoordinates, gridCoordinatesToWorldCoordinates, getNearestFreeMapGridPoint} from "../../core.js";
import { Order } from "./order.js";
import * as THREE from "../../vendor/three.module.js"
import { MoveOrder } from "./moveOrder.js";

export class MoveOrderPathfindOrder extends Order {

    target = new THREE.Vector3();
    moveOrders = [];
    hasBeenExpanded = false;
    displayText = "Moving";

    constructor(actor, x, z, onComplete) {
        super(actor, onComplete);
        this.target.set(x, 0, z);
    }

    performStep(delta, speed) {
        if (!this.hasBeenExpanded) {
            //expand this order now into a sub array of normal orders for each step of the pathfind
            const tempGrid = Globals.pathfindingGrid.clone();
            const cA = worldCoordinatesToGridCoordinates(this.actor.pivot.position.x, this.actor.pivot.position.z);
            let cB = worldCoordinatesToGridCoordinates(this.target.x, this.target.z);
            if (!tempGrid.isWalkableAt(cB.x, cB.y)) {
                cB = getNearestFreeMapGridPoint(cB.x, cB.y);
                if (!cB) {
                    //something went wrong?
                    console.warn("Couldn't find a free point around target to pathfind to", this);
                    this.isComplete = true;
                    return;
                }
            }     
            const path = this.actor._pathfinder.findPath(cA.x, cA.y, cB.x, cB.y, tempGrid);
            //the path built by the pathfinder is on the grid coordinates
            //position 1 is the current position and is ignored
            for (let i = 0; i < path.length - 1; i++) {
                const pathWorldCoordinates = gridCoordinatesToWorldCoordinates(path[i][0], path[i][1]);
                const moveOrder = new MoveOrder(this.actor, pathWorldCoordinates.x, pathWorldCoordinates.y);
                this.moveOrders.push(moveOrder);
            }
            //the final move order's precision has been lost so must be added back manually
            //TODO: this might cause the unit to move into places it shouldn't?
            if (this.moveOrders.length) {
                this.moveOrders[this.moveOrders.length-1].target.x = this.target.x;
                this.moveOrders[this.moveOrders.length-1].target.z = this.target.z;
                this.hasBeenExpanded = true;
            } else {
                //something went wrong?
                console.warn("pathfind order expanded but found no path", this);
                this.isComplete = true;
            }
        } else {
            //now that this has been expanded, run through all the sub move orders like normal
            if (this.moveOrders.length) {
                const order = this.moveOrders[0];
                order.performStep(delta, speed);
                if (order.isComplete) {
                    this.moveOrders.splice(0, 1);
                }
            } else {
                //all suborders done, we're all finished!
                this.isComplete = true;
                this.onComplete();
            }
        }
    }

}