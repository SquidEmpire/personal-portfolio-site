import {Globals, worldCoordinatesToGridCoordinates, gridCoordinatesToWorldCoordinates} from "../../core.js";
import { Order } from "./order.js";
import * as THREE from "../../vendor/three.module.js"
import { MoveOrderPathfindOrder } from "./moveOrderPathfindOrder.js";

export class ChopTreeOrder extends Order {

    targetTree;
    targetDepot;
    delayBetweenChops = 1000;
    chopCooldown = 0;
    pathfindOrder;
    displayText = "Chopping trees";

    constructor(actor, tree) {
        super(actor);
        this.targetTree = tree;
    }


    //TODO: finding nearby things has to be done seriously. Library? Quadtree? https://github.com/oguzeroglu/Nearby?
    tempFindNearestMapObjectWithName(name) {
        let nearestObject = null;
        Globals.gameObjects.forEach(obj => {
            if (obj.name === name && !obj.isPlaceState) {
                if (nearestObject) {
                    if (this.actor.pivot.position.distanceToSquared(obj.pivot.position) < 
                        this.actor.pivot.position.distanceToSquared(nearestObject.pivot.position)) {
                            nearestObject = obj;
                    }
                } else {
                    nearestObject = obj;
                }
            }
        });
        return nearestObject;
    }


    performStep(delta, speed) {
        //this order only ends when there are no more trees left to chop that can be found

        //if our tree has been used up, find a new tree
        if (this.targetTree.isDestroyed) {
            this.targetTree = this.tempFindNearestMapObjectWithName("tree");
            if (!this.targetTree) {
                //there are no trees, give up
                this.isComplete = true;
                return;
            }
        }
        
        //do any movement we need to do
        if (this.pathfindOrder && !this.pathfindOrder.isComplete) {
            this.pathfindOrder.performStep(delta, speed);
            return;
        }

        //if we haven't hit our carry capacity yet, work on our tree
        if (this.actor.carryAmount < this.actor.carryCapacity) {

            //go to our tree
            if (this.actor.pivot.position.distanceToSquared(this.targetTree.pivot.position) > 5) {
                if (!this.pathfindOrder || this.pathfindOrder.isComplete === true) {
                    this.pathfindOrder = new MoveOrderPathfindOrder(this.actor, this.targetTree.pivot.position.x, this.targetTree.pivot.position.z);
                    return;
                }            
            }           

            //now that we're at our tree, perform extraction until we can't carry any more        
            if (this.chopCooldown <= 0) {
                this.actor.onChopTree(this.targetTree);
                this.chopCooldown = this.delayBetweenChops;
                return;
            } else {
                this.chopCooldown -= delta;
                return;
            }

        } else {
            //when full, find the nearest stockpile to deliver to
            
            if (!this.targetDepot || this.targetDepot.isDestroyed) {                
                this.targetDepot = this.tempFindNearestMapObjectWithName("materialDepot");
                if (!this.targetDepot) {
                    //there is no depot, give up
                    this.isComplete = true;
                    return;
                }
            }

            if (this.actor.pivot.position.distanceTo(this.targetDepot.pivot.position) > 1) {
                if (!this.pathfindOrder || this.pathfindOrder.isComplete === true) {
                    this.pathfindOrder = new MoveOrderPathfindOrder(this.actor, this.targetDepot.pivot.position.x, this.targetDepot.pivot.position.z);
                }
            } else {
                //we're at our depot
                //TODO: move this to the depot side?
                console.log("bazinga");
                Globals.resources.material += this.actor.carryAmount;
                this.actor.carryAmount = 0;
                this.actor.carryType = null;
            }          
            
        }     

    }

}