export class Order {

    isComplete = false;
    actor = null;
    displayText = "Completing task";
    onComplete; //callback to run when the task is done

    constructor(actor, onComplete = () => {}) {
        this.actor = actor;
        this.onComplete = onComplete;
    }

    performStep() {
        
    }
}