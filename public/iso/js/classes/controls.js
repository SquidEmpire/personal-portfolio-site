import * as THREE from "../vendor/three.module.js";
import {Globals} from "../core.js";
import { SelectionBox } from "../vendor/three.selectionBox.module.js";
import { SelectionHelper } from "../vendor/three.selectionHelper.module.js";
import { ControlMappings } from "../controlMappings.js";
import { GameObject } from "./gameObject.js";

export class Controls extends GameObject {

    domElement = null;
    isLocked = false;
    blockerElement = null;

    cursorElement = null;
    pointerScreenLocation = new THREE.Vector2(0, 0);
    noramlisedPointerLocation = new THREE.Vector2(0, 0);
    pointerMin = new THREE.Vector2(0, 0);
    pointerMax = new THREE.Vector2(document.documentElement.clientWidth - 5, document.documentElement.clientHeight - 5);
    raycaster = new THREE.Raycaster();

    mouseIntersects = [];
    lastMouseTerrainIntersectionPoint = new THREE.Vector2();
    pickedObject = null;
    lastClickedObject = null;
    mousePauseTime = 1000; //milliseconds of no mouse movement before we raycast for a "hover"/pick
    timeSinceLastMouseMove = 0;
    hasRaycastAtMousePosition = false;

    cameraInputHorizontal = 0;
    cameraInputVertical = 0;
    newCameraPosition = new THREE.Vector3(0, 0, 0);
    cameraVirtualPosition = new THREE.Vector3(0, 0, 0);
    cameraNextVirtualPosition = new THREE.Vector3(0, 0, 0);

    selectionBox = null;
	selectionHelper = null;

    controlMappings;

    _cameFromSelection = false; //hack

    constructor(domElement) {
        super();
        this.domElement = domElement;
    }

    initialize() {
        super.initialize();
        this.controlMappings = ControlMappings;

        this.blockerElement = document.getElementById("blocker");
        this.blockerElement.addEventListener( 'click', (e) => {
            e.preventDefault();
            e.stopPropagation();
            this.pointerScreenLocation.set(e.clientX, e.clientY);
            this.lock();
        }, false );
        this.raycaster.linePrecision = 0.01;
        this.cursorElement = document.getElementById("cursor");

        this.selectionBox = new SelectionBox( Globals.camera, Globals.scene );
		this.selectionHelper = new SelectionHelper( this.selectionBox, this, 'selectBox' );

        this.newCameraPosition.copy(Globals.camera.position);
    }

    onMouseClick(e) {
        if (this._cameFromSelection) {
            this._cameFromSelection = false;
            return;
        }    
        if (e.button === 0) {
            //left mouse
            //first check if we've clicked a UI element, and if so, click that instead of the game element
            const clickedElement = document.elementFromPoint(this.pointerScreenLocation.x, this.pointerScreenLocation.y);
            if (clickedElement.id !== "game-canvas") {
                //we're on the ui
                clickedElement.click();
                return;
            }

            this.raycastAtMouse();

            if (this.pickedObject) {
                if (this.lastClickedObject) {
                    this.lastClickedObject.onDeclicked();
                }
                this.pickedObject.onClicked();
                this.lastClickedObject = this.pickedObject;
            }
            if (Globals.activeObjects.size > 0) {
                Globals.activeObjects.forEach(activeObject => {            
                    if (activeObject.type === "mapObject" && activeObject.isPlaceState) {
                        activeObject.onClicked();
                    } else if (this.lastClickedObject.name === "terrain") {
                        activeObject.onDeclicked();
                    }
                });
            }
        } else if (e.button === 2) {
            //right mouse
            if (Globals.activeObjects.size > 0) {
                this.raycastAtMouse();
                if (this.mouseIntersects.length) {
                    const object = Globals.gameObjects.get(this.mouseIntersects[0].object.userData.uuid);
                    Globals.activeObjects.forEach(activeObject => {
                        activeObject.rightClickedOther(object, this.mouseIntersects[0].point.x, this.mouseIntersects[0].point.z, e.shiftKey);
                    });
                }
            }
        }
    }

    onMouseDown(e) {
        if (e.button === 0) {

            //check if we've clicked a UI element, and if so, disregard this
            const clickedElement = document.elementFromPoint(this.pointerScreenLocation.x, this.pointerScreenLocation.y);
            if (clickedElement.id !== "game-canvas") {
                //we're on the ui
                return;
            }

            for ( const item of this.selectionBox.collection ) {
                const object = Globals.gameObjects.get(item.userData.uuid);
                if (object && object.isSelectable) {
                    object.deselect();
                }
            }
            this.selectionBox.startPoint.set( this.noramlisedPointerLocation.x, this.noramlisedPointerLocation.y, 0.5 );
            this.selectionBox.endPoint.set( this.noramlisedPointerLocation.x, this.noramlisedPointerLocation.y, 0.5 );
        }
    }

    onMouseUp(e) {
        if (e.button === 0) {
            
            //check if we've clicked a UI element, and if so, disregard this
            const clickedElement = document.elementFromPoint(this.pointerScreenLocation.x, this.pointerScreenLocation.y);
            if (clickedElement.id !== "game-canvas") {
                //we're on the ui
                return;
            }

            this.selectionBox.endPoint.set( this.noramlisedPointerLocation.x, this.noramlisedPointerLocation.y, 0.5 );
            if (!this.selectionBox.startPoint.equals(this.selectionBox.endPoint)) {
                const allSelected = this.selectionBox.select();
                for ( let i = 0; i < allSelected.length; i ++ ) {
                    const object = Globals.gameObjects.get(allSelected[i].userData.uuid);
                    if (object && object.isSelectable) {
                        object.select();
                    }
                }
                this._cameFromSelection = true;
            }    
        }
    }

    onMouseMove(event) {
        if (this.isLocked) {
            const movementX = event.movementX || event.mozMovementX || event.webkitMovementX || 0;
            const movementY = event.movementY || event.mozMovementY || event.webkitMovementY || 0;
            this.pointerScreenLocation.x += movementX;
            this.pointerScreenLocation.y += movementY;
            this.pointerScreenLocation.clamp( this.pointerMin, this.pointerMax );

            //move the pointer
            this.cursorElement.style.left = `${this.pointerScreenLocation.x}px`;
            this.cursorElement.style.top = `${this.pointerScreenLocation.y}px`;

            this.noramlisedPointerLocation.x = ( this.pointerScreenLocation.x / this.pointerMax.x ) * 2 - 1;
	        this.noramlisedPointerLocation.y = - ( this.pointerScreenLocation.y / this.pointerMax.y ) * 2 + 1;

            if ( this.selectionHelper.isSelecting ) {
                for ( let i = 0; i < this.selectionBox.collection.length; i ++ ) {
                    const object = Globals.gameObjects.get(this.selectionBox.collection[i].userData.uuid);
                    if (object && object.isSelectable) {
                        object.deselect();
                    }
                }

                this.selectionBox.endPoint.set( this.noramlisedPointerLocation.x, this.noramlisedPointerLocation.y, 0.5 );

                const allSelected = this.selectionBox.select();
                for ( let i = 0; i < allSelected.length; i ++ ) {
                    const object = Globals.gameObjects.get(allSelected[i].userData.uuid);
                    if (object && object.isSelectable) {
                        object.select();
                    }
                }

            }

            //reset any hover/pick objects
            this.timeSinceLastMouseMove = 0;
            this.hasRaycastAtMousePosition = false;
            if (this.pickedObject) {
                this.pickedObject.onDepicked();
            }
        }        
    }

    onPointerlockChange() {
        if ( document.pointerLockElement === this.domElement ) {
            Globals.paused = false;
            this.isLocked = true;
            this.connect();
            this.blockerElement.style.display = "none";
            this.cursorElement.style.display = "block";
        } else {
            Globals.paused = true;
            this.isLocked = false;
            this.disconnect();
            this.blockerElement.style.display = "flex";
        }
    }

    onPointerlockError() {
		console.error( ' Unable to use Pointer Lock API' );
	}

    onKeyDown(e) {
        let matchedFunction = this.controlMappings.get(e.keyCode);
        if (matchedFunction && typeof matchedFunction === "function") {
            matchedFunction(this);
        }
    }

    connect() {
        this.disconnect();
        this._boundOnMousemove = this.onMouseMove.bind(this);
        this._boundOnMouseClicked = this.onMouseClick.bind(this);
        this._boundOnMouseUp = this.onMouseUp.bind(this);
        this._boundOnMouseDown = this.onMouseDown.bind(this);

        this._boundKeyPress = this.onKeyDown.bind(this);

        document.addEventListener( 'mousemove', this._boundOnMousemove, true );
        document.addEventListener( 'click', this._boundOnMouseClicked, true );
        document.addEventListener( 'mousedown', this._boundOnMouseDown, true );
        document.addEventListener( 'mouseup', this._boundOnMouseUp, true );
        document.addEventListener( 'keydown', this._boundKeyPress, true );
    };

    disconnect() {
        document.removeEventListener( 'mousemove', this._boundOnMousemove, true );        
        document.removeEventListener( 'click', this._boundOnMouseClicked, true );
        document.removeEventListener( 'mousedown', this._boundOnMouseDown, true );
        document.removeEventListener( 'mouseup', this._boundOnMouseUp, true );
        document.removeEventListener( 'keydown', this._boundKeyPress, true );
    };
    
    lock() {
        this._boundOnPointerlockChange = this.onPointerlockChange.bind(this);
        this._boundOnPointerlockError = this.onPointerlockError.bind(this);
        document.addEventListener( 'pointerlockchange', this._boundOnPointerlockChange, true );
        document.addEventListener( 'pointerlockerror', this._boundOnPointerlockError, true );
        this.domElement.requestPointerLock();
    };
    
    unlock() {
        document.removeEventListener( 'pointerlockchange', this._boundOnPointerlockChange, true );
        document.removeEventListener( 'pointerlockerror', this._boundOnPointerlockError, true );
        this.document.exitPointerLock();
    };

    //this is a super expensive operation and should be done as little as possible
    raycastAtMouse() {
        this.raycaster.setFromCamera( this.noramlisedPointerLocation, Globals.camera );
        this.mouseIntersects = this.raycaster.intersectObjects( Globals.clickableMeshes );
        if (this.mouseIntersects.length) {
            for (let i = 0; i < this.mouseIntersects.length; i++) {
                const object = Globals.gameObjects.get(this.mouseIntersects[i].object.userData.uuid);
                if (!object) {
                    console.warn("raycast something bad", this.mouseIntersects);
                    return;
                }
                if (i == 0) {
                    //intersects 0 is the closest to the camera
                    if (object) {
                        object.onPicked();
                        this.pickedObject = object;
                    }
                }
                if (object.name === "terrain") {
                    this.lastMouseTerrainIntersectionPoint.set(this.mouseIntersects[i].point.x, this.mouseIntersects[i].point.z);
                }
            }

        } else {
            this.pickedObject = null;
        }
        this.hasRaycastAtMousePosition = true;
    }

    //TODO: this used to be in render() but that is a huge performance bottleneck
    //TODO: if we want to increase or decrease game speed later on this will need to be in a different loop
    onGameTick(delta) {
        //camera movement from mouse location
        if ( this.pointerScreenLocation.x === this.pointerMax.x ) {
            this.moveCameraEast();                
        } else if ( this.pointerScreenLocation.x === this.pointerMin.x ) {
            this.moveCameraWest();
        }

        if ( this.pointerScreenLocation.y === this.pointerMax.y ) {
            this.moveCameraSouth();
        } else if ( this.pointerScreenLocation.y === this.pointerMin.y ) {
            this.moveCameraNorth();
        }

        //apply camera motion            
        this.getNextCameraPosition();
        Globals.camera.position.copy(this.newCameraPosition);

        if (this.timeSinceLastMouseMove >= this.mousePauseTime) {
            //raycasting the mouse to update pointer position
            if (!this.hasRaycastAtMousePosition) {
                this.raycastAtMouse();
            } 
        } else {
            this.timeSinceLastMouseMove++;
        }
    }

    //public

    updatePointerMax = function() {
        this.pointerMax.set(document.documentElement.clientWidth - 5, document.documentElement.clientHeight - 5);
    }

    getPointerLocationOnTerraion = function() {
        this.raycaster.setFromCamera( this.noramlisedPointerLocation, Globals.camera );
        this.mouseIntersects = this.raycaster.intersectObject( Globals.terrain.mesh );
        if (this.mouseIntersects.length) {
            this.lastMouseTerrainIntersectionPoint.set(this.mouseIntersects[0].point.x, this.mouseIntersects[0].point.z);
        }
        return this.lastMouseTerrainIntersectionPoint;
    }

    getNextCameraPosition() {
        //note that this might have to be world position and not local position if later the camera is made a child of something
        this.newCameraPosition.copy(Globals.camera.position);

        if (this.cameraInputHorizontal != 0 || this.cameraInputVertical != 0) {
            const direction = new THREE.Vector3(this.cameraInputHorizontal, 0, this.cameraInputVertical);
            direction.applyAxisAngle( new THREE.Vector3(0, 1, 0), Math.PI / 4 ).normalize();
            const speed = 1;
            this.newCameraPosition.addScaledVector(direction, speed);
            this.cameraNextVirtualPosition.addScaledVector(direction, speed);
            this.cameraInputHorizontal = 0;
            this.cameraInputVertical = 0;

            if (!this.isCameraPositionValid(this.cameraNextVirtualPosition)) {
                //if the new position is not valid, undo it
                this.newCameraPosition.copy(Globals.camera.position);
                this.cameraNextVirtualPosition.copy(this.cameraVirtualPosition);
            } else {
                this.cameraVirtualPosition.copy(this.cameraNextVirtualPosition);
            }
        }
    }

    isCameraPositionValid = function(virtualPosition) {
        const halfWworldWidth = Globals.terrain.width / 2;
        const halfWorldHeight = Globals.terrain.height / 2;
        if (virtualPosition.x > halfWworldWidth || virtualPosition.x < -halfWworldWidth) {
            return false;
        }
        if (virtualPosition.z > halfWorldHeight || virtualPosition.z < -halfWorldHeight) {
            return false;
        }
        return true;
    }

    moveCameraNorth = function() {
        this.cameraInputVertical = -1;
    }

    moveCameraSouth = function() {
        this.cameraInputVertical = 1;
    }

    moveCameraEast = function() {
        this.cameraInputHorizontal = 1;
    }

    moveCameraWest = function() {
        this.cameraInputHorizontal = -1;
    }

}