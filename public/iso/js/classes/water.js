import {Globals} from "../core.js";
import { GameObject } from "./gameObject.js";
import * as THREE from '../vendor/three.module.js';

//water singleton class
//https://codesandbox.io/s/black-mountain-gojcn?fontsize=14&hidenavigation=1&theme=dark&file=/src/index.js:0-5925
//https://discourse.threejs.org/t/unlit-water-shader-with-foam/11641
export class Water extends GameObject {

    supportsDepthTextureExtension;
    clock;

    type = "water";
    name = "water";
    mesh;
    width;
    height;

    minWaterLevel;
    maxWaterLevel;
    targetWaterLevel;
    waterLevel;
    waterLevelChangeSpeed = 0.1;

    renderTarget;
    depthMaterial;

    pivot;
    waterMesh;
    waterMaterial;
    side1;
    side2;

    depthMaterialNeedsUpdate = true;

    maxPollutedWaterColour = new THREE.Color(0x512E03);
    midPollutedWaterColour = new THREE.Color(0x281E15);
    minPollutedWaterColour = new THREE.Color(0x678FD3);

    maxPollutedFoamColour = new THREE.Color(0x7C5E3B);
    midPollutedFoamColour = new THREE.Color(0xD3C4B6);
    minPollutedFoamColour = new THREE.Color(0xFFFFFF);//(0xC9D8F2)

    constructor(width, height, waterLevel = 0, maxWaterLevel = 2) {
        super();
        this.width = width;
        this.height = height;        
        this.minWaterLevel = -0.01; //needs to be slightly below 0 to avoid z-fighting
        this.targetWaterLevel = waterLevel + this.minWaterLevel;
        this.maxWaterLevel = maxWaterLevel + this.minWaterLevel;

        this.supportsDepthTextureExtension = true;//!!renderer.extensions.get("WEBGL_depth_texture");

        this.renderTarget = new THREE.WebGLRenderTarget(
            window.innerWidth * window.devicePixelRatio,
            window.innerHeight * window.devicePixelRatio
        );
        this.renderTarget.texture.minFilter = THREE.NearestFilter;
        this.renderTarget.texture.magFilter = THREE.NearestFilter;
        this.renderTarget.texture.generateMipmaps = false;
        this.renderTarget.stencilBuffer = false;
    
        if (this.supportsDepthTextureExtension === true) {
            this.renderTarget.depthTexture = new THREE.DepthTexture();
            this.renderTarget.depthTexture.type = THREE.UnsignedShortType;
            this.renderTarget.depthTexture.minFilter = THREE.NearestFilter;
            this.renderTarget.depthTexture.maxFilter = THREE.NearestFilter;
        }
    
        this.depthMaterial = new THREE.MeshDepthMaterial();
        this.depthMaterial.depthPacking = THREE.RGBADepthPacking;
        this.depthMaterial.blending = THREE.NoBlending;

    }

    initialize() {
        super.initialize();
        this.clock = new THREE.Clock();
        this.waterLevel = this.targetWaterLevel;
        this.pivot = new THREE.Group();

        const dudvMap = Globals.assets.find(x => x.name === 'water').contents;
        const texture = Globals.assets.find(x => x.name === 'water2').contents;
        dudvMap.wrapS = dudvMap.wrapT = THREE.RepeatWrapping;
        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;

        const uniforms = {
            time: {
              value: 0
            },
            threshold: {
              value: 0.3
            },
            tDudv: {
              value: null
            },
            tDepth: {
              value: null
            },
            cameraNear: {
              value: Globals.camera.near
            },
            cameraFar: {
              value: Globals.camera.far
            },
            resolution: {
              value: new THREE.Vector2()
            },
            foamColor: {
              value: this.minPollutedFoamColour
            },
            waterColor: {
              value: this.minPollutedWaterColour
            },
            opacity: {
                value: 0.8
            },
            map: {
                value: null
            },
            speed: {
                value: 0.01
            }
        };

        const waterGeometry = new THREE.PlaneGeometry( this.height, this.width );
        const sideMaterial = new THREE.MeshLambertMaterial({
            color: 0x678FD3,
            opacity: 0.8,
            transparent: true,
            polygonOffset: true,
            polygonOffsetFactor: 1
        });
        const sideGeometry = new THREE.PlaneGeometry( this.maxWaterLevel, this.width );

        this.waterMaterial = new THREE.ShaderMaterial({
            defines: {
                DEPTH_PACKING: this.supportsDepthTextureExtension === true ? 0 : 1,
                ORTHOGRAPHIC_CAMERA: 1
            },
            uniforms: THREE.UniformsUtils.merge([THREE.UniformsLib["fog"], uniforms]),
            vertexShader: document.getElementById("vertexShader").textContent,
            fragmentShader: document.getElementById("fragmentShader").textContent,
            transparent: true,
            fog: true
        });
        
        this.waterMaterial.uniforms.cameraNear.value = Globals.camera.near;
        this.waterMaterial.uniforms.cameraFar.value = Globals.camera.far;
        this.waterMaterial.uniforms.resolution.value.set(
            window.innerWidth * window.devicePixelRatio,
            window.innerHeight * window.devicePixelRatio
        );

        this.waterMaterial.uniforms.tDudv.value = dudvMap;
        this.waterMaterial.uniforms.map.value = texture;
        this.waterMaterial.uniforms.tDepth.value =
        this.supportsDepthTextureExtension === true
            ? this.renderTarget.depthTexture
            : this.renderTarget.texture;
    
        this.waterMesh = new THREE.Mesh(waterGeometry, this.waterMaterial);
        this.waterMesh.rotation.x = -Math.PI * 0.5;

        this.side1 = new THREE.Mesh(sideGeometry, sideMaterial);
        this.side2 = new THREE.Mesh(sideGeometry, sideMaterial);

        this.side1.rotation.z = Math.PI * 0.5;
        this.side2.rotation.y = Math.PI * 0.5;
        this.side2.rotation.z = Math.PI * 0.5;

        this.side1.position.z = this.width/2;
        this.side2.position.x = this.height/2;
        
        this.side1.position.y = -this.maxWaterLevel/2;
        this.side2.position.y = -this.maxWaterLevel/2;

        this.pivot.add(this.waterMesh);
        this.pivot.add(this.side1);
        this.pivot.add(this.side2);

        this.pivot.position.y = this.waterLevel;

        Globals.scene.add(this.pivot);

        //we need to change the shader resolution vaule when the window resizes
        window.addEventListener( 'resize', this.onWindowResize.bind(this), false );

    }

    raiseWaterLevel() {
        if (this.targetWaterLevel < this.maxWaterLevel) {
            let newWaterLevel = this.targetWaterLevel + 1;
            newWaterLevel = THREE.MathUtils.clamp(newWaterLevel, this.minWaterLevel, this.maxWaterLevel);
            this.targetWaterLevel = parseFloat(newWaterLevel.toFixed(2));
        }
    }

    lowerWaterLevel() {
        if (this.targetWaterLevel > this.minWaterLevel) {
            let newWaterLevel = this.targetWaterLevel - 1;
            newWaterLevel = THREE.MathUtils.clamp(newWaterLevel, this.minWaterLevel, this.maxWaterLevel);
            this.targetWaterLevel = parseFloat(newWaterLevel.toFixed(2));
        }
    }

    updateWaterColour() {
        let waterColour = new THREE.Color();
        let foamColour = new THREE.Color();
        if (Globals.pollution.waterPollutionLevel > 0.5) {

            const newPercent = ((Globals.pollution.waterPollutionLevel - 0.5) / 0.5);            
            waterColour.lerpColors(this.midPollutedWaterColour, this.maxPollutedWaterColour, newPercent);
            this.waterMaterial.uniforms.waterColor.value = waterColour;

            foamColour.lerpColors(this.midPollutedFoamColour, this.maxPollutedFoamColour, newPercent);
            this.waterMaterial.uniforms.foamColor.value = foamColour;

            //side 1 and 2 use the same material so we only need to change one of them
            this.side1.material.color = waterColour;

        } else {

            const newPercent = (Globals.pollution.waterPollutionLevel / 0.5);
            waterColour.lerpColors(this.minPollutedWaterColour, this.midPollutedWaterColour, newPercent);
            this.waterMaterial.uniforms.waterColor.value = waterColour;

            foamColour.lerpColors(this.minPollutedFoamColour, this.midPollutedFoamColour, newPercent);
            this.waterMaterial.uniforms.foamColor.value = foamColour;

            this.side1.material.color = waterColour;
        }
    }

    onRender = function(renderer) {
        // depth pass
        // this is a MAJOR performance bottleneck and should only be done when necessary 
        if (this.depthMaterialNeedsUpdate) {
            //turn off anything we don't want to be able to affect the depth pass to save time
            this.waterMesh.visible = false; // we don't want the depth of the water
            if (Globals.terrain.enableGrassInstancedMesh) {
                Globals.terrain.grassInstancedMesh.visible = false; //we don't want the grass planes
            }
            Globals.particles.hide();
            Globals.scene.overrideMaterial = this.depthMaterial;
    
            //we're doing 2 renders per frame now
            renderer.shadowMap.enabled = false;
            renderer.setRenderTarget(this.renderTarget);
            renderer.render(Globals.scene, Globals.camera);
            renderer.setRenderTarget(null);
    
            Globals.scene.overrideMaterial = null;
    
            //turn back on the things we made invisible before
            renderer.shadowMap.enabled = true;
            this.waterMesh.visible = true;
            if (Globals.terrain.enableGrassInstancedMesh) {
                Globals.terrain.grassInstancedMesh.visible = true;
            }
            Globals.particles.show();

            this.depthMaterialNeedsUpdate = false;
        }

        if (!Globals.paused) {
            const time = this.clock.getElapsedTime();
            this.waterMesh.material.uniforms.time.value = time;
        }
    }

    onGameTick(delta) {
        if (this.waterLevel !== this.targetWaterLevel) {
            if (this.waterLevel > this.targetWaterLevel) {
                this.waterLevel -= this.waterLevelChangeSpeed;
            } else {
                this.waterLevel += this.waterLevelChangeSpeed;
            }
            this.waterLevel = parseFloat(THREE.MathUtils.clamp(this.waterLevel, this.minWaterLevel, this.maxWaterLevel).toFixed(2));
            this.pivot.position.y = this.waterLevel;

            Globals.gameObjects.forEach(object => {
                if (object.floats) {
                    object.setY();
                }
            });
        }
        this.depthMaterialNeedsUpdate = true;
    }

    onWindowResize() {
        this.waterMaterial.uniforms.resolution.value.set(
            window.innerWidth * window.devicePixelRatio,
            window.innerHeight * window.devicePixelRatio
        );
    }

}