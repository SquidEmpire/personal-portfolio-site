import {Globals, setActiveObject, deleteActiveObject} from "../core.js";
import { ClickableGameObject } from "./clickableGameObject.js";

export class SelectableGameObject extends ClickableGameObject {

    isSelected = false;
    isSelectable = true;
    displayName = "Selectable Game Object";
    icon = "img/icon/default.png"

    constructor() {
        super();
    }

    select() {
        this.isSelected = true;
        setActiveObject(this.uuid, this);
    }

    deselect() {
        this.isSelected = false;
        deleteActiveObject(this.uuid);
    }

    rightClickedOther(other, x, y, shiftKey) {
        //while this unit is active, something else has been right clicked, decide what to do in subclassses
    }

    //return either a string or an HTML element
    getDisplayHTML() {
        return ``;
    }

}