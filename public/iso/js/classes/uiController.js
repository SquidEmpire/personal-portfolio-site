import {Globals} from "../core.js";
import { clearActiveObjects} from "../core.js";
import { MaterialDepot } from "./mapObjects/materialDepot.js";
import { PollutionMachine } from "./mapObjects/pollutionMachine.js";
import { CleanupMachine } from "./mapObjects/cleanupMachine.js";
import { Tent } from "./mapObjects/tent.js";
import { Lodge } from "./mapObjects/lodge.js";
import { WindTurbine } from "./mapObjects/windTurbine.js";
import { DieselGenerator } from "./mapObjects/dieselGenerator.js";
import { ChemicalExchanger } from "./mapObjects/chemicalExchanger.js";
import { SolarPanel } from "./mapObjects/solarPanel.js";
import { AirFilter } from "./mapObjects/airFilter.js";
import { MaterialRefinery } from "./mapObjects/materialRefinery.js";
import { WaterTank } from "./mapObjects/waterTank.js";
import { SoilPost } from "./mapObjects/soilPost.js";
import { SickBay } from "./mapObjects/sickBay.js";

export class UiController {

    selectedTitleElement;
    selectedDetailsElement;

    tentButtonElement;
    lodgeButtonElement;
    sickBayButtonElement;
    dieselGeneratorButtonElement;
    turbineButtonElement;
    chemicalExchangerButtonElement;
    solarPanelButtonElement;
    materialDepotButtonElement;
    materialRefineryButtonElement;
    airFilterButtonElement;
    waterTreatmentButtonElement;
    soilTreatmentButtonElement;

    pollutionMachineButtonElement;
    cleanupMachineButtonElement;
    raiseAllPollutionButtonElement;
    lowerAllPollutionButtonElement;
    raiseLandPollutionButtonElement;
    lowerLandPollutionButtonElement;
    raiseAirPollutionButtonElement;
    lowerAirPollutionButtonElement;
    raiseWaterPollutionButtonElement;
    lowerWaterPollutionButtonElement;
    raiseWaterButtonElement;
    lowerWaterButtonElement;
    weatherControlButtonElement;

    workersResourceValueElement;
    materialResourceValueElement;
    powerResourceValueElement;

    landPollutionValueElement;
    airPollutionValueElement;
    waterPollutionValueElement;

    needsUpdate = false;

    constructor() {

    }

    init() {
        this.selectedTitleElement = document.getElementById("selected-box-title");
        this.selectedDetailsElement = document.getElementById("selected-box-details");
        this.selectedImageElement = document.getElementById("selected-box-image");

        this.tentButtonElement = document.getElementById("tent-button");
        this.lodgeButtonElement = document.getElementById("lodge-button");
        this.sickBayButtonElement = document.getElementById("sick-bay-button");
        this.dieselGeneratorButtonElement = document.getElementById("generator-button");
        this.turbineButtonElement = document.getElementById("turbine-button");
        this.chemicalExchangerButtonElement = document.getElementById("exchanger-button");
        this.solarPanelButtonElement = document.getElementById("solar-button");
        this.materialDepotButtonElement = document.getElementById("depot-button");
        this.materialRefineryButtonElement = document.getElementById("refinery-button");
        this.airFilterButtonElement = document.getElementById("air-filter-button");
        this.waterTreatmentButtonElement = document.getElementById("water-treatment-button");
        this.soilTreatmentButtonElement = document.getElementById("soil-treatment-button");

        //debug menu
        this.pollutionMachineButtonElement = document.getElementById("pollution-machine-button");
        this.cleanupMachineButtonElement = document.getElementById("cleanup-machine-button");
        this.raiseAllPollutionButtonElement = document.getElementById("raise-pollution-level");
        this.lowerAllPollutionButtonElement = document.getElementById("lower-pollution-level");
        this.raiseLandPollutionButtonElement = document.getElementById("raise-land-pollution-level");
        this.lowerLandPollutionButtonElement = document.getElementById("lower-land-pollution-level");
        this.raiseAirPollutionButtonElement = document.getElementById("raise-air-pollution-level");
        this.lowerAirPollutionButtonElement = document.getElementById("lower-air-pollution-level");
        this.raiseWaterPollutionButtonElement = document.getElementById("raise-water-pollution-level");
        this.lowerWaterPollutionButtonElement = document.getElementById("lower-water-pollution-level");
        this.raiseWaterButtonElement = document.getElementById("raise-water-level");
        this.lowerWaterButtonElement = document.getElementById("lower-water-level");
        this.weatherControlButtonElement = document.getElementById("weather");

        this.workersResourceValueElement = document.getElementById("workers-resource-value");
        this.materialResourceValueElement = document.getElementById("material-resource-value");
        this.powerResourceValueElement = document.getElementById("power-resource-value");

        this.landPollutionValueElement = document.getElementById("land-pollution-meter-inside");
        this.airPollutionValueElement = document.getElementById("air-pollution-meter-inside");
        this.waterPollutionValueElement = document.getElementById("water-pollution-meter-inside");


        this.tentButtonElement.addEventListener("click", () => {
            let newObject = new Tent();
            newObject.initialize(Globals.controls.lastMouseTerrainIntersectionPoint.x, 0, Globals.controls.lastMouseTerrainIntersectionPoint.y, true);
            clearActiveObjects();
            newObject.select();
        });

        this.lodgeButtonElement.addEventListener("click", () => {
            let newObject = new Lodge();
            newObject.initialize(Globals.controls.lastMouseTerrainIntersectionPoint.x, 0, Globals.controls.lastMouseTerrainIntersectionPoint.y, true);
            clearActiveObjects();
            newObject.select();
        });

        this.sickBayButtonElement.addEventListener("click", () => {
            let newObject = new SickBay();
            newObject.initialize(Globals.controls.lastMouseTerrainIntersectionPoint.x, 0, Globals.controls.lastMouseTerrainIntersectionPoint.y, true);
            clearActiveObjects();
            newObject.select();
        });

        this.dieselGeneratorButtonElement.addEventListener("click", () => {
            let newObject = new DieselGenerator();
            newObject.initialize(Globals.controls.lastMouseTerrainIntersectionPoint.x, 0, Globals.controls.lastMouseTerrainIntersectionPoint.y, true);
            clearActiveObjects();
            newObject.select();
        });

        this.turbineButtonElement.addEventListener("click", () => {
            let newObject = new WindTurbine();
            newObject.initialize(Globals.controls.lastMouseTerrainIntersectionPoint.x, 0, Globals.controls.lastMouseTerrainIntersectionPoint.y, true);
            clearActiveObjects();
            newObject.select();
        });

        this.chemicalExchangerButtonElement.addEventListener("click", () => {
            let newObject = new ChemicalExchanger();
            newObject.initialize(Globals.controls.lastMouseTerrainIntersectionPoint.x, 0, Globals.controls.lastMouseTerrainIntersectionPoint.y, true);
            clearActiveObjects();
            newObject.select();
        });

        this.solarPanelButtonElement.addEventListener("click", () => {
            let newObject = new SolarPanel();
            newObject.initialize(Globals.controls.lastMouseTerrainIntersectionPoint.x, 0, Globals.controls.lastMouseTerrainIntersectionPoint.y, true);
            clearActiveObjects();
            newObject.select();
        });

        this.airFilterButtonElement.addEventListener("click", () => {
            let newObject = new AirFilter();
            newObject.initialize(Globals.controls.lastMouseTerrainIntersectionPoint.x, 0, Globals.controls.lastMouseTerrainIntersectionPoint.y, true);
            clearActiveObjects();
            newObject.select();
        });

        this.waterTreatmentButtonElement.addEventListener("click", () => {
            let newObject = new WaterTank();
            newObject.initialize(Globals.controls.lastMouseTerrainIntersectionPoint.x, 0, Globals.controls.lastMouseTerrainIntersectionPoint.y, true);
            clearActiveObjects();
            newObject.select();
        });

        this.soilTreatmentButtonElement.addEventListener("click", () => {
            let newObject = new SoilPost();
            newObject.initialize(Globals.controls.lastMouseTerrainIntersectionPoint.x, 0, Globals.controls.lastMouseTerrainIntersectionPoint.y, true);
            clearActiveObjects();
            newObject.select();
        });


        this.materialDepotButtonElement.addEventListener("click", () => {
            let newObject = new MaterialDepot();
            newObject.initialize(Globals.controls.lastMouseTerrainIntersectionPoint.x, 0, Globals.controls.lastMouseTerrainIntersectionPoint.y, true);
            clearActiveObjects();
            newObject.select();
        });

        this.materialRefineryButtonElement.addEventListener("click", () => {
            let newObject = new MaterialRefinery();
            newObject.initialize(Globals.controls.lastMouseTerrainIntersectionPoint.x, 0, Globals.controls.lastMouseTerrainIntersectionPoint.y, true);
            clearActiveObjects();
            newObject.select();
        });


        //debug menu

        this.pollutionMachineButtonElement.addEventListener("click", () => {
            let newObject = new PollutionMachine();
            newObject.initialize(Globals.controls.lastMouseTerrainIntersectionPoint.x, 0, Globals.controls.lastMouseTerrainIntersectionPoint.y, true);
            clearActiveObjects();
            newObject.select();
        });

        this.cleanupMachineButtonElement.addEventListener("click", () => {
            let newObject = new CleanupMachine();
            newObject.initialize(Globals.controls.lastMouseTerrainIntersectionPoint.x, 0, Globals.controls.lastMouseTerrainIntersectionPoint.y, true);
            clearActiveObjects();
            newObject.select();
        });

        this.weatherControlButtonElement.addEventListener("click", () => {
            let index = Globals.weather.currentWeatherIndex + 1;
            if (index > Globals.weather.weatherEnum.length - 1) {
                index = 0;
            }
            let weather = Globals.weather.weatherEnum[index];
            Globals.weather.setWeather(weather);
            this.weatherControlButtonElement.querySelector("span").innerText = weather;
        });

        this.raiseWaterButtonElement.addEventListener("click", () => {Globals.water.raiseWaterLevel()});
        this.lowerWaterButtonElement.addEventListener("click", () => {Globals.water.lowerWaterLevel()});
        this.raiseAllPollutionButtonElement.addEventListener("click", () => {Globals.pollution.increaseAllBasePollutionLevels(0.1)});
        this.lowerAllPollutionButtonElement.addEventListener("click", () => {Globals.pollution.decreaseAllBasePollutionLevels(0.1)});
        this.raiseLandPollutionButtonElement.addEventListener("click", () => {Globals.pollution.increaseBaseLandPollutionLevel(0.1)});
        this.lowerLandPollutionButtonElement.addEventListener("click", () => {Globals.pollution.decreaseBaseLandPollutionLevel(0.1)});
        this.raiseAirPollutionButtonElement.addEventListener("click", () => {Globals.pollution.increaseBaseAirPollutionLevel(0.1)});
        this.lowerAirPollutionButtonElement.addEventListener("click", () => {Globals.pollution.decreaseBaseAirPollutionLevel(0.1)});
        this.raiseWaterPollutionButtonElement.addEventListener("click", () => {Globals.pollution.increaseBaseWaterPollutionLevel(0.1)});
        this.lowerWaterPollutionButtonElement.addEventListener("click", () => {Globals.pollution.decreaseBaseWaterPollutionLevel(0.1)});
        
        this.selectedDetailsElement.addEventListener("click", e => {
            //if we click on an icon in a group selection, go to that object
            const dataAttributes = e.target.dataset;
            if (dataAttributes && dataAttributes.uuid) {
                const object = Globals.gameObjects.get(dataAttributes.uuid);
                clearActiveObjects();
                object.select();
            }
        });

        this.clearDisplayedObject();
    }

    //public
    onRender = function() {
        if (!Globals.paused && this.needsUpdate) {
            //refresh selected object
            if (Globals.activeObjects.size) {
                if (Globals.activeObjects.size > 1) {
                    this.displaySelectedObjects(Array.from(Globals.activeObjects.values()));
                } else {
                    this.displaySelectedObject(Globals.activeObjects.entries().next().value[1]);
                }
            } else {
                this.displaySelectedObject(null);
            }
            //refresh resource vaules
            if (this.workersResourceValueElement.innerHTML != Globals.resources.totalWorkers) {
                this.workersResourceValueElement.innerHTML = Globals.resources.totalWorkers;
            }
            if (this.materialResourceValueElement.innerHTML != Globals.resources.material) {
                this.materialResourceValueElement.innerHTML = Globals.resources.material;
            }
            if (this.powerResourceValueElement.innerHTML != Globals.resources.power.toFixed(2)) {
                this.powerResourceValueElement.innerHTML = Globals.resources.power.toFixed(2);
            }
            //refresh meters
            this.updatePollutionMeters();

            this.needsUpdate = false;        
        }
    }

    displaySelectedObjects = function(selectableGameObjects) {
        if (selectableGameObjects && selectableGameObjects.length) {
            //it's ok to wipe the html and reset every time because this function should never be able
            //to run without changes to the selected objects array (i.e. it's rare)
            this.selectedTitleElement.innerHTML = "Selection";
            this.selectedDetailsElement.innerHTML = "";
            this.selectedImageElement.classList.add('hidden');

            let groupSelectionIconList = "<div id='group-selection-icon-list'>";
            for (let i = 0; i < selectableGameObjects.length; i++) {
                const objectUUID = selectableGameObjects[i].uuid;
                const src = selectableGameObjects[i].icon;
                groupSelectionIconList += `
                    <img class="group-selection-icon" data-uuid="${objectUUID}" src="${src}">
                `;                
            }
            groupSelectionIconList += "</div>";
            this.selectedDetailsElement.innerHTML = groupSelectionIconList;
        } else {
            this.clearDisplayedObject();
        }
    }

    displaySelectedObject = function(selectableGameObject) {
        if (selectableGameObject) {
            this.selectedTitleElement.innerHTML = selectableGameObject.displayName;
            this.selectedImageElement.src = selectableGameObject.icon;
            this.selectedImageElement.classList.remove('hidden');

            const newDisplayHTML = selectableGameObject.getDisplayHTML();
            if (newDisplayHTML instanceof Node) {
                //html node (must be only 1!!!)
                if (this.selectedDetailsElement.children.length === 0) {
                    this.selectedDetailsElement.appendChild(newDisplayHTML);
                } else if (this.selectedDetailsElement.children.length === 1) {
                    //if the currently displayed node isn't our new one, replace it. Otherwise, do nothing
                    if (!this.selectedDetailsElement.children[0].isEqualNode(newDisplayHTML)) {
                        this.selectedDetailsElement.removeChild(this.selectedDetailsElement.children[0]);
                        this.selectedDetailsElement.appendChild(newDisplayHTML);
                    }
                } else {
                    console.error("Invalid selected item display state!!");
                }
            } else {
                //string
                this.selectedDetailsElement.innerHTML = newDisplayHTML;
            }
        } else {
            this.clearDisplayedObject();
        }
    }

    clearDisplayedObject = function() {
        this.selectedTitleElement.innerHTML = "";
        this.selectedDetailsElement.innerHTML = "";
        this.selectedImageElement.classList.add('hidden');
    }

    updatePollutionMeters = function() {
        const landPollutionPercentage = Globals.pollution.landPollutionLevel * 100;
        const airPollutionPercentage = Globals.pollution.airPollutionLevel * 100;
        const waterPollutionPercentage = Globals.pollution.waterPollutionLevel * 100;

        this.landPollutionValueElement.style.width = `${landPollutionPercentage}%`;
        this.airPollutionValueElement.style.width = `${airPollutionPercentage}%`;
        this.waterPollutionValueElement.style.width = `${waterPollutionPercentage}%`;
    }

}