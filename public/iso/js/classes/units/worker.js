import {Globals} from "../../core.js";
import * as HELPER from "../../helper.js";
import * as THREE from "../../vendor/three.module.js";
import {SkeletonUtils} from "../../vendor/utils/SkeletonUtils.js";
import { Unit } from "../unit.js";
import { ChopTreeOrder } from "../orders/chopTreeOrder.js";

export class Worker extends Unit {

    name = "worker";
    displayName = "Worker";
    skeleton;
    model;

    icon = "img/icon/icon-worker.png";

    _animationMixer;
    _currentAnimation;
    _idleAnimation;
    _walkingAnimation;
    _walkingCarryingAnimation;
    _interactAnimation;

    lastPosition = new THREE.Vector3();

    carryAmount = 0;
    carryType = null;
    carryCapacity = 100;
    workSpeed = {
        wood: 10,
    }
    isWorking = false;
    workTarget = null;
    workAction = null;

    constructor() {
        super();
        this.pivot.clear();
        this.buildMesh();
    }

    buildMesh() {
        const skincolours = [
            0xF3EAE5,
            0xEFE2AD,
            0xC7A464,
            0x643116
        ];

        const originalModel = Globals.assets.find(x => x.name === 'worker').contents;
        this.model = SkeletonUtils.clone(originalModel);

        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                //there should be only one
                this.mesh = child;
                this.mesh.receiveShadow = true;
                this.mesh.castShadow = true;
                this.mesh.name = "unit_mesh";
                for (let i = 0; i < child.material.length; i++) {
                    if (child.material[i].name === "Skin") {
                        //I cannot for the life of me work out how to stop the skin change from being global
                        //when you clone this clone's skin material, the originalModel's skin material UUID changes to the clone's
                        const randomSkinColour = HELPER.getRandomValueFromArray(skincolours);
                        child.material[i] = child.material[i].clone();
                        child.material[i].color.set(randomSkinColour);
                        break;
                    }
                }
            }
        } );

        this.model.name = "worker_model";
        this.model.scale.multiplyScalar(0.008);
        
        this.pivot.add(this.model);
        this.pivot.add(this.ring);

        this.skeleton = new THREE.SkeletonHelper( this.model );
        this.skeleton.visible = false;
        Globals.scene.add( this.skeleton );        

        this._animationMixer = new THREE.AnimationMixer( this.model ); 
        //this is what to do when a single action animation has finished
        this._animationMixer.addEventListener( 'finished', () => {this.workActionFinished()});

        this._idleAnimation = this._animationMixer.clipAction( originalModel.animations[ 5 ] );
        this._idleAnimation.setEffectiveTimeScale( 0.001 );
        this._idleAnimation._clip.optimise; //worth doing?
        this._walkingAnimation = this._animationMixer.clipAction( originalModel.animations [ 7 ] );
        this._walkingAnimation.setEffectiveTimeScale( 0.001 );
        this._walkingAnimation._clip.optimise;
        this._walkingCarryingAnimation = this._animationMixer.clipAction( originalModel.animations [ 8 ] );
        this._walkingCarryingAnimation.setEffectiveTimeScale( 0.001 );
        this._walkingCarryingAnimation._clip.optimise;
        this._interactAnimation = this._animationMixer.clipAction( originalModel.animations [ 10 ] );
        this._interactAnimation.setEffectiveTimeScale( 0.001 );
        this._interactAnimation.setLoop(THREE.LoopOnce, 1);
        this._interactAnimation._clip.optimise;

        this.lastPosition.copy(this.pivot.position);

    }

    initialize(x, y, z) {
        super.initialize(x, y, z);
        this._idleAnimation.play();
        this._currentAnimation = this._idleAnimation;
    }

    issueChopTreeCommand(tree, queued = false) {        
        const treeTreeOrder = new ChopTreeOrder(this, tree);
        if (!queued) {
            this.orderQueue = [treeTreeOrder];
        } else {
            this.orderQueue.push(treeTreeOrder);
        }
        if (this.isSelected) {
            Globals.uiController.needsUpdate = true;
        }
    }

    rightClickedOther(other, x, y, shiftKey) {
        //while this unit is active, something else has been right clicked
        if (other.name === "terrain") {
            let targetX = x;
            let targetZ = y;
            if (Globals.activeObjects.size > 1) {
                //if move orders are being issued to multiple at the same time, add a tiny bit of randomness to our destination so not
                //all the units converge on the same position
                targetX += HELPER.getRandom(-2, 2);
                targetZ += HELPER.getRandom(-2, 2);
            }
            this.issueMoveCommand(targetX, targetZ, shiftKey);
        } else if (other.name === "tree") { //|| other.name === "materialDepot") { //TODO: something to manually tell the worker to dump their goods
            this.issueChopTreeCommand(other, shiftKey);
        }
    }

    onChopTree(tree) {
        this.isWorking = true;
        this.workTarget = tree;
        this.workAction = "chop";
    }

    workActionFinished() {
        if (this.workAction === "chop") {
            const extractedWood = this.workTarget.extractWood(this.workSpeed.wood);
            if (extractedWood) {
                this.carryType = "wood";
                this.carryAmount += extractedWood;
            }
        }
        if (this.isSelected) {
            Globals.uiController.needsUpdate = true;
        }
        this.isWorking = false;
    }

    stopAllAnimations(animation) {
        this._idleAnimation.stop();
        this._walkingAnimation.stop();
        this._walkingCarryingAnimation.stop();
        this._interactAnimation.stop();
    }

    playAnimation(animation) {
        this.stopAllAnimations(animation);
        animation.play();
        this._currentAnimation = animation;
    }

    updatePlayingAnimation() {
        this.isMoving = (this.lastPosition.x != this.pivot.position.x) ||  (this.lastPosition.z != this.pivot.position.z);//this is some bad stuff

        //TODO: animation mixer has stuff in it to allow smooth mixing of animations instead of binary states
        if (this.isMoving) {
            if (this.carryAmount == 0 && this._currentAnimation !== this._walkingAnimation) {
                this.playAnimation(this._walkingAnimation);
            } else if (this.carryAmount > 0 && this._currentAnimation !== this._walkingCarryingAnimation) {
                this.playAnimation(this._walkingCarryingAnimation);
            }
        } else if (!this.isMoving) {
            if (this.isWorking && this._currentAnimation !== this._interactAnimation) {
                this.playAnimation(this._interactAnimation);
            } else if (!this.isWorking && this._currentAnimation !== this._idleAnimation) {
                this.playAnimation(this._idleAnimation);
            }
        }

        this.lastPosition.copy(this.pivot.position);
    }

    onGameTick(delta) {
        super.onGameTick(delta);
        this._animationMixer.update( delta );
        this.updatePlayingAnimation();
    }

    getCurrentOrderDisplayText() {
        if (this.orderQueue.length && this.orderQueue[0].isComplete === false) {
            return this.orderQueue[0].displayText;
        }
        return "idle";
    }

    getDisplayHTML() {
        return `
        <p>${this.getCurrentOrderDisplayText()}</p>
        <p>Carrying: ${this.carryAmount} / ${this.carryCapacity} ${this.carryType !== null ? this.carryType : ""}</p>
        `;
    }

}