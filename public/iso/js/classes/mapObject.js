import {Globals, deleteActiveObject, getMapGridObjectValue, getMapGridValueAtGridCoordinates, setMapGridObjectValue} from "../core.js";
import { SelectableGameObject } from "./selectableGameObject.js";
import * as THREE from "../vendor/three.module.js"

export class MapObject extends SelectableGameObject {

    isPlaceState = false;
    currentPlacementIsValid = true;
    isSolid = false;
    placeColour = 0xaaaaaa;
    normalColour = 0xffaacc;
    usePlacementColours = true;

    contributesPower = false;

    contributesGlobalAirPollution = false;
    contributesLandPollution = false;
    contributesGlobalWaterPollution = false;

    type = "mapObject";
    name = "mapObject";
    displayName = "Map Object"
    _selectSquare;
    placementHeightThreshold = 3; //the max difference in terrain height this can be placed over
    placementDepthThreshold = -2; //the maximum distance below the terrain height any part of this object can be placed at

    footprint = [
        1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ]; //16x16 grid of cells (each = 1 Global.tileSize) that this object takes up

    orderQueue = [];

    constructor() {
        super();
        this.pivot = new THREE.Group();
        const meshGeometry = new THREE.BoxGeometry( Globals.tileSize, Globals.tileSize, Globals.tileSize );
        const meshMaterial = new THREE.MeshLambertMaterial( {color: this.placeColour} );
        this.mesh = new THREE.Mesh( meshGeometry, meshMaterial );
        this.mesh.receiveShadow = true;
        this.mesh.castShadow = true;

        this.pivot.add(this.mesh);        
        this.mesh.position.y += (Globals.tileSize/2);

        this.mesh.name = "mapObject_mesh";
    }

    initialize( x = 0, y = 0, z = 0, isPlaceState = false ) {
        super.initialize();
        this.isPlaceState = isPlaceState;
        this.generateSelectorSquare();
        this._selectSquare.visible = false;
        Globals.scene.add(this.pivot);
        this.moveTo(x, z);
        this.setY(y);
        if (isPlaceState === false) {
            if (this.isPositionValidForPlacement(this.pivot.position.x, this.pivot.position.z)) {
                this.placeAtCurrentPosition();
            } else {
                console.error(`Attempted to initialize a map object at an invalid position`, this.pivot.position);
                this.destroy();
            }
        }      
    }

    generateSelectorSquare() {
        //add the highlighter square
        let width = 0;
        let height = 0;
        for (let row = 0, i = 0; row < 16; row++) {
            for (let col = 0; col < 16; col++) {
                if (this.footprint[i] === 1) {
                    if (row + 1 > height) {
                        height = row + 1;
                    }
                    if (col + 1 > width) {
                        width = col + 1;
                    }
                }
                i++;
            }
        }
        width *= 1.2; //extend the selection box out a little
        height *= 1.2;
        const selectSquareGeometry = new THREE.PlaneGeometry( width, height );
        selectSquareGeometry.rotateX(Math.PI * 1.5);
        selectSquareGeometry.translate( -width/2, 0, -height/2 );
        const edges = new THREE.EdgesGeometry( selectSquareGeometry );
        this._selectSquare = new THREE.LineSegments( edges, new THREE.LineBasicMaterial( { color: 0xffffff, depthTest: false } ) );
        this.pivot.add(this._selectSquare);
    }

    destroy() {
        super.destroy();
        //reset the placement materials if any before destruction
        this.setPlacementGoodMaterial();
        this.mesh.geometry.dispose();
        if (Array.isArray(this.mesh.material)) {
            for (let i =0; i < this.mesh.material.length; i++) {
                this.mesh.material[i].dispose();
            }
        } else {
            this.mesh.material.dispose();
        }
        Globals.scene.remove( this.pivot );
    }

    moveTo(x, z) {
        this.pivot.position.set(x, 0, z);
        this.setY();
    }

    setPlacementBadMaterial() {

    }

    setPlacementGoodMaterial() {

    }

    setY(overwriteValue = 0) {
        if (overwriteValue) {
            this.pivot.position.y = overwriteValue;
        } else {
            // let maxFootprintHeight = 0;
            // //TODO is this more user friendly to just use the pivot position instead of the entire footprint?
            // for (let row = 0, i = 0; row < 16; row++) {
            //     for (let col = 0; col < 16; col++) {
            //         if (this.footprint[i] === 1) {
            //             const gridValue = getMapGridValueAtGridCoordinates(this.pivot.position.x - col, this.pivot.position.z - row, true);
            //             if (gridValue.height > maxFootprintHeight) {
            //                 maxFootprintHeight = gridValue.height;
            //             }
            //         }
            //         i++;
            //     }
            // }
            
            // this.pivot.position.y = maxFootprintHeight;
            const gridValue = getMapGridValueAtGridCoordinates(this.pivot.position.x, this.pivot.position.z, true);
            this.pivot.position.y = gridValue.height;
        }        
    }

    isPositionValidForPlacement(x, y) {
        let maxFootprintHeight = 0;
        let minFootprintHeight = 100;
        for (let row = 0, i = 0; row < 16; row++) {
            for (let col = 0; col < 16; col++) {
                if (this.footprint[i] === 1) {
                    const gridValue = getMapGridValueAtGridCoordinates(x - col, y - row, true);
                    //no placements off the grid!
                    if (!gridValue) {
                        return false;
                    }
                    //don't allow placement is the footprint overlaps another object
                    if (gridValue.mapObject !== null) {
                        return false;
                    }
                    //don't allow any part of the footprint to go below the depth threshold
                    if (this.pivot.position.y - gridValue.height < this.placementDepthThreshold) {
                        return false;
                    }

                    if (gridValue.height > maxFootprintHeight) {
                        maxFootprintHeight = gridValue.height;
                    }
                    if (gridValue.height < minFootprintHeight) {
                        minFootprintHeight = gridValue.height;
                    }
                }
                i++;
            }
        }
        //if the highest point is too far from the lowest point this is not a valid location either
        if (maxFootprintHeight - minFootprintHeight > this.placementHeightThreshold) {
            return false;
        }
        return true;
    }

    placeAtCurrentPosition() {
        if (this.mesh.material.color && this.usePlacementColours) {
            this.mesh.material.color.setHex(this.normalColour);
        }
        this.isPlaceState = false;
        this.isSolid = true;
        for (let row = 0, i = 0; row < 16; row++) {
            for (let col = 0; col < 16; col++) {
                if (this.footprint[i] === 1) {
                    setMapGridObjectValue(this.pivot.position.x - col, this.pivot.position.z - row, this, true);
                }
                i++;
            }
        }
        if (this.isSelected) {
            Globals.uiController.needsUpdate = true;
        }
    }

    onClicked() {
        //exit placement mode if in it
        if (this.isPlaceState && this.currentPlacementIsValid) {
            this.placeAtCurrentPosition();
        } else {
            this.select();
        }
    }

    onDeclicked() {
        this.deselect();   
    }

    select() {
        super.select();
        this._selectSquare.visible = true;
    }

    deselect() {
        super.deselect();
        this._selectSquare.visible = false;
        if (this.isPlaceState) {
            this.destroy();
            deleteActiveObject(this.uuid);
        }  
    }

    rightClickedOther(other, x, y, shiftKey) {
        if (this.isPlaceState) {
            this.deselect();
        }
    }

    getPowerEffect() {
        return 0;
    }

    getGlobalAirPollutionEffect() {
        return 0;
    }

    getGlobalWaterPollutionEffect() {
        return 0;
    }

    onGameTick(delta) {
        //placement mode
        if (this.isPlaceState) {
            const pointerLocation = Globals.controls.getPointerLocationOnTerraion();
            this.moveTo( Math.round(pointerLocation.x/Globals.tileSize)*Globals.tileSize, Math.round(pointerLocation.y/Globals.tileSize)*Globals.tileSize);

            let placementValid = this.isPositionValidForPlacement(this.pivot.position.x, this.pivot.position.z);
            if (placementValid && !this.currentPlacementIsValid) {
                this.setPlacementGoodMaterial();
            } else if (!placementValid && this.currentPlacementIsValid) {
                this.setPlacementBadMaterial();
            }
            this.currentPlacementIsValid = placementValid;
        }
    }

}