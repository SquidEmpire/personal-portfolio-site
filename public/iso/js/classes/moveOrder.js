import { Order } from "./order.js";
import * as THREE from "../vendor/three.module.js"

export class MoveOrder extends Order {

    target = new THREE.Vector3();
    steps = [];

    constructor(actor, x, z) {
        super(actor);
        this.target.set(x, 0, z);
    }

    performStep(delta, speed) {
        this.target.y = this.actor.pivot.position.y;
        this.actor.pivot.lookAt(this.target);
        this.actor.pivot.translateZ(delta * speed * 0.005);
        this.actor.setY();
        if (this.actor.pivot.position.distanceTo(this.target) < 0.5) {
            this.target.y = this.actor.pivot.position.y;
            this.isComplete = true;
        }
    }

}