import {Globals} from "../../core.js";
import { MapObject } from "../mapObject.js";
import * as THREE from "../../vendor/three.module.js"
import { BufferGeometryUtils } from "../../vendor/utils/BufferGeometryUtils.js";

export class MaterialDepot extends MapObject {

    name = "materialDepot";
    displayName = "Material Depot";
    icon = "img/icon/icon-depot.png";

    _material;

    footprint = [
        0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ];

    constructor() {
        super();
        this.pivot = new THREE.Group();
        this.usePlacementColours = false;
        
        const originalModel = Globals.assets.find(x => x.name === `materialDepot`).contents;
        this.model = originalModel.clone();

        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                this.mesh = child; //TODO
                this.mesh.receiveShadow = true;
                this.mesh.castShadow = true;
                child.userData.uuid = this.uuid;
                Globals.clickableMeshes.push(child);
                
                child.material = child.material.clone();
                child.material.userData.originalColour = child.material.color.clone();
                child.material.userData.originalEmissive = child.material.emissive.clone();
            }
            if (child.name === "material") {
                this._material = child;
            }
        });

        this.model.name = "material_depot_model";
        this.model.rotateZ(Math.PI/2);
        this.model.scale.multiplyScalar(1.5);
        
        this.pivot.add(this.model);
    }

    setPlacementBadMaterial() {
        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                child.material.color.setHex(0xff2200);
                child.material.emissive.setHex(0xff2200);
            }
        });
    }

    setPlacementGoodMaterial() {
        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                child.material.color.copy(child.material.userData.originalColour);
                child.material.emissive.copy(child.material.userData.originalEmissive);
            }
        });
    }

    getDisplayHTML() {
        if (this.isPlaceState) {
            return "";
        }
        const displayHTML = document.createElement('div');

        const buildingInfo = document.createElement('p');
        buildingInfo.innerText = "Tools and containers to help workers collect and store material";

        displayHTML.appendChild(buildingInfo);
        return displayHTML;
    }
}