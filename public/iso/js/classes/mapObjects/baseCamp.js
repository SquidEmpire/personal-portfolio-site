import {Globals} from "../../core.js";
import { MapObject } from "../mapObject.js";
import * as THREE from "../../vendor/three.module.js"
import { Worker } from "../units/worker.js";
import * as HELPER from "../../helper.js";

export class Basecamp extends MapObject {

    name = "basecamp";
    displayName = "Base camp";
    icon = "img/icon/icon-base.png";

    supportedWorker;
    workersRemaining = 1;

    _weathervane;

    footprint = [
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ];

    constructor() {
        super();
        this.pivot = new THREE.Group();
        this.usePlacementColours = false;
        
        const originalModel = Globals.assets.find(x => x.name === `camp`).contents;
        this.model = originalModel.clone();

        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                this.mesh = child; //TODO
                this.mesh.receiveShadow = true;
                this.mesh.castShadow = true;
                child.userData.uuid = this.uuid;
                Globals.clickableMeshes.push(child);

                child.material = child.material.clone();
                if (child.material.name === "emissive") {
                    child.material.emissive.copy(child.material.color);
                }
                child.material.userData.originalColour = child.material.color.clone();
                child.material.userData.originalEmissive = child.material.emissive.clone();
            }
            if (child.name === "weathervane") {
                this._weathervane = child;
            }
        });

        this.model.name = "camp_model";
        this.model.rotateZ(Math.PI/2);
        this.model.scale.multiplyScalar(1.6);
        
        this.pivot.add(this.model);
    }

    setPlacementBadMaterial() {
        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                child.material.color.setHex(0xff2200);
                child.material.emissive.setHex(0xff2200);
            }
        });
    }

    setPlacementGoodMaterial() {
        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                child.material.color.copy(child.material.userData.originalColour);
                child.material.emissive.copy(child.material.userData.originalEmissive);
            }
        });
    }

    onCreateWorker() {
        if (this.workersRemaining) {
            //make a worker and make them move randomly out of the building they're in initially
            let newWorker = new Worker();
            newWorker.initialize(this.pivot.position.x, this.pivot.position.y, this.pivot.position.z - 1.2);
            const targetX = this.pivot.position.x + HELPER.getRandomInt(1, 5);
            const targetZ = this.pivot.position.z + HELPER.getRandomInt(-5, 5);
            newWorker.issueMoveCommand(targetX, targetZ, false);
            this.workersRemaining--;
            this.supportedWorker = newWorker;
            Globals.resources.totalWorkers++;
            Globals.uiController.needsUpdate = true;
        }
    }

    getDisplayHTML() {
        if (this.isPlaceState) {
            return "";
        }
        const displayHTML = document.createElement('div');

        const buildingInfo = document.createElement('p');
        buildingInfo.innerText = "Your base of operations. Supports 1 worker and is a drop off point for materials";

        const workersRemainingInfo = document.createElement('p');
        workersRemainingInfo.innerText = `Workers remaining: ${this.workersRemaining}`;

        const button = document.createElement('button');
        if (this.workersRemaining) {
            if (Globals.resources.totalWorkers < Globals.totalWorkerLimit) {
                button.innerHTML = "Deploy worker";
                button.addEventListener('click', this.onCreateWorker.bind(this));
            } else {
                button.innerHTML = "Maximum worker population reached";
                button.disabled = true;
            }
        } else {
            button.innerHTML = "No workers remaining";
            button.disabled = true;
        }

        displayHTML.appendChild(buildingInfo);
        displayHTML.appendChild(workersRemainingInfo);
        displayHTML.appendChild(button);
        return displayHTML;
    }

    onGameTick(delta) {
        super.onGameTick();
        if (!this.isPlaceState) {
            this._weathervane.rotateZ(Math.abs(Globals.weather.windStrength));
        }        
    }
}