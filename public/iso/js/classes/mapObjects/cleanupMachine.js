import {Globals, worldCoordinatesToGridCoordinates, getMapGridValueAtGridCoordinates} from "../../core.js";
import { MapObject } from "../mapObject.js";
import * as THREE from "../../vendor/three.module.js"
import { BufferGeometryUtils } from "../../vendor/utils/BufferGeometryUtils.js";

export class CleanupMachine extends MapObject {

    normalColour = 0xDEF4EC;
    name = "cleanupMachine";
    displayName = "Cleanup machine";

    radius = 10;
    strength = 1;

    footprint = [
        1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ];

    constructor() {
        super();
        this.pivot = new THREE.Group();
        const mesh1Geometry = new THREE.BoxGeometry( Globals.tileSize * 2, Globals.tileSize * 1, Globals.tileSize * 2 );
        const mesh2Geometry = new THREE.CylinderGeometry( Globals.tileSize, Globals.tileSize, Globals.tileSize * 4 );

        mesh1Geometry.translate(-(Globals.tileSize * 0.5), (Globals.tileSize * 0.5), -(Globals.tileSize * 0.5));
        mesh2Geometry.translate(-(Globals.tileSize * 0.5), (Globals.tileSize * 2.5), -(Globals.tileSize * 0.5));

        const mergedGeometry = BufferGeometryUtils.mergeBufferGeometries([mesh1Geometry, mesh2Geometry]);
        const meshMaterial = new THREE.MeshLambertMaterial( {color: this.placeColour} );

        this.mesh = new THREE.Mesh( mergedGeometry, meshMaterial );
        this.mesh.receiveShadow = true;
        this.mesh.castShadow = true;

        this.pivot.add(this.mesh);
    }

    onGameTick(delta) {
        super.onGameTick(delta);

        //randomly make the tiles around us less polluted
        if (!this.isPlaceState) {
            const r = this.radius * Math.sqrt(Math.random());
            const theta = Math.random() * 2 * Math.PI;
            const x = this.pivot.position.x + r * Math.cos(theta);
            const z = this.pivot.position.z + r * Math.sin(theta);

            const target = new THREE.Vector3(x, this.pivot.position.y, z);
            const distance = this.pivot.position.distanceToSquared(target);

            let gridCoordinates = worldCoordinatesToGridCoordinates(target.x, target.z);
            let gridValue = getMapGridValueAtGridCoordinates(gridCoordinates.x, gridCoordinates.y);
            if (gridValue) {
                if (gridValue.localPollutionEffect > this.strength) {
                    gridValue.localPollutionEffect -= (this.strength / distance);
                }
                gridValue.localPollutionEffect = THREE.MathUtils.clamp(gridValue.localPollutionEffect, -2, 2);
                gridValue.needsUpdate = true;
            }            
        }
    }
}