import {Globals} from "../../core.js";
import { MapObject } from "../mapObject.js";
import * as THREE from "../../vendor/three.module.js"
import { BufferGeometryUtils } from "../../vendor/utils/BufferGeometryUtils.js";

export class SoilPost extends MapObject {

    name = "soilPost";
    displayName = "Soil Treatment Post";
    icon = "img/icon/icon-soil.png";

    asssignedWorkers = [];

    footprint = [
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ];

    constructor() {
        super();
        this.pivot = new THREE.Group();
        this.usePlacementColours = false;
        
        const originalModel = Globals.assets.find(x => x.name === `soilPost`).contents;
        this.model = originalModel.clone();

        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                this.mesh = child; //TODO
                this.mesh.receiveShadow = true;
                this.mesh.castShadow = true;
                child.userData.uuid = this.uuid;
                Globals.clickableMeshes.push(child);
                
                if (child.material instanceof Array) {
                    for (let i = 0; i < child.material.length; i++) {
                        child.material[i] = child.material[i].clone();
                        child.material[i].userData.originalColour = child.material[i].color.clone();
                        child.material[i].userData.originalEmissive = child.material[i].emissive.clone();
                    }
                } else {
                    child.material = child.material.clone();
                    child.material.userData.originalColour = child.material.color.clone();
                    child.material.userData.originalEmissive = child.material.emissive.clone();
                } 
            }
        });

        this.model.name = "soil_post_model";
        this.model.rotateZ(Math.PI/2);
        this.model.scale.multiplyScalar(1.5);
        
        this.pivot.add(this.model);
    }

    setPlacementBadMaterial() {
        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                if (child.material instanceof Array) {
                    for (let i = 0; i < child.material.length; i++) {
                        child.material[i].color.setHex(0xff2200);
                        child.material[i].emissive.setHex(0xff2200);
                    }
                } else {
                    child.material.color.setHex(0xff2200);
                    child.material.emissive.setHex(0xff2200);
                }                
            }
        });
    }

    setPlacementGoodMaterial() {
        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                if (child.material instanceof Array) {
                    for (let i = 0; i < child.material.length; i++) {
                        child.material[i].color.copy(child.material[i].userData.originalColour);
                        child.material[i].emissive.copy(child.material[i].userData.originalEmissive);
                    }
                } else {
                    child.material.color.copy(child.material.userData.originalColour);
                    child.material.emissive.copy(child.material.userData.originalEmissive);
                }          
            }
        });
    }

    getDisplayHTML() {
        if (this.isPlaceState) {
            return "";
        }
        const displayHTML = document.createElement('div');

        const buildingInfo = document.createElement('p');
        buildingInfo.innerText = "Contains tools for workers and a depot for dug up dirt which cleans and enriches it into healthy soil.";
        const useageInfo = document.createElement('p');
        useageInfo.innerText = "Assign workers here to have them clean up nearby polluted land.";

        displayHTML.appendChild(buildingInfo);
        displayHTML.appendChild(useageInfo);
        return displayHTML;
    }
}