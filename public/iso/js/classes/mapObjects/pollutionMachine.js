import {Globals, worldCoordinatesToGridCoordinates, getMapGridValueAtGridCoordinates} from "../../core.js";
import { MapObject } from "../mapObject.js";
import * as THREE from "../../vendor/three.module.js"
import { BufferGeometryUtils } from "../../vendor/utils/BufferGeometryUtils.js";
import * as SPEModule from "../../vendor/SPE.min.js"

export class PollutionMachine extends MapObject {

    normalColour = 0x938B86;
    name = "pollutionMachine";
    displayName = "Pollution machine";

    radius = 20;
    strength = 1;

    footprint = [
        1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ];

    constructor() {
        super();
        this.pivot = new THREE.Group();
        const mesh1Geometry = new THREE.BoxGeometry( Globals.tileSize * 2, Globals.tileSize * 1, Globals.tileSize * 2 );
        const mesh2Geometry = new THREE.CylinderGeometry( Globals.tileSize * 0.5, Globals.tileSize * 0.5, Globals.tileSize * 4 );

        mesh1Geometry.translate(-(Globals.tileSize * 0.5), (Globals.tileSize * 0.5), -(Globals.tileSize * 0.5));
        mesh2Geometry.translate(-(Globals.tileSize * 0.5), (Globals.tileSize * 2.5), -(Globals.tileSize * 0.5));

        const mergedGeometry = BufferGeometryUtils.mergeBufferGeometries([mesh1Geometry, mesh2Geometry]);

        const meshMaterial = new THREE.MeshLambertMaterial( {color: this.placeColour} );

        this.mesh = new THREE.Mesh( mergedGeometry, meshMaterial );
        this.mesh.receiveShadow = true;
        this.mesh.castShadow = true;

        this.pivot.add(this.mesh);

        this.particleGroup = Globals.particles.get('smoke');
    }

    placeAtCurrentPosition() {
        super.placeAtCurrentPosition();  
        
        let smokeEmitterPosition = this.pivot.position.clone();
        smokeEmitterPosition.y -= Globals.particles.particleYOffset;
        smokeEmitterPosition.y += 5;

        let smokeEmitter = new SPEModule.SPE.Emitter({
            maxAge: { value: 12 },
            position: { 
                value: smokeEmitterPosition,
                spread: new THREE.Vector3( 0.4, 0.5, 0.8 ),
            },
            size: {
                value: [ 16, 64 ],
                spread: [ 0, 1, 2 ]
            },
            acceleration: {
                value: new THREE.Vector3( 0, 0, 0 ),
            },
            rotation: {
                axis: new THREE.Vector3( 0, 1, 0 ),
                spread: new THREE.Vector3( 0, 20, 0 ),
                angle: 100 * Math.PI / 180,
            },
            velocity: {
                value: new THREE.Vector3( 0, 1, -0.5 ),
                spread: new THREE.Vector3( 0.25, 0.5, 0.25 )
            },
            opacity: {
                value: [ 0.3, 0.1, 0 ]
            },
            color: {
                value: [ new THREE.Color( 0x333333 ), new THREE.Color( 0x111111 ) ],
                spread: [ new THREE.Vector3( 0.2, 0.1, 0.1 ), new THREE.Vector3( 0, 0, 0 ) ]
            },
            particleCount: 100,
        });
        this.particleGroup.addEmitter( smokeEmitter );
    }

    onGameTick(delta) {
        super.onGameTick(delta);

        //randomly make the tiles around us more polluted
        if (!this.isPlaceState) {
            const r = this.radius * Math.sqrt(Math.random());
            const theta = Math.random() * 2 * Math.PI;
            const x = this.pivot.position.x + r * Math.cos(theta);
            const z = this.pivot.position.z + r * Math.sin(theta);

            const target = new THREE.Vector3(x, this.pivot.position.y, z);
            const distance = this.pivot.position.distanceToSquared(target);

            let gridCoordinates = worldCoordinatesToGridCoordinates(target.x, target.z);
            let gridValue = getMapGridValueAtGridCoordinates(gridCoordinates.x, gridCoordinates.y);

            if (gridValue) {
                //this isn't a great solution (doesn't work perfectly with other sources) but it's cheap
                if (gridValue.localPollutionEffect < this.strength) {
                    gridValue.localPollutionEffect += (this.strength / distance);
                }       
                gridValue.localPollutionEffect = THREE.MathUtils.clamp(gridValue.localPollutionEffect, -2, 2);
                gridValue.needsUpdate = true;
            }
        }
    }
}