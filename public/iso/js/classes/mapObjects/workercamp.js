import {Globals} from "../../core.js";
import { MapObject } from "../mapObject.js";
import * as THREE from "../../vendor/three.module.js"
import { TestWorker } from "../units/testWorker.js";
import * as HELPER from "../../helper.js";

export class Workercamp extends MapObject {

    name = "workercamp";
    displayName = "Worker camp";

    _weathervane;

    footprint = [
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ];

    constructor() {
        super();
        this.pivot = new THREE.Group();
        this.usePlacementColours = false;
        
        const originalModel = Globals.assets.find(x => x.name === `camp`).contents;
        this.model = originalModel.clone();

        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                this.mesh = child; //TODO
                this.mesh.receiveShadow = true;
                this.mesh.castShadow = true;
                child.userData.uuid = this.uuid;
                Globals.clickableMeshes.push(child);
                if (child.material.name === "emissive") {
                    child.material.emissive.copy(child.material.color);
                }
                child.material.userData.originalColour = child.material.color.clone();
                child.material.userData.originalEmissive = child.material.emissive.clone();
            }
            if (child.name === "weathervane") {
                this._weathervane = child;
            }
        });

        this.model.name = "camp_model";
        this.model.rotateZ(Math.PI/2);
        this.model.scale.multiplyScalar(1.6);
        
        this.pivot.add(this.model);
    }

    setPlacementBadMaterial() {
        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                child.material.color.setHex(0xff2200);
                child.material.emissive.setHex(0xff2200);
            }
        });
    }

    setPlacementGoodMaterial() {
        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                child.material.color.copy(child.material.userData.originalColour);
                child.material.emissive.copy(child.material.userData.originalEmissive);
            }
        });
    }

    onCreateWorker() {
        //make a worker and make them move randomly out of the building they're in initially
        let newWorker = new TestWorker();
        newWorker.initialize(this.pivot.position.x, this.pivot.position.y, this.pivot.position.z - 1.2);
        const targetX = this.pivot.position.x + HELPER.getRandomInt(1, 5);
        const targetZ = this.pivot.position.z + HELPER.getRandomInt(-5, 5);
        newWorker.issueMoveCommand(targetX, targetZ, false);
    }

    getDisplayHTML() {
        if (this.isPlaceState) {
            return "";
        }
        const button = document.createElement('button');
        button.innerHTML = "Create worker";
        button.addEventListener('click', this.onCreateWorker.bind(this));
        return button;
    }

    onGameTick(delta) {
        super.onGameTick();
        if (!this.isPlaceState) {
            //animate the weathervane lol
            this._weathervane.rotateZ(0.01);
        }        
    }
}