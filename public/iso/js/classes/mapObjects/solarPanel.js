import {Globals, worldCoordinatesToGridCoordinates, gridCoordinatesToWorldCoordinates, getNearestFreeMapGridPoint, getMapGridValueAtGridCoordinates} from "../../core.js";
import { MapObject } from "../mapObject.js";
import * as THREE from "../../vendor/three.module.js"
import * as HELPER from "../../helper.js";

export class SolarPanel extends MapObject {

    name = "SolarPanel";
    displayName = "Solar Panel";
    icon = "img/icon/icon-solar.png";

    contributesPower = true;

    maxPower = 4; //kilowatts
    minPower = 2;
    powerOutput = 0;
    efficiency = 4; //sun intensity -> power conversion

    _panel;
    _pathfinder;
    _powerCable;

    footprint = [
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ];

    constructor() {
        super();
        this.pivot = new THREE.Group();
        this.usePlacementColours = false;
        
        const originalModel = Globals.assets.find(x => x.name === `solarPanel`).contents;
        this.model = originalModel.clone();

        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                this.mesh = child; //TODO
                this.mesh.receiveShadow = true;
                this.mesh.castShadow = true;
                child.userData.uuid = this.uuid;
                Globals.clickableMeshes.push(child);
                if (child.material instanceof Array) {
                    for (let i = 0; i < child.material.length; i++) {
                        child.material[i] = child.material[i].clone();
                        child.material[i].userData.originalColour = child.material[i].color.clone();
                        child.material[i].userData.originalEmissive = child.material[i].emissive.clone();
                    }
                } else {
                    child.material = child.material.clone();
                    child.material.userData.originalColour = child.material.color.clone();
                    child.material.userData.originalEmissive = child.material.emissive.clone();
                }   
            }
            if (child.name === "panel") {
                this._panel = child;
            }
        });

        this.model.name = "solar_panel_model";
        this.model.rotateZ(Math.PI/2);
        this.model.position.y += 1;
        this.model.scale.multiplyScalar(1.6);
        
        this.pivot.add(this.model);

        this._pathfinder = new PF.AStarFinder({
            allowDiagonal: true,
            dontCrossCorners: true
        });
    }

    destroy() {        
        if (this._powerCable) {
            this._powerCable.geometry.dispose();
            this._powerCable.material.dispose();
            Globals.scene.remove( this._powerCable );
        }        
        super.destroy();
    }

    setPlacementBadMaterial() {
        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                if (child.material instanceof Array) {
                    for (let i = 0; i < child.material.length; i++) {
                        child.material[i].color.setHex(0xff2200);
                        child.material[i].emissive.setHex(0xff2200);
                    }
                } else {
                    child.material.color.setHex(0xff2200);
                    child.material.emissive.setHex(0xff2200);
                }                
            }
        });
    }

    setPlacementGoodMaterial() {
        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                if (child.material instanceof Array) {
                    for (let i = 0; i < child.material.length; i++) {
                        child.material[i].color.copy(child.material[i].userData.originalColour);
                        child.material[i].emissive.copy(child.material[i].userData.originalEmissive);
                    }
                } else {
                    child.material.color.copy(child.material.userData.originalColour);
                    child.material.emissive.copy(child.material.userData.originalEmissive);
                }          
            }
        });
    }

    getDisplayHTML() {
        if (this.isPlaceState) {
            return "";
        }
        const displayHTML = document.createElement('div');

        const buildingInfo = document.createElement('p');
        buildingInfo.innerText = "Cutting edge high efficiency solar cells with integrated capacitors";
        const powerOutputInfo = document.createElement('p');
        powerOutputInfo.innerText = `Current power output: ${this.powerOutput.toFixed(2)}kW`;

        displayHTML.appendChild(buildingInfo);
        displayHTML.appendChild(powerOutputInfo);
        return displayHTML;
    }

    placeAtCurrentPosition() {
        super.placeAtCurrentPosition();
        this.buildCable();
    }

    buildCable() {
        let target = null;
        Globals.gameObjects.forEach(obj => {
            if (obj.name === "basecamp") {
                target = obj;
                return;
            }
        });
        const tempGrid = Globals.pathfindingGrid.clone();
        const cA = worldCoordinatesToGridCoordinates(this.pivot.position.x, this.pivot.position.z);
        let cB = worldCoordinatesToGridCoordinates(target.pivot.position.x, target.pivot.position.z);
        if (!tempGrid.isWalkableAt(cB.x, cB.y)) {
            cB = getNearestFreeMapGridPoint(cB.x, cB.y);
            if (!cB) {
                //something went wrong?
                console.warn("Couldn't find a free point around target to pathfind to", this);
                return;
            }
        } 
        const path = this._pathfinder.findPath(cA.x, cA.y, cB.x, cB.y, tempGrid);
        let cablePoints = [];
        for (let i = 0; i < path.length - 1; i++) {
            const pathWorldCoordinates = gridCoordinatesToWorldCoordinates(path[i][0], path[i][1]);
            const gridValue = getMapGridValueAtGridCoordinates(path[i][0], path[i][1]);
            cablePoints.push(new THREE.Vector3(pathWorldCoordinates.x, gridValue.height, pathWorldCoordinates.y));
        }
        //manually tweak the cable points start and end a little
        cablePoints[cablePoints.length - 1].x = target.pivot.position.x - 1;
        cablePoints[cablePoints.length - 1].z = target.pivot.position.z - 1;
        const curve = new THREE.CatmullRomCurve3( cablePoints );
        const cableGeometry = new THREE.TubeGeometry( curve, 16, 0.1, 3, false );
        const cableMaterial = new THREE.MeshLambertMaterial( { color: 0x5A9175 } );
        this._powerCable = new THREE.Mesh( cableGeometry, cableMaterial );
        this._powerCable.receiveShadow = true;
        this._powerCable.castShadow = true;
        Globals.scene.add( this._powerCable );
    }

    getPowerEffect() {
        return this.powerOutput;
    }

    onGameTick(delta) {
        super.onGameTick();
        if (!this.isPlaceState) {

            //rotate to the sun direction if not already
            if (!this._panel.quaternion.equals(Globals.weather.sunDirection)) {
                this._panel.quaternion.slerp(Globals.weather.sunDirection, 0.001);
            }

            const newPowerLevel = this.calculatePowerOutput();
            if (this.isSelected && newPowerLevel !== this.powerOutput) {
                Globals.uiController.needsUpdate = true;
            }
            this.powerOutput = newPowerLevel;
        }
    }

    calculatePowerOutput(windStrength) {
        return THREE.MathUtils.clamp(Globals.weather.sun.intensity * this.efficiency, this.minPower, this.maxPower);
    }
}