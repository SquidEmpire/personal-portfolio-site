import {Globals} from "../../core.js";
import * as Helper from "../../helper.js";
import { MapObject } from "../mapObject.js";
import * as THREE from "../../vendor/three.module.js"

export class PineTree extends MapObject {

    name = "tree";
    displayName = "Pine tree";
    icon = "img/icon/icon-tree.png";
    knockedOver = false;
    remainingWood = 100;

    footprint = [
        1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ];
    constructor() {
        super();

        this.pivot = new THREE.Group();
        const randomIdex = Helper.getRandomInt(1, 5);
        const originalModel = Globals.assets.find(x => x.name === `pine${randomIdex}`).contents;
        this.model = originalModel.clone();

        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                //there must be only one
                this.mesh = child;
                this.mesh.receiveShadow = true;
                this.mesh.castShadow = true;
                this.mesh.name = "mapObject_mesh";

                //quick hack because the default material on the placeholder model is way too shiney
                for (let i = 0; i < child.material.length; i++) {
                    child.material[i].shininess = 0;
                }
            }
        } );

        this.model.name = "pine_model";
        this.model.scale.multiplyScalar(0.03);
        this.model.rotateY(Math.random()*Math.PI*2);
        
        this.pivot.add(this.model);
    }

    extractWood(effort) {
        if (this.knockedOver) {
           this.remainingWood -= effort;
           if (this.isSelected) {
                Globals.uiController.needsUpdate = true;
            }
           //TODO: some sort of chop animation/sound here pls
           console.log("Chop!");
           this.model.rotateY(Math.random()*Math.PI*2); //rudimentory aniation
           if (this.remainingWood > 0) {
                return effort;
           } else {
                //this tree has been used up, destroy it and return any remaining wood
                this.destroy();
                return this.remainingWood + effort;
           }
        } else {
            //knock this sucker down
            this.model.rotateZ(Math.PI/2);
            this.model.rotateY(Math.random()*Math.PI*2);
            this.model.position.y += 0.5;
            this.knockedOver = true;
        }
    }

    getDisplayHTML() {
        return `<p>Material: ${this.remainingWood}</p>`;
    }
}