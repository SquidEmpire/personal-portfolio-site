import {Globals} from "../../core.js";
import { MapObject } from "../mapObject.js";
import * as THREE from "../../vendor/three.module.js"

export class WaterTank extends MapObject {

    name = "WaterTank";
    displayName = "Water Treatment Tank";
    icon = "img/icon/icon-tank.png";

    contributesGlobalWaterPollution = true;
    contributesPower = true;

    powerOutput = -4;

    _globalWaterCleanupEffect = -0.4;
    _cleanupDelay = 300000; //slow warm up
    _timeSpentCleaning = 0;
    _currentGlobalWaterCleanupEffect = 0;

    footprint = [
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ];

    constructor() {
        super();
        this.pivot = new THREE.Group();
        this.usePlacementColours = false;
        
        const originalModel = Globals.assets.find(x => x.name === `waterTank`).contents;
        this.model = originalModel.clone();

        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                this.mesh = child; //TODO
                this.mesh.receiveShadow = true;
                this.mesh.castShadow = true;
                child.userData.uuid = this.uuid;
                Globals.clickableMeshes.push(child);
                if (child.material instanceof Array) {
                    for (let i = 0; i < child.material.length; i++) {
                        child.material[i] = child.material[i].clone();
                        child.material[i].userData.originalColour = child.material[i].color.clone();
                        child.material[i].userData.originalEmissive = child.material[i].emissive.clone();
                    }
                } else {
                    child.material = child.material.clone();
                    child.material.userData.originalColour = child.material.color.clone();
                    child.material.userData.originalEmissive = child.material.emissive.clone();
                }   
            }
        });

        this.model.name = "water_tank_model";
        this.model.rotateZ(Math.PI/2);
        this.model.scale.multiplyScalar(1.6);
        
        this.pivot.add(this.model);
    }

    setPlacementBadMaterial() {
        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                if (child.material instanceof Array) {
                    for (let i = 0; i < child.material.length; i++) {
                        child.material[i].color.setHex(0xff2200);
                        child.material[i].emissive.setHex(0xff2200);
                    }
                } else {
                    child.material.color.setHex(0xff2200);
                    child.material.emissive.setHex(0xff2200);
                }                
            }
        });
    }

    setPlacementGoodMaterial() {
        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                if (child.material instanceof Array) {
                    for (let i = 0; i < child.material.length; i++) {
                        child.material[i].color.copy(child.material[i].userData.originalColour);
                        child.material[i].emissive.copy(child.material[i].userData.originalEmissive);
                    }
                } else {
                    child.material.color.copy(child.material.userData.originalColour);
                    child.material.emissive.copy(child.material.userData.originalEmissive);
                }          
            }
        });
    }

    getDisplayHTML() {
        if (this.isPlaceState) {
            return "";
        }
        const displayHTML = document.createElement('div');

        const buildingInfo = document.createElement('p');
        buildingInfo.innerText = "This tank contains sludge biotreaters that clean polluted water before releaseing it back into the water table.";
        const powerOutputInfo = document.createElement('p');
        powerOutputInfo.innerText = `Current power consumption: ${Math.abs(this.powerOutput.toFixed(2))}kW`; 

        displayHTML.appendChild(buildingInfo);
        displayHTML.appendChild(powerOutputInfo);
        return displayHTML;
    }

    getPowerEffect() {
        if (!this.isPlaceState) {
            return this.powerOutput;
        }
        return 0;
    }

    getGlobalWaterPollutionEffect() {
        if (!this.isPlaceState) {
            return this._currentGlobalWaterCleanupEffect;
        }
        return 0;
    }

    onGameTick(delta) {
        super.onGameTick(delta);
        if (!this.isPlaceState) {
            if (this._timeSpentCleaning !== this._cleanupDelay) {
                let lerpFactor = this._timeSpentCleaning / this._cleanupDelay;
                this._currentGlobalWaterCleanupEffect = THREE.MathUtils.lerp(this._currentGlobalWaterCleanupEffect, this._globalWaterCleanupEffect, lerpFactor);
                this._timeSpentCleaning++;
            }        
        }
    }
}