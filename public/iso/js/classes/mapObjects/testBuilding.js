import {Globals} from "../../core.js";
import { MapObject } from "../mapObject.js";
import * as THREE from "../../vendor/three.module.js"

export class TestBuilding extends MapObject {

    colour2 = 0xaaffcc;
    name = "testBuilding";

    footprint = [
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ];
    constructor() {
        super();
        this.pivot = new THREE.Group();
        const mesh1Geometry = new THREE.BoxGeometry( Globals.tileSize * 4, Globals.tileSize, Globals.tileSize );
        const mesh2Geometry = new THREE.BoxGeometry( Globals.tileSize, Globals.tileSize, Globals.tileSize * 2 );
        const mesh1Material = new THREE.MeshBasicMaterial( {color: this.placeColour} );
        const mesh2Material = new THREE.MeshBasicMaterial( {color: this.colour2} );
        this.mesh = new THREE.Mesh( mesh1Geometry, mesh1Material );
        this.mesh2 = new THREE.Mesh( mesh2Geometry, mesh2Material );
        this.mesh.receiveShadow = true;
        this.mesh.castShadow = true;
        this.mesh2.receiveShadow = true;
        this.mesh2.castShadow = true;

        this.pivot.add(this.mesh);
        this.pivot.add(this.mesh2);
        this.mesh.position.y += (Globals.tileSize/2);
        this.mesh.position.x -= (Globals.tileSize * 2) - (Globals.tileSize / 2);
        this.mesh2.position.y += (Globals.tileSize/2);
        this.mesh2.position.z -= (Globals.tileSize * 2) - (Globals.tileSize / 2);
    }
}