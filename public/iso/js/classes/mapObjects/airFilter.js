import {Globals, worldCoordinatesToGridCoordinates, gridCoordinatesToWorldCoordinates, getNearestFreeMapGridPoint, getMapGridValueAtGridCoordinates} from "../../core.js";
import { MapObject } from "../mapObject.js";
import * as THREE from "../../vendor/three.module.js"
import * as SPEModule from "../../vendor/SPE.min.js"

export class AirFilter extends MapObject {

    name = "AirFilter";
    displayName = "Air Filter Stack";
    icon = "img/icon/icon-filter.png";

    contributesGlobalAirPollution = true;
    contributesPower = true;

    powerOutput = -2;

    _modelXOffset = -1;
    _modelZOffset = -1;

    _globalAirCleanupEffect = -0.2;
    _cleanupDelay = 600000; //very slow warm up
    _timeSpentCleaning = 0;
    _currentGlobalAirCleanupEffect = 0;

    footprint = [
        1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ];

    constructor() {
        super();
        this.pivot = new THREE.Group();
        this.usePlacementColours = false;
        
        const originalModel = Globals.assets.find(x => x.name === `airFilter`).contents;
        this.model = originalModel.clone();

        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                this.mesh = child; //TODO
                this.mesh.receiveShadow = true;
                this.mesh.castShadow = true;
                child.userData.uuid = this.uuid;
                Globals.clickableMeshes.push(child);
                if (child.material instanceof Array) {
                    for (let i = 0; i < child.material.length; i++) {
                        child.material[i] = child.material[i].clone();
                        child.material[i].userData.originalColour = child.material[i].color.clone();
                        child.material[i].userData.originalEmissive = child.material[i].emissive.clone();
                    }
                } else {
                    child.material = child.material.clone();
                    child.material.userData.originalColour = child.material.color.clone();
                    child.material.userData.originalEmissive = child.material.emissive.clone();
                }   
            }
        });

        this.model.name = "air_filter_model";
        this.model.rotateZ(Math.PI/1.6);
        this.model.scale.multiplyScalar(1.6);
        this.model.position.x += this._modelXOffset;
        this.model.position.z += this._modelZOffset;
        
        this.pivot.add(this.model);
    }

    setPlacementBadMaterial() {
        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                if (child.material instanceof Array) {
                    for (let i = 0; i < child.material.length; i++) {
                        child.material[i].color.setHex(0xff2200);
                        child.material[i].emissive.setHex(0xff2200);
                    }
                } else {
                    child.material.color.setHex(0xff2200);
                    child.material.emissive.setHex(0xff2200);
                }                
            }
        });
    }

    setPlacementGoodMaterial() {
        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                if (child.material instanceof Array) {
                    for (let i = 0; i < child.material.length; i++) {
                        child.material[i].color.copy(child.material[i].userData.originalColour);
                        child.material[i].emissive.copy(child.material[i].userData.originalEmissive);
                    }
                } else {
                    child.material.color.copy(child.material.userData.originalColour);
                    child.material.emissive.copy(child.material.userData.originalEmissive);
                }          
            }
        });
    }

    getDisplayHTML() {
        if (this.isPlaceState) {
            return "";
        }
        const displayHTML = document.createElement('div');

        const buildingInfo = document.createElement('p');
        buildingInfo.innerText = "This tower uses electrostatic precipitators and thermal buffer traps that capture and remove contaminants from the air";
        const powerOutputInfo = document.createElement('p');
        powerOutputInfo.innerText = `Current power consumption: ${Math.abs(this.powerOutput.toFixed(2))}kW`; 

        displayHTML.appendChild(buildingInfo);
        displayHTML.appendChild(powerOutputInfo);
        return displayHTML;
    }

    getPowerEffect() {
        if (!this.isPlaceState) {
            return this.powerOutput;
        }
        return 0;
    }

    getGlobalAirPollutionEffect() {
        if (!this.isPlaceState) {
            return this._currentGlobalAirCleanupEffect;
        }
        return 0;
    }

    onGameTick(delta) {
        super.onGameTick(delta);
        if (!this.isPlaceState) {
            if (this._timeSpentCleaning !== this._cleanupDelay) {
                let lerpFactor = this._timeSpentCleaning / this._cleanupDelay;
                this._currentGlobalAirCleanupEffect = THREE.MathUtils.lerp(this._currentGlobalAirCleanupEffect, this._globalAirCleanupEffect, lerpFactor);
                this._timeSpentCleaning++;
            }        
        }
    }
}