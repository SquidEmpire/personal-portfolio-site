import {Globals} from "../../core.js";
import { MapObject } from "../mapObject.js";
import * as THREE from "../../vendor/three.module.js"

export class TestBuildingB extends MapObject {

    colour2 = 0xaaffcc;
    name = "testBuilding";
    displayName = "Test building B";

    footprint = [
        1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ];
    constructor() {
        super();
        this.pivot = new THREE.Group();
        const mesh1Geometry = new THREE.BoxGeometry( Globals.tileSize, Globals.tileSize * 5, Globals.tileSize );
        const mesh2Geometry = new THREE.BoxGeometry( Globals.tileSize * 4, Globals.tileSize * 2, Globals.tileSize * 4 );

        const meshMaterial1 = new THREE.MeshLambertMaterial( {color: this.placeColour} );
        const meshMaterial2 = new THREE.MeshLambertMaterial( {color: this.colour2} );

        this.mesh1 = new THREE.Mesh( mesh1Geometry, meshMaterial1 );
        this.mesh2 = new THREE.Mesh( mesh1Geometry, meshMaterial1 );
        this.mesh3 = new THREE.Mesh( mesh1Geometry, meshMaterial1 );
        this.mesh4 = new THREE.Mesh( mesh1Geometry, meshMaterial1 );
        this.mesh5 = new THREE.Mesh( mesh2Geometry, meshMaterial2 );
        this.mesh1.receiveShadow = true;
        this.mesh1.castShadow = true;
        this.mesh2.receiveShadow = true;
        this.mesh2.castShadow = true;
        this.mesh3.receiveShadow = true;
        this.mesh3.castShadow = true;
        this.mesh4.receiveShadow = true;
        this.mesh4.castShadow = true;
        this.mesh5.receiveShadow = true;
        this.mesh5.castShadow = true;

        this.pivot.add(this.mesh1);
        this.pivot.add(this.mesh2);
        this.pivot.add(this.mesh3);
        this.pivot.add(this.mesh4);
        this.pivot.add(this.mesh5);

        this.mesh1.position.y += (Globals.tileSize * 2.5);
        this.mesh2.position.y += (Globals.tileSize * 2.5);
        this.mesh2.position.x -= (Globals.tileSize * 3);
        this.mesh3.position.y += (Globals.tileSize * 2.5);
        this.mesh3.position.z -= (Globals.tileSize * 3);
        this.mesh4.position.y += (Globals.tileSize * 2.5);
        this.mesh4.position.x -= (Globals.tileSize * 3);
        this.mesh4.position.z -= (Globals.tileSize * 3);

        this.mesh5.position.x -= (Globals.tileSize * 1.5);
        this.mesh5.position.y += (Globals.tileSize * 6);
        this.mesh5.position.z -= (Globals.tileSize * 1.5);
    }
}