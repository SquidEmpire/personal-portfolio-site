import {Globals} from "../../core.js";
import { MapObject } from "../mapObject.js";
import * as THREE from "../../vendor/three.module.js"

export class TestBuildingA extends MapObject {

    colour2 = 0xaaffcc;
    name = "testBuilding";
    displayName = "Test building A";

    footprint = [
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ];
    constructor() {
        super();
        this.pivot = new THREE.Group();
        const mesh1Geometry = new THREE.BoxGeometry( Globals.tileSize * 4, Globals.tileSize, Globals.tileSize );
        const mesh2Geometry = new THREE.BoxGeometry( Globals.tileSize, Globals.tileSize, Globals.tileSize * 2 );
        const mesh1Material = new THREE.MeshLambertMaterial( {color: this.placeColour} );
        const mesh2Material = new THREE.MeshLambertMaterial( {color: this.colour2} );
        this.mesh = new THREE.Mesh( mesh1Geometry, mesh1Material );
        this.mesh2 = new THREE.Mesh( mesh2Geometry, mesh2Material );
        this.mesh.receiveShadow = true;
        this.mesh.castShadow = true;
        this.mesh2.receiveShadow = true;
        this.mesh2.castShadow = true;

        this.pivot.add(this.mesh);
        this.pivot.add(this.mesh2);
        this.mesh.position.y += (Globals.tileSize * 0.5);
        this.mesh.position.x -= (Globals.tileSize * 1.5);
        this.mesh2.position.y += (Globals.tileSize * 0.5);
        this.mesh2.position.z -= (Globals.tileSize * 1.5);
    }
}