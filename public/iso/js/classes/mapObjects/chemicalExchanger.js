import {Globals, worldCoordinatesToGridCoordinates, gridCoordinatesToWorldCoordinates, getNearestFreeMapGridPoint, getMapGridValueAtGridCoordinates} from "../../core.js";
import { MapObject } from "../mapObject.js";
import * as THREE from "../../vendor/three.module.js"
import * as SPEModule from "../../vendor/SPE.min.js"

export class ChemicalExchanger extends MapObject {

    name = "ChemicalExchanger";
    displayName = "Chemcial Exchanger";
    icon = "img/icon/icon-exchanger.png";

    contributesPower = true;
    particleGroup;

    maxPower = 15.0; //kilowatts
    powerOutput = 0;
    minPower = 0.0;
    efficiency = 7; //modifier for land pollution -> power

    landCleanupStrength = 2;
    landCleanupRadius = 10;

    _powerGeneratedSinceLastCheck = 0;

    _pathfinder;
    _powerCable;
    _particleEmitter;

    _modelXOffset = -4;
    _modelZOffset = -3;

    footprint = [
        0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ];

    constructor() {
        super();
        this.pivot = new THREE.Group();
        this.usePlacementColours = false;
        
        const originalModel = Globals.assets.find(x => x.name === `chemicalExchanger`).contents;
        this.model = originalModel.clone();

        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                this.mesh = child; //TODO
                this.mesh.receiveShadow = true;
                this.mesh.castShadow = true;
                child.userData.uuid = this.uuid;
                Globals.clickableMeshes.push(child);
                if (child.material instanceof Array) {
                    for (let i = 0; i < child.material.length; i++) {
                        child.material[i] = child.material[i].clone();
                        child.material[i].userData.originalColour = child.material[i].color.clone();
                        child.material[i].userData.originalEmissive = child.material[i].emissive.clone();
                    }
                } else {
                    child.material = child.material.clone();
                    child.material.userData.originalColour = child.material.color.clone();
                    child.material.userData.originalEmissive = child.material.emissive.clone();
                }   
            }
        });

        this.model.name = "chemical_exchanger_model";
        this.model.rotateZ(Math.PI/1.6);
        this.model.scale.multiplyScalar(1.6);
        this.model.position.x += this._modelXOffset;
        this.model.position.z += this._modelZOffset;
        
        this.pivot.add(this.model);

        this._pathfinder = new PF.AStarFinder({
            allowDiagonal: true,
            dontCrossCorners: true
        });

        this.particleGroup = Globals.particles.get('smoke');
    }

    placeAtCurrentPosition() {
        super.placeAtCurrentPosition();  
        
        let smokeEmitterPosition = this.pivot.position.clone();
        smokeEmitterPosition.y -= Globals.particles.particleYOffset;
        smokeEmitterPosition.y += 14;
        smokeEmitterPosition.x += this._modelXOffset - 0.4;
        smokeEmitterPosition.z += this._modelZOffset;

        this._particleEmitter = new SPEModule.SPE.Emitter({
            maxAge: { value: 6 },
            position: { 
                value: smokeEmitterPosition,
                spread: new THREE.Vector3( 0.1, 0.1, 0.1 ),
            },
            size: {
                value: [ 8, 96 ],
                spread: [ 0, 1, 2 ]
            },
            angle: {
                value: [0, 0, 0],
                spread: [ 10, 10, 10 ],
            },
            velocity: {
                value: new THREE.Vector3( 0, 1, -1 ),
                spread: new THREE.Vector3( 0.2, 0.2, 0.2 )
            },
            opacity: {
                value: [ 0.5, 0.7, 0 ]
            },
            color: {
                value: new THREE.Color( 0xFFFFFF ),
            },
            particleCount: 16,
        });
        this.particleGroup.addEmitter( this._particleEmitter );

        this.buildCable();
    }

    destroy() {        
        if (this._powerCable) {
            this._powerCable.geometry.dispose();
            this._powerCable.material.dispose();
            Globals.scene.remove( this._powerCable );
        }
        if (this._particleEmitter) {
            this._particleGroup.removeEmitter(this._particleEmitter);
        }
        super.destroy();
    }

    setPlacementBadMaterial() {
        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                if (child.material instanceof Array) {
                    for (let i = 0; i < child.material.length; i++) {
                        child.material[i].color.setHex(0xff2200);
                        child.material[i].emissive.setHex(0xff2200);
                    }
                } else {
                    child.material.color.setHex(0xff2200);
                    child.material.emissive.setHex(0xff2200);
                }                
            }
        });
    }

    setPlacementGoodMaterial() {
        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                if (child.material instanceof Array) {
                    for (let i = 0; i < child.material.length; i++) {
                        child.material[i].color.copy(child.material[i].userData.originalColour);
                        child.material[i].emissive.copy(child.material[i].userData.originalEmissive);
                    }
                } else {
                    child.material.color.copy(child.material.userData.originalColour);
                    child.material.emissive.copy(child.material.userData.originalEmissive);
                }          
            }
        });
    }

    getDisplayHTML() {
        if (this.isPlaceState) {
            return "";
        }
        const displayHTML = document.createElement('div');

        const buildingInfo = document.createElement('p');
        buildingInfo.innerText = "Using molten salt oxidation this device turns chemical and radiological contaminants in the soil into usable power while simultaneously purging them";
        const powerOutputInfo = document.createElement('p');
        powerOutputInfo.innerText = `Current power output: ${this.powerOutput.toFixed(2)}kW`;

        displayHTML.appendChild(buildingInfo);
        displayHTML.appendChild(powerOutputInfo);
        return displayHTML;
    }

    buildCable() {
        let target = null;
        Globals.gameObjects.forEach(obj => {
            if (obj.name === "basecamp") {
                target = obj;
                return;
            }
        });
        const tempGrid = Globals.pathfindingGrid.clone();
        const cA = worldCoordinatesToGridCoordinates(this.pivot.position.x + this._modelXOffset, this.pivot.position.z + this._modelZOffset);
        let cB = worldCoordinatesToGridCoordinates(target.pivot.position.x, target.pivot.position.z);
        if (!tempGrid.isWalkableAt(cB.x, cB.y)) {
            cB = getNearestFreeMapGridPoint(cB.x, cB.y);
            if (!cB) {
                //something went wrong?
                console.warn("Couldn't find a free point around target to pathfind to", this);
                return;
            }
        } 
        const path = this._pathfinder.findPath(cA.x, cA.y, cB.x, cB.y, tempGrid);
        let cablePoints = [];
        for (let i = 0; i < path.length - 1; i++) {
            const pathWorldCoordinates = gridCoordinatesToWorldCoordinates(path[i][0], path[i][1]);
            const gridValue = getMapGridValueAtGridCoordinates(path[i][0], path[i][1]);
            cablePoints.push(new THREE.Vector3(pathWorldCoordinates.x, gridValue.height, pathWorldCoordinates.y));
        }
        //manually tweak the cable points start and end a little
        cablePoints[cablePoints.length - 1].x = target.pivot.position.x - 1;
        cablePoints[cablePoints.length - 1].z = target.pivot.position.z - 1;
        const curve = new THREE.CatmullRomCurve3( cablePoints );
        const cableGeometry = new THREE.TubeGeometry( curve, 16, 0.1, 3, false );
        const cableMaterial = new THREE.MeshLambertMaterial( { color: 0x5A9175 } );
        this._powerCable = new THREE.Mesh( cableGeometry, cableMaterial );
        this._powerCable.receiveShadow = true;
        this._powerCable.castShadow = true;
        Globals.scene.add( this._powerCable );
    }

    getPowerEffect() {
        if (!this.isPlaceState) {
            this.powerOutput = THREE.Math.lerp(this.powerOutput, this._powerGeneratedSinceLastCheck, 0.1); //smoothing
            this.powerOutput = THREE.MathUtils.clamp(this.powerOutput, this.minPower, this.maxPower);
            this._powerGeneratedSinceLastCheck = 0;
            if (this.isSelected) {
                Globals.uiController.needsUpdate = true;
            }        
            return this.powerOutput;
        }
        return 0;
    }

    onGameTick(delta) {
        super.onGameTick(delta);
        //randomly pick a square around us to see if we can clean it up
        if (!this.isPlaceState) {
            const r = this.landCleanupRadius * Math.sqrt(Math.random());
            const theta = Math.random() * 2 * Math.PI;
            const x = this.pivot.position.x + r * Math.cos(theta);
            const z = this.pivot.position.z + r * Math.sin(theta);

            const target = new THREE.Vector3(x + this._modelXOffset, this.pivot.position.y, z + this._modelZOffset);
            const distance = this.pivot.position.distanceToSquared(target);

            let gridCoordinates = worldCoordinatesToGridCoordinates(target.x, target.z);
            if (gridCoordinates) {
                let gridValue = getMapGridValueAtGridCoordinates(gridCoordinates.x, gridCoordinates.y);
                if (gridValue) {
                    const originalPollutionLevel = gridValue.localPollutionEffect;
                    if (originalPollutionLevel > 0) {
                        gridValue.localPollutionEffect -= (this.landCleanupStrength / distance);
                        gridValue.localPollutionEffect = THREE.MathUtils.clamp(gridValue.localPollutionEffect, -2, 2);
                        gridValue.needsUpdate = true;

                        const effect = originalPollutionLevel - gridValue.localPollutionEffect;
                        this._powerGeneratedSinceLastCheck += effect * this.efficiency;
                    }
                }    
            }        
        }
    }
}