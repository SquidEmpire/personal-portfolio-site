import {Globals, worldCoordinatesToGridCoordinates, gridCoordinatesToWorldCoordinates, getNearestFreeMapGridPoint, getMapGridValueAtGridCoordinates} from "../../core.js";
import { MapObject } from "../mapObject.js";
import * as THREE from "../../vendor/three.module.js"
import * as HELPER from "../../helper.js";
import * as SPEModule from "../../vendor/SPE.min.js"

export class DieselGenerator extends MapObject {

    name = "DieselGenerator";
    displayName = "Diesel Generator";
    icon = "img/icon/icon-generator.png";

    contributesPower = true;
    particleGroup;

    powerOutput = 5; //kilowatts

    contributesGlobalAirPollution = true;
    contributesLandPollution = true;

    _globalAirPollutionEffect = 0.15;
    _pollutionDelay = 500000; //for easing in the effects
    _timeSpentPolluting = 0;
    _currentGlobalAirPollutionEffect = 0;

    landPollutionStrength = 0.4;
    landPollutionRadius = 20;

    _pathfinder;
    _powerCable;
    _particleEmitter;

    footprint = [
        1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ];

    constructor() {
        super();
        this.pivot = new THREE.Group();
        this.usePlacementColours = false;
        
        const originalModel = Globals.assets.find(x => x.name === `dieselGenerator`).contents;
        this.model = originalModel.clone();

        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                this.mesh = child; //TODO
                this.mesh.receiveShadow = true;
                this.mesh.castShadow = true;
                child.userData.uuid = this.uuid;
                Globals.clickableMeshes.push(child);
                if (child.material instanceof Array) {
                    for (let i = 0; i < child.material.length; i++) {
                        child.material[i] = child.material[i].clone();
                        child.material[i].userData.originalColour = child.material[i].color.clone();
                        child.material[i].userData.originalEmissive = child.material[i].emissive.clone();
                    }
                } else {
                    child.material = child.material.clone();
                    child.material.userData.originalColour = child.material.color.clone();
                    child.material.userData.originalEmissive = child.material.emissive.clone();
                }   
            }
        });

        this.model.name = "diesel_generator_model";
        //this.model.rotateZ(Math.PI/2);
        this.model.position.x -= 2;
        this.model.scale.multiplyScalar(1.6);
        
        this.pivot.add(this.model);

        this._pathfinder = new PF.AStarFinder({
            allowDiagonal: true,
            dontCrossCorners: true
        });

        this.particleGroup = Globals.particles.get('smoke');
    }

    placeAtCurrentPosition() {
        super.placeAtCurrentPosition();  
        
        let smokeEmitterPosition = this.pivot.position.clone();
        smokeEmitterPosition.y -= Globals.particles.particleYOffset;
        smokeEmitterPosition.y += 5.5;
        smokeEmitterPosition.z -= 4.4;

        this._particleEmitter = new SPEModule.SPE.Emitter({
            maxAge: { value: 12 },
            position: { 
                value: smokeEmitterPosition,
                spread: new THREE.Vector3( 0.1, 0.1, 0.1 ),
            },
            size: {
                value: [ 8, 128 ],
                spread: [ 0, 1, 2 ]
            },
            rotation: {
                axis: new THREE.Vector3( 0, 1, 0 ),
                spread: new THREE.Vector3( 0, 20, 0 ),
                angle: 100 * Math.PI / 180,
            },
            velocity: {
                value: new THREE.Vector3( 0, 1, -0.5 ),
                spread: new THREE.Vector3( 0.25, 0.5, 0.25 )
            },
            opacity: {
                value: [ 0.1, 0.5, 0 ]
            },
            color: {
                value: [ new THREE.Color( 0x333333 ), new THREE.Color( 0x111111 ) ],
                spread: [ new THREE.Vector3( 0.2, 0.1, 0.1 ), new THREE.Vector3( 0, 0, 0 ) ]
            },
            particleCount: 16,
        });
        this.particleGroup.addEmitter( this._particleEmitter );

        this.buildCable();
    }

    destroy() {        
        if (this._powerCable) {
            this._powerCable.geometry.dispose();
            this._powerCable.material.dispose();
            Globals.scene.remove( this._powerCable );
        }
        if (this._particleEmitter) {
            this._particleGroup.removeEmitter(this._particleEmitter);
        }
        super.destroy();
    }

    setPlacementBadMaterial() {
        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                if (child.material instanceof Array) {
                    for (let i = 0; i < child.material.length; i++) {
                        child.material[i].color.setHex(0xff2200);
                        child.material[i].emissive.setHex(0xff2200);
                    }
                } else {
                    child.material.color.setHex(0xff2200);
                    child.material.emissive.setHex(0xff2200);
                }                
            }
        });
    }

    setPlacementGoodMaterial() {
        this.model.traverse( ( child ) => {
            if ( child.isMesh ) {
                if (child.material instanceof Array) {
                    for (let i = 0; i < child.material.length; i++) {
                        child.material[i].color.copy(child.material[i].userData.originalColour);
                        child.material[i].emissive.copy(child.material[i].userData.originalEmissive);
                    }
                } else {
                    child.material.color.copy(child.material.userData.originalColour);
                    child.material.emissive.copy(child.material.userData.originalEmissive);
                }          
            }
        });
    }

    getDisplayHTML() {
        if (this.isPlaceState) {
            return "";
        }
        const displayHTML = document.createElement('div');

        const buildingInfo = document.createElement('p');
        buildingInfo.innerText = "Efficient and reliable power generation that produces air and land pollution";
        const powerOutputInfo = document.createElement('p');
        powerOutputInfo.innerText = `Current power output: 5.00kW`;

        displayHTML.appendChild(buildingInfo);
        displayHTML.appendChild(powerOutputInfo);
        return displayHTML;
    }

    buildCable() {
        let target = null;
        Globals.gameObjects.forEach(obj => {
            if (obj.name === "basecamp") {
                target = obj;
                return;
            }
        });
        const tempGrid = Globals.pathfindingGrid.clone();
        const cA = worldCoordinatesToGridCoordinates(this.pivot.position.x, this.pivot.position.z);
        let cB = worldCoordinatesToGridCoordinates(target.pivot.position.x, target.pivot.position.z);
        if (!tempGrid.isWalkableAt(cB.x, cB.y)) {
            cB = getNearestFreeMapGridPoint(cB.x, cB.y);
            if (!cB) {
                //something went wrong?
                console.warn("Couldn't find a free point around target to pathfind to", this);
                return;
            }
        } 
        const path = this._pathfinder.findPath(cA.x, cA.y, cB.x, cB.y, tempGrid);
        let cablePoints = [];
        for (let i = 0; i < path.length - 1; i++) {
            const pathWorldCoordinates = gridCoordinatesToWorldCoordinates(path[i][0], path[i][1]);
            const gridValue = getMapGridValueAtGridCoordinates(path[i][0], path[i][1]);
            cablePoints.push(new THREE.Vector3(pathWorldCoordinates.x, gridValue.height, pathWorldCoordinates.y));
        }
        //manually tweak the cable points start and end a little
        cablePoints[cablePoints.length - 1].x = target.pivot.position.x - 1;
        cablePoints[cablePoints.length - 1].z = target.pivot.position.z - 1;
        const curve = new THREE.CatmullRomCurve3( cablePoints );
        const cableGeometry = new THREE.TubeGeometry( curve, 16, 0.1, 3, false );
        const cableMaterial = new THREE.MeshLambertMaterial( { color: 0x5A9175 } );
        this._powerCable = new THREE.Mesh( cableGeometry, cableMaterial );
        this._powerCable.receiveShadow = true;
        this._powerCable.castShadow = true;
        Globals.scene.add( this._powerCable );
    }

    getGlobalAirPollutionEffect() {
        if (!this.isPlaceState) {
            return this._currentGlobalAirPollutionEffect;
        }
        return 0;
    }

    getPowerEffect() {
        return this.powerOutput;
    }

    onGameTick(delta) {
        super.onGameTick(delta);
        //randomly make the tiles around us more polluted
        if (!this.isPlaceState) {
            const r = this.landPollutionRadius * Math.sqrt(Math.random());
            const theta = Math.random() * 2 * Math.PI;
            const x = this.pivot.position.x + r * Math.cos(theta);
            const z = this.pivot.position.z + r * Math.sin(theta);

            const target = new THREE.Vector3(x, this.pivot.position.y, z);
            const distance = this.pivot.position.distanceToSquared(target);

            let gridCoordinates = worldCoordinatesToGridCoordinates(target.x, target.z);
            if (gridCoordinates) {
                let gridValue = getMapGridValueAtGridCoordinates(gridCoordinates.x, gridCoordinates.y);

                if (gridValue) {
                    if (gridValue.localPollutionEffect < this.landPollutionStrength) {
                        gridValue.localPollutionEffect += (this.landPollutionStrength / distance);
                        gridValue.localPollutionEffect = THREE.MathUtils.clamp(gridValue.localPollutionEffect, -2, 2);
                        gridValue.needsUpdate = true;
                    } 
                }
            }
            
            if (this._timeSpentPolluting !== this._pollutionDelay) {
                let lerpFactor = this._timeSpentPolluting / this._pollutionDelay;
                this._currentGlobalAirPollutionEffect = THREE.MathUtils.lerp(this._currentGlobalAirPollutionEffect, this._globalAirPollutionEffect, lerpFactor);
                this._timeSpentPolluting++;
            }            
        }
    }
}