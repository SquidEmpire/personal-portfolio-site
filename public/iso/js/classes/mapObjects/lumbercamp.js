import {Globals} from "../../core.js";
import { MapObject } from "../mapObject.js";
import * as THREE from "../../vendor/three.module.js"
import { BufferGeometryUtils } from "../../vendor/utils/BufferGeometryUtils.js";

export class Lumbercamp extends MapObject {

    name = "lumbercamp";
    displayName = "Lumbercamp";

    footprint = [
        0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    ];

    constructor() {
        super();
        this.pivot = new THREE.Group();
        const mesh1Geometry = new THREE.BoxGeometry( Globals.tileSize * 4, Globals.tileSize * 4, Globals.tileSize * 3 );
        const mesh2Geometry = new THREE.BoxGeometry( Globals.tileSize * 2, Globals.tileSize * 4, Globals.tileSize );

        mesh1Geometry.translate(-(Globals.tileSize * 1.5), (Globals.tileSize * 2.5), -(Globals.tileSize * 2));
        mesh2Geometry.translate(-(Globals.tileSize * 2.5), (Globals.tileSize * 2.5), 0);

        const mergedGeometry = BufferGeometryUtils.mergeBufferGeometries([mesh1Geometry, mesh2Geometry]);

        const meshMaterial = new THREE.MeshLambertMaterial( {color: this.placeColour} );

        this.mesh = new THREE.Mesh( mergedGeometry, meshMaterial );
        this.mesh.receiveShadow = true;
        this.mesh.castShadow = true;

        this.pivot.add(this.mesh);
    }
}