
import {Globals, Settings, getMapGridValueAtGridCoordinates} from "../core.js";
import { GameObject } from "./gameObject.js";
import { MapObject } from "./mapObject.js";
import * as THREE from "../vendor/three.module.js"

export class PollutionController extends GameObject {

    //TODO: in the future it would be good to have pollution effects constantly added to/removed from
    //instead of a periodical refresh of the total, that way we can have more interesting tug of war between
    //sources, and things like map specific faster or slower air/land/water pollution recovery
    //naturalAirPollutionReductionEffect = 0.01; //the amount the air is automatically cleaned per check

    baseLandPollution = 0;
    baseAirPollution = 0;
    baseWaterPollution = 0;

    minPollutionLevel = 0;
    maxPollutionLevel = 1;

    totalPollutionLevel = 0; //total land/air/water combined pollution level

    landPollutionLevel = 0; //global land pollution modifier
    airPollutionLevel = 0; //global air pollution level
    waterPollutionLevel = 0; //global water pollution level

    timeSinceLastPollutionCycle = 0;
    pollutionCycleFrequency = Settings.gameSpeed * 1; //every 1 second we'll scan all pollution contributing/requiring map objects to update the global pollution levels
    terrainPollutionCycleColumnIndex = 0;

    rollingTotalLandPollutionLevel = 0;
    lastCheckedTotalLandPollutionLevel = 0;

    constructor() {
        super();
    }

    initialize() {
        super.initialize();
    }

    onGameTick(delta) {
        //each tick handle 1 column of the terrain map
        //TODO: redo so that global land pollution comes from the sum total of local land pollution levels, NOT the other way around
        for (let i = 0; i < Globals.terrain.width; i++) {
            const gridValue = getMapGridValueAtGridCoordinates(this.terrainPollutionCycleColumnIndex, i);
            //gridValue.localPollutionEffect += this.landPollutionLevel;
            this.rollingTotalLandPollutionLevel += gridValue.localPollutionEffect;
        }
        this.terrainPollutionCycleColumnIndex++;
        if (this.terrainPollutionCycleColumnIndex >= Globals.terrain.height) {
            this.terrainPollutionCycleColumnIndex = 0;
            this.lastCheckedTotalLandPollutionLevel = this.rollingTotalLandPollutionLevel / Globals.terrain.totalSize;
            //reset to base for the next check
            this.rollingTotalLandPollutionLevel = this.baseLandPollution;
        }

        if (Settings.caculatePollution) {
            const performPollutionCycle = (this.timeSinceLastPollutionCycle === this.pollutionCycleFrequency);
            let newLandPollutionLevel = this.lastCheckedTotalLandPollutionLevel;
            let newAirPollutionLevel = this.baseAirPollution;
            let newWaterPollutionLevel = this.baseWaterPollution;

            //TODO: register a smaller local list of pollution input/output objects?
            if (performPollutionCycle) {
                Globals.gameObjects.forEach(object => {
                    if (object instanceof MapObject) {
                        if (object.contributesGlobalAirPollution) {
                            newAirPollutionLevel += object.getGlobalAirPollutionEffect();
                        }
                        if (object.contributesGlobalWaterPollution) {
                            newWaterPollutionLevel += object.getGlobalWaterPollutionEffect();
                        }
                    }
                });

                if (newLandPollutionLevel != this.landPollutionLevel ||
                    newAirPollutionLevel != this.airPollutionLevel ||
                    newWaterPollutionLevel != this.waterPollutionLevel) {

                    if (newLandPollutionLevel != this.landPollutionLevel) {
                        this.landPollutionLevel = THREE.MathUtils.clamp(newLandPollutionLevel, 0, 1);
                    }
                    if (newAirPollutionLevel != this.airPollutionLevel) {
                        this.airPollutionLevel = THREE.MathUtils.clamp(newAirPollutionLevel, 0, 1);
                        Globals.weather.updateWeatherForPollution();
                        Globals.weather.weatherNeedsUpdate = true;
                    }
                    if (newWaterPollutionLevel != this.waterPollutionLevel) {
                        this.waterPollutionLevel = THREE.MathUtils.clamp(newWaterPollutionLevel, 0, 1);
                        Globals.water.updateWaterColour();
                    }

                    this.onPollutionLevelChange();
                }
                
                this.timeSinceLastPollutionCycle = 0;
            } else {
                this.timeSinceLastPollutionCycle++;
            }
        }
    }

    //public

    increaseBaseLandPollutionLevel = function(amount) {
        if (this.baseLandPollution < this.maxPollutionLevel) {

            this.baseLandPollution += amount;
            if (this.baseLandPollution > this.maxPollutionLevel) {
                this.baseLandPollution = this.maxPollutionLevel;
            }
            
            Globals.terrain.forceAllTerrainUpdate = true;
            this.onPollutionLevelChange();
        }       
    }
    
    decreaseBaseLandPollutionLevel = function(amount) {
        if (this.baseLandPollution > 0) {

            this.landPollutionLevel -= amount;
            if (this.baseLandPollution < this.minPollutionLevel) {
                this.baseLandPollution = this.minPollutionLevel;
            }

            Globals.terrain.forceAllTerrainUpdate = true;
            this.onPollutionLevelChange();
        }        
    }

    increaseBaseAirPollutionLevel = function(amount) {
        if (this.baseAirPollution < this.maxPollutionLevel) {

            this.baseAirPollution += amount;
            if (this.baseAirPollution > this.maxPollutionLevel) {
                this.baseAirPollution = this.maxPollutionLevel;
            }
            
            Globals.weather.updateWeatherForPollution();
            Globals.weather.weatherNeedsUpdate = true;
            this.onPollutionLevelChange();
        }        
    }
    
    decreaseBaseAirPollutionLevel = function(amount) {
        if (this.baseAirPollution > 0) {

            this.baseAirPollution -= amount;
            if (this.baseAirPollution < this.minPollutionLevel) {
                this.baseAirPollution = this.minPollutionLevel;
            }

            Globals.weather.updateWeatherForPollution();
            Globals.weather.weatherNeedsUpdate = true;
            this.onPollutionLevelChange();
        }        
    }

    increaseBaseWaterPollutionLevel = function(amount) {
        if (this.baseWaterPollution < this.maxPollutionLevel) {

            this.baseWaterPollution += amount;
            if (this.baseWaterPollution > this.maxPollutionLevel) {
                this.baseWaterPollution = this.maxPollutionLevel;
            }
            
            Globals.water.updateWaterColour();
            this.onPollutionLevelChange();
        }
    }
    
    decreaseBaseWaterPollutionLevel = function(amount) {
        if (this.baseWaterPollution > 0) {

            this.baseWaterPollution -= amount;
            if (this.baseWaterPollution < this.minPollutionLevel) {
                this.baseWaterPollution = this.minPollutionLevel;
            }

            Globals.water.updateWaterColour();
            this.onPollutionLevelChange();
        }        
    }

    increaseAllBasePollutionLevels = function(amount) {
        this.increaseLandPollutionLevel(amount);
        this.increaseAirPollutionLevel(amount);
        this.increaseWaterPollutionLevel(amount);
    }
    
    decreaseAllPollutionLevels = function(amount) {
        this.decreaseLandPollutionLevel(amount);
        this.decreaseAirPollutionLevel(amount);
        this.decreaseWaterPollutionLevel(amount);
    }

    onPollutionLevelChange = function() {
        this.totalPollutionLevel = (this.airPollutionLevel + this.landPollutionLevel + this.waterPollutionLevel) / 3;
        Globals.uiController.needsUpdate = true;
    }

}