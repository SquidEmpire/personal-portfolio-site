import { GameObject } from "./gameObject.js";
import {Globals} from "../core.js";

export class ClickableGameObject extends GameObject {

    mesh = null;
    isClickable = true;

    constructor() {
        super();
        //mesh should be created here in subclasses
    }

    initialize() {
        super.initialize();
        this.mesh.userData.uuid = this.uuid;
        Globals.clickableMeshes.push(this.mesh);
    }

    destroy() {
        super.destroy();
        for (let i = 0; i < Globals.clickableMeshes.length; i++) {
            if (Globals.clickableMeshes[i].userData.uuid === this.uuid) {
                Globals.clickableMeshes.splice(i, 1);
            }
        }
    }

    onPicked() {

    }

    onDepicked() {

    }

    onClicked() {

    }

    onDeclicked() {

    }
}