import {Globals} from "../core.js";
import { TerrainTypesMap } from "../mapDataCodes.js";
import* as THREE from "../vendor/three.module.js";

//this class represents a cell on the map grid
export class MapGridCellData {

    mapObject;
    terrainType = TerrainTypesMap.get(0);
    position;
    vertexIndex;
    needsUpdate;
    pollutionLevel; //between 0 and 1
    depressGrass; //manually depress the grass instance at this point (for structures)
    localPollutionEffect; //local additive affects on the pollution score of this cell between -2 and 2
    height; //local height of this cell (maps to 3D Y values)

    constructor(x, y) {
        this.mapObject = null,
        this.terrainType = null;
        this.position = {
            x: x,
            y: y
        };
        this.needsUpdate = false;
        this.depressGrass = false;
        this.localPollutionEffect = 0;
        this.pollutionLevel = 0;
    }

    getColour() {

        let newColour = new THREE.Color();

        if (this.pollutionLevel > 0.5) {
            const newPercent = ((this.pollutionLevel - 0.5) / 0.5);
            newColour.lerpColors(this.terrainType.midPollutedColour, this.terrainType.maxPollutedColour, newPercent);
        } else {
            const newPercent = (this.pollutionLevel / 0.5);
            newColour.lerpColors(this.terrainType.minPollutedColour, this.terrainType.midPollutedColour, newPercent);
        }

        return newColour;
    }

    onUpdate() {
        this.localPollutionEffect = THREE.MathUtils.clamp(this.localPollutionEffect, -2, 2);
        this.pollutionLevel = THREE.MathUtils.clamp(this.localPollutionEffect, 0, 1);
    }

    //returns 0 when the grass is at full height (no pollution, grass terrain type)
    getGrassHeightModifier() {
        if (!this.depressGrass && this.terrainType.name === "Grass") {
            const pollutionBench = Math.max(this.pollutionLevel, Globals.pollution.totalPollutionLevel);
            return -pollutionBench * 2.5;
        }
        return -10;
    }

}