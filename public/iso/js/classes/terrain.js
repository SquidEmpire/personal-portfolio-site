import {Globals, setMapGridTerrainTypeValue, gridCoordinatesToWorldCoordinates, getMapGridValueAtGridCoordinates} from "../core.js";
import * as HELPER from '../helper.js';
import * as THREE from '../vendor/three.module.js';
import { ClickableGameObject } from "./clickableGameObject.js";
import { TerrainTypesMap, MapObjectDataCodesMap } from "../mapDataCodes.js";

//terrain singleton class
export class Terrain extends ClickableGameObject {

    modifier;
    heightmap;
    heightmapImageData;
    pollutionmapImageData;
    terraintypeImageData;
    datamapImageData;

    heightmapsrc;
    pollutionmapsrc;
    terraintypemapsrc;
    datamapsrc;
    terrainmapsrc;
    terrainmapTexture;

    forceAllTerrainUpdate;

    type = "terrain";
    name = "terrain";
    geometry;
    mesh;
    width;
    height;
    totalSize;
    sizeAmplifier;
    heightModifier;

    terrainTypeValues = []; //r
    heightValues = []; //g
    entityValues = []; //b
    pollutionValues = []; //?

    enableGrassInstancedMesh = true; //turn off for performance
    grassInstancedMesh;
    grassInstanceRandomRotationValues = []; //we fill this array with random values but later need to use the same value every time
    grassInstanceRandomScaleValues = [];
    grassMeshBrightnessFactor;
    grassMeshColourAdjust;
    grassInstanceColour = new THREE.Color();
    grassMatrix = new THREE.Matrix4();

    constructor(heightmapsrc, pollutionmapsrc, terraintypemapsrc, datamapsrc, terrainmapsrc) {
        super();
        this.heightmapsrc = heightmapsrc;
        this.pollutionmapsrc = pollutionmapsrc;
        this.terraintypemapsrc = terraintypemapsrc;
        this.datamapsrc = datamapsrc;
        this.terrainmapsrc = terrainmapsrc;
        this.sizeAmplifier = 1; //map pixels to in-game units
        this.heightModifier = 0.05;
        this.grassMeshBrightnessFactor = 0.0; //this is to make the grass a bit brighter
        this.grassMeshColourAdjust = new THREE.Color(0x585500); //this is to make the grass slightly yellower
    }

    async loadDataImage(src) {
        return new Promise(function(resolve, reject) {
            let image = new Image();
            image.src = src;
            image.onload = () => {
                this.width = image.width;
                this.height = image.height;
        
                let canvas = document.createElement('canvas');
                canvas.width = this.width;
                canvas.height = this.height;
                let context = canvas.getContext('2d');
        
                context.drawImage(image, 0, 0);
                resolve(context.getImageData(0, 0, this.width, this.height).data);
            }
        }.bind(this));
    }

    async prepareImageData() {
        this.terrainmapTexture = THREE.ImageUtils.loadTexture( this.terrainmapsrc );
        const p1 = this.loadDataImage(this.heightmapsrc);
        const p2 = this.loadDataImage(this.pollutionmapsrc);
        const p3 = this.loadDataImage(this.terraintypemapsrc);
        const p4 = this.loadDataImage(this.datamapsrc);

        const results = await Promise.all([p1, p2, p3, p4]);

        this.heightmapImageData = results[0];
        this.pollutionmapImageData = results[1];
        this.terraintypeImageData = results[2];
        this.datamapImageData = results[3];
    }

    async initialize(depth = 10) {
        this.totalSize = this.height * this.width;
        this.geometry = new THREE.PlaneBufferGeometry(this.width * this.sizeAmplifier, this.height * this.sizeAmplifier, this.width - 1, this.height - 1);   
        this.geometry.castShadow = true;
        this.geometry.receiveShadow = true;

        let sideGeometry1 = new THREE.PlaneBufferGeometry(this.width * this.sizeAmplifier, depth, this.width - 1, depth - 1);
        let sideVerticies1 = sideGeometry1.attributes.position.array; 
        let sideGeometry2 = new THREE.PlaneBufferGeometry(this.width * this.sizeAmplifier, depth, this.width - 1, depth - 1);
        let sideVerticies2 = sideGeometry2.attributes.position.array; 

        var vertices = this.geometry.attributes.position.array;
        const colors = [];
        //here we go over the hightmap data and buffer geometry verticies and apply the heightmap data to the buffer's Z verticies
        //why Z and not Y? Because the plane geometry is built along the X and Y axis by default. We're using Z for the height for now and then flipping the geometry later
        //why start at j = 3 and increase by +3 each iteration? Because it is a long array of verticies which comes in triplets (x,y,z)
        //imageData(s) contains all the rbga values (one by one) for the image in an array, so in the same fashion we jump forward 4 points each loop
        let sideVertex1Index = 1;
        let sideVertex2Index = this.height * 3 - 2;
        //all data images must have the same size as the heightmap
        for (var i = 0, j = 0, k = 0; i < this.heightmapImageData.length; i += 4, j += 3, k++) {
            let r = this.terraintypeImageData[i];
            const g = this.heightmapImageData[i+1];
            const b = this.datamapImageData[i+2];
            const b2 = this.pollutionmapImageData[i+2];

            const x = j;
            const y = j + 1;
            const z = j + 2;
            
            vertices[z] = g * this.heightModifier;

            //if the current index is on one of the sides we want to have our "side" geometry on
            //take the value of the height and apply it to the relevant index of that side geometry
            if (k >= ((this.height - 1) * this.width)) {        
                //"front left" facing side
                sideVerticies1[sideVertex1Index] = vertices[z] + depth/2;
                sideVertex1Index += 3;
            }
            if (k % this.width === (this.height - 1)) {
                //"front right" facing side
                sideVerticies2[sideVertex2Index] = vertices[z] + depth/2;
                sideVertex2Index -= 3;
            }

            //apply terrain type
            //let location = new THREE.Vector3(vertices[j - 2], vertices[j], -vertices[j - 1]);
            let terrainType = TerrainTypesMap.get(r);
            if (!terrainType) {
                console.warn("missing terrain type code!", r);
                r = 0;
                terrainType = TerrainTypesMap.get(r);
            }
            
            colors.push( terrainType.minPollutedColour.r, terrainType.minPollutedColour.g, terrainType.minPollutedColour.b );
            this.terrainTypeValues.push(r);
            this.heightValues.push(g);
            this.entityValues.push(b);
            this.pollutionValues.push(b2);
        }

        this.geometry.setAttribute( 'color', new THREE.Float32BufferAttribute( colors, 3 ) );

        //here's where we flip the geometry to be rightways up
        this.geometry.rotateX(- Math.PI/2);
        let material = new THREE.MeshLambertMaterial( {vertexColors: THREE.VertexColors, map: this.terrainmapTexture } );
        let floor = new THREE.Mesh(this.geometry, material);
        floor.name = "terrain_mesh";
        floor.receiveShadow = true;

        let sideMaterial = new THREE.MeshLambertMaterial({
            color: 0x91886F,
            map: this.terrainmapTexture,
        });
        sideGeometry2.rotateY(Math.PI/2);

        //TODO: merge geometries instead of two meshes?
        let side1 = new THREE.Mesh(sideGeometry1, sideMaterial);
        let side2 = new THREE.Mesh(sideGeometry2, sideMaterial);
        side1.position.y = -depth/2;
        side2.position.y = -depth/2;
        side1.position.z = this.width / 2;
        side2.position.x = this.height / 2;
        
        Globals.scene.add( side1 );
        Globals.scene.add( side2 );

        Globals.scene.add( floor );
        this.mesh = floor;

        //now we set the terrain codes and buid the instanced grass mesh at the same time
        const grassGeometry = new THREE.PlaneGeometry( 1, 1 );
        grassGeometry.receiveShadow = true;
        const grassTexture = Globals.assets.find(x => x.name === 'grass').contents; 
        const grassMaterial = new THREE.MeshBasicMaterial( {
            map: grassTexture,
            side: THREE.DoubleSide,
            transparent: false,
            alphaTest: 0.5
        } );
        const totalSize = (this.height*this.width);
        if (this.enableGrassInstancedMesh) {
            this.grassInstancedMesh = new THREE.InstancedMesh( grassGeometry, grassMaterial, totalSize );
            this.grassInstancedMesh.instanceMatrix.setUsage( THREE.DynamicDrawUsage );
        }
        
        var m = 0;
        for (let k = 0; k < (this.height); k++) {
            for (let l = 0; l < (this.width); l++) {
                //apply the r values to the map grid
                setMapGridTerrainTypeValue(l, k, this.terrainTypeValues[m], false);

                const height = this.heightValues[m] * this.heightModifier;
                const gridValue = getMapGridValueAtGridCoordinates(l, k);
                gridValue.height = height;

                //add any map objects for the b values
                const mapObjectClass = MapObjectDataCodesMap.get(this.entityValues[m]);
                const worldCoordinates = gridCoordinatesToWorldCoordinates(l, k);
                if (mapObjectClass) {
                    const newMapObject = new mapObjectClass();                    
                    newMapObject.initialize(worldCoordinates.x, height, worldCoordinates.y);
                }

                //apply the pollution levels
                const pollutionValue = this.pollutionValues[m] / 255;
                gridValue.localPollutionEffect = pollutionValue;

                //move the grass geometry instance into place
                if (this.enableGrassInstancedMesh) {
                    const grassHeightModifier = gridValue.getGrassHeightModifier();
                    this.getGrassMatrix( 
                        this.grassMatrix,
                        worldCoordinates.x,
                        height + 1 + grassHeightModifier,
                        worldCoordinates.y,
                        m,
                        0
                    );
                    this.grassInstancedMesh.setMatrixAt( m, this.grassMatrix );
                    this.grassInstancedMesh.setColorAt( m, this.grassInstanceColour );                
                    this.grassInstancedMesh.setColorAt(k, this.grassInstanceColour.setRGB(
                        TerrainTypesMap.get(1).minPollutedColour.r,
                        TerrainTypesMap.get(1).minPollutedColour.g,
                        TerrainTypesMap.get(1).minPollutedColour.b
                    ).add(
                        this.grassMeshColourAdjust
                    ).addScalar(
                        this.grassMeshBrightnessFactor
                    ));
                }

                m++;
            }
        }
        
        if (this.enableGrassInstancedMesh) {
            Globals.scene.add( this.grassInstancedMesh );
        }        
        
        super.initialize();
    }

    getGrassMatrix(matrix, x, y, z, index, windStrength) {
        const position = new THREE.Vector3();
        const rotation = new THREE.Euler();
        const quaternion = new THREE.Quaternion();
        const scale = new THREE.Vector3();

        position.x = x;
        position.y = y;
        position.z = z;

        //if the grass is on the leading or trailing edges we don't want it to appear at all
        if (index >= ((this.height - 1) * this.width)) {        
            //"front left" facing side
            position.y = -100;
        } else if (index % this.width === (this.height - 1)) {
            //"front right" facing side
            position.y = -100;
        }

        let randomyRotation = this.grassInstanceRandomRotationValues[index];
        if (!randomyRotation) {
            randomyRotation = Math.random() - 0.5;
            this.grassInstanceRandomRotationValues[index] = randomyRotation;
        }

        rotation.x = 0;
        rotation.y = Math.PI / 3 + randomyRotation;
        rotation.z = windStrength;

        quaternion.setFromEuler( rotation );

        let randomScale = this.grassInstanceRandomScaleValues[index];
        if (!randomScale) {
            randomScale = HELPER.getRandom(1.5, 2);
            this.grassInstanceRandomScaleValues[index] = randomScale;
        }

        scale.x = scale.y = scale.z = randomScale;

        matrix.compose( position, quaternion, scale );
    }

    onGameTick(delta) {
        //TODO: performance bottleneck
        //TODO: rough, big loop. Have a list of grid values that need to be updated?
        //this is to update the terrain as the pollution level changes
        let m = 0;
        let k =0;
        for (let i = 0; i < this.width; i++) {
            for (let j = 0; j < this.height; j++) {
                let gridValue = getMapGridValueAtGridCoordinates(j, i);
                if (gridValue.needsUpdate || this.forceAllTerrainUpdate) {
                    gridValue.onUpdate();

                    const vert = m;
                    const colour = gridValue.getColour();
                    this.geometry.attributes.color["array"][vert] = colour.r;
                    this.geometry.attributes.color["array"][vert+1] = colour.g;
                    this.geometry.attributes.color["array"][vert+2] = colour.b;
                    this.geometry.attributes.color.needsUpdate = true;

                    //update the instanced grass too
                    if (this.enableGrassInstancedMesh) {
                        const grassHeightModifier = gridValue.getGrassHeightModifier();
                        const worldCoordinates = gridCoordinatesToWorldCoordinates(j, i);
                        this.getGrassMatrix( 
                            this.grassMatrix,
                            worldCoordinates.x,
                            gridValue.height + 1 + grassHeightModifier,
                            worldCoordinates.y,
                            k,
                            HELPER.getRandom(-Globals.weather.windStrength, Globals.weather.windStrength)
                        );
                        this.grassInstancedMesh.setMatrixAt( k, this.grassMatrix );
                        this.grassInstancedMesh.setColorAt(k, this.grassInstanceColour.setRGB(
                                colour.r, colour.g, colour.b
                            ).add(this.grassMeshColourAdjust).addScalar(this.grassMeshBrightnessFactor)
                        );

                        this.grassInstancedMesh.instanceMatrix.needsUpdate = true;
                        this.grassInstancedMesh.instanceColor.needsUpdate = true;
                    }
                    gridValue.needsUpdate = false;
                }
                m += 3;
                k++;
            }
        }
        if (this.forceAllTerrainUpdate) {
            this.forceAllTerrainUpdate = false;
            if (this.enableGrassInstancedMesh) {
                this.grassInstancedMesh.instanceMatrix.needsUpdate = true;
                this.grassInstancedMesh.instanceColor.needsUpdate = true;
            }
        }
    }

};