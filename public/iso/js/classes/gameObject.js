import {Globals} from "../core.js";
import * as HELPER from '../helper.js';

class GameObject {

    uuid;
    isDestroyed = false;

    constructor(  ) {
        this.uuid = HELPER.getUUID();
    }

    initialize() {
        Globals.gameObjects.set(this.uuid, this);
    }

    destroy() {
        Globals.gameObjects.delete(this.uuid);
        this.isDestroyed = true;
    }

    onGameTick(delta) {

    }

    onGameTickAlways(delta) {
        if (Globals.paused == false) {
            this.onGameTick(delta);
        }
    }

};

export { GameObject };