
import {Globals, Settings} from "../core.js";
import { GameObject } from "./gameObject.js";
import { MapObject } from "./mapObject.js";

export class PowerController extends GameObject {

    timeSinceLastPowerCycle = 0;
    powerCycleFrequency = Settings.gameSpeed * 1; //every 1 second we'll scan all power contributing/requiring map objects to update the global power levels

    constructor() {
        super();
    }

    onGameTick(delta) {
        const performPowerCycle = (this.timeSinceLastPowerCycle === this.powerCycleFrequency);
        let newPowerLevel = 0;

        //TODO: register a smaller local list of power input/output objects?
        if (performPowerCycle) {
            Globals.gameObjects.forEach(object => {
                if (object instanceof MapObject) {
                    if (object.contributesPower) {
                        newPowerLevel += object.getPowerEffect();
                    }
                }
            });
            Globals.resources.power = newPowerLevel;
            Globals.uiController.needsUpdate = true;
            this.timeSinceLastPowerCycle = 0;
        } else {
            this.timeSinceLastPowerCycle++;
        }
    }
}