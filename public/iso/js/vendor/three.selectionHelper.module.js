import {
	Vector2
} from './three.module.js';

class SelectionHelper {

	constructor( selectionBox, controls, cssClassName ) {

		this.element = document.createElement( 'div' );
		this.element.classList.add( cssClassName );
		this.element.style.pointerEvents = 'none';

		this.startPoint = new Vector2();
		this.pointTopLeft = new Vector2();
		this.pointBottomRight = new Vector2();

		this.isDown = false;
        this.isSelecting = false;
        this.controls = controls;
        this.selectionToleraneDelta = 0.6; //min difference required in a mousemove event to start a selection

        //TODO: duplicated event listeners
		document.addEventListener( 'pointerdown', function ( event ) {

			//we only want to set the isselected to be true when the left mouse is used
			if (event.button === 0) {
				this.isDown = true;
			}			

		}.bind( this ) );

		document.addEventListener( 'pointermove', function ( event ) {

            const movementX = event.movementX || event.mozMovementX || event.webkitMovementX || 0;
            const movementY = event.movementY || event.mozMovementY || event.webkitMovementY || 0;

			if ( this.isDown && (movementX > this.selectionToleraneDelta || movementY > this.selectionToleraneDelta) ) {

                if (!this.isSelecting) {
                    this.onSelectStart( event );
                }

				this.onSelectMove( event );

			}

		}.bind( this ) );

		document.addEventListener( 'pointerup', function ( event ) {

			this.isDown = false;

            if (this.isSelecting) {
			    this.onSelectOver( event );
            }

		}.bind( this ) );

	}

	onSelectStart( event ) {

		document.body.appendChild( this.element );

		this.element.style.left = this.controls.pointerScreenLocation.x + 'px';
		this.element.style.top = this.controls.pointerScreenLocation.y + 'px';
		this.element.style.width = '0px';
		this.element.style.height = '0px';

		this.startPoint.x = this.controls.pointerScreenLocation.x;
		this.startPoint.y = this.controls.pointerScreenLocation.y;

        this.isSelecting = true;

	}

	onSelectMove( event ) {

		this.pointBottomRight.x = Math.max( this.startPoint.x, this.controls.pointerScreenLocation.x );
		this.pointBottomRight.y = Math.max( this.startPoint.y, this.controls.pointerScreenLocation.y );
		this.pointTopLeft.x = Math.min( this.startPoint.x, this.controls.pointerScreenLocation.x );
		this.pointTopLeft.y = Math.min( this.startPoint.y, this.controls.pointerScreenLocation.y );

		this.element.style.left = this.pointTopLeft.x + 'px';
		this.element.style.top = this.pointTopLeft.y + 'px';
		this.element.style.width = ( this.pointBottomRight.x - this.pointTopLeft.x ) + 'px';
		this.element.style.height = ( this.pointBottomRight.y - this.pointTopLeft.y ) + 'px';

	}

	onSelectOver() {

		document.body.removeChild( this.element );

        this.isSelecting = false;

	}

}

export { SelectionHelper };