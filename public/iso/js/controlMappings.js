import { clearActiveObjects} from "./core.js";
import { MapObject } from "./classes/mapObject.js";
import { TestBuildingA } from "./classes/mapObjects/testBuildingA.js";
import { TestBuildingB } from "./classes/mapObjects/testBuildingB.js";

//mapping for control keycodes into functions
export var ControlMappings = new Map();

//TODO: instead of arrow functions these should be mappings into named functions elsewhere where possible

//left arrow
ControlMappings.set(37, (controls) => {controls.moveCameraWest()});
//up arrow
ControlMappings.set(38, (controls) => {controls.moveCameraNorth()});
//right arrow
ControlMappings.set(39, (controls) => {controls.moveCameraEast()});
//down arrow
ControlMappings.set(40, (controls) => {controls.moveCameraSouth()});

//b key
ControlMappings.set(66, (controls) => {
    let newObject = new MapObject();
    newObject.initialize(controls.lastMouseTerrainIntersectionPoint.x, 0, controls.lastMouseTerrainIntersectionPoint.y, true);
    clearActiveObjects();
    newObject.select();
});

//n key
ControlMappings.set(78, (controls) => {
    let newObject = new TestBuildingA();
    newObject.initialize(controls.lastMouseTerrainIntersectionPoint.x, 0, controls.lastMouseTerrainIntersectionPoint.y, true);
    clearActiveObjects();
    newObject.select();
});

//m key
ControlMappings.set(77, (controls) => {
    let newObject = new TestBuildingB();
    newObject.initialize(controls.lastMouseTerrainIntersectionPoint.x, 0, controls.lastMouseTerrainIntersectionPoint.y, true);
    clearActiveObjects();
    newObject.select();
});

//l key
ControlMappings.set(76, (controls) => {
    const debugOptions = document.getElementById("debug-box");
    debugOptions.classList.toggle('hidden');
});