import * as THREE from "./vendor/three.module.js";
import {Asset} from "./classes/asset.js";
import {Loader} from  "./loader.js";
import {Controls} from "./classes/controls.js";
import { Terrain } from "./classes/terrain.js";
import * as Helper from "./helper.js";
import { UiController } from "./classes/uiController.js";
import { TerrainTypesMap } from "./mapDataCodes.js";
import { MapGridCellData } from "./classes/mapGridCellData.js";
import { Water } from "./classes/water.js";
import { WeatherController } from "./classes/weatherController.js";
import { PollutionController } from "./classes/pollutionController.js";
import { PowerController } from "./classes/powerController.js";
import { ParticleController } from "./classes/particleController.js";

export const Settings = {
    gameSpeed: 200, //ticks per second
    caculatePollution: true, //debug option to turn off automatic pollution calculation
}

export var Globals = {
    scene: null,
    camera: null,
    controls: null,
    uiController: null,

    paused: true,
    tileSize: 1, //three js units to a tile

    assets: [], //loaded assets
    materials: new Map(), //shared materials

    gameObjects: new Map(), //things in the game
    clickableMeshes: [], //meshes that can be raytraced for clicking
    activeObjects: new Map(), //Read Only. Modify through functions. SelectableGameObjects that are currently selected
    pathfindingGrid: null, //Read Only. It is tied to the mapGrid array

    //singleton objects
    terrain: null,
    water: null,
    //controllers    
    weather: null,
    pollution: null,
    power: null,
    particles: null,

    totalWorkerLimit: 14, //max number of workers (performance tanks with higher numbers of workers)
    resources: {
        totalWorkers: 0,
        material: 0,
        power: 0
    },
}

const renderObects = []; //shouldn't really be used by many things - three.js handles rendering three objects automatically
const camFactor = 20;

let mapGrid = []; //array of arrays - each point contains a map object and terrain type. NOTE: only access through getters and setters here to avoid offset issues
let renderer = null;
let sun = null;

let lastUpdate = performance.now();


function buildAssetList() {

    //models

    //https://drive.google.com/drive/folders/1sNi1AfenfPRrvRt5yfaj5QMMd6KKcUJ5
    //https://drive.google.com/drive/folders/1-Kl0L_Jg8awbh0S5T-z3zxh4mVlnxTpa
    Globals.assets.push(new Asset('worker', 'models/Worker_Male.fbx', "FBX"));
    Globals.assets.push(new Asset('pine1', 'models/PineTree_1.fbx', "FBX"));
    Globals.assets.push(new Asset('pine2', 'models/PineTree_2.fbx', "FBX"));
    Globals.assets.push(new Asset('pine3', 'models/PineTree_3.fbx', "FBX"));
    Globals.assets.push(new Asset('pine4', 'models/PineTree_4.fbx', "FBX"));
    Globals.assets.push(new Asset('pine5', 'models/PineTree_5.fbx', "FBX"));

    Globals.assets.push(new Asset('camp', 'models/camp.dae', "Collada"));
    Globals.assets.push(new Asset('tent', 'models/tent.dae', "Collada"));
    Globals.assets.push(new Asset('lodge', 'models/lodge.dae', "Collada"));
    Globals.assets.push(new Asset('sickbay', 'models/sickbay.dae', "Collada"));
    Globals.assets.push(new Asset('dieselGenerator', 'models/dieselGen.dae', "Collada"));
    Globals.assets.push(new Asset('windTurbine', 'models/windTurbine.dae', "Collada"));
    Globals.assets.push(new Asset('chemicalExchanger', 'models/exchanger.dae', "Collada"));
    Globals.assets.push(new Asset('solarPanel', 'models/solarPanel.dae', "Collada"));
    Globals.assets.push(new Asset('materialDepot', 'models/depot.dae', "Collada"));
    Globals.assets.push(new Asset('materialRefinery', 'models/refinery.dae', "Collada"));
    Globals.assets.push(new Asset('airFilter', 'models/airFilter.dae', "Collada"));
    Globals.assets.push(new Asset('waterTank', 'models/waterTank.dae', "Collada"));
    Globals.assets.push(new Asset('soilPost', 'models/soilPost.dae', "Collada"));

    Globals.assets.push(new Asset('landfill', 'models/landfill.dae', "Collada"));    

    //audio

    //textures
    Globals.assets.push(new Asset('heightmap', 'img/map1/heightmap.png', THREE.Texture));
    Globals.assets.push(new Asset('pollutionmap', 'img/map1/pollution.png', THREE.Texture));
    Globals.assets.push(new Asset('terraintypemap', 'img/map1/terraintype.png', THREE.Texture));
    Globals.assets.push(new Asset('datamap', 'img/map1/data.png', THREE.Texture));
    Globals.assets.push(new Asset('terrainmap', 'img/terrainmap.png', THREE.Texture));

    Globals.assets.push(new Asset('water', 'img/water.png', THREE.Texture));
    Globals.assets.push(new Asset('water2', 'img/water2.png', THREE.Texture));
    Globals.assets.push(new Asset('smoke', 'img/smoke.png', THREE.Texture));
    Globals.assets.push(new Asset('grass', 'img/grass.png', THREE.Texture));
    Globals.assets.push(new Asset('rain', 'img/rain.png', THREE.Texture));

}

async function init() {

    //build our asset list
    buildAssetList();

    //load the assets
    const loaderElement = document.getElementById( 'loader' );
    const loaderTextElement = document.getElementById( 'loader-description' );
    const loader = new Loader(loaderTextElement);
    await loader.loadAssets(Globals.assets);
    loaderElement.style.display = 'none';

    Globals.scene = new THREE.Scene();
    Globals.scene.background = new THREE.Color( 0x444444 );

    const aspect = window.innerWidth / window.innerHeight;
    Globals.camera = new THREE.OrthographicCamera( - camFactor * aspect, camFactor * aspect, camFactor, - camFactor, 1, 1000 );
    Globals.camera.position.set( 40, 40, 40 );
    Globals.camera.lookAt( 0, 0, 0 );
    Globals.scene.add( Globals.camera );

    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.VSMShadowMap;
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.domElement.id = "game-canvas";
    document.body.appendChild( renderer.domElement );

    //start the game ticks
    setInterval(tick.bind(this), 1000/Settings.gameSpeed);
    render();

    window.addEventListener( 'resize', onWindowResize.bind(this), false );

    Globals.controls = new Controls(document.body);
    Globals.controls.initialize();

    Globals.uiController = new UiController(document.body);
    Globals.uiController.init();
    renderObects.push(Globals.uiController);

    Globals.particles = new ParticleController();
    Globals.particles.initialize();

    //add the pollution controller
    Globals.pollution = new PollutionController();
    Globals.pollution.initialize();

    await addTerrain();
    Globals.power = new PowerController();
    Globals.power.initialize();
    addTestObjects();
}

function tick() {
    const now = performance.now();
    const delta = now - lastUpdate;
    lastUpdate = now;

    Globals.gameObjects.forEach(object => {
        object.onGameTickAlways(delta);
    });
    
}

function render() {
    for (let i = 0; i < renderObects.length; i++) {
        renderObects[i].onRender(renderer);
    }
    renderer.render( Globals.scene, Globals.camera );
    requestAnimationFrame( render );
}

function onWindowResize() {
    const aspect = window.innerWidth / window.innerHeight;
    renderer.setSize( window.innerWidth, window.innerHeight );
    Globals.camera.left = - camFactor * aspect;
    Globals.camera.right = camFactor * aspect;
    Globals.camera.top = camFactor;
    Globals.camera.bottom = -camFactor;
    Globals.camera.updateProjectionMatrix();
    Globals.controls.updatePointerMax();
    Globals.water.depthMaterialNeedsUpdate = true; //this depends on screen resolution
}

async function addTerrain() {
    const terrainDepth = 15;

    const heightmap = Globals.assets.find(x => x.name === 'heightmap');
    const pollutionmap = Globals.assets.find(x => x.name === 'pollutionmap');
    const terraintypemap = Globals.assets.find(x => x.name === 'terraintypemap');
    const datamap = Globals.assets.find(x => x.name === 'datamap');

    const terrainmap = Globals.assets.find(x => x.name === 'terrainmap');

    Globals.terrain = new Terrain(heightmap.path, pollutionmap.path, terraintypemap.path, datamap.path, terrainmap.path);
    await Globals.terrain.prepareImageData();

    //initialise the mapgrid and pathfinding grid
    mapGrid = new Array(Globals.terrain.width);
    for (let i = 0; i < mapGrid.length; i++) {
        mapGrid[i] = new Array(Globals.terrain.height);
        for (let j = 0; j < mapGrid[i].length; j++) {
            mapGrid[i][j] = new MapGridCellData(i, j);
        }
    }
    Globals.pathfindingGrid = new PF.Grid(Globals.terrain.width, Globals.terrain.height);

    //initialize the terrain - this fills the mapgrid's terrainType and mapobject data from the terrain data
    await Globals.terrain.initialize(terrainDepth);

    //add the water
    Globals.water = new Water(Globals.terrain.width, Globals.terrain.height, 1, terrainDepth);
    Globals.water.initialize();
    renderObects.push(Globals.water);


    //add the directional light
    const directionalLight = new THREE.DirectionalLight( 0xffffff, 1 );
    const d = 42;
    directionalLight.castShadow = true;
    directionalLight.shadow.camera.left = -d;
    directionalLight.shadow.camera.right = d;
    directionalLight.shadow.camera.top = d;
    directionalLight.shadow.camera.bottom = -d;
    directionalLight.shadow.camera.near = 0;
    directionalLight.shadow.camera.far = 100;
    directionalLight.shadow.mapSize.width = 1024;
    directionalLight.shadow.mapSize.height = 1024;
    directionalLight.shadow.radius = 1.5;
    directionalLight.shadow.bias = -0.005
        
    Globals.camera.add( directionalLight );
    Globals.camera.add( directionalLight.target );
    const helper = new THREE.CameraHelper( directionalLight.shadow.camera );
    //Globals.scene.add( helper );

    //it is impossible to make this shadow camera always correct without tying it into screen resolution
    directionalLight.position.set(14, 29, -22);
    directionalLight.target.position.set(-6, -1, -52);
    // document.addEventListener( 'keydown', e => {
    //     switch (e.keyCode) {
    //         case 81:
    //             //q
    //             directionalLight.position.x += 1;
    //             directionalLight.target.position.x += 1;
    //             break;
    //         case 65:
    //             //a
    //             directionalLight.position.x -= 1;
    //             directionalLight.target.position.x -= 1;
    //             break;
    //         case 87:
    //             //w
    //             directionalLight.position.y += 1;
    //             directionalLight.target.position.y += 1;
    //             break;
    //         case 83:
    //             //s
    //             directionalLight.position.y -= 1;
    //             directionalLight.target.position.y -= 1;
    //             break;        
    //         case 69:
    //             //e
    //             directionalLight.position.z += 1;
    //             directionalLight.target.position.z += 1;
    //             break;    
    //         case 68:
    //             directionalLight.position.z -= 1;
    //             directionalLight.target.position.z -= 1;
    //             //d
    //             break;
    //     }
    //     console.log(directionalLight.position, directionalLight.target.position);
    // }, true );

    sun = new THREE.AmbientLight( 0xAAAAAA );
    Globals.scene.add( sun ); 

    //add the weather controller
    Globals.scene.fog = new THREE.Fog(new THREE.Color(0xffffff), 0.0025, 1000); //initially the fog is too distant to be visible
    Globals.weather = new WeatherController(sun);
    Globals.weather.initialize();      
}

function addTestObjects() {
    // const testUnit2 = new Worker();
    // testUnit2.initialize(Helper.getRandomInt(-10, 10), 0, Helper.getRandomInt(-10, 10));
}

//all operations on the map grid must be offset to deal with three.js coordiates (which go negative)
//TODO: would it be better in any way to instead transform the terrain at start to be always positive?
export function worldCoordinatesToGridCoordinates(x, y) {
    if (x > Globals.terrain.width/2 || x < -Globals.terrain.width/2 || y > Globals.terrain.height/2 || y < -Globals.terrain.height/2) {
        return null;
    }
    return {x: Math.max(Math.round(x + Globals.terrain.width/2) - 1, 0), y: Math.max(Math.round(y + Globals.terrain.height/2) - 1, 0)};
}

//TODO: at this point should we move the mapgrid into the terrain class?? (or another new controller?)
export function getMapGridValueAtGridCoordinates(x, y, useWorldCoordinates = false) {
    if (useWorldCoordinates) {
        const c = worldCoordinatesToGridCoordinates(x, y);
        if (!c) {
            return null;
        }
        x = c.x;
        y = c.y;
    }
    if (x < mapGrid.length && x >= 0) {
        if (y < mapGrid[x].length && y >= 0) {
            return mapGrid[x][y];
        }
    }
    //console.warn("Attempted to read mapgrid value at invalid position", x, y);
    return null;
}

//this results in lost precision
export function gridCoordinatesToWorldCoordinates(x, y) {
    return {x: Math.min((x - Globals.terrain.width/2) + 1, Globals.terrain.width), y: Math.min((y - Globals.terrain.height/2) + 1, Globals.terrain.height)};
}

export function getMapGridObjectValue(x, y) {
    const c = worldCoordinatesToGridCoordinates(x, y);
    return mapGrid[c.x][c.y].mapObject;
}

export function getMapGridTerrainTypeValue(x, y) {
    const c = worldCoordinatesToGridCoordinates(x,y);
    return mapGrid[c.x][c.y].terrainType;
}

export function setMapGridObjectValue(x, y, mapObject, depressGrass = false) {
    const c = worldCoordinatesToGridCoordinates(x, y);
    const gridvalue = mapGrid[c.x][c.y];
    gridvalue.mapObject = mapObject;
    gridvalue.depressGrass = depressGrass;
    gridvalue.needsUpdate = true;
    //update the pathfinding grid
    if (mapObject === null) {
        Globals.pathfindingGrid.setWalkableAt(c.x, c.y, true);
    } else if (mapObject.isSolid) {
        Globals.pathfindingGrid.setWalkableAt(c.x, c.y, false);
    }
}

export function setMapGridTerrainTypeValue(x, y, terrainTypeCode, useWorldCoordinates = true) {
    const terrainType = TerrainTypesMap.get(terrainTypeCode);
    if (terrainType) {
        if (useWorldCoordinates) {
            const c = worldCoordinatesToGridCoordinates(x, y);
            x = c.x;
            y = c.y;
        }
        mapGrid[x][y].terrainType = terrainType;
        mapGrid[x][y].needsUpdate = true;
    } else {
        console.error(`Invalid terrain type ("${terrainTypeCode}") specified for grid`);
    }
}

//https://stackoverflow.com/a/3330257
export function getNearestFreeMapGridPoint(xs, ys, maxDistance = 10) {
    for (let i = 1; i < maxDistance; i++)
    {
        for (let j = 0; j < i + 1; j++)
        {
            const x1 = xs - i + j;
            const y1 = ys - j;
            if (Globals.pathfindingGrid.isWalkableAt(x1, y1)){
                return {x: x1, y: y1};
            }

            const x2 = xs + i - j;
            const y2 = ys + j;
            if (Globals.pathfindingGrid.isWalkableAt(x2, y2)){
                return {x: x2, y: y2};
            }
        }


        for (let j = 1; j < i; j++)
        {
            const x1 = xs - j;
            const y1 = ys + i - j;
            if (Globals.pathfindingGrid.isWalkableAt(x1, y1)){
                return {x: x1, y: y1};
            }

            const x2 = xs + j;
            const y2 = ys - i + j;
            if (Globals.pathfindingGrid.isWalkableAt(x2, y2)){
                return {x: x2, y: y2};
            }
        }
    }
    return false;
}



export function setActiveObject(key, value) {
    Globals.activeObjects.set(key, value);
    Globals.uiController.needsUpdate = true;
}

export function deleteActiveObject(key) {
    Globals.activeObjects.delete(key);
    Globals.uiController.needsUpdate = true;
}

export function clearActiveObjects() {
    Globals.activeObjects.forEach(activeObject => {
        activeObject.deselect();
    });
    Globals.activeObjects.clear();
    Globals.uiController.needsUpdate = true;
}

init();