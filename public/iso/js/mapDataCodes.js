import { TerrainType } from "./classes/terrainType.js";
import { PineTree } from "./classes/mapObjects/pineTree.js";
import { Color } from "./vendor/three.module.js";
import { Landfill } from "./classes/mapObjects/landfill.js";
import { Basecamp } from "./classes/mapObjects/baseCamp.js";

//mapping of terrain type codes into terrain data
export var TerrainTypesMap = new Map();
//mapping of object type codes
export var MapObjectDataCodesMap = new Map();

//create the terrain codes
TerrainTypesMap.set(0, new TerrainType(0, "DEFAULT", new Color(0xffffff), new Color(0xBFBFBF), new Color(0x7F7F7F)));
TerrainTypesMap.set(1, new TerrainType(1, "Grass", new Color(0x8B9514), new Color(0x796C50), new Color(0x2F2F27)));
TerrainTypesMap.set(75, new TerrainType(75, "Sand", new Color(0xD6BA90), new Color(0xA08C65), new Color(0x50483D)));
TerrainTypesMap.set(150, new TerrainType(150, "Rock", new Color(0x77673B), new Color(0x615032), new Color(0x2F291C)));
TerrainTypesMap.set(255, new TerrainType(250, "Gravel", new Color(0x5B5543), new Color(0x3D342C), new Color(0x494336)));

//create the map object codes
MapObjectDataCodesMap.set(0, null);
MapObjectDataCodesMap.set(100, Basecamp);
MapObjectDataCodesMap.set(255, PineTree);