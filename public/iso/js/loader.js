import * as THREE from './vendor/three.module.js';
import { FBXLoader } from "./vendor/three.fbxloader.module.js";
import { ColladaLoader } from "./vendor/ColladaLoader.js";

class Loader {

    texturepath;

    loadingManager;
    textureLoader;
    cubeTextureLoader;
    objectLoader;
    audioLoader;
    fbxLoader;
    colladaLoader;

    loaderTextElement;

    constructor( displayElement ) {
        this.loadingManager = new THREE.LoadingManager();

        this.textureLoader = new THREE.TextureLoader(this.loadingManager);
        this.cubeTextureLoader = new THREE.CubeTextureLoader(this.loadingManager);
        this.objectLoader = new THREE.ObjectLoader(this.loadingManager);
        this.audioLoader = new THREE.AudioLoader(this.loadingManager);
        this.fbxLoader = new FBXLoader();
        this.colladaLoader = new ColladaLoader();

        this.loadingManager.onStart = this.onLoadStart.bind(this);
        this.loadingManager.onLoad = this.onLoad.bind(this);
        this.loadingManager.onProgress = this.onLoadProgress.bind(this);
        this.loadingManager.onError = this.onLoadError.bind(this);

        this.loaderTextElement = displayElement;
    }

    onLoadStart(url, itemsLoaded, itemsTotal) {
        console.log( 'Started loading file: ' + url + '.\nLoaded ' + itemsLoaded + ' of ' + itemsTotal + ' files.' );
    }

    onLoad() {
        console.log( 'Loading complete!');    
    };

    onLoadProgress( url, itemsLoaded, itemsTotal ) {
        let displayFilename = url;
        if (displayFilename.startsWith("data:")) {
            displayFilename = "Encoded data";
        }
        if (this.loaderTextElement) {
            this.loaderTextElement.innerHTML = `<p>${displayFilename}</p><p>${itemsLoaded}/${itemsTotal}</p>`;
        }   
    };
    
    onLoadError( url ) {
        console.log( 'There was an error loading ' + url );
    };

    async loadAssets(assets) {
        let assetPromises = [];

        assets.forEach(asset => {

            switch (asset.type) {

                case THREE.Texture:
                    assetPromises.push(new Promise(function(resolve, reject) {
                        this.textureLoader.load(asset.path, (textureContent) => {
                            textureContent.magFilter = THREE.NearestFilter;
                            textureContent.minFilter = THREE.LinearMipMapLinearFilter;
                            asset.setContents(textureContent);
                            resolve();
                        }, undefined, (e) => {
                            reject(e);
                        });
                    }.bind(this)));
                    break;

                case THREE.CubeTexture:
                    assetPromises.push(new Promise(function(resolve, reject) {
                        this.cubeTextureLoader.load(asset.path, (textureContent) => {
                            textureContent.magFilter = THREE.NearestFilter;
                            textureContent.minFilter = THREE.LinearMipMapLinearFilter;
                            asset.setContents(textureContent);
                            resolve();
                        }, undefined, (e) => {
                            reject(e);
                        });
                    }.bind(this)));
                    break;

                case THREE.Object3D:
                    assetPromises.push(new Promise(function(resolve, reject) {
                        this.objectLoader.load(asset.path, (objectContent) => {
                            asset.setContents(objectContent);
                            resolve();
                        }, undefined, (e) => {
                            reject(e);
                        });
                    }.bind(this)));
                    break;

                case THREE.Audio:
                    assetPromises.push(new Promise(function(resolve, reject) {
                        this.audioLoader.load( asset.path, ( buffer ) => {
                            asset.setContents(buffer);
                            resolve();
                        }, undefined, (e) => {
                            reject(e);
                        });
                    }.bind(this)));
                    break;

                case "FBX":
                    assetPromises.push(new Promise(function (resolve, reject) {
                        this.fbxLoader.load( asset.path, ( object ) => {
                            asset.setContents(object);
                            resolve();
                        }, undefined, (e) => {
                            reject(e);
                        });
                    }.bind(this)));
                    break;

                case "Collada":
                    assetPromises.push(new Promise(function (resolve, reject) {
                        this.colladaLoader.load( asset.path, ( object ) => {
                            object = object.scene;
                            asset.setContents(object);
                            resolve();
                        }, undefined, (e) => {
                            reject(e);
                        });
                    }.bind(this)));
                    break;

                default:
                    console.error("Attempted to load unknown asset type", asset.type);
                    break;

            }

        });

        return Promise.all(assetPromises);
    }

};

export { Loader };