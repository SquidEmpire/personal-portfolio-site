import { TerrainType } from "./classes/terrainType.js";
import { Color } from "./vendor/three.module.js";

//mapping of terrain type codes into terrain data
export var TerrainTypesMap = new Map();

TerrainTypesMap.set(0, new TerrainType(0, "DEFAULT", new Color(0xffffff)));
TerrainTypesMap.set(1, new TerrainType(1, "Grass", new Color(0x00ff00)));
TerrainTypesMap.set(75, new TerrainType(75, "Sand", new Color(0xffff00)));
TerrainTypesMap.set(150, new TerrainType(150, "Rock", new Color(0xffff00)));
TerrainTypesMap.set(255, new TerrainType(250, "Gravel", new Color(0xffff00)));