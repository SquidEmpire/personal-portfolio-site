function LowerCaseFirstLetter(string) {
    return string.substring(0, 1).toLowerCase() + string.substring(1);
}

function GetSummaryText() {
    let text = "";
        
    /* basic info */

    let errorText = "";

    //gender
    let genderValue;
    const genderRow = getActiveTableRow(document.getElementById("1-1"));
    if (genderRow) {
        genderValue = genderRow.getElementsByTagName("input")[0].value;
    }
    let genderString = "";
    if (genderValue === "male") {
        genderString = "man";
    } else if (genderValue === "female") {
        genderString = "woman";
    } else {
        genderString = "person";
    }
    

    //home planet
    const planetRow = getActiveTableRow(document.getElementById("2-1"));
    let planetString = "";
    if (!planetRow) {
        errorText += "ERR: missing home planet\n";
    } else {
        const planetValue = planetRow.getElementsByTagName("input")[0].value;
        planetString = toSentenceCase(planetValue);
        if (planetValue === "none") {
            planetString = "somewhere in space";
        } else if (planetValue === "moon") {
            planetString = "The Moon";
        }
    }
    

    //features
    const features = document.getElementById("selected-features").getElementsByClassName("selected-row");
    let featuresData = Array.from(features, (x) => x.getAttribute('data-value'));
    
    const featuresMappedToCounts = {};
    featuresData.forEach(i => {
        featuresMappedToCounts[i] = (featuresMappedToCounts[i] || 0) + 1;
    });

    let isAnArticant = false;
    let featurecount = Object.keys(featuresMappedToCounts).length;
    let featuresString = featurecount ? " who " : "";
    
    if (featuresMappedToCounts.articant) {
        isAnArticant = true;
        delete featuresMappedToCounts.articant;
        featurecount--;
    }

    let i = 0;
    for (const feature in featuresMappedToCounts) {
        switch (feature) {
            case "unusual":
                if (featuresMappedToCounts[feature] > 2) {
                    featuresString += "has an almost alien face";
                } else if (featuresMappedToCounts[feature] > 1) {
                    featuresString += "has a very unusual facial structure";
                } else {
                    featuresString += "has an unusual face";
                }
            break;
            case "glasses":
                if (featuresMappedToCounts[feature] > 2) {
                    featuresString += "refuses to ever remove their special glasses";
                } else if (featuresMappedToCounts[feature] > 1) {
                    featuresString += "is blind without their glasses";
                } else {
                    featuresString += "wears glasses";
                }
            break;
            case "smoker":
                if (featuresMappedToCounts[feature] > 2) {
                    featuresString += "is literally always smoking";
                } else if (featuresMappedToCounts[feature] > 1) {
                    featuresString += "chainsmokes";
                } else {
                    featuresString += "smokes";
                }
            break;
            case "tattoos":
                if (featuresMappedToCounts[feature] > 2) {
                    featuresString += "is covered all over in tattoos";
                } else if (featuresMappedToCounts[feature] > 1) {
                    featuresString += "has many elaborate tattoos";
                } else {
                    featuresString += "has tattoos";
                }                
            break;
            case "hair":
                if (featuresMappedToCounts[feature] > 2) {
                    featuresString += "has extremely unusual hair";
                } else if (featuresMappedToCounts[feature] > 1) {
                    featuresString += "has very unusual hair";
                } else {
                    featuresString += "has unusual hair";
                }
            break;
            case "scaring":
                if (featuresMappedToCounts[feature] > 2) {
                    featuresString += "has extensive, intense scarring";
                } else if (featuresMappedToCounts[feature] > 1) {
                    featuresString += "has many heavy scars";
                } else {
                    featuresString += "has some scars";
                }
            break;
            case "attractive":
                if (featuresMappedToCounts[feature] > 2) {
                    if (genderValue === "male") {
                        featuresString += "is handsome beyond words";
                    } else if (genderValue === "female") {
                        featuresString += "is beautiful beyond words";
                    } else {
                        featuresString += "is attractive beyond words";
                    }
                } else if (featuresMappedToCounts[feature] > 1) {
                    if (genderValue === "male") {
                        featuresString += "is extremely handsome";
                    } else if (genderValue === "female") {
                        featuresString += "is extremely beautiful";
                    } else {
                        featuresString += "is extremely attractive";
                    }
                } else {
                    if (genderValue === "male") {
                        featuresString += "is unusually handsome";
                    } else if (genderValue === "female") {
                        featuresString += "is unusually beautiful";
                    } else {
                        featuresString += "is unusually attractive";
                    }
                }
            break;
            case "fashion":
                if (featuresMappedToCounts[feature] > 2) {
                    featuresString += "dresses in a totally unique way";
                } else if (featuresMappedToCounts[feature] > 1) {
                    featuresString += "stands out by dressing unconventionally";
                } else {
                    featuresString += "dresses unconventionally";
                }
            break;
            case "cybernetic-limb":
                if (featuresMappedToCounts[feature] > 2) {
                    featuresString += "has all their limbs replaced with cybernetics";
                } else if (featuresMappedToCounts[feature] > 2) {
                    featuresString += "has three cybernetic limbs";
                } else if (featuresMappedToCounts[feature] > 1) {
                    featuresString += "has two cybernetic limbs";
                } else {
                    featuresString += "has a cybernetic limb";
                }
            break;
            case "cybernetic-eye":
                if (featuresMappedToCounts[feature] > 2) {
                    featuresString += "has both their eyes replaced with unique cybernetics";
                } else if (featuresMappedToCounts[feature] > 1) {
                    featuresString += "has both their eyes replaced with cybernetics";
                } else {
                    featuresString += "has a cybernetic eye";
                }      
            break;
        }
        if (i < featurecount - 2) {
            featuresString += ", ";
        } else if (featurecount > 2 && i < featurecount - 1) {
            featuresString += ", and ";
        } else if (featurecount === 2 && i === 0) {
            featuresString += " and ";
        }
        i++;
    }
    featuresString += ".";

    if (isAnArticant) {
        text = `You are an Articant from ${planetString} who appears like a ${genderString}${featuresString}\n\n`;
    } else {
        text = `You are a ${genderString} from ${planetString}${featuresString}\n\n`;
    }

    /* upbringing */

    let childhoodString = "";
    const childhoodRow = getActiveTableRow(document.getElementById("3-1"));
    if (!childhoodRow) {
        errorText += "ERR: missing childhood\n";
    } else {
        const childhoodValue = childhoodRow.children[1].innerText.toLowerCase();
        childhoodString = "As a child, " + childhoodValue;
    }    

    let endOfChildhoodString = "";
    const endOfChildhoodRow = getActiveTableRow(document.getElementById("3-2"));
    if (!endOfChildhoodRow) {
        errorText += "ERR: missing end of childhood\n";
    } else {
        const endOfChildhoodValue = endOfChildhoodRow.getElementsByTagName("input")[0].value;
        switch (endOfChildhoodValue) {
            case "dead-by-you": 
            endOfChildhoodString = "They're all dead now, because of you.";
            break;
            case "dead-by-accident": 
            endOfChildhoodString = "They're all dead now.";
            break;
            case "turned-on-you": 
            endOfChildhoodString = "In the end, they turned on you, and you barely escaped.";
            break;
            case "fell-apart": 
            endOfChildhoodString = "It all fell apart in the end, and you haven't heard of them in years.";
            break;
            case "fallen": 
            endOfChildhoodString = "They fell on hard times after you left.";
            break;
            case "doing-well": 
            endOfChildhoodString = "As far as you know, they're doing well.";
            break;
        }
    }   

    youthString = "";
    const youthRow = getActiveTableRow(document.getElementById("3-3"));
    if (!youthRow) {
        errorText += "ERR: missing youth\n";
    } else {
        const youthValue = youthRow.children[1].innerText.toLowerCase();
        youthString = "After you grew up, " + youthValue;
    }

    text += `${childhoodString}\n${endOfChildhoodString}\n\n${youthString}\n\n`;

    /* class & wealth */

    const careerRow = getActiveTableRow(document.getElementById("collars"));
    let careerString = "";
    if (!careerRow) {
        errorText += "ERR: missing career\n";
    } else {
        let careerValue = careerRow.getElementsByTagName("input")[0].value;
        if (careerValue === "againNeg" || careerValue === "againPos") {
            errorText += "ERR: invalid career\n";
        } else {
            //childNodes[0] returns just the top level element to ignore the child span (learn more) text if it's present
            let careerInnerText = careerRow.children[1].childNodes[0].textContent;

            careerString = "You became a"
        
            if (careerInnerText === "Space Crew") {
                careerInnerText = "Space Crewmember";
            }
            if (careerInnerText === "Worker") {
                careerInnerText = "Industrial Worker";
            }
            if (careerInnerText === "Corporate") {
                careerInnerText = "Corporate Stooge";
            }
            const fl = careerInnerText[0].toLowerCase();
            if (fl == "a" || fl == "e" || fl == "i" || fl == "o" || fl == "u") {
                careerString += "n"
            }

            careerString += " "+ careerInnerText;
        }
    }


    const wealthRow = getActiveTableRow(document.getElementById("5-1"));
    let wealthString = "";
    if (!wealthRow) {
        errorText += "ERR: missing wealth\n";
    } else {
        const wealthValue = wealthRow.children[1].innerText;
        wealthString = LowerCaseFirstLetter(wealthValue);
    }


    text += `${careerString}, and now ${wealthString}\n\n`;


    /* relationships */

    const relationships = document.getElementById("selected-relationships").getElementsByClassName("selected-row");
    let relationshipsString = "";
    if (relationships.length) {
        for (let i = 0; i < relationships.length; i++) {
            let relationshipString = relationships[i].children[0].innerText;
            relationshipString = relationshipString.substring(9, relationshipString.length -1); //cuts "you know " and the final full stop
            if (i < relationships.length - 2) {
                relationshipsString += relationshipString + ", ";
            } else if (relationships.length > 2 && i < relationships.length - 1) {
                relationshipsString += relationshipString + ", and ";
            } else if (relationships.length === 2 && i === 0) {
                relationshipsString += relationshipString + " and ";
            } else {
                relationshipsString += relationshipString;
            }
        }

        text += `There are a few people in your life. You know ${relationshipsString}.\n\n`;
    }

    /* goals */

    const goalRow = getActiveTableRow(document.getElementById("7-2"));
    let goalString = "";
    if (!goalRow) {
        errorText += "ERR: missing goal";
    } else {
        goalString = goalRow.children[1].innerText;
        goalString = "y" + goalString.substring(1); //lowercase the Y in You
    }

    text += `Ultimately, ${goalString}\n`;

    const startingStateRow = getActiveTableRow(document.getElementById("7-1"));
    if (startingStateRow) {
        startingStateString = LowerCaseFirstLetter(startingStateRow.children[1].innerText);
        text += `\nYou are here because ${startingStateString}\n`;
    }

    if (errorText !== "") {
        return errorText;
    }

    return text;
}