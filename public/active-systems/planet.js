
class Planet {

    name;
    desc;
    terrain;
    clouds;
    lights;
    rotationSpeed = 0.015;
    oceanColour;
    rimColour;

    constructor(name, desc, terrainsrc = null, cloudssrc = null, lightssrc = null, oceanColour = "rgb(1, 76, 144)", rimColour = "rgb(176, 208, 230)") {
        this.name = name;
        this.desc = desc;
        this.oceanColour = oceanColour;
        this.rimColour = rimColour;

        if (terrainsrc) {
            this.terrain = new Image();
            this.terrain.src = terrainsrc;
        }

        if (cloudssrc) {
            this.clouds = new Image();
            this.clouds.src = cloudssrc;
        }

        if (lightssrc) {
            this.lights = new Image();
            this.lights.src = lightssrc;
        }
    }
    
}

class Planetbox {

    element;
    canvas;
    title;
    label;

    planets = [];
    planet;

    tickTotal;
    lastTick;

    constructor(element) {
        this.element = element;
        this.canvas = this.element.getElementsByTagName("canvas")[0];
        this.title = this.element.getElementsByTagName("h2")[0];
        this.label = this.element.getElementsByTagName("p")[0];

        this.planets.push(new Planet("clean-earth",
            "isn't it wonderful?",
            "./images/earthterrain.png", "./images/earthclouds.png", "./images/earthLights.png",  "rgb(1, 76, 144)", "rgb(176, 208, 230)"));

        this.planets.push(new Planet("earth",
            "Home of Humanity and the Solar Union. About 12 billion people live here. Much of the surface is nuclear wastelands; the rest is polluted and overcrowded.",
            "./images/earthterrain2.png", "./images/earthclouds.png", "./images/earthLights.png",   "rgb(56, 85, 112)", "rgb(176, 208, 230)"));
        this.planets.push(new Planet("mars",
            "Earth’s twin, after decades of terraforming Mars is now close to being fully habitable. About 2 billion unruly people live here.",
            "./images/marsterrain.png", "./images/marsclouds.png", "./images/marslights.png",     "rgb(7, 72, 94)", "rgb(99, 101, 117)"));
        this.planets.push(new Planet("paranoir",
            "About 7 billion people live on Paranoir, the largest population of Humans outside of Earth. It is a forested, green world, with lakes and large cities.",
            "./images/paranoirterrain.png", "./images/paranoirclouds.png", "./images/paranoirlights.png",     "rgb(1, 39, 144)", "rgb(167, 190, 206)"));
        this.planets.push(new Planet("moon",
            "Earth’s Moon used to be important for space logistics. Now, only 500 million people live here. It remains important for the Solar Union’s navy.",
            "./images/moonterrain.png", null, "./images/moonlights.png",                                      "rgb(1, 76, 144)", "rgb(33, 36, 37)"));
        this.planets.push(new Planet("mikkauri",
            "The city of Ezo on the cold moon Mikkauri is the most distant inhabited place from Earth. The population contains a large number of Articants and the extremist Shishi movement. ",
            "./images/mikkauriterrain.png", "./images/marsclouds.png", "./images/mikkaurilights.png",         "rgb(1, 76, 144)", "rgb(136, 150, 189)"));
        this.planets.push(new Planet("solden",
            "A newly terraformed planet, undergoing a land grab. Corporate and political machinations mean that settlers have to deal with imported “freebooter” mercenaries.",
            "./images/soldenterrain.png", "./images/paranoirclouds.png", "./images/soldenlights.png",         "rgb(1, 76, 144)", "rgb(196, 215, 218)"));

        this.showPlanet("earth");

        this.lastTick = performance.now();
        this.tickTotal = 0;

        setInterval(this.render.bind(this), 10);
        setInterval(this.tick.bind(this), 10);
    }

    showPlanet(name) {

        this.title.innerText = "";
        this.label.innerText = "People born in space outside of the Solar Union or Republic are often entirely unregistered as citizens, making them extremely hard to track. Estimates indicate there may be hundreds of millions of them, most the children of outlaws squatting in abandoned stations. ";
        this.planet = null;

        for (let i = 0; i < this.planets.length; i++) {
            const planet = this.planets[i];
            if (planet.name === name) {
                
                this.title.innerText = planet.name;
                this.label.innerText = planet.desc;

                this.planet = planet;
                break;
            }
        }

    }

    pointatangle(angle, r) {
        angle = angle + 90;
        angle *= (Math.PI / 180);
        return [r * Math.sin(angle), r * Math.cos(angle)];
    }

    tick() {
        const now = performance.now();
        const delta = now - this.lastTick;
        this.lastTick = now;
        this.tickTotal += delta;
    }

    render() {
        const ctx = this.canvas.getContext("2d");
        const center = {x: this.canvas.width / 2, y: this.canvas.height / 2};
        const atmosphereheight = 10;
        const planetRadius = center.x - atmosphereheight;

        ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

        if (!this.planet) {
            return;
        }

        //background glow
        ctx.beginPath();
        ctx.arc(center.x, center.y, center.x, 0, 2 * Math.PI);
        const rimGradient = ctx.createRadialGradient(center.x, center.y, planetRadius, center.x, center.y, planetRadius + atmosphereheight);
        rimGradient.addColorStop(0, this.planet.rimColour);
        rimGradient.addColorStop(1, this.planet.rimColour.replace(")", ", 0)"));
        ctx.fillStyle = rimGradient;
        ctx.fill();

        //surface ocean
        ctx.beginPath();
        ctx.arc(center.x, center.y, planetRadius, 0, 2 * Math.PI);
        ctx.fillStyle = this.planet.oceanColour;
        ctx.fill();

        //ocean glare
        ctx.beginPath();
        const ogX = center.x * 0.6;
        const ogY = center.y * 0.9;
        const ogR = planetRadius * 0.45;
        ctx.arc(ogX, ogY, ogR, 0, 2 * Math.PI);
        const oceanGlareGradient = ctx.createRadialGradient(ogX, ogY, ogR * 0, ogX, ogY, ogR);
        oceanGlareGradient.addColorStop(0, "rgba(255,255,255, 0.8)");
        oceanGlareGradient.addColorStop(1, "rgba(255,255,255, 0)");
        ctx.fillStyle = oceanGlareGradient;
        ctx.fill();

        //surface terrain
        ctx.beginPath();
        ctx.arc(center.x, center.y, planetRadius, 0, 2 * Math.PI);
        const matrix = new DOMMatrix([1, 0, 0, 1, 0, 0]);
        const pattern = ctx.createPattern(this.planet.terrain, "repeat-x");
        if (pattern) {
            pattern.setTransform(matrix.translate(this.tickTotal * this.planet.rotationSpeed, 0, 0));
            ctx.fillStyle = pattern;
        }        
        ctx.fill();

        //surface glare
        ctx.beginPath();
        ctx.arc(ogX, ogY, ogR, 0, 2 * Math.PI);
        const surfaceGlareGradient = ctx.createRadialGradient(ogX, ogY, ogR * 0, ogX, ogY, ogR);
        surfaceGlareGradient.addColorStop(0, "rgba(255,255,255, 0.6)");
        surfaceGlareGradient.addColorStop(1, "rgba(255,255,255, 0)");
        ctx.fillStyle = surfaceGlareGradient;
        ctx.fill();

        //edge darkener
        ctx.beginPath();
        const darkAngle = 120;
        ctx.arc(center.x, center.y, planetRadius, (Math.PI/180) * (360-darkAngle), (Math.PI/180) * darkAngle);

        const arcEnd = this.pointatangle(360 - darkAngle, planetRadius);
        const arcStart = this.pointatangle(darkAngle, planetRadius);
        ctx.bezierCurveTo(center.x * 1.3, center.y * 1.5, center.x * 1.3, center.y * 0.5, center.x + arcStart[0], center.y + arcStart[1]);
        const darkenerGradient = ctx.createRadialGradient(center.x * 0.4, center.y, 120, center.x * 0.4, center.y, 190);
        darkenerGradient.addColorStop(0, "rgba(15,15,15,0)");
        darkenerGradient.addColorStop(1, "rgba(15,15,15,1)");
        ctx.fillStyle = darkenerGradient;

        ctx.strokeStyle = "white";
        ctx.fill();

        //cloudz
        if (this.planet.clouds) {
            ctx.beginPath();
            ctx.arc(center.x, center.y, planetRadius, 0, 2 * Math.PI);
            const matrix2 = new DOMMatrix([1, 0, 0, 1, 0, 0]);
            const pattern2 = ctx.createPattern(this.planet.clouds, "repeat-x");
            if (pattern2) {
                pattern2.setTransform(matrix2.translate(this.tickTotal * this.planet.rotationSpeed * 1.3, 0, 0));
                ctx.fillStyle = pattern2;
            }        
            ctx.fill();
        }

        //edge darkener2 (on clouds)
        ctx.beginPath();
        ctx.arc(center.x, center.y, planetRadius, (Math.PI/180) * (360-darkAngle), (Math.PI/180) * darkAngle);

        ctx.bezierCurveTo(center.x * 1.3, center.y * 1.5, center.x * 1.3, center.y * 0.5, center.x + arcStart[0], center.y + arcStart[1]);
        const darkenerGradient2 = ctx.createRadialGradient(center.x * 0.4, center.y, 120, center.x * 0.4, center.y, 200);
        darkenerGradient2.addColorStop(0, "rgba(15,15,15,0)");
        darkenerGradient2.addColorStop(1, "rgba(15,15,15,1)");
        ctx.fillStyle = darkenerGradient2;

        ctx.strokeStyle = "white";
        ctx.fill();        

        

        //lights (above clouds!)
        if (this.planet.lights) {
            ctx.beginPath();
            ctx.arc(center.x, center.y, planetRadius, 0, 2 * Math.PI);
            const pattern3 = ctx.createPattern(this.planet.lights, "repeat-x");
            if (pattern3) {
                pattern3.setTransform(matrix.translate(this.tickTotal * this.planet.rotationSpeed, 0, 0));
                ctx.fillStyle = pattern3;
            }        
            ctx.fill();
        }
    }

}