var section2PScore = section3_1PScore = section3_3Pscore = section4_1Pscore = section5PScore = 0;
var section1_1Dice, section1_2Dice, section1_3Dice, section2_1Dice, section3_1Dice, section3_2Dice, section3_3Dice, section4_1Dice, section6_1Dice, section6_2Dice, section7_1Dice, section7_2Dice;
var section4PScoreModifier = 0;
var section6LeftSideActiveRow, section6RightSideActiveRow;
var currentStyleIndex = 0;




function init() {
    initStyleButton();
    initRadioButtons();
    initLearnMoreTags();
    
    initSection1Logic();
    initSection2Logic();
    initSection3Logic();
    initSection4Logic();
    initSection5Logic();
    initSection6Logic();
    initSection7Logic();
    initSection8Logic();
}




function initStyleButton() {
    const style1 = document.getElementById("cpustyle");
    const style2 = document.getElementById("beigestyle");
    const styles = [style1, style2];
    const button = document.getElementById("style-swap");
    const label = document.getElementById("style-name");
    button.addEventListener("click", () => {
        currentStyleIndex += 1;
        if (currentStyleIndex > styles.length - 1) {
            currentStyleIndex = 0;
        }
        for (let i = 0; i < styles.length; i++) {
            const style = styles[i];
            if (currentStyleIndex === i) {
                style.disabled = false;
                label.innerText = style.getAttribute("data-name");
            } else {
                style.disabled = true;
            }
        }
    });
}


function initRadioButtons() {
    let radiobuttonElements = document.getElementsByTagName("input");
    for (let i = 0; i < radiobuttonElements.length; i++) {
        const radio = radiobuttonElements[i];
        if (radio.type === "radio") {
            const parentRow = radio.closest("tr");
            const parentRowIndex = [...parentRow.parentElement.children].indexOf(parentRow);

            parentRow.classList.add("clickable");

            parentRow.addEventListener("click", () => {
                radio.click();
            });

            radio.addEventListener("change", () => {
                selectTableRow(parentRow.parentElement, parentRowIndex);
            });
        }
    }
}

function selectTableRow(table, index) {
    const rows = table.getElementsByTagName("tr");
    for (let i = 0; i < rows.length; i++) {
        const row = rows[i];
        if (i === index) {
            row.classList.add("active");
            row.getElementsByTagName('input')[0].checked = true;
        } else {
            row.classList.remove("active");
        }
    }
    table.dispatchEvent(new Event("change"));
}

function getActiveTableRow(table) {
    const rows = table.getElementsByTagName("tr");
    for (let i = 0; i < rows.length; i++) {
        const row = rows[i];
        if (row.classList.contains("active")) {
            return row;
        }
    }
    return null;
}


function initLearnMoreTags() {
    const learnMoreElement = document.getElementById("learn-more-box");
    const learnMoreCloseButton = document.getElementById("learn-more-hide");
    learnMoreCloseButton.addEventListener("click", () => {
        learnMoreElement.classList.add("hidden");
    });
    document.addEventListener("keydown", (e) =>{
        if (e.key === "Escape") {
            learnMoreElement.classList.add("hidden");
        }
    });

    const learnModeTitleElement = document.getElementById("learn-more-title");
    const learnModeTextElement = document.getElementById("learn-more-text");
    const tags = document.getElementsByClassName("learn-more");   
    for (let i = 0; i < tags.length; i++) {
        const tag = tags[i];
        tag.addEventListener("click", (e) => {
            e.stopPropagation();
            learnMoreElement.classList.remove("hidden");
            const data = GetLearnMoreDataForDataValue(tag.getAttribute('data-value'));
            learnModeTitleElement.innerText = data.title;
            learnModeTextElement.innerText = data.text;
        });
    }
}


function initSection1Logic() { 

    //register add feature buttons
    const buttons = document.getElementById("1-2").getElementsByTagName("button");
    for (let i = 0; i < buttons.length; i++) {
        const button = buttons[i];
        const val = button.value;
        //childNodes[0] returns just the top level element to ignore the child span (learn more) text if it's present
        const text = button.closest("tr").children[1].childNodes[0].textContent;
        button.addEventListener("click", () => {
            addFeature(val, text);
            updateFeaturesWarning();
            button.closest("tr").classList.add("active");
            setTimeout(() => {button.closest("tr").classList.remove("active")}, 1100);
        });
    }
    //register row clicks as add feature clicks
    const featureTable = document.getElementById("1-2");
    const rows = featureTable.getElementsByTagName("tr");
    for (let i = 0; i < rows.length; i++) {
        const row = rows[i];
        row.classList.add("clickable");
        row.addEventListener("click", () => {row.getElementsByTagName("button")[0].click()});
    }

    updateFeaturesWarning();

    //gender dice roll
    const gTable = document.getElementById("1-1");
    section1_1Dice = new Dicebox(document.getElementById("d-1-1"));
    section1_1Dice.addEventListener("rolled", () => {
        let roll = section1_1Dice.getTotal();
        let index;
        if (roll < 4) {
            index = 0;
        } else {
            index = 1;
        }
        selectTableRow(gTable, index);
    });

    //features count dice roll
    section1_2Dice = new Dicebox(document.getElementById("d-1-2"));
    section1_2Dice.addEventListener("rolled", () => {
        let roll = section1_2Dice.getTotal();
        roll = Math.ceil(roll/2);
        document.getElementById("dice-feature-count").innerText = roll;
    });

    //add feature roll
    section1_3Dice = new Dicebox(document.getElementById("d-1-3"));
    section1_3Dice.addEventListener("rolled", () => {
        let roll = section1_3Dice.getTotal();
        const button = rows[roll - 2].getElementsByTagName("button")[0];
        button.click();
    });

}

function addFeature(value, text) {
    const container = document.getElementById("selected-features");
    const newFeatureHtml = `<div class="selected-row" data-value="${value}">
                                <p>${text}</p>
                                <button class="remove-selected-row">X</button>
                            </div>`;
    container.insertAdjacentHTML( 'beforeend', newFeatureHtml );

    const addedFeature = container.children[container.children.length - 1];
    const removeButton = addedFeature.getElementsByClassName("remove-selected-row")[0];
    removeButton.addEventListener("click", () => {
        container.removeChild(addedFeature);
        updateFeaturesWarning();
    });
}

function updateFeaturesWarning() {
    const container = document.getElementById("selected-features");
    const warningElement = document.getElementById("warning-features");

    if (container.children.length === 0) {
        warningElement.innerText = "No features selected";
        warningElement.classList.remove("hidden");
    } else if (container.children.length > 6) {
        warningElement.innerText = "Many features selected";
        warningElement.classList.remove("hidden");
    } else {
        warningElement.classList.add("hidden");
    }
}

function initSection2Logic() {

    //planet preview
    const planetbox = new Planetbox(document.getElementById("planetbox"));

    //home planet dice roll
    const table = document.getElementById("2-1");
    section2_1Dice = new Dicebox(document.getElementById("d-2-1"));
    section2_1Dice.addEventListener("rolled", () => {
        let roll = section2_1Dice.getTotal();
        let index = roll - 2;
        selectTableRow(table, index);
        updateSection2PScore();
    });

    //P scoring
    table.addEventListener("change", () => {
        updateSection2PScore();
        const selectedRow = table.getElementsByClassName("active")[0];
        const planetname = selectedRow.getAttribute("data-value");
        planetbox.showPlanet(planetname);
    });
}

function initSection3Logic() {

    //childhood dice roll
    section3_1Dice = new Dicebox(document.getElementById("d-3-1"));
    section3_1Dice.addEventListener("rolled", () => {
        updateSection3_1PScore();
    });

    //end of childhood dice roll
    section3_2Dice = new Dicebox(document.getElementById("d-3-2"));
    const endOfChildhoofTable = document.getElementById("3-2");
    section3_2Dice.addEventListener("rolled", () => {
        let roll = section3_2Dice.getTotal();
        let index = roll - 1;
        selectTableRow(endOfChildhoofTable, index);
    });

    //youth dice roll
    section3_3Dice = new Dicebox(document.getElementById("d-3-3"));
    const youthTable = document.getElementById("3-3");
    section3_3Dice.addEventListener("rolled", () => {
        let roll = section3_3Dice.getTotal();
        let index = roll - 2;
        selectTableRow(youthTable, index);
        updateSection3_2PScore();
    });

    youthTable.addEventListener("change", () => {
        updateSection3_2PScore();
    });

}

function initSection4Logic() {

    //register the rows on the collar tables to activate the radio buttons choices
    const blueRows = document.getElementById("4-1").getElementsByTagName("tr");
    const greyRows = document.getElementById("4-2").getElementsByTagName("tr");
    const whiteRows = document.getElementById("4-3").getElementsByTagName("tr");

    const collarRows = [...blueRows].concat([...greyRows], [...whiteRows]);

    for (let i = 0; i < collarRows.length; i++) {
        const row = collarRows[i];
        row.addEventListener("change", () => {
            updateSection4Selection();
            updateSection4PModifier();
            updateCareerImagebox();
        });
    }

    //career dice roll
    section4_1Dice = new Dicebox(document.getElementById("d-4-1"));
    section4_1Dice.addEventListener("rolled", () => {
 
        //pick class from P score and roll
        const roll = section4_1Dice.getTotal();
        let table;
        if (section4_1Pscore > 14) {
            table = document.getElementById("4-3");
        } else if (section4_1Pscore >= 7) {
            table = document.getElementById("4-2");
        } else {
            table = document.getElementById("4-1");
        }

        selectTableRow(table, roll - 1);
        updateSection4Selection();
        updateSection4PModifier();
        updateCareerImagebox();
    });

}

function initSection5Logic() {
    //wealth dice roll
    section5_1Dice = new Dicebox(document.getElementById("d-5-1"));
    section5_1Dice.addEventListener("rolled", () => {
        updateSection5PScore();
    });
}

function initSection6Logic() {

    //relationships count dice roll
    section6_1Dice = new Dicebox(document.getElementById("d-6-1"));
    section6_1Dice.addEventListener("rolled", () => {
        let roll = section6_1Dice.getTotal();
        roll = Math.ceil(roll/2);
        document.getElementById("dice-relationship-count").innerText = roll;
    });

    //add relationship roll
    const relationshipstableLeft = document.getElementById("6-1");
    const relationshipstableRight = document.getElementById("6-2");
    const lRows = relationshipstableLeft.getElementsByTagName("tr");
    const rRows = relationshipstableRight.getElementsByTagName("tr");
    
    section6_2Dice = new Dicebox(document.getElementById("d-6-2"));
    section6_2Dice.addEventListener("rolled", async () => {

        let rollResults = section6_2Dice.getResults();
        const lIndex = rollResults[0] - 2;
        selectTableRow(relationshipstableLeft, lIndex);
        section6LeftSideActiveRow = lRows[lIndex];

        await new Promise(resolve => setTimeout(resolve, 1000));
        
        const rIndex = rollResults[1] - 2;
        selectTableRow(relationshipstableRight, rIndex);
        section6RightSideActiveRow = rRows[rIndex];
        
        checkForRelationshipAdd();
    });   

    for (let i = 0; i < lRows.length; i++) {
        const row = lRows[i];
        const radio = row.getElementsByTagName("input")[0];
        radio.addEventListener("click", () => {
            section6LeftSideActiveRow = row;
            checkForRelationshipAdd();
        });
    }
    for (let i = 0; i < rRows.length; i++) {
        const row = rRows[i];
        const radio = row.getElementsByTagName("input")[0];
        radio.addEventListener("click", () => {
            section6RightSideActiveRow = row;
            checkForRelationshipAdd();
        });
    }

    updateRelationshipsWarning();

}

async function checkForRelationshipAdd() {
    if (section6LeftSideActiveRow && section6RightSideActiveRow) {

        await new Promise(resolve => setTimeout(resolve, 1000));

        //childNodes[0] returns just the top level element to ignore the child span (learn more) text if it's present
        let lText = section6LeftSideActiveRow.children[1].childNodes[0].textContent.toLowerCase();
        lText = lText.substring(0, lText.length - 1);
        const rText = section6RightSideActiveRow.children[1].childNodes[0].textContent.toLowerCase();
        const relationshipString = `You know ${lText} who ${rText}`;
        addRelationship(0, relationshipString);

        const relationshipstableLeft = document.getElementById("6-1");
        const relationshipstableRight = document.getElementById("6-2");
        const lRows = relationshipstableLeft.getElementsByTagName("tr");
        const rRows = relationshipstableRight.getElementsByTagName("tr");
        const allRows = [...lRows].concat([...rRows]);
        for (let i = 0; i < allRows.length; i++) {
            const row = allRows[i];
            const radio = row.getElementsByTagName("input")[0];
            row.classList.remove("active");
            radio.checked = false;
        }
        section6LeftSideActiveRow = null;
        section6RightSideActiveRow = null;

        updateRelationshipsWarning();
    }
}

function addRelationship(value, text) {
    const container = document.getElementById("selected-relationships");
    const newRelationshipHtml = `<div class="selected-row" data-value="${value}">
                                <p>${text}</p>
                                <button class="remove-selected-row">X</button>
                            </div>`;
    container.insertAdjacentHTML( 'beforeend', newRelationshipHtml );

    const addedRelationship = container.children[container.children.length - 1];
    const removeButton = addedRelationship.getElementsByClassName("remove-selected-row")[0];
    removeButton.addEventListener("click", () => {
        container.removeChild(addedRelationship);
        updateRelationshipsWarning();
    });
}

function updateRelationshipsWarning() {
    const container = document.getElementById("selected-relationships");
    const warningElement = document.getElementById("warning-relationships");

    if (container.children.length === 0) {
        warningElement.innerText = "No relationships added";
        warningElement.classList.remove("hidden");
    } else if (container.children.length > 3) {
        warningElement.innerText = "Many relationships added";
        warningElement.classList.remove("hidden");
    } else {
        warningElement.classList.add("hidden");
    }
}

function initSection7Logic() {
    
    section7_1Dice = new Dicebox(document.getElementById("d-7-1"));
    const startingTable = document.getElementById("7-1");
    section7_1Dice.addEventListener("rolled", () => {
        let roll = section7_1Dice.getTotal();
        let index = roll - 1;
        selectTableRow(startingTable, index);
    });

    section7_2Dice = new Dicebox(document.getElementById("d-7-2"));
    const goalTable = document.getElementById("7-2");
    section7_2Dice.addEventListener("rolled", () => {
        let roll = section7_2Dice.getTotal();
        let index = roll - 2;
        selectTableRow(goalTable, index);
    });    
}


function initSection8Logic() {
    //get summary button
    const textarea = document.getElementById("summary");
    const button = document.getElementById("get-summary");

    button.addEventListener("click", () => {        
        const text = GetSummaryText();
        textarea.value = text;
    });
}


function toSentenceCase(string) {
    return string[0].toUpperCase() + string.substring(1);
}


function updateSection2PScore() {
    //home planet privilege
    const table = document.getElementById("2-1");
    const selectedRow = table.getElementsByClassName("active")[0];
    const selectedRowPValue = selectedRow.getElementsByClassName("p-score")[0];
    const pValue = parseInt(selectedRowPValue.getAttribute('data-value'));

    const subsectionSpanElement = document.getElementById("p-s-2-1");
    subsectionSpanElement.innerText = pValue;

    section2PScore = pValue;
    updateSection3_1PScore();
}

function updateSection3_1PScore() {
    //upbringing
    let total = 0;
    total += section2PScore;
    total += section3_1Dice.getTotal();

    const subsectionSpanElement = document.getElementById("p-s-3-1");
    subsectionSpanElement.innerText = total;

    section3_1PScore = total;

    const table = document.getElementById("3-1");
    let index = 0;
    if (section3_1PScore < 2) {
        index = 0;
    } else if (section3_1PScore > 12) {
        index = 10;
    } else {
        index = section3_1PScore - 2;
    }
    selectTableRow(table, index);

    updateSection3_2PScore();
}

function updateSection3_2PScore() {
    //youth
    let total = section3_1PScore;

    const table = document.getElementById("3-3");
    const selectedRow = table.getElementsByClassName("active")[0];
    if (selectedRow) { //may not be set yet
        const selectedRowPValue = selectedRow.getElementsByClassName("p-score")[0];
        const pValue = parseInt(selectedRowPValue.getAttribute('data-value'));
        total += pValue;
    }

    section3_3Pscore = total;

    const subsectionSpanElement = document.getElementById("p-s-3-3");
    subsectionSpanElement.innerText = section3_3Pscore;

    updateSection4PScore();
}


function updateSection4PModifier() {

    //get the current selected class
    const blueRows = document.getElementById("4-1").getElementsByTagName("tr");
    const greyRows = document.getElementById("4-2").getElementsByTagName("tr");
    const whiteRows = document.getElementById("4-3").getElementsByTagName("tr");

    const collarRows = [...blueRows].concat([...greyRows], [...whiteRows]);

    for (let i = 0; i < collarRows.length; i++) {
        const row = collarRows[i];
        if (row.classList.contains("active")) {
            const rowRadio = row.getElementsByTagName("input")[0];
            if (rowRadio.value === "againNeg" || rowRadio.value === "againPos") {

                if (rowRadio.value === "againNeg") {
                    section4PScoreModifier -= 1;
                } else if (rowRadio.value === "againPos") {
                    section4PScoreModifier += 1;                    
                }
                const subsectionSpanElement = document.getElementById("p-s-4-0");
                subsectionSpanElement.innerText = section4PScoreModifier;
                updateSection4PScore();

            }
            
            break;
        }
    }

}

function updateSection4PScore() {

    let total = section3_3Pscore;
    total += section4PScoreModifier;
    section4_1Pscore = total;

    const subsectionSpanElement = document.getElementById("p-s-4-1");
    subsectionSpanElement.innerText = section4_1Pscore;

    updateSection4Selection();

    updateSection5PScore();

}

function updateSection4Selection() {

    //highlight selected table as well
    const cB = document.getElementById("collar-blue");
    const cG = document.getElementById("collar-grey");
    const cW = document.getElementById("collar-white");
    cB.classList.remove("active");
    cG.classList.remove("active");
    cW.classList.remove("active");

    //update active card
    const blueRows = document.getElementById("4-1").getElementsByTagName("tr");
    const greyRows = document.getElementById("4-2").getElementsByTagName("tr");
    const whiteRows = document.getElementById("4-3").getElementsByTagName("tr");

    const collarRows = [...blueRows].concat([...greyRows], [...whiteRows]);

    for (let i = 0; i < collarRows.length; i++) {
        const row = collarRows[i];
        const rowRadio = row.getElementsByTagName("input")[0];
        if (rowRadio.checked) {
            row.classList.add("active");
            row.closest("table").parentElement.classList.add("active");
        } else {
            row.classList.remove("active");
        }
    }

}

function updateSection5PScore() {
    let total = section4_1Pscore;
    let modifier = 0;
    if (section5_1Dice.getTotal() !== 0) { //may not have been rolled
        modifier = section5_1Dice.getTotal() - 4;
        total += modifier;
    }
    section5Pscore = total;

    const subsectionSpanElement = document.getElementById("p-s-5-1");
    subsectionSpanElement.innerText = section5Pscore;

    const subsectionModifierSpanElement = document.getElementById("p-s-5-2");
    subsectionModifierSpanElement.innerText = modifier > 0 ? "+"+modifier : modifier;

    const table = document.getElementById("5-1");
    let index = 0;
    if (section5Pscore > 19) {
        index = 9;
    } else if (section5Pscore < 4) {
        index = 0;
    } else {
        index = Math.floor(section5Pscore/2) - 1;
    }
    selectTableRow(table, index);
}


function updateCareerImagebox() {
    const imagebox = document.getElementById("career-images");
    const img1 = imagebox.children[0];
    const img2 = imagebox.children[1];
    const img3 = imagebox.children[2];
    const img4 = imagebox.children[3];
    const careerRow = getActiveTableRow(document.getElementById("collars"));
    let careerValue = careerRow.getElementsByTagName("input")[0].value;
    switch (careerValue) {
        case "worker":
            img1.src = "./images/ai/careers/worker1.png";
            img1.alt = "A picture of a tired industrial worker";
            img2.src = "./images/ai/careers/worker2.jpg";
            img2.alt = "A picture of a group of industrial workers";
            img3.src = "./images/ai/careers/worker3.jpg";
            img3.alt = "A picture of busy industrial worksite";
            img4.src = "./images/ai/careers/worker4.jpg";
            img4.alt = "A picture of two asteroid miners in heavy industrial spacesuits";
            break;
        case "soldier":
            img1.src = "./images/ai/careers/soldier1.png";
            img1.alt = "A picture of serious soldier in an armoury";
            img2.src = "./images/ai/careers/soldier2.jpg";
            img2.alt = "A picture of soldiers riding a large, scifi tank";
            img3.src = "./images/ai/careers/soldier3.jpg";
            img3.alt = "A picture of soldiers in long coats guarding a muddy, rainy, ravine";
            img4.src = "./images/ai/careers/soldier4.jpg";
            img4.alt = "A picture of a soldier on guard on a mountain";
            break;
        case "pioneer":
            img1.src = "./images/ai/careers/pioneer1.jpg";
            img1.alt = "A picture of frontier explorer on a distant planet";
            img2.src = "./images/ai/careers/pioneer2.jpg";
            img2.alt = "A picture of pioneers trekking across a desert planet";
            img3.src = "./images/ai/careers/pioneer3.jpg";
            img3.alt = "A picture of explorers on the edge of a frozen lake";
            img4.src = "./images/ai/careers/pioneer4.jpg";
            img4.alt = "A picture of explorers in a forest, with a large tracked vehicle";
            break;
        case "merchant":
            img1.src = "./images/ai/careers/merchant1.png";
            img1.alt = "A picture of careful merchant in a street market";
            img2.src = "./images/ai/careers/merchant2.jpg";
            img2.alt = "A picture of a dark street market at night";
            img3.src = "./images/ai/careers/merchant3.jpg";
            img3.alt = "A picture of a busy commercial district of a scifi city";
            img4.src = "./images/ai/careers/merchant4.jpg";
            img4.alt = "A picture of a sleazy barman in a futuristic bar";
            break;
        case "engineer":
            img1.src = "./images/ai/careers/engineer1.png";
            img1.alt = "A picture of an engineer on a bridge";
            img2.src = "./images/ai/careers/engineer2.jpg";
            img2.alt = "A picture of an engineer controlling a robot";
            img3.src = "./images/ai/careers/engineer3.jpg";
            img3.alt = "A picture of a group of engineers examining a blueprint";
            img4.src = "./images/ai/careers/engineer4.png";
            img4.alt = "A picture of an engineer with equipment inside a data center";
            break;
        case "space-crew":
                img1.src = "./images/ai/careers/spacecrew1.png";
                img1.alt = "A picture of an exhausted spaceship pilot";
                img2.src = "./images/ai/careers/spacecrew2.jpg";
                img2.alt = "A picture of an upside down spaceman in zero gravity";
                img3.src = "./images/ai/careers/spacecrew3.jpg";
                img3.alt = "A picture of a space ship crew relaxing in the mess room";
                img4.src = "./images/ai/careers/spacecrew4.jpg";
                img4.alt = "A picture of a spaceship in space";
                break;
        case "enforcer":
            img1.src = "./images/ai/careers/enforcer1.jpg";
            img1.alt = "A picture of a police detective in body armour on the street";
            img2.src = "./images/ai/careers/enforcer2.jpg";
            img2.alt = "A picture of an investigator exploring an abandoned warehouse";
            img3.src = "./images/ai/careers/enforcer3.jpg";
            img3.alt = "A picture of a detective questioning someone";
            img4.src = "./images/ai/careers/enforcer4.jpg";
            img4.alt = "A picture of a noir detective on a rainy city street";
            break;
        case "entertainer":
            img1.src = "./images/ai/careers/entertainer1.png";
            img1.alt = "A picture of a dreamy pop singer";
            img2.src = "./images/ai/careers/entertainer2.jpg";
            img2.alt = "A picture of a cheesy entertainer, posing next to a tv showing a video of himself";
            img3.src = "./images/ai/careers/entertainer3.jpg";
            img3.alt = "A picture of a man in a robe in the street, with crowds gathering to see him";
            img4.src = "./images/ai/careers/entertainer4.jpg";
            img4.alt = "A picture of a rock star, posing next to a large poster of herself";
            break;
        case "hacker":
            img1.src = "./images/ai/careers/hacker1.png";
            img1.alt = "A picture of a smoking hacker in a basement, using a computer";
            img2.src = "./images/ai/careers/hacker2.jpg";
            img2.alt = "A security camera photo of a pair of hackers in suits as they break into a computer";
            img3.src = "./images/ai/careers/hacker3.jpg";
            img3.alt = "A picture of a hacker in the rain";
            img4.src = "./images/ai/careers/hacker4.jpg";
            img4.alt = "A picture of hacker on a rooftop, patching into exposed wires";
            break;
        case "scientist":
            img1.src = "./images/ai/careers/scientist1.png";
            img1.alt = "A picture of a scientist on a spacestation, with a view to the stars outside";
            img2.src = "./images/ai/careers/scientist2.jpg";
            img2.alt = "A picture of a conference of scientists";
            img3.src = "./images/ai/careers/scientist3.jpg";
            img3.alt = "A picture of a scientist holding a glowing vial of chemicals";
            img4.src = "./images/ai/careers/scientist4.jpg";
            img4.alt = "A picture a chemical laboratory";
            break;
        case "scientist":
            img1.src = "./images/ai/careers/scientist1.png";
            img1.alt = "A picture of a scientist on a spacestation, with a view to the stars outside";
            img2.src = "./images/ai/careers/scientist2.jpg";
            img2.alt = "A picture of a conference of scientists";
            img3.src = "./images/ai/careers/scientist3.jpg";
            img3.alt = "A picture of a scientist holding a glowing vial of chemicals";
            img4.src = "./images/ai/careers/scientist4.jpg";
            img4.alt = "A picture a chemical laboratory";
            break;
        case "corporate":
            img1.src = "./images/ai/careers/corporate1.png";
            img1.alt = "A picture of a stern businessman in a sharp suit";
            img2.src = "./images/ai/careers/corporate2.jpg";
            img2.alt = "A picture of a group of businesmen smoking cigars in a lounge";
            img3.src = "./images/ai/careers/corporate3.jpg";
            img3.alt = "A picture of a pair of well-dressed figures on a rooftop of a cyberpunk city";
            img4.src = "./images/ai/careers/corporate4.jpg";
            img4.alt = "A picture a sharp dressed businesswoman adjusting her coat";
            break;
        case "corporate":
            img1.src = "./images/ai/careers/corporate1.png";
            img1.alt = "A picture of a stern businessman in a sharp suit";
            img2.src = "./images/ai/careers/corporate2.jpg";
            img2.alt = "A picture of a group of businesmen smoking cigars in a lounge";
            img3.src = "./images/ai/careers/corporate3.jpg";
            img3.alt = "A picture of a pair of well-dressed figures on a rooftop of a cyberpunk city";
            img4.src = "./images/ai/careers/corporate4.jpg";
            img4.alt = "A picture a sharp dressed businesswoman adjusting her coat";
            break;
        case "diplomat":
            img1.src = "./images/ai/careers/diplomat1.jpg";
            img1.alt = "A picture of focused diplomat at a desk";
            img2.src = "./images/ai/careers/diplomat2.jpg";
            img2.alt = "A picture of an exhausted traveller asleep in a chair at the spaceport";
            img3.src = "./images/ai/careers/diplomat3.jpg";
            img3.alt = "A picture of diplomat introducing two people to each other at an event";
            img4.src = "./images/ai/careers/diplomat4.png";
            img4.alt = "A picture a a diplomat wearing a namebadge shaking hands with someone";
            break;
        default:
            img1.src = "./images/rollagain.png";
            img1.alt = "roll again message image";
            img2.src = "./images/rollagain.png";
            img2.alt = "roll again message image";
            img3.src = "./images/rollagain.png";
            img3.alt = "roll again message image";
            img4.src = "./images/rollagain.png";
            img4.alt = "roll again message image";
            break;
    }
}