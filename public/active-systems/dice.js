class Dice {
    element;
    value;
    diceElement;

    constructor(diceElement) {
        this.element = diceElement;
        this.value = 1;
        this.replaceCanvasWithDiceElements();
        this.updateVisual();
    }

    getRandom() {
        return Math.ceil(Math.random() * 6);
    }

    async onRoll() {
        await this.spin(500);
        this.value = this.getRandom();
        this.updateVisual();
        await new Promise(resolve => setTimeout(resolve, 1000));
        this.element.setAttribute('data-value', this.value);
        this.element.getElementsByClassName("dice-result")[0].innerText = this.value;
    }

    replaceCanvasWithDiceElements() {
        const diceHtml = `<div class="dice3dscene">
                            <div class="dice3d">
                               <div class="dice3d__face dice3d__face--1">1</div>
                               <div class="dice3d__face dice3d__face--6">6</div>
                               <div class="dice3d__face dice3d__face--4">4</div>
                               <div class="dice3d__face dice3d__face--3">3</div>
                               <div class="dice3d__face dice3d__face--2">2</div>
                               <div class="dice3d__face dice3d__face--5">5</div>
                            </div>
                          </div>`;
        this.element.getElementsByTagName("canvas")[0].remove();
        this.element.getElementsByClassName("dice-result")[0].classList.add("hidden");
        this.element.insertAdjacentHTML( 'afterbegin', diceHtml );
        this.diceElement = this.element.getElementsByClassName("dice3d")[0];
    }

    updateVisual() {
        this.diceElement.classList.remove("show-1");
        this.diceElement.classList.remove("show-2");
        this.diceElement.classList.remove("show-3");
        this.diceElement.classList.remove("show-4");
        this.diceElement.classList.remove("show-5");
        this.diceElement.classList.remove("show-6");

        this.diceElement.classList.add(`show-${this.value}`);
    }

    async spin(duration) {
        const delay = 500;
        this.diceElement.style.transition = `transform ${delay/1000}s`;

        let timeSpendSpinning = 0;
        let yr = 0;
        let xr = 0;
        while (timeSpendSpinning < duration) {
            yr += Math.random() * 190;
            xr += Math.random() * 190;
            this.diceElement.style.transform = `rotateY(${yr}deg) rotateX(${xr}deg)`;
            await new Promise(resolve => setTimeout(resolve, delay));
            timeSpendSpinning += delay;
        }
        this.diceElement.style.removeProperty("transform");
        this.diceElement.style.transition = `transform 1s`;
    }
}




class Diceset {
    element;
    dice = [];
    total;

    constructor(dicesetElement) {
        this.element = dicesetElement;
        const diceElements = this.element.getElementsByClassName("dice");
        for (let i = 0; i < diceElements.length; i++) {
            this.dice.push(new Dice(diceElements[i]));
        }
        this.total = 0;
    }

    async onRoll() {
        this.element.getElementsByClassName("dice-total")[0].innerText = "-";
        let promises = [];
        for (let i = 0; i < this.dice.length; i++) {
           promises.push(this.dice[i].onRoll());
           await new Promise(resolve => setTimeout(resolve, Math.random()*100));
        }
        await Promise.all(promises);
        this.updateDiceResults();
    }

    updateDiceResults() {
        let total = 0;
        for (let i = 0; i < this.dice.length; i++) {
            const value = this.dice[i].value;
            total += value;
        }
        this.element.getElementsByClassName("dice-total")[0].innerText = total;
        this.total = total;
    }

    getTotal() {
        return this.total;
    }
}




class Dicebox extends EventTarget {
    root;
    dicesets = [];

    constructor(diceboxElement) {
        super();
        this.root = diceboxElement;
        const dicesetElements = this.root.getElementsByClassName("dices");
        for (let i = 0; i < dicesetElements.length; i++) {
            this.dicesets.push(new Diceset(dicesetElements[i]));
        }

        this.root.getElementsByClassName("dice-roll")[0].addEventListener("click", this.onRoll.bind(this));
    }

    async onRoll() {
        for (let i = 0; i < this.dicesets.length; i++) {
            await this.dicesets[i].onRoll();
            await new Promise(resolve => setTimeout(resolve, Math.random()*100));
        }
        this.dispatchEvent(new Event("rolled"));
    }

    updateDiceResults() {
        for (let i = 0; i < this.dicesets.length; i++) {
            this.diceset[i].updateDiceResults();
        }
    }

    getTotal() {
        let total = 0;
        for (let i = 0; i < this.dicesets.length; i++) {
            total += this.dicesets[i].getTotal();
        }
        return total;
    }

    getResults() {
        let results = [];
        for (let i = 0; i < this.dicesets.length; i++) {
            results.push(this.dicesets[i].getTotal());
        }
        return results;
    }

}