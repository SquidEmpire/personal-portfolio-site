function GetLearnMoreDataForDataValue(dataValue) {
    let title = "ERROR!";
    let text = "Missing Data";
    switch(dataValue)
    {
        case "articant":
            title = "Articants";
            text =
            `Articants are synthetic life forms, also known as androids, robots, etc. They are created by large corporations and sold and owned as property.

            The most expensive and highest quality Articants have bio-engineered organic bodies, while cheaper or older models are made of synthetic electromechanical parts. 
            Once assembled they spend some time in monitoring and educating facilities before being registered and sold. 
            From starting the brain growing process to sale typically takes 15 years. The Positronic wetware used for Articant brains decays over time, giving Articants a lifetime of somewhere between 50-200 years (depending on grade).

            Initial Articants were used for cheap labour, but as the technology improved and it became more expensive to produce new models, they were gradually used for more refined tasks, until by 2200 Articants were treated practically as equal to humans in all places except The Romance Alliance and their successor Republic - where Articants were and are considered below humans, and have reduced legal rights. 
            Despite the increasing sophistication of Articant technology and consciousness, they are still treated as property or tools by the corporations that produce them.`;
            break;
        case "cybernetics":
            title = "Cybernetics";
            text = 
            `Cybernetics constitutes the replacement or augmentation of parts of the human body with manufactured parts. This was made possible by research from the Pacific Prosperity Pact, and is fairly common across the universe now.
            Most cybernetics are replacement parts, but there is a subculture of people interested in cosmetic or transhumanist cybernetic replacement. Typically cybernetic parts are not as efficient as natural parts, but may have unexpected advantages in novel ways. E.g. a cybernetic eye may have less visual acuity than a natural eye, but may be able to detect wavelengths of light invisible to ordinary eyes.`;
            break;
        case "worker":
            title = "Workers";
            text = `Workers are the core demographic of industrial worlds. They perform all sorts of duties in all sorts of environments, typically doing hands-on labouring in polluted or dangerous places such as mines, spaceports, or factories. Workers are ubiquitous and anonymous across the universe.
            
            The Worker class has bonuses in toughness, machinery, and blending in.`;
            break;
        case "soldier":
            title = "Soldiers";
            text = `Soldiers and mercenaries are recruited by governments, corporations, pirate gangs, and private individuals to act as security or force projection. Specially trained and motivated, their main job is to be able to defeat any threats in combat. Soldiers also sometimes specialise in space combat or in non-combat roles, such as peacekeeping or reconnaissance.
            
            The Soldier class has bonuses in combat and military activities.`;
            break;
        case "pioneer":
            title = "Pioneers";
            text = `On remote planets, frontier wastelands, or untamed wilderness, you will find Pioneers: rugged frontiersmen and women, exploring and settling the boundaries of human civilisation. They are individualistic and tough.
            
            The Pioneer class has bonuses in survival, medicine, and construction.`;
            break;
        case "merchant":
            title = "Merchants";
            text = `Merchants work in densely settled cities and towns as well as on space stations to make a profit from the transfer of goods and services. The job requires a lot of savvy, and merchants are often versed in the darker arts of commerce as well.
            
            The Merchant class has bonuses in commerce, smuggling, and stealth.`;
            break;
        case "engineer":
            title = "Engineers";
            text = `Engineers are educated technicians, employed to solve technical problems. Without engineers, the large scale projects and advanced technologies that define the era couldn’t function. Every ship, station, construction site, or settlement is guaranteed to have at least a handful or engineers involved with keeping things running smoothly.
            
            The Engineer class has bonuses to machinery, electronics, and robotics.`;
            break;
        case "space_crew":
            title = "Space Crewmembers";
            text = `Space travel is a specialised endevour and required specially trained crews operate spaceships. Space crew are are also found on space stations, operating machinery and hydroponics. Some crews spend virtually their entire lives in space, without setting foot on planets. Crews often form tight-knit groups with their own internal rituals and lingo.
            
            The Space Crewmember class has bonuses to survivalism, secrecy, and precision.`;
            break;
        case "enforcer":
            title = "Enforcers";
            text = `Enforcers are similar to soldiers in that they are tasked with keeping peace and are hired by companies, governments, criminal gangs, and individuals: but where they differ is their methods. Enforcers are more akin to detectives, tasked with hunting bounties, tracking fugitives, investigating crimes, and so on. They usually work alone, and are mostly found in large cities.
            
            The Enforcer class has bonuses to perception, intuition, and fighting.`;
            break;
        case "entertainer":
            title = "Entertainers";
            text = `Entertainers are found across the universe in settlements and stations, where they bring joy and art to people’s lives while also spreading culture and messages they care about. Celebrity entertainers can have fanbases in the billions, while even niche performers can have considerable sway over their fans.
            
            The Entertainer class has bonuses to performance, hypnosis, and disguise.`;
            break;
        case "hacker":
            title = "Hackers";
            text = `Hackers are highly sought after by corporations, criminals, and governments for their abilities to both create and defeat security systems. Often living in obscurity under pseudonyms and in basements, hackers are rarely seen in person.
            
            The Hacker class has bonuses to stealth and electronics.`;
            break;
        case "scientist":
            title = "Scientists";
            text = `Scientists and researchers are employed by corporations and governments to develop new ways to solve problems, or to clarify new problems. Aided greatly by their access to Machine Intelligences, competent scientists are able to synthesise incredible technologies for regular use, such as cybernetics or Gates. Scientists are greatly respected, especially among the general public, and all politicians like to keep a few around as advisors. 
            
            The Scientists class has bonuses to medicine and mathematics.`;
            break;
        case "corporate":
            title = "Corporates";
            text = `Corporates are seen in urban places in their tailored suits and penthouse apartments, but are mostly disconnected from the realities of the majority of the population. Instead, Corporate workers fight high stakes games in boardrooms and on spreadsheets. Sometimes things get more “hands-on” though, and Corporate raiders are known to be as ruthless as they are efficient.
            
            The Coporate class has bonuses to investment, fraud, and drug usage.`;
            break;            
        case "diplomat":
            title = "Diplomats";
            text = `Maintaining peace throughout the universe is a small army of diplomats, who spend most of their time shuttling between worlds and shaking hands. Jesuit diplomats from the Vatican are some of the most prolific and tireless.
            
            The Diplomat class has bonuses to persuasion, non-lethal combat, and travel.`;
            break;
        case "megacorps":
            title = "Megacorporations";
            text = `The companies of the 20th century grew and merged and eventually became so powerful they challenged nations for the right to rule Earth. Corrupt politicians and corporate ambition led to continual weakening of regulations and government power, until by the time the Solar Union was formed, large corporations were essentially independent nations that controlled their employees like citizens.
            


            Major Solar Union Corporation include:
            
            Yen-Gladstone Proprietary - Megacorporporation from Australia/Japan with diverse businesses. Core sectors include finance, mining, heavy industry, and space infrastructure.

            Solcorp - Banking, agriculture, food, and consumer services megacorpotation from Mexico.

            Nordika - Technology, heavy industry, and energy corporation from Scandinavia. Focused on energy infrastructure, fossil fuels, starship construction, and electronics.

            Cyberatomics - An old megacorpoation from the 3P that is the leader in the fields of nuclear power, Articants, cybernetics, and positronics. 

            Vickson Adams - Large manufacturing company from England/USA focused on consumer goods and also a leader in arms and spaceship manufacturing. 

            Vonphysik - The largest medical and chemical corporation, and a producer of specialist scientific equipment. Originally from Germany. Also a strong research and development company, in medicine and in physics. Responsible for Gate development and maintenance.

            Ryoshi Group - Manufacturing megacorporation with a powerful arms manufacturing and paramilitary component, from Japan.

            COM-PAC - Mining, freight, and colonial infrastructure company from South Africa.`;
            break;
        case "machine_intelligence":
            title = "Machine Intelligences";
            text = `Early Positronic intelligence research was conducted at massive supercomputer installations, and many of the most powerful modern machine intelligences evolved in these facilities. New positronic machine intelligence facilities are no longer constructed, but surviving facilities are maintained and the machine intelligences put to use, mainly by corporations.
            


            Notable Machine Intelligences include:

            FUJI - the first machine considered to have achieved sentience. Built in Japan, very stable. Used mainly for materials research.
            
            Mryivac - built to run London's infrastructure, presumed destroyed in the nuclear war of 2222. Rumoured still operational in some capacity.

            SOCii - most powerful machine currently in operation. Operates Southern Hemisphere astronomy facilies from central Australia. Solved the Casimir-Chen Bridge Equations that allowed gateway development.

            Sunchaser - earth-orbiting machine tasked with archiving governmental and coporate records, suffers from neutrino degredation.
            
            Nerio - The only positionic facility outside earth (built on Mars), considered unstable and only used by the Mars University undergraduate cohort as a training machine.
            
            Kaznik - central Asian machine used to manage Articant production facilites until it went haywire and began inserting malicious backdoor code, resulting in "Kaznik Drone" Articants, which still cause problems.
            `;
            break;
        case "pirates":
            title = "Pirates";
            text = `Freebooters and raiders, pirates are found around their bases within solar systems. There is only one known Class A (interstellar) pirates vessel capable of interstellar travel (Revenge) and Gate travel is highly protected and regulated, and so they are typically a local nuisance around the worlds or stations they hide on. However, shady backdeals mean that corporate or government sponsored privateers sometimes sneak through gates to harass rival targets. 

            Piracy is most common in interplanetary space and towards Class B (interplanetary) ships. 
            
            
            Notable pirate groups include:

            The Cryler Gang - The most powerful pirate gang. Operates within the Solar System from the hijacked Atlantic Superstate Class A Starship "AS Steadfast", renamed "Revenge".

            The Rabbit Horde - Originally a Chinese triad criminal organisation, the Horde now operates from the Moon and Mars. They focus on extortion over piracy.

            M & A - A highly sophisticated and powerful privateer group which targets ascendant corporations or other high level targets with overwhelming firepower. Typically strikes in the deep space between spaceports and Gates, in the Sol or Alpha Centauri systems.

            Resurrection Men - A dark and sinister group operating in the outer limits of Alpha Centauri and with groups elsewhere that focuses on kidnapping victims for organ harvesting. Modus operandi includes fake distress calls to lure in victims.

            Booters - Amid the ongoing landgrab on Solden (Ross 128b) large numbers of quasi-sanctioned Solar Union privateers have landed to harass and destroy claims by rival powers. Booters are usually poorly organised and are often convicts simply dumped on Solden with weapons by Solar Union ships in order to cause trouble. `;
            break;
        case "political_extremists":
            title = "Political Extremists";
            text = `Many groups across the universe seek to apply their political goals via violence. Typically the bulk of these groups are composed of people from the margins of society, while the core leadership more often includes powerful, organised, and very educated thought-leaders.
            
            Notable political extremist groups include:
            
            Anarchists - Typically anarchist groups are formed from dissent within the Solar Union. Anarchists seek to destroy political and corporate power and generally leave private travellers alone. Concentrated around Earth and on Mars, but with support in many colonial industrial and urban areas as well.
            
            Reconquestors - Splinter factions of the Republic which seeks revenge for the dissolution of, and aims for the restoration of, the Romance Alliance. Mostly active on Paranoir with smaller cells found in most Republic areas, including in former Romance Alliance territory on Earth.
            
            Shishi - An old splinter ultranationalist remnant group of the Pacific Prosperity Pact (3P) which seeks to gain independence and control of former 3P territory from the Solar Union, and to restore the Technodaimyo. They are based around Mikkauri at Wolf 395, with clandestine support from both the Ryoshi Group megacorporation, and the Republic.
            
            Martian People's Army - The MPA is a communist paramilitary force operating on Mars which is attempting to overthrow the Mars Solar Union government and install a communist replacement. They attack and destroy Solar Union facilities and forces, and enjoy support from Martian ranchers and industrial workers.`;
            break;
        case "governments":
            title = "Governments";
            text = `Many governments and super-governments ahev risen and fallen. At the moment, the two main government bodies in existance are the Solar Union and the Republic.
            
            The Solar Union is the government of Earth, and owner of many colonies. Realistically it is a figurehead government fully controlled by the largest corporations on Earth. The Union is fully committed to laissez-faire market principles, with extremely minimal oversight. It is presiding over a disjointed and fragmentary civilisation. Most speak English. Japanese is a common second language, and Mandarin is spoken on the Moon and by Naval personnel.

            There is a congress and the leader of the Union is the President. Congressmen/women are elected from Megacity-states and corporations. 

            There is little internal harmony in the Union, with the various Corporations and Megacity-states that make it up constantly vying and competing with each other.
            

            Notable Solar Union Subfactions: 
            
            Megacity-states - After the nuclear war of 2222, most of Earth was turned into wasteland and the majority of the planet’s cities were destroyed and abandoned. Populations fled to larger, more stable cities, which became what are now called Megacities. Each Megacity is basically independent of the others but all are fairly similar, being extremely crowded and polluted. Major megacity-states include Osakyo, Phoenix, Wuhan, Chicago, and Belo Horizonte.
            
            Solar Union Navy - the SUN is based on the Moon. It is an effective and prestigious organisation. The Navy focuses on protecting Solar Union staions and ships from Pirates. Naval personal must speak both Mandarin and English.
            
            Solar Union Security - the SUS is the land forces of the Solar Union. It is considered unprestigious and ineffective. The SUS employs convicts as a way to serve their time.
            
            

            
            The Republic (Officially “La République Romanes”) consists of former colonies of an earth-based superstate called the Romance Alliance which was destroyed in the nuclear war of 2222. The former Romance Alliance colonies are now loosely allied due to former allegiance as provinces, but still independent of each other. All Republic departments share a vibrant culture and massive refugee populations, as well as a focus on pure-humanism and culture. The Republic bans Articants and genetic manipulation, and strongly regulates cybernetics all throughout their territory. Most citizens speak English and French. Spanish, Portuguese, Italian, and Latin are still spoken as well.

            The leader of the Republic is the President, and the leadership is the Directory, who are recruited from the various provinces. Many provinces have noble houses as well, with the law system in particular being populated by judges from dynastic law families. 

            The most powerful province in the Republic is Vinci, the original RA colony, which is based on Paranoir in Alpha Centauri.

            Notable Republic Subfactions:

            Provinces - the various colonies and settlements of the Republic are organised in Provinces. Notable provinces include Vinci, Basé, Espérance, Ney, Algomezia (all on Paranoir), Novo Gallia (Solden), Vagus (Alpha Centauri objects), Ocaso (Barnard's Star), and the Vatican (Earth). 
            
            Republic Navy - Compared to the Solar Union Navy, the Republic Navy is much larger, but consists of smaller and less well organised and equipped vessels. The Republic’s navy is highly decentralised and chronically disorganised, relying on the judgement of individual captains to maintain their ships and sectors. Republic Navy crews typically forgo terrestial life and commit to living in space full-time.
            
            Republic Armies - The various Republic departments and provinces have their own armies. There are provisions to centralise these into a Grande Armée for the Republic if the necessity arises. Provincial armies of the Republic are typically better equipped and trained than the Solar Union ground forces and the armies are considered prestigious.`;
            break;
    }
    return {title: title, text: text};
}