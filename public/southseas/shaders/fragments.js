 // https://thebookofshaders.com/10/
const Noise = `
    float Noise (vec2 st) 
    {
        return fract( sin( dot( st.xy, vec2(12.9898,78.233 ) ) ) *  43758.5453123);
    }
`;

 // https://www.youtube.com/watch?v=zXsWftRdsvU
 // requires Noise to work
const SmoothNoise = `
    float SmoothNoise( vec2 ip )
    {
        vec2 lv = fract( ip );
        vec2 id = floor( ip );
        
        lv = lv * lv * ( 3. - 2. * lv );
        
        float bl = Noise( id );
        float br = Noise( id + vec2( 1, 0 ));
        float b = mix( bl, br, lv.x );
        
        float tl = Noise( id + vec2( 0, 1 ));
        float tr = Noise( id + vec2( 1, 1 ));
        float t = mix( tl, tr, lv.x );
        
        return mix( b, t, lv.y );
    }
`;

const PlantMotion = `
    float theta = sin( time + position.y ) / 5.0;
    float theta2 = sin( time ) / 5.0;

    float n = SmoothNoise(position.xz * 0.5 + vec2(0.0, time));
    n = pow(n * 0.5 + 0.5, 2.0) * 2.0;
    n += theta;

    //this provides the rotational y motion
    //float py = (theta2 - n * 0.1) * 0.2;
    float py = n * 0.01;

    //this is used for the leaf in-out motion
    float hn = n;

    //mat3 m = mat3( hn,0,0,  0,1,0, 0,py,hn );
    mat3 m2 = mat3( 1,0,0,  0,1,0, 0,py,1 );

    vec3 transformed = vec3( position ) * m2;
    vNormal = vNormal * m2;
`;

const RiverMotion = `
    gl_FragColor = texture2D(waterTexture, vec2(vUv.x + time, vUv.y));
`;


export { Noise, SmoothNoise, PlantMotion, RiverMotion }