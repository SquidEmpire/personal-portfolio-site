varying vec2 vUv;
uniform float time;
varying float z;
varying float disp;

float N (vec2 st) { // https://thebookofshaders.com/10/
    return fract( sin( dot( st.xy, vec2(12.9898,78.233 ) ) ) *  43758.5453123);
}

float smoothNoise( vec2 ip ){ // https://www.youtube.com/watch?v=zXsWftRdsvU
    vec2 lv = fract( ip );
    vec2 id = floor( ip );
    
    lv = lv * lv * ( 3. - 2. * lv );
    
    float bl = N( id );
    float br = N( id + vec2( 1, 0 ));
    float b = mix( bl, br, lv.x );
    
    float tl = N( id + vec2( 0, 1 ));
    float tr = N( id + vec2( 1, 1 ));
    float t = mix( tl, tr, lv.x );
    
    return mix( b, t, lv.y );
}

void main() {
    vUv = uv;

    float t = time * 0.25;

    vec4 mvPosition = vec4( position, 1.0 );

    float noise = smoothNoise(mvPosition.xy * 0.1 + vec2(0.0, t));
    noise = pow(noise * 1.0, 2.0) * 1.0;

    float displacement = noise;
    mvPosition.z += displacement;
    disp = displacement;

    vec4 modelViewPosition = modelViewMatrix * mvPosition;
    vec4 pos = projectionMatrix * modelViewPosition;
    z = pos.z;
    gl_Position = pos;
}