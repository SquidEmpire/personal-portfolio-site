varying vec2 vUv;
  
void main() {
  const vec3 tipColour = vec3( 0.75, 0.88, 0.28 );
  const vec3 baseColour = vec3( 0.49, 0.60, 0.0 );
  vec3 colour = mix(baseColour, tipColour, vUv.y);
  gl_FragColor = vec4( colour, 1 );
}