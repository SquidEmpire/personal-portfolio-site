uniform vec3 colour;
varying vec2 vUv;
uniform float time;
uniform float geoSize;
uniform float dataSize;
uniform sampler2D noiseTexture;
uniform sampler2D depthTexture;
uniform float opacity;

//https://www.shadertoy.com/view/Xl2XWz
//Created by Shane in 2015-09-22
float smoothNoise(vec2 p) {
	vec2 i = floor(p);
    p-=i;
    p *= p*(3.0-p-p);
    return dot( mat2( fract( sin( vec4(0, 1, 27, 28) + i.x + i.y * 27.0) * 1e5) ) * vec2(1.0 - p.y, p.y), vec2(1.0 - p.x, p.x));
}

float fractalNoise(vec2 p) {
    return smoothNoise(p) * 0.5333 + smoothNoise(p * 2.0) * 0.2667 + smoothNoise(p * 4.0) * 0.1333 + smoothNoise(p * 8.0) * 0.0667;    
}

float warpedNoise(vec2 p, float t) {
    vec2 m = vec2(t, -t)*0.5;
    float x = fractalNoise(p + m);
    float y = fractalNoise(p + m.yx + x);
    float z = fractalNoise(p - m - x + y);
    return fractalNoise(p + vec2(x, y) + vec2(y, z) + vec2(z, x) + length(vec3(x, y, z))*0.25);
}

float PureNoise (vec2 st) 
{
    return fract( sin( dot( st.xy, vec2(12.9898,78.233 ) ) ) *  43758.5453123);
}


void main() {

    vec4 deepestColour = (vec4(0.124, 0.218, 0.388, 1.0));
    vec4 deepColour = (vec4(0.14, 0.34, 0.68, 1.0));
    vec4 midColour = (vec4( 0.14, 0.38, 0.59, 1.0 ));
    vec4 closeColour = (vec4( 0.12, 0.43, 0.66, 1.0 ));
    vec4 shoreColour = (vec4( 0.21, 0.57, 0.77, 1.0 ));
    vec4 shorestColour = (vec4( 0.982, 1.0, 1.0, 1.0 ));

    float scale = geoSize * 0.1;
    float speed = scale * 0.0001;

    float n = warpedNoise(vUv * scale * 3.0, time * speed * 10.0);
    
    vec4 mix1 = mix(deepestColour, deepColour, n);

    //gl_FragColor = mix1;

    vec2 vUv2 = vec2(vUv) * scale * 5.0 + time * speed;
    vec4 noise = texture2D(noiseTexture, vUv2);

    float foam = abs(max(
        n - noise.x,
        0.0
    )) * 3.0;

    foam -= gl_FragCoord.w * 7.0; //this makes the foam vanish when close to the camera

    vec4 mix2 = mix1 + min(max(foam, 0.0), 1.0);
    //gl_FragColor = mix2;

    vec2 vUv3 = vUv * 2.0 - 1.0;
    vUv3 *= (geoSize - dataSize) * 0.008;
    vUv3 = vUv3 * 0.5 + 0.5;
    vec4 depth = texture2D(depthTexture, vUv3);
    depth = vec4(1.0 - depth.r, 1.0 -depth.g, 1.0 -depth.b, 1); //invert so black = deep
    
    depth *= 0.6;
    vec4 mix3 = mix(mix2, shoreColour, depth.r); //+ min(max(foam, 0.0), 1.0);

    //depth *= 0.1;
    //vec4 mix4 = mix1 + depth + min(max(foam, 0.0), 1.0);

    mix3.a = opacity;
    gl_FragColor = mix3;

}