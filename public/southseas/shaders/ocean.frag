varying vec2 vUv;
varying float disp;
varying float z;

uniform sampler2D depthTexture;
uniform float time;


//https://www.shadertoy.com/view/Xl2XWz
//Created by Shane in 2015-09-22
float smoothNoise(vec2 p) {
	vec2 i = floor(p); p-=i; p *= p*(3.0-p-p);
    return dot( mat2( fract( sin( vec4(0, 1, 27, 28) + i.x + i.y * 27.0) * 1e5) ) * vec2(1.0 - p.y, p.y), vec2(1.0 - p.x, p.x));
}

float fractalNoise(vec2 p) {
    return smoothNoise(p) * 0.5333 + smoothNoise(p * 2.0) * 0.2667 + smoothNoise(p * 4.0) * 0.1333 + smoothNoise(p * 8.0) * 0.0667;    
}

float warpedNoise(vec2 p, float t) {
    vec2 m = vec2(t, -t)*0.5;
    float x = fractalNoise(p + m);
    float y = fractalNoise(p + m.yx + x);
    float z = fractalNoise(p - m - x + y);
    return fractalNoise(p + vec2(x, y) + vec2(y, z) + vec2(z, x) + length(vec3(x, y, z))*0.25);
}

void main() {

    //find a way to make these const
    vec3 deepColour = LinearTosRGB(vec4( 0.04, 0.24, 0.38, 1.0 )).xyz; //0C3C60
    vec3 midColour = LinearTosRGB(vec4( 0.04, 0.28, 0.49, 1.0 )).xyz; //09487D
    vec3 closeColour = LinearTosRGB(vec4( 0.02, 0.33, 0.56, 1.0 )).xyz; //04538E
    vec3 shoreColour = LinearTosRGB(vec4( 0.11, 0.47, 0.67, 1.0 )).xyz; //1B79AC

    float depthValue = texture2D(depthTexture, vUv).x; //all channels should be the same!!

    vec3 colour = deepColour;

    if ( depthValue < 0.01 ) {
        colour = shoreColour;
    } else if (depthValue < 0.05 ) {
        colour = closeColour;
    } else if (depthValue < 0.1) {
        colour = midColour;
    }

    //noise stuff    
    // Take two noise function samples near one another.
    // float n = warpedNoise(vUv * 2.0, time * 0.05);
    // float n2 = warpedNoise(vUv * 2.0 + 0.02, time * 0.05);
    
    // float bump = max(n2 - n, 0.0)/0.02*0.7071;
    // float bump2 = max(n - n2, 0.0)/0.02*0.7071;
    
    // // Produce a color based on the original noise function, then add the highlights.
    // vec3 col = n*n*(vec3(1, 0.7, 0.6)*vec3(bump, (bump + bump2)*0.4, bump2)*0.2 + 0.5);
    
    // //gl_FragColor = vec4( colour, 1 );
    // //gl_FragColor = vec4( mix(closeColour, deepColour, depthValue), 1 );
    //col + colour

    float a = 0.5;
    a *= (smoothstep(0.0, 0.2, vUv.x)) * (1.0 - smoothstep(1.0 - 0.2, 1.0, vUv.x));
    a *= (smoothstep(0.0, 0.2, vUv.y)) * (1.0 - smoothstep(1.0 - 0.2, 1.0, vUv.y));

    colour.yz -= ((1.0 - disp) * 0.1);
    colour *= max( 0.5, min(z * 0.01, 1.5));

    gl_FragColor = vec4(colour, a);

}