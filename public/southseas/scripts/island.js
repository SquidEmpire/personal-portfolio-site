import * as THREE from './vendor/three.core.js';
import * as BufferGeometryUtils from './vendor/BufferGeometryUtils.js';

import * as GAME from './game.js';
import { GameObject } from './gameObject.js';
import { RandomNumberGenerator } from './randomNumberGenerator.js';
import { Cube } from './tiles/cube.js';
import { Ramp } from './tiles/ramp.js';
import { InnerCorner } from './tiles/innerCorner.js';
import { OuterCorner } from './tiles/outerCorner.js';
import { River } from './river.js';
import { SandCube } from './tiles/sandCube.js';
import { SandInnerCorner } from './tiles/sandInnerCorner.js';
import { SandOuterCorner } from './tiles/sandOuterCorner.js';
import { SandRamp } from './tiles/sandRamp.js';
import { CoastalCube } from './tiles/coastalCube.js';
import { RiverbedCorner } from './tiles/riverbedCorner.js';
import { RiverbedStraight } from './tiles/riverbedStraight.js';
import { RiverbedEnd } from './tiles/riverbedEnd.js';
import { RiverbedRamp } from './tiles/riverbedRamp.js';
import { Pinetree } from './resources/pinetree.js';
import { OliveTree } from './resources/olivetree.js';
import { BasilPlant } from './resources/basilPlant.js';
import { MintPlant } from './resources/mintPlant.js';
import { SagePlant } from './resources/sagePlant.js';
import { ThymePlant } from './resources/thymePlant.js';
import { Bush } from './resources/bush.js';
import { LavenderBush } from './resources/lavenderBush.js';
import { RosemaryBush } from './resources/rosemaryBush.js';
import { CornflowersFlowers } from './resources/cornFlowers.js';
import { GoldenrodFlowers } from './resources/goldenrodFlowers.js';
import { RoseBush } from './resources/roseBush.js';
import { CoastalRamp } from './tiles/coastalRamp.js';
import { CoastalInnerCorner } from './tiles/coastalInnerCorner.js';
import { CoastalOuterCorner } from './tiles/coastalOuterCorner.js';
import { Pinetree2 } from './resources/pinetree2.js';

class Island extends GameObject {

    isGenerated = false;
    isGenerating = false;
    /*
    0 = basic geometry tiles
    1 = terrain smoothing
    2 = rivers
    3 = the ol' merge 'n purge
    4 = trees / props
    5 = dock
    */
    generationStage = 0;
    passNumber = 0;
    PASSES_PER_LAYER = [];

    pivot;

    //maxTiles = 10000;
    tilesPlaced = 0;
    tilesPerLayer = [];
    nextTilePosition = new THREE.Vector3();

    layer = 0; //the layer of the island we're currently working on

    maxX
    halfMaxX;
    maxY;
    halfMaxY;
    maxZ;
    halfMaxZ;

    maxTilesPerLayer = this.maxX * this.maxY;
    validCoordinatesLeftOnLayer; //queue of valid coordinates left in this layer

    theGrid; //3d array x,y,z

    rivers = [];
    maxRivers = null;

    mergedGeometry;
    material;
    mergedIsland;

    generateFast;

    constructor() {
        super();
    }

    initialize(size) {
        super.initialize();

        let maxX;
        let maxY;
        let maxZ;

        switch (size) {
            default:
            case "small":
                console.log("small");
                maxX = 50;
                maxY = 50;
                maxZ = 9;
                this.PASSES_PER_LAYER = [
                    3, 3, 2, 1, 1, 
                    1, 1, 1, 1, 1,
                    1, 1, 1, 1, 1,
                    1, 1, 1, 1, 1
                ];
                break;
            case "medium":
                console.log("medium");
                maxX = 150;
                maxY = 150;
                maxZ = 14;
                this.PASSES_PER_LAYER = [
                    6, 4, 4, 3, 2, 
                    2, 2, 1, 1, 1,
                    1, 1, 1, 1, 1,
                    1, 1, 1, 1, 1
                ];
                break;
            case "large":
                console.log("large");
                maxX = 400;
                maxY = 400;
                maxZ = 20;
                this.PASSES_PER_LAYER = [
                    8, 4, 4, 4, 3, 
                    3, 2, 2, 2, 2,
                    2, 1, 1, 1, 1,
                    1, 1, 1, 1, 1
                ];
                break;
        }

        this.maxX = maxX;
        this.maxY = maxY;
        this.maxZ = maxZ;
        this.halfMaxX = this.maxX/2;
        this.halfMaxY = this.maxY/2;
        this.halfMaxZ = this.maxZ/2;

        this.pivot = new THREE.Group();
        GAME.Globals.scene.add( this.pivot );
    }

    resetTheGrid() {
        //fill the grid with empties
        this.theGrid = new Array();
        for (let i = 0; i < this.maxX; i++) {
            this.theGrid[i] = new Array();
            for (let j = 0; j < this.maxY; j++) {
                this.theGrid[i][j] = new Array();
                for (let k = 0; k < this.maxZ; k++) {
                    this.theGrid[i][j][k] = null;
                }
            }
        }

        for (let i = 0; i < this.maxZ; i++) {
            this.tilesPerLayer[i] = 0;
        }

        this.validCoordinatesLeftOnLayer = new Array();
    }

    generateIsland( fast = false ) {

        this.updateStageText();

        if (this.isGenerated || this.isGenerating) {
            return false;
        }
        this.resetTheGrid();
        
        this.changeLayer( 0 );
        this.isGenerating = true;
        this.generateFast = fast;

        //start with an initial tile at the centre as the nucleus
        this.nextTilePosition.set( this.halfMaxX, this.layer, this.halfMaxY );
        this.addTileAtNextPosition( Cube );

        this.fillVaidCoordsWithEmptyCoords();
    }

    /*
    destroy the entire island and clear the grid data
    pray to jesus the garbage collector deals with all the old data
    */
    clearIsland() {

        //kill rivers
        for(let i = 0; i < this.rivers.length; i++) {
            this.pivot.remove(this.rivers[i].mergedWater);
            this.rivers[i].destroy();
        }
        this.rivers = [];

        //destroy the surface features by killing and remaking the instance managers
        for (const controller of Object.values(GAME.Globals.instanceControllers)) { 
            controller.destroy();
        }

        GAME.BuildInstanceControllers();

        //destroy every tile in the grid and replace with an empty
        if (this.theGrid) {
            for (let i = 0; i < this.maxX; i++) {
                for (let j = 0; j < this.maxY; j++) {
                    for (let k = 0; k < this.maxZ; k++) {
                        if (this.theGrid[i][j][k] !== null) {
                            this.theGrid[i][j][k].destroy();
                            this.theGrid[i][j][k] = null;
                        }
                    }
                }
            }
        }

        for (let i = 0; i < this.maxZ; i++) {
            this.tilesPerLayer[i] = 0;
        }

        this.validCoordinatesLeftOnLayer = new Array();

        this.pivot.remove(this.mergedIsland);
        this.pivot.clear();
        if (this.mergedGeometry) {
            this.mergedGeometry.dispose();
            this.material.dispose();
        }

        //this might be sus
        GAME.Globals.solidObjects = [];
        
        this.tilesPlaced = 0;
        this.layer = 0;

        GAME.Globals.island = null;
        GAME.Globals.ocean.updateDepth();
        GAME.Globals.ocean.destroyWaves();

        this.isGenerating = false;
        this.isGenerated = false;
        this.generationStage = 0;
        this.passNumber = 0;
        this.updateStageText();
    }






    /*
    returns a string with the data needed to recreate this island
    To save space only grid values with something in them need to be saved.
    each saved grid space will have their grid position, type, and rotations saved
    in addition, they will have the number and type of plants, bushes, flowers, and trees saved
    and for each of the above, the position and rotation
    */
    //TODO: this should all be rewritten to be as small as possible. No JSON names - all handwritten
    serialiseIsland() {
        //header data
        let data = `{"mx":${this.maxX}, "my":${this.maxY}, "mz":${this.maxZ}, "seed":"${GAME.Globals.seed}",`;
        //the grid
        data += `"tiles":[`;
        for (let i = 0; i < this.maxX; i++) {
            for (let j = 0; j < this.maxY; j++) {
                for (let k = 0; k < this.maxZ; k++) {
                    const tile = this.theGrid[i][j][k];
                    if (tile !== null && tile.purged === false) {
                        data += tile.serialise() + ",";
                    }
                }
            }
        }
        if (data.endsWith(",")) {
            data = data.substring(0, data.length - 1);
        }
        data += '],';
        //rivers
        data += `"rivers":[`;
        for (let i = 0; i < this.rivers.length; i++) {
            data += this.rivers[i].serialise() + ",";
        }
        if (data.endsWith(",")) {
            data = data.substring(0, data.length - 1);
        }
        data += ']';
        data += "}";
        return data;
    }

    /*
    given a string of island data, decode the data and build the island
    TODO: huge function. Liable to get bigger. Move out into a new file?
    */
    async deserialiseAndLoadIsland(data) {
        this.generateFast = false;
        
        //header data
        GAME.Globals.seed = data.seed;
        GAME.Globals.Ui.setSeedInput(GAME.Globals.seed);
        GAME.Globals.sRand = new RandomNumberGenerator(GAME.Globals.seed);

        this.maxX = data.mx;
        this.maxY = data.my;
        this.maxZ = data.mz;
        this.halfMaxX = this.maxX/2;
        this.halfMaxY = this.maxY/2;
        this.halfMaxZ = this.maxZ/2;

        const islandCentre = this.getWorldCentre();
        this.pivot.position.x = -islandCentre.x;
        this.pivot.position.z = -islandCentre.z;

        this.resetTheGrid();

        let tilesWithBushesCount = 0;
        let tilesWithPlantsCount = 0;
        let tilesWithFlowersCount = 0;
        let surfaceFeatures = [];

        let loadedTiles = [];

        for (let i = 0; i < data.tiles.length; i++) {
            const tileData = data.tiles[i];
            const x = tileData.pos[0];
            const y = tileData.pos[1];
            const layer = tileData.pos[2];
            let TileType;
            switch(tileData.type)
            {
                case "cube":
                    TileType = Cube;
                    break;
                case "ramp":
                    TileType = Ramp;
                    break;
                case "innerCorner":
                    TileType = InnerCorner;
                    break;
                case "outerCorner":
                    TileType = OuterCorner;
                    break;
                case "riverbedCorner":
                    TileType = RiverbedCorner;
                    break;
                case "riverbedStraight":
                    TileType = RiverbedStraight;
                    break;
                case "riverbedEnd":
                    TileType = RiverbedEnd;
                    break;
                case "riverbedRamp":
                    TileType = RiverbedRamp;
                    break;
            }

            this.changeLayer( layer ); //nextTilePosition doesn't respect the layer we pass in... (it probably should)
            this.nextTilePosition.set( x, layer, y );
            let newTile = this.addTileAtNextPosition(TileType);
            loadedTiles.push(newTile);

            newTile.forceNoAirgapXPos = !tileData.airgaps[0];
            newTile.forceNoAirgapXNeg = !tileData.airgaps[1];
            newTile.forceNoAirgapYPos = !tileData.airgaps[2];
            newTile.forceNoAirgapYNeg = !tileData.airgaps[3];

            for (let i = 0; i < tileData.rot; i++) {
                newTile.rotate();
            }

            //TODO: this is a pretty good argument for saving both type and subtype?
            //extra data needed for water for rivers
            if (TileType === RiverbedCorner || TileType === RiverbedStraight || TileType === RiverbedEnd || TileType === RiverbedRamp) {
                newTile.hasWaterfall = tileData.hasWaterfall;
                newTile.reverseWaterFlow = tileData.reverseWaterFlow;
                newTile.reverseWaterFlowFix = tileData.reverseWaterFlowFix;
            }

            if (tileData.plants) {
                tilesWithPlantsCount++;
                for (let i = 0; i < tileData.plants.length; i++) {
                    const plantData = tileData.plants[i];
                    let PlantType;
                    switch(plantData.type)
                    {
                        case "basil":
                            PlantType = BasilPlant;
                            break;
                        case "mint":
                            PlantType = MintPlant;
                            break;
                        case "sage":
                            PlantType = SagePlant;
                            break;
                        case "thyme":
                            PlantType = ThymePlant;
                            break;
                        default:
                            console.warn("Unidentified plant type in load: ", plantData.type);
                            continue;
                    }
                    let plant = new PlantType(plantData.offset[0], plantData.offset[1], plantData.offset[2], plantData.rot);
                    plant.currentGrowth = plantData.grow;
                    plant.initialize(newTile);
                    newTile.plants.push(plant);
                    surfaceFeatures.push(plant);
                }
            }
            if (tileData.bushes) {
                tilesWithBushesCount++;
                for (let i = 0; i < tileData.bushes.length; i++) {
                    const bushData = tileData.bushes[i];
                    let BushType;
                    switch(bushData.type)
                    {
                        case "bush":
                            BushType = Bush;
                            break;
                        case "lavender":
                            BushType = LavenderBush;
                            break;
                        case "rosemary":
                            BushType = RosemaryBush;
                            break;
                        case "rose":
                            BushType = RoseBush;
                            break;
                        default:
                            console.warn("Unidentified bush type in load: ", bushData.type);
                            continue;
                    }
                    let bush = new BushType(bushData.offset[0], bushData.offset[1], bushData.offset[2], bushData.rot);
                    bush.currentGrowth = bushData.grow;
                    bush.initialize(newTile);         
                    newTile.bushes.push(bush);
                    surfaceFeatures.push(bush);
                }
            }
            if (tileData.flowers) {
                tilesWithFlowersCount++;
                for (let i = 0; i < tileData.flowers.length; i++) {
                    const flowerData = tileData.flowers[i];
                    let FlowerType;
                    switch(flowerData.type)
                    {
                        case "cornflowers":
                            FlowerType = CornflowersFlowers;
                            break;
                        case "goldenrod":
                            FlowerType = GoldenrodFlowers;
                            break;
                        default:
                            console.warn("Unidentified flower type in load: ", flowerData.type);
                            continue;
                    }
                    let flowers = new FlowerType(flowerData.offset[0], flowerData.offset[1], flowerData.offset[2], flowerData.rot);
                    flowers.currentGrowth = flowerData.grow;
                    flowers.initialize(newTile);             
                    newTile.flowers.push(flowers);
                    surfaceFeatures.push(flowers);
                }
            }

            tree: if (TileType === Cube && tileData.tree) {
                let TreeType;
                switch(tileData.tree.type)
                {
                    case "PineTree":
                        TreeType = Pinetree;
                        break;
                    case "PineTree2":
                        TreeType = Pinetree2;
                        break;
                    case "OliveTree":
                        TreeType = OliveTree;
                        break;
                    default:
                        console.warn("Unidentified tree type in load: ", tileData.tree.type);
                        break tree;
                }
                const tree = new TreeType();
                newTile.addTree(tree, tileData.tree.offset[0], tileData.tree.offset[1], tileData.tree.offset[2], tileData.tree.scale[0], tileData.tree.scale[1], tileData.tree.offset[2], tileData.tree.rot);
            }
        }

        this.mergeGeometies();
        this.pivot.add(this.mergedIsland);
        //we have to wait for the island to actually be added to the world space before we can raycast it
        await GAME.AwaitNextFrame();
        //this raycasts
        this.initializeSurfaceFeatures();

        //reconstruct rivers
        for (let i = 0; i < data.rivers.length; i++) {
            const river = new River();
            river.initialize();
            for (let j = 0; j < data.rivers[i].tiles.length; j++) {
                const tile = this.theGrid[data.rivers[i].tiles[j].pos[0]][data.rivers[i].tiles[j].pos[1]][data.rivers[i].tiles[j].pos[2]];
                tile.markedForRiver = true;
                river.riverTiles.push(tile);
                tile.setWater(false);

                river.lastX = tile.gridX;
                river.lastY = tile.gridY;
                river.lastLayer = tile.layer;
            }

            river.mergeWaters();
            this.pivot.add(river.mergedWater);
        }

        //add surface features
        //TODO: actually line up the number of feature to the controllers here!!!!
        const mostFeaturesCount = Math.max(tilesWithBushesCount, tilesWithPlantsCount, tilesWithFlowersCount);
        for (const controller of Object.values(GAME.Globals.instanceControllers)) { 
            controller.initialize( mostFeaturesCount );
        }

        //add the bushes and plants and flowers...
        for (let i = 0; i < surfaceFeatures.length; i++) {
            surfaceFeatures[i].setGlobalInstance();
        }

        for (const controller of Object.values(GAME.Globals.instanceControllers)) { 
            controller.updateMatricies();
        }

        this.changeLayer( 0 );
        this.fillValidCoordsWithEdgeCoords(true);
        GAME.Globals.ocean.updateDepth( this.validCoordinatesLeftOnLayer );

        GAME.Globals.Ui.enableFirstPersonButton();
        GAME.Globals.Ui.disableGenerationButtons();
        GAME.Globals.Ui.enableSaveButton();
        GAME.Globals.Ui.enableClearButton();

        this.isGenerating = false;
        this.isGenerated = true;
        this.generationStage = 6;
        this.passNumber = 0;
        this.updateStageText();

        GAME.Globals.solidObjects.push( this.mergedIsland ); //pivot?
    }










    updateStageText() {
        let resultText = "";

        if (this.isGenerated) {
            resultText = "done"
        } else {

            let stageText = "";
            const layersText = `${this.layer}`;
            let passText = "1/1";

            switch (this.generationStage) {
                case 0:
                    stageText = "Generating crude terrain";
                    passText = `${this.passNumber}/${this.PASSES_PER_LAYER[this.layer]}`;
                    break;
                case 1:
                    stageText = "Smoothing terrain";
                    passText = `${this.passNumber}/2}`;
                    break;
                case 2:
                    stageText = "Adding rivers";
                    break;
                case 3:
                    stageText = "Culling and merging geometries";
                    break;
                case 4:
                    stageText = "Adding grass and trees";
                    break;
                case 5:
                    stageText = "Doing water";
                    break;
            }

            resultText = `${stageText} (layer ${layersText} pass ${this.passNumber})`;

        }

        GAME.Globals.Ui.setStageText(resultText);
    }

    onGameTick(delta) {
        if (this.isGenerating) {
            
            switch (this.generationStage) {
            case 0:
                //initial crude tile placement    
                if (!this.findAndPlaceNewTileOnLayer()) {
                    // can't place any more tiles on this layer...
                    // each layer should have less passes than the one before
                    if (this.passNumber < this.PASSES_PER_LAYER[this.layer]) {
    
                        this.passNumber++;
                        this.changeLayer( this.layer );
                        this.fillVaidCoordsWithEmptyCoords();
                        this.updateStageText();
    
                    } else {

                        //after finishing a layer, purge the layer below of now-covered blocks  
                        if ( this.layer > 0 ) {
                            this.purgeCoveredTilesOnLayer( this.layer - 1 );
                        }
                        
                        //go up a layer if we can
                        //if we didn't place any tiles on the layer we just did, don't bother going up any more
                        if (this.tilesPerLayer[this.layer] > 0 && this.layer < this.maxZ) {
                            this.passNumber = 0;
                            this.changeLayer( this.layer + 1 );
                        } else {
                            //can't do any more chief, prepare for smoothing                            
                            this.changeLayer( this.maxZ - 1 );
                            this.fillValidCoordsWithEmptyEdgeCoords();
                            this.generationStage++;
                            this.passNumber = 0;
                            this.updateStageText();
                        }

                        this.updateStageText();
                        
                    }
                }
                break;
            case 1:                
                //terrain smoothing
                //we do two passes
                if (!this.smoothTileOnLayer()) {
                    //this layer has been smoothed as much as possible
                    if (this.layer >= 0) {
                        this.changeLayer( this.layer - 1 );
                        this.fillValidCoordsWithEmptyEdgeCoords();
                        this.updateStageText();
                    } else {
                        if (this.passNumber < 1) {
                            this.changeLayer( this.maxZ - 1 );
                            this.fillValidCoordsWithEmptyEdgeCoords();
                            this.passNumber = 1;
                            this.updateStageText();
                        } else {
                            //all layers smoothed, time to move on
                            //we added tiles so first purge all the layers
                            for (let i = 0; i < this.maxZ; i++) {
                                this.purgeCoveredTilesOnLayer( i );
                            }

                            this.passNumber = 0;
                            //choose how many rivers to add in the next stage
                            this.maxRivers = GAME.Globals.sRand.getRandomInt(0, 4);
                            this.generationStage++;
                            this.updateStageText();
                        }
                    }
                }
                
                break;
            case 2:
                //rivers
                if (this.rivers.length < this.maxRivers && ( this.rivers.length === 0 || this.rivers[this.rivers.length - 1].replaced)) {

                    //add a new river
                    this.changeLayer( 0 );
                    this.fillValidCoordsWithEdgeCoords();
                    //get a random edge on layer 0 to start the river at
                    const r1 = GAME.Globals.sRand.getRandomValueFromArray( this.validCoordinatesLeftOnLayer );
                    const r2 = GAME.Globals.sRand.getRandomValueFromArray( r1 );
                    const river = new River();
                    river.prepareForGeneration( r2.x, r2.y, 0 );
                    river.initialize();
                    this.rivers.push(river);

                } else if (this.rivers.length && !this.rivers[this.rivers.length - 1].plotted) {

                    //extend the working river until it's complete
                    this.rivers[this.rivers.length - 1].plotNextTile( this );

                } else if (this.rivers.length && this.rivers[this.rivers.length - 1].tiles.length < this.rivers[this.rivers.length - 1].minLength) {

                    //if the river is less than it's min length destroy it
                    //a new river will be made to replace it on the next cycle
                    this.rivers[this.rivers.length - 1].destroy();
                    this.rivers.splice(this.rivers.length - 1, 1);

                } else if (this.rivers.length && !this.rivers[this.rivers.length - 1].replaced) {

                    //the working river has been plotted, now replace the tiles it overlaps with river tiles
                    this.rivers[this.rivers.length - 1].setNextTile( this );

                } else {

                    //rivers done, prepare for face culling & merging
                    this.changeLayer( 0 );
                    this.fillVaidCoordsWithFilledCoords();
                    this.generationStage++;
                    this.passNumber = 0;
                    this.updateStageText();

                }

                break;
            case 3:
                //merge
                if ( this.passNumber == 0 ) {

                    //merge island
                    this.mergeGeometies();
                    this.pivot.add(this.mergedIsland);

                    this.passNumber++;

                } else {

                    //merge river waters
                    const riverIndex = this.passNumber - 1;
                    if (riverIndex < this.rivers.length) {
                        const river = this.rivers[riverIndex];
                        river.mergeWaters();
                        this.pivot.add(river.mergedWater);
                        this.passNumber++;
                    } else {
                        this.generationStage++;
                        this.passNumber = 0;
                        this.updateStageText();
                    }

                }
                
                break;
            case 4:
                //surface features, trees

                if ( this.passNumber == 0 ) {
                    //we have to do this after the merge because in fast gen the geometry doesn't exist until now (so we can't do the lazy raytrace)
                    //is it possible the island won't have rendered before now? If so might want to add  await GAME.AwaitNextFrame();
                    this.initializeSurfaceFeatures();

                    //no trees on layer 0
                    this.changeLayer( 1 );
                    this.fillValidCoordsWithSurfaceCubesWithoutTreeCoords();
                    this.passNumber++;
                    this.updateStageText();

                } if (this.passNumber < 3) {

                    if (this.layer < this.maxZ) {

                        const placedTree = this.placeTreeOnLayer();

                        if (!placedTree) {
                            //no more trees to plant on this layer, move up
                            this.changeLayer( this.layer + 1 );
                            this.fillValidCoordsWithSurfaceCubesWithoutTreeCoords();
                        }

                    } else {
                        this.changeLayer( 1 );
                        this.fillValidCoordsWithSurfaceCubesWithoutTreeCoords();
                        this.passNumber++;

                        if (this.passNumber > 2) {
                            //prepare to place plants
                            //no plants on layer 0
                            this.changeLayer( 1 );
                            //prepare the instaced geometries
                            let length = 0;
                            while (this.layer < this.maxZ) {
                                this.fillValidCoordsWithSurfaceCubes();
                                length += this.validCoordinatesLeftOnLayer.flat().length;
                                this.changeLayer( this.layer + 1 );
                            }
                            
                            this.changeLayer( 1 );
                            this.fillValidCoordsWithSurfaceCubes();

                            length *= 3; //max of 3x of each surface feature per tile

                            for (const controller of Object.values(GAME.Globals.instanceControllers)) { 
                                controller.initialize( length );
                            }
                        }
                    }

                    this.updateStageText();

                } else if (this.passNumber < 4) {
                    //add surface features to EVERY tile
                    if (this.layer < this.maxZ) {

                        for ( let i = 0; i < this.validCoordinatesLeftOnLayer.length; i++) {

                            if (this.validCoordinatesLeftOnLayer[i].length) {

                                for ( let j = 0; j < this.validCoordinatesLeftOnLayer[i].length; j++) {

                                    const corrds = this.validCoordinatesLeftOnLayer[i][j];
                                    const tile = this.theGrid[corrds.x][corrds.y][this.layer];
                                    tile.addSurfaceFeatures();
                                }

                            }
                            
                        }

                        this.changeLayer( this.layer + 1 );
                        this.fillValidCoordsWithSurfaceCubes();

                    } else {
                        this.passNumber++;
                    }

                } else {
                    this.generationStage++;
                    this.updateStageText();
                    //this updates the instance geometries matricies so they start to render
                    for (const controller of Object.values(GAME.Globals.instanceControllers)) { 
                        controller.updateMatricies();
                    }
                }

                
                break;
            case 5:
                //water depth
                this.changeLayer( 0 );
                this.fillValidCoordsWithEdgeCoords(true);
                GAME.Globals.ocean.updateDepth( this.validCoordinatesLeftOnLayer );
                this.generationStage++;
                this.updateStageText();
                break;
            default:
                //all done!
                this.isGenerating = false;
                this.isGenerated = true;
                GAME.Globals.Ui.enableFirstPersonButton();
                GAME.Globals.Ui.disableGenerationButtons();
                GAME.Globals.Ui.enableSaveButton();
                GAME.Globals.Ui.enableClearButton();
                this.updateStageText();
                GAME.Globals.solidObjects.push( this.mergedIsland ); //pivot?
                return;
            }            

        }
    }

    fillVaidCoordsWithEmptyCoords() {
        const filter = ( x, y, layer ) => { return this.theGrid[x][y][layer] === null; };
        this.fillValidCoordsWithCoordsThatFilter(filter);
    }

    fillValidCoordsWithEdgeCoords(allowTilesWithTilesAbove = false) {
        const filter = ( x, y, layer ) => { 
            const xpN = this.getPosXNeighbour( x, y, layer );
            const xnN = this.getNegXNeighbour( x, y, layer );
            const ypN = this.getPosYNeighbour( x, y, layer );
            const ynN = this.getNegYNeighbour( x, y, layer );
            const above = this.getTileAbove( x, y, layer );
            return this.theGrid[x][y][layer] !== null && (!above || allowTilesWithTilesAbove) && !(xpN && xnN && ypN && ynN) && (xpN || xnN || ypN || ynN);
        };
        this.fillValidCoordsWithCoordsThatFilter(filter);
    }


    fillValidCoordsWithEmptyEdgeCoords() {
        const filter = ( x, y, layer ) => { 
            const xpN = this.getPosXNeighbour( x, y, layer );
            const xnN = this.getNegXNeighbour( x, y, layer );
            const ypN = this.getPosYNeighbour( x, y, layer );
            const ynN = this.getNegYNeighbour( x, y, layer );
            return this.theGrid[x][y][layer] === null && (xpN || xnN || ypN || ynN);
        };
        this.fillValidCoordsWithCoordsThatFilter(filter);
    }

    fillVaidCoordsWithFilledCoords() {
        const filter = ( x, y, layer ) => { return this.theGrid[x][y][layer] !== null; };
        this.fillValidCoordsWithCoordsThatFilter(filter);
    }
    
    fillValidCoordsWithSurfaceCubes() {
        const filter = ( x, y, layer ) => {
            const tile = this.theGrid[x][y][layer];
            if (!tile) {
                return false;
            }
            const tileAbove = this.getTileAbove( x, y, layer );
            const isCube = tile.type === "cube";
            return isCube && !tileAbove;
        };
        this.fillValidCoordsWithCoordsThatFilter(filter);
    }

    fillValidCoordsWithSurfaceCubesWithoutTreeCoords() {
        const filter = ( x, y, layer ) => {
            const tile = this.theGrid[x][y][layer];
            if (!tile) {
                return false;
            }
            const tileAbove = this.getTileAbove( x, y, layer );
            const isCube = tile.type === "cube";
            const hasTree = tile.tree != null;
            return isCube && !hasTree && !tileAbove;
        };
        this.fillValidCoordsWithCoordsThatFilter(filter);
    }

    fillValidCoordsWithCoordsThatFilter( filter ) {
        //refresh the list of valid positions
        this.validCoordinatesLeftOnLayer = new Array();
        for (let i = 0; i < this.maxX; i++) {
            this.validCoordinatesLeftOnLayer[i] = new Array();
            for (let j = 0; j < this.maxY; j++) {
                //only include cells that pass the filter
                if ( filter( i, j, this.layer) ) {
                    this.validCoordinatesLeftOnLayer[i][j] = {x: i, y: j};
                }
            }
            if (this.validCoordinatesLeftOnLayer[i].length) {
                this.validCoordinatesLeftOnLayer[i] = this.validCoordinatesLeftOnLayer[i].filter( () => { return true } ); //resets the indexes
            } else {
                this.validCoordinatesLeftOnLayer.splice(i, 1); //remove empty array parent
            }            
        }
        this.validCoordinatesLeftOnLayer = this.validCoordinatesLeftOnLayer.filter( () => { return true } ); //resets the indexes
    }

    changeLayer( layer ) {
        this.layer = layer;
    }


    getTile(x,y,layer) {
        let tile = null;
        if (x < this.maxX && x >= 0 && y < this.maxY && y >= 0 && layer >= 0 && layer < this.maxZ) {
            tile = this.theGrid[x][y][layer];
        }
        return tile;
    }
    getTileAbove(x,y,layer) {
        let aboveNeighbour = null;
        if (layer + 1 < this.maxZ) {
            aboveNeighbour = this.theGrid[x][y][layer + 1];
        }
        return aboveNeighbour;
    }
    getTileBelow(x,y,layer) {
        let belowNeighbour = null;
        if (layer - 1 > -1) {
            belowNeighbour = this.theGrid[x][y][layer - 1];
        }
        return belowNeighbour;
    }
    getPosXNeighbour(x,y,layer) {
        let posXNeighbour = null;
        if (x + 1 < this.maxX) {
            posXNeighbour = this.theGrid[x + 1][y][layer];
        }
        return posXNeighbour;
    }
    getNegXNeighbour(x,y,layer) {
        let negXNeighbour = null;
        if (x - 1 >= 0) {
            negXNeighbour = this.theGrid[x - 1][y][layer];
        }
        return negXNeighbour;
    }
    getPosYNeighbour(x,y,layer) {
        let posYNeighbour = null;
        if (y + 1 < this.maxY) {
            posYNeighbour = this.theGrid[x][y + 1][layer];
        }
        return posYNeighbour;
    }
    getNegYNeighbour(x,y,layer) {
        let negYNeighbour = null;
        if (y -1 >= 0) {
            negYNeighbour = this.theGrid[x][y - 1][layer];
        }
        return negYNeighbour;
    }

    //TODO: very inefficient! Look into an algorithm
    //returns false is no neighbours found
    getNearestTileOnLayer(tile) {
        let nearestDistance = Infinity;
        let neartestTile = false;
        let tile2 = null;
        for (let x = 0; x < this.maxX; x++) {
            for (let y = 0; y < this.maxY; y++) {
                tile2 = this.theGrid[x][y][tile.layer];
                if (tile2) {
                    const dist = tile.pivot.position.distanceToSquared(tile2.pivot.position);
                    if (tile2 && dist !== 0 && dist < nearestDistance) {
                        neartestTile = tile2;
                        nearestDistance = dist;
                    }
                }
            }
        }
        return neartestTile;
    }
    
    
    
    

    purgeCoveredTilesOnLayer( layer ) {

        //two passes for purging, so we can avoid tiles being purged and showing fake "airgaps" to other interior tiles

        if (layer < this.maxZ) {
            for (let x = 0; x < this.maxX; x++) {
                for (let y = 0; y < this.maxY; y++) {
                    const tile = this.theGrid[x][y][layer];
                    const tileAbove = this.getTileAbove(x, y, layer);
                    if (tile && tileAbove) {
                        //this grid cell has a tile and a tile above it as well
                        //if it doesn't have an airgap on one of it's side, purge it
                        
                        const xPos = this.getPosXNeighbour( x, y, layer );
                        const xNeg = this.getNegXNeighbour( x, y, layer );
                        const yPos = this.getPosYNeighbour( x, y, layer );
                        const yNeg = this.getNegYNeighbour( x, y, layer );

                        const hasAirgapXPos = xPos === null || xPos.xNegFace.hasLessEdgesThan(tile.xPosFace) ||
                            (xPos.xNegFace.hasSameCountEdgesAs(tile.xPosFace) && !xPos.xNegFace.equalsMirrored(tile.xPosFace));

                        const hasAirgapXNeg = xNeg === null || xNeg.xPosFace.hasLessEdgesThan(tile.xNegFace) ||
                            (xNeg.xPosFace.hasSameCountEdgesAs(tile.xNegFace) && !xNeg.xPosFace.equalsMirrored(tile.xNegFace));

                        const hasAirgapYPos = yPos === null || yPos.yNegFace.hasLessEdgesThan(tile.yPosFace) ||
                            (yPos.yNegFace.hasSameCountEdgesAs(tile.yPosFace) && !yPos.yNegFace.equalsMirrored(tile.yPosFace));

                        const hasAirgapYNeg = yNeg === null || yNeg.yPosFace.hasLessEdgesThan(tile.yNegFace) ||
                            (yNeg.yPosFace.hasSameCountEdgesAs(tile.yNegFace) && !yNeg.yPosFace.equalsMirrored(tile.yNegFace));

                        if (!hasAirgapXPos && !hasAirgapXNeg && !hasAirgapYPos && !hasAirgapYNeg) {
                            //this tile is surrounded on all side.
                            //HOWEVER! in the case of the tile below a waterfall tile, we still want to render the face of this tile
                            //so it must not be purged! We can detect these tiles by checking the force airgaps
                            //there might also be other cases where a surrounded tile should be rendered if the force no airgaps are active
                            if (!(tile.forceNoAirgapXPos || tile.forceNoAirgapXNeg || tile.forceNoAirgapYPos || tile.forceNoAirgapYNeg)) {
                                this.theGrid[x][y][layer].markedForPurge = true;
                            }              
                        }
                    }
                }
            }

            for (let x = 0; x < this.maxX; x++) {
                for (let y = 0; y < this.maxY; y++) {
                    const tile = this.theGrid[x][y][layer];
                    if (tile && tile.markedForPurge) {
                        if (!this.generateFast) {
                            this.pivot.remove(tile.pivot);
                        }
                        tile.purged = true;
                        //don't do this so we can un-purge these tiles later (?)
                        //tile.destroy();
                        //this.theGrid[x][y][layer] = null;
                    }     
                }
            }
        }
        
    }

    //places a tree in a valid coord left on the layer, or return false if it can't be done
    //prefers to place trees next to exisiting trees
    placeTreeOnLayer() {
        let nextX = null;
        let nextY = null;
        let i;
        let j;

        if (this.validCoordinatesLeftOnLayer.length === 0) {
            return false;
        }

        i = GAME.Globals.sRand.getRandomInt( 0, this.validCoordinatesLeftOnLayer.length -1 );
        j = GAME.Globals.sRand.getRandomInt( 0, this.validCoordinatesLeftOnLayer[i].length - 1);

        const nextCoords = this.validCoordinatesLeftOnLayer[i][j];
        nextX = nextCoords.x;
        nextY = nextCoords.y;

        //randomly skip checks to place a tree regardless of neighbours (to seed initial forests or to place isolated single trees)
        //TODO: clean up this ungodly flow, looks so bad
        if (GAME.Globals.sRand.random() > 0.1) {

            //this is a valid position, but before we choose it we want to bias towards picking positions next to other trees
            const hasPosXNeighbour = this.getPosXNeighbour( nextX, nextY, this.layer )?.tree != null;
            const hasNegXNeighbour = this.getNegXNeighbour( nextX, nextY, this.layer )?.tree != null;
            const hasPosYNeighbour = this.getPosYNeighbour( nextX, nextY, this.layer )?.tree != null;
            const hasNegYNeighbour = this.getNegYNeighbour( nextX, nextY, this.layer )?.tree != null;
            //including neighbours above
            const hasUpstairsPosXNeighbour = this.layer < this.maxZ && this.getPosXNeighbour( nextX, nextY, this.layer + 1 )?.tree != null;
            const hasUpstairsNegXNeighbour = this.layer < this.maxZ && this.getNegXNeighbour( nextX, nextY, this.layer + 1 )?.tree != null;
            const hasUpstairsPosYNeighbour = this.layer < this.maxZ && this.getPosYNeighbour( nextX, nextY, this.layer + 1 )?.tree != null;
            const hasUpstairsNegYNeighbour = this.layer < this.maxZ && this.getNegYNeighbour( nextX, nextY, this.layer + 1 )?.tree != null;
            //...or below
            const hasDownstairsPosXNeighbour = this.layer !== 0 && this.getPosXNeighbour( nextX, nextY, this.layer - 1 )?.tree != null;
            const hasDownstairsNegXNeighbour = this.layer !== 0 && this.getNegXNeighbour( nextX, nextY, this.layer - 1 )?.tree != null;
            const hasDownstairsPosYNeighbour = this.layer !== 0 && this.getPosYNeighbour( nextX, nextY, this.layer - 1 )?.tree != null;
            const hasDownstairsNegYNeighbour = this.layer !== 0 && this.getNegYNeighbour( nextX, nextY, this.layer - 1 )?.tree != null;

            const hasNeighbour = hasPosXNeighbour || hasNegXNeighbour || hasPosYNeighbour || hasNegYNeighbour;
            const hasUpstairsNeighbour = hasUpstairsPosXNeighbour || hasUpstairsNegXNeighbour || hasUpstairsPosYNeighbour || hasUpstairsNegYNeighbour;
            const hasDownstairsNeighbour = hasDownstairsPosXNeighbour || hasDownstairsNegXNeighbour || hasDownstairsPosYNeighbour || hasDownstairsNegYNeighbour;
            
            if (hasNeighbour || hasUpstairsNeighbour || hasDownstairsNeighbour) {
                //neighbour found
                //before we place, do another random check, so we can thin out the forests a bit
                if (GAME.Globals.sRand.random() > 0.9) {

                    nextX = null;
                    nextY = null;
                }

            } else {

                nextX = null;
                nextY = null;

            }

        }

        //place a tree on the cube at the coordinates
        if (nextX != null && nextY != null) {
            this.theGrid[nextX][nextY][this.layer].generateAndAddTree();
        }

        //remove the tile we looked at from the list of valid tiles now
        this.validCoordinatesLeftOnLayer[i].splice(j, 1);
        if (this.validCoordinatesLeftOnLayer[i].length == 0) {
            this.validCoordinatesLeftOnLayer.splice(i, 1);
        }

        return true;
    }

    isValidPosition(x, y, layer) {
        let cellIsFree = this.theGrid[x][y][layer] === null;

        //if there's a tile above don't bother placing one here
        cellIsFree = cellIsFree && !this.getTileAbove(x, y, layer);  

        if (layer === 0) {
            return cellIsFree;
        }
        
        //if we're on a higher layer there must be a block below as well
        const tileBelow = this.theGrid[x][y][layer - 1];
        if (cellIsFree && tileBelow !== null) {
            //if the block below is a slope that doesn't count either
            if (!this.getIsTileSloped(tileBelow)) {
                return true;
            }
        }

        return false;
    }

    findAndPlaceNewTileOnLayer() {
        let nextX = null;
        let nextY = null;
        let i;
        let j;

        while (this.validCoordinatesLeftOnLayer.length > 0) {

            i = GAME.Globals.sRand.getRandomInt( 0, this.validCoordinatesLeftOnLayer.length -1 );
            j = GAME.Globals.sRand.getRandomInt( 0, this.validCoordinatesLeftOnLayer[i].length - 1);

            const nextCoords = this.validCoordinatesLeftOnLayer[i][j];
            nextX = nextCoords.x;
            nextY = nextCoords.y;

            if (this.isValidPosition(nextX, nextY, this.layer)) {

                //the higher the layer, the greater chance to just place a tile randomly in a new (valid) place regardless of adjacent tiles
                if (GAME.Globals.sRand.random() < this.layer / 300) {
                    break;
                }

                //this is a valid position, but before we choose it we want to bias towards picking positions next to other tiles
                const hasPosXNeighbour = this.getPosXNeighbour( nextX, nextY, this.layer );
                const hasNegXNeighbour = this.getNegXNeighbour( nextX, nextY, this.layer );
                const hasPosYNeighbour = this.getPosYNeighbour( nextX, nextY, this.layer );
                const hasNegYNeighbour = this.getNegYNeighbour( nextX, nextY, this.layer );

                const hasNeighbour = hasPosXNeighbour || hasNegXNeighbour || hasPosYNeighbour || hasNegYNeighbour;
                
                if (hasNeighbour) {
                    //neighbour found, go ahead and place!
                    break;
                }
                //no neighbours and unlucky, so just try again    
            }
            
            //didn't place a tile here, mark as invalid and try again
            this.validCoordinatesLeftOnLayer[i].splice(j, 1);
            if (this.validCoordinatesLeftOnLayer[i].length == 0) {
                this.validCoordinatesLeftOnLayer.splice(i, 1);
            }

            nextX = null;
            nextY = null;

        };

        if (this.validCoordinatesLeftOnLayer.length === 0 && nextX === null && nextY === null) {
            //didn't find any valid free tiles
            return false;
        }

        this.nextTilePosition.set( nextX, this.layer, nextY );

        this.addTileAtNextPosition( Cube );

        return true;
    }

    //given a tile type, return the tile type adjusted for biome; e.g. Cube => SandCube
    getBiomeTileForType( TileType, x, z, layer ) {

        //sand
        if (layer === 0) {
            switch (TileType)
            {
                case Cube: 
                    TileType = SandCube;
                    break;
                case InnerCorner: 
                    TileType = SandInnerCorner;
                    break;
                case OuterCorner: 
                    TileType = SandOuterCorner;
                    break;
                case Ramp: 
                    TileType = SandRamp;
                    break;
            }
        }

        //coastal
        if (layer === 1) {
            switch (TileType)
            {
                case Cube: 
                    TileType = CoastalCube;
                    break;
                case InnerCorner:
                    TileType = CoastalInnerCorner;
                    break;
                case OuterCorner:
                    TileType = CoastalOuterCorner;
                    break;
                case Ramp:
                    TileType = CoastalRamp;
                    break;
            }
        }

        return TileType;

    }

    addTileAtNextPosition( TileType ) {

        TileType = this.getBiomeTileForType( TileType, this.nextTilePosition.x, this.nextTilePosition.z, this.layer );

        const newTile = new TileType( this.nextTilePosition.x, this.nextTilePosition.z, this.layer );
        newTile.layer = this.layer;

        this.nextTilePosition.multiplyScalar( GAME.Globals.gridScale );
        newTile.initialize( this.nextTilePosition );
        this.nextTilePosition.multiplyScalar( 1 / GAME.Globals.gridScale );

        this.nextTilePosition.floor();

        if (!this.generateFast) {
            this.pivot.add(newTile.pivot);
        }
        this.theGrid[this.nextTilePosition.x][this.nextTilePosition.z][this.layer] = newTile;

        this.tilesPlaced++;
        this.tilesPerLayer[this.layer]++;

        return newTile;
    }

    replaceTile( x, y, layer, NewTileType ) {
        
        const oldTile = this.theGrid[x][y][layer];
        if (!oldTile) {
            return false;
        }

        if (!this.generateFast) {
            this.pivot.remove( oldTile.pivot );
        }
        oldTile.destroy();
        
        const newTile = new NewTileType( x, y, layer );
        this.nextTilePosition.set( x, layer, y );

        this.nextTilePosition.multiplyScalar( GAME.Globals.gridScale );
        newTile.initialize( this.nextTilePosition );
        this.nextTilePosition.multiplyScalar( 1 / GAME.Globals.gridScale );

        this.nextTilePosition.floor();

        if (!this.generateFast) {
            this.pivot.add( newTile.pivot );
        }

        this.theGrid[x][y][layer] = newTile;

        return newTile;

    }

    removeTile( x, y, layer ) {

        const oldTile = this.theGrid[x][y][layer];
        if (!oldTile) {
            return false;
        }

        this.pivot.remove(oldTile.pivot);
        oldTile.destroy();
        this.theGrid[x][y][layer] = null;

        return true;

    }

    removeTileAndAbove(x, y, layer) {

        let result = false;
        if (layer > this.maxZ + 1 || layer < 0) {
            return result;
        }

        for (let i = layer; i < this.maxZ; i++) {
            result = result || this.removeTile(x, y, i);
        }

        return result;

    }


    /*
    smooth a tile on this layer
    returns false when no tiles left to try smoothing
    */
    smoothTileOnLayer() {

        //go through the coords one by one and find a tile to smooth
        let i = this.validCoordinatesLeftOnLayer.length - 1;
        let j;

        loop1: while (i >= 0) {

            j = this.validCoordinatesLeftOnLayer[i].length - 1;

            while (j >= 0) {

                const c = this.validCoordinatesLeftOnLayer[i][j];
                if (this.addSmoothTile( c.x, c.y, this.layer )) {
                    break loop1;
                }

                this.validCoordinatesLeftOnLayer[i].splice(j, 1);
                j--;
            }

            this.validCoordinatesLeftOnLayer.splice(i, 1);
            i--;

        }    

        if (this.validCoordinatesLeftOnLayer.length === 0) {
            //couldny't find a tile to smooth or smoothed the last tile in the array
            return false;
        }

        this.validCoordinatesLeftOnLayer[i].splice(j, 1);
        if (this.validCoordinatesLeftOnLayer[i].length == 0) {
            this.validCoordinatesLeftOnLayer.splice(i, 1);
        }

        return true;

    }

    getIsTileSloped( tile ) {
        return (tile.type === "ramp" || tile.type === "innerCorner" || tile.type === "outerCorner" || tile.type === "riverbedRamp");
    }

    /*
    add a smoothing tile (downwards) on the space if possible
    */
    addSmoothTile( x, y, layer ) {

        if ( !this.isValidPosition(x, y, layer) ) {
            return false;
        }

        const posXNeighbour = this.getPosXNeighbour( x, y, layer );
        const negXNeighbour = this.getNegXNeighbour( x, y, layer );
        const posYNeighbour = this.getPosYNeighbour( x, y, layer );
        const negYNeighbour = this.getNegYNeighbour( x, y, layer ); 

        //if this tile has neighbours all around, fill it in tbqh
        if (posXNeighbour && negXNeighbour && posYNeighbour && negYNeighbour) {
            this.nextTilePosition.set( x, layer, y );
            this.addTileAtNextPosition( Cube );
            return true;
        }

        let rampCase1, rampCase2, rampCase3, rampCase4;
        
        if (layer === 0) {

            //on layer 0, treat it as if there is always tiles below us (the ocean)
            //but don't add smoothing tiles adjacent to double-high cliffs, to allow sea cliffs
            /*
                <posXNeighbourAbove>
                [posXNeighbour     ][x]<negXNeighbour>
            */
            const posXNeighbourAbove = this.getPosXNeighbour( x, y, layer + 1 );
            const negXNeighbourAbove = this.getNegXNeighbour( x, y, layer + 1 );
            const posYNeighbourAbove = this.getPosYNeighbour( x, y, layer + 1 );
            const negYNeighbourAbove = this.getNegYNeighbour( x, y, layer + 1 );

            rampCase1 = posXNeighbour && !posXNeighbourAbove && !negXNeighbour && posXNeighbour.type === "cube"; //pos x to neg x >
            rampCase2 = negXNeighbour && !negXNeighbourAbove && !posXNeighbour && negXNeighbour.type === "cube"; //neg x to pos x <
            rampCase3 = posYNeighbour && !posYNeighbourAbove && !negYNeighbour && posYNeighbour.type === "cube"; //pos y to neg y ^
            rampCase4 = negYNeighbour && !negYNeighbourAbove && !posYNeighbour && negYNeighbour.type === "cube"; //neg y to pos y v

        } else {

            //on every other layer, only add smoothing tiles when there's a tile below and beyond 
            /*
                [posXNeighbour][x]<negXNeighbour>
                [             ][ ][negXNeighbourBelow]
            */
            const posXNeighbourBelow = this.getPosXNeighbour( x, y, layer - 1 );
            const negXNeighbourBelow = this.getNegXNeighbour( x, y, layer - 1 );
            const posYNeighbourBelow = this.getPosYNeighbour( x, y, layer - 1 );
            const negYNeighbourBelow = this.getNegYNeighbour( x, y, layer - 1 );

            rampCase1 = posXNeighbour && negXNeighbourBelow && !negXNeighbour && posXNeighbour.type === "cube"; //pos x to neg x >
            rampCase2 = negXNeighbour && posXNeighbourBelow && !posXNeighbour && negXNeighbour.type === "cube"; //neg x to pos x <
            rampCase3 = posYNeighbour && negYNeighbourBelow && !negYNeighbour && posYNeighbour.type === "cube"; //pos y to neg y ^
            rampCase4 = negYNeighbour && posYNeighbourBelow && !posYNeighbour && negYNeighbour.type === "cube"; //neg y to pos y v

        }

        

        //we only want to add outer corners once the inner corners and ramps have already been added (pass 1)
        if (this.passNumber != 0) {
            const outerCornerCase1 = posXNeighbour && posYNeighbour && posXNeighbour.slopesNegY && posYNeighbour.slopesNegX;
            const outerCornerCase2 = negXNeighbour && posYNeighbour && negXNeighbour.slopesNegY && posYNeighbour.slopesPosX;
            const outerCornerCase3 = posXNeighbour && negYNeighbour && posXNeighbour.slopesPosY && negYNeighbour.slopesNegX;
            const outerCornerCase4 = negXNeighbour && negYNeighbour && negXNeighbour.slopesPosY && negYNeighbour.slopesPosX;

            if (outerCornerCase1 || outerCornerCase2 || outerCornerCase3 || outerCornerCase4) {
                let newTile;
                this.nextTilePosition.set( x, layer, y );
                newTile = this.addTileAtNextPosition( OuterCorner );
    
                if (outerCornerCase1) {  // v >

                    newTile.rotate();
                    newTile.rotate();
                    newTile.rotate();

                } else if (outerCornerCase2) { // v <

                    newTile.rotate();
                    newTile.rotate();

                } else if (outerCornerCase3) { // ^ >
     
    
                } else if (outerCornerCase4) { // ^ <

                    newTile.rotate();  

                }
    
                return true;
            }
        }

        if (rampCase1 || rampCase2 || rampCase3 || rampCase4) {

            let newTile;
            this.nextTilePosition.set( x, layer, y );

            //if two ramp cases are available, use an inner corner
            if (rampCase1 && rampCase3) { // > ^
            
                newTile = this.addTileAtNextPosition( InnerCorner );
                newTile.rotate();
                newTile.rotate();
                newTile.rotate();

            } else if (rampCase1 && rampCase4) { // > v
            
                newTile = this.addTileAtNextPosition( InnerCorner );       

            } else if (rampCase2 && rampCase3) { // < ^
            
                newTile = this.addTileAtNextPosition( InnerCorner );
                newTile.rotate();
                newTile.rotate();

            } else if (rampCase2 && rampCase4) { // < v
                
                newTile = this.addTileAtNextPosition( InnerCorner );
                newTile.rotate();

            } else {

                //just a normal ramp
                newTile = this.addTileAtNextPosition( Ramp );

                if (rampCase1) { // >

                    newTile.rotate();
                    newTile.rotate();
                    newTile.rotate();

                } else if (rampCase2) { // <

                    newTile.rotate();

                } else if (rampCase3) { // ^

                    newTile.rotate();
                    newTile.rotate();

                } else if (rampCase4) { // v
                    
                }

            }          

            return true;
        }
        
        return false;

    }



    

    mergeGeometies() {
        //destroy the tile geometries and instead create a single mesh from all their geometries
        //while doing this, only take the faces that are going to be visible

        let geometries = [];

        for (let x = 0; x < this.maxX; x++) {
            for (let y = 0; y < this.maxY; y++) {
                for (let layer = 0; layer < this.maxZ; layer++) {

                    const tile = this.theGrid[x][y][layer];

                    if (tile !== null ) { //&& !tile.purged) {

                        const xPos = this.getPosXNeighbour( x, y, layer );
                        const xNeg = this.getNegXNeighbour( x, y, layer );
                        const yPos = this.getPosYNeighbour( x, y, layer );
                        const yNeg = this.getNegYNeighbour( x, y, layer );

                        const isOnEdge = x === 0 || x === this.maxX - 1 || y === 0 || y === this.maxY - 1;

                        const hasAirgapAbove = layer === this.maxZ || this.getTileAbove( x, y, layer ) === null;

                        const hasAirgapXPos = xPos === null || isOnEdge ||
                            xPos.xNegFace.hasLessEdgesThan(tile.xPosFace) ||
                            (xPos.xNegFace.hasSameCountEdgesAs(tile.xPosFace) && !xPos.xNegFace.equalsMirrored(tile.xPosFace));

                        const hasAirgapXNeg = xNeg === null || isOnEdge ||
                            xNeg.xPosFace.hasLessEdgesThan(tile.xNegFace) ||
                            (xNeg.xPosFace.hasSameCountEdgesAs(tile.xNegFace) && !xNeg.xPosFace.equalsMirrored(tile.xNegFace));

                        const hasAirgapYPos = yPos === null || isOnEdge ||
                            yPos.yNegFace.hasLessEdgesThan(tile.yPosFace) ||
                            (yPos.yNegFace.hasSameCountEdgesAs(tile.yPosFace) && !yPos.yNegFace.equalsMirrored(tile.yPosFace));

                        const hasAirgapYNeg = yNeg === null || isOnEdge 
                            || yNeg.yPosFace.hasLessEdgesThan(tile.yNegFace) ||
                            (yNeg.yPosFace.hasSameCountEdgesAs(tile.yNegFace) && !yNeg.yPosFace.equalsMirrored(tile.yNegFace));

                        const tileGeometries = tile.getMinimalFacesGeometries( hasAirgapAbove, hasAirgapXPos, hasAirgapXNeg, hasAirgapYPos, hasAirgapYNeg );

                        if (tileGeometries && tileGeometries.length) {
                            geometries = geometries.concat( tileGeometries );
                        }        

                        //might be sus
                        if (this.material == null) {
                            this.material = tile.object.material;
                        }

                        if (!this.generateFast) { 
                            this.pivot.remove(tile.pivot);
                        }

                        tile.merged = true;

                    }
                }
            }
        }

        this.mergedGeometry = BufferGeometryUtils.mergeBufferGeometries(geometries, false);
        this.mergedGeometry = BufferGeometryUtils.mergeVertices(this.mergedGeometry);
        this.mergedGeometry.computeBoundingSphere();
        this.mergedIsland = new THREE.Mesh(this.mergedGeometry, this.material);
        this.mergedIsland.receiveShadow = true;

        this.mergedGeometry.computeBoundsTree(); //bvh

        this.mergedIsland.updateMatrixWorld( true );
        

    }

    initializeSurfaceFeatures() {
        for (let x = 0; x < this.maxX; x++) {
            for (let y = 0; y < this.maxY; y++) {
                for (let layer = 0; layer < this.maxZ; layer++) {

                    const tile = this.theGrid[x][y][layer];

                    if (tile !== null ) {
                        tile.initializeSurfaceFeatures();
                    }
                }
            }
        }
    }




    getWorldCentre() {
        return new THREE.Vector3( this.halfMaxX * GAME.Globals.gridScale, 0, this.halfMaxY * GAME.Globals.gridScale );
    }

};

export { Island };