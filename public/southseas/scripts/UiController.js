import * as GAME from './game.js';

class UiController {

    topControls;
    generateButton;
    generateFastButton;
    clearButton;
    saveButton;
    loadInput;
    firstPersonButton;
    thirdPersonButton;
    stageLabel;
    seedInput;
    sizeRadioInputs;

    inventoryContainer;
    inventorySlots = [];

    constructor() {
        this.topControls = document.getElementById("top-controls");
        this.generateButton = document.getElementById("generate");
        this.generateFastButton = document.getElementById("generate-fast");
        this.clearButton = document.getElementById("clear");
        this.saveButton = document.getElementById("save");
        this.loadInput = document.getElementById("loadInput");
        this.firstPersonButton = document.getElementById("first-person");
        this.thirdPersonButton = document.getElementById("third-person");
        this.stageLabel = document.getElementById("stage");
        this.seedInput = document.getElementById("input-seed");
        this.sizeRadioInputs = document.querySelectorAll("input[name='input-island-size']");

        this.inventoryContainer = document.getElementById("inventory-container");

        this.inventorySlots[0] = document.getElementById("inventory-slot-1");
        this.inventorySlots[1] = document.getElementById("inventory-slot-2");
        this.inventorySlots[2] = document.getElementById("inventory-slot-3");
        this.inventorySlots[3] = document.getElementById("inventory-slot-4");
        this.inventorySlots[4] = document.getElementById("inventory-slot-5");
        this.inventorySlots[5] = document.getElementById("inventory-slot-6");
        this.inventorySlots[6] = document.getElementById("inventory-slot-7");
        this.inventorySlots[7] = document.getElementById("inventory-slot-8");
        this.inventorySlots[8] = document.getElementById("inventory-slot-9");
    }

    initialize() {
        this.generateButton.addEventListener("click", () => { GAME.GenerateIsland( false ); });
        this.generateFastButton.addEventListener("click", () => { GAME.GenerateIsland( true ); });
        this.clearButton.addEventListener("click", () => { GAME.ClearIsland(); });
        this.firstPersonButton.addEventListener("click", () => { GAME.SwitchToFirstPerson(); });
        this.thirdPersonButton.addEventListener("click", () => {GAME.SwitchToThirdPerson(); });
        this.saveButton.addEventListener("click", () => { GAME.Save(); });
        this.loadInput.addEventListener("change", this.onLoad.bind(this));
        this.seedInput.addEventListener("keydown", (e) => { if (e.key === "Enter") {GAME.GenerateIsland( false )} }); //enter key on the seed box generates island
    }

    enableFirstPersonButton() {
        this.firstPersonButton.disabled = false;
    }

    disableFirstPersonButton() {
        this.firstPersonButton.disabled = true;
    }

    enableThirdPersonButton() {
        this.thirdPersonButton.disabled = false;
    }

    disableThirdPersonButton() {
        this.thirdPersonButton.disabled = true;
    }

    enableClearButton() {
        this.clearButton.disabled = false;
    }

    disableClearButton() {
        this.clearButton.disabled = true;
    }

    disableGenerationButtons() {
        this.generateButton.disabled = true;
        this.generateFastButton.disabled = true;
    }

    enableGenerationButtons() {
        this.generateButton.disabled = false;
        this.generateFastButton.disabled = false;
    }

    enableSaveButton() {
        this.saveButton.disabled = false;
    }

    disableSaveButton() {
        this.saveButton.disabled = true;
    }

    setStageText(text) {
        this.stageLabel.innerText = text;
    }


    //seed

    getSeedFromInput() {
        return this.seedInput.value;
    }

    setSeedInput(value) {
        this.seedInput.value = value;
    }

    setAndLockSeedInput(value) {
        this.seedInput.disabled = true;
        this.seedInput.value = value;
    }

    unlockSeedInput() {
        this.seedInput.disabled = false;
    }


    //size

    getSizeFromInput() {
        return this.sizeRadioInputs.values().find(r => r.checked === true).value;
    }

    setSizeInput(value) {
        this.sizeRadioInputs.values().find(r => r.value === value).checked = true;
    }

    lockSizeInput() {
        this.sizeRadioInputs.forEach(r => r.disabled = true);
    }

    unlockSizeInput() {
        this.sizeRadioInputs.forEach(r => r.disabled = false);
    }



    onLoad(e) {
        if (this.loadInput.value !== null && this.loadInput.files.length == 1) {
            GAME.Load(this.loadInput.files[0]);
        }
    }

    clearLoadInput() {
        this.loadInput.value = null;
    }


    hideTopControls() {
        this.topControls.classList.add("hidden");
    }
    showTopControls() {
        this.topControls.classList.remove("hidden");
    }


    showInventory() {
        this.inventoryContainer.classList.remove("hidden");
    }

    hideInventory() {
        this.inventoryContainer.classList.add("hidden");
    }

    showInventory() {
        this.inventoryContainer.classList.remove("hidden");
    }

    setInventorySlot(slot, item, count) {
        if (!item || count === 0) {
            //clear the slot
            this.inventorySlots[slot].innerHTML = ""; //todo; check performance
        } else {
            this.inventorySlots[slot].innerHTML =
                `<div class="inventory-item" style="background-image: url('./sprites/items/${item.imagesrc}');">${count}</div>`;
        }
    }

};

export { UiController };