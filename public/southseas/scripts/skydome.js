import * as THREE from './vendor/three.core.js';
import * as GAME from './game.js';
import { GameObject } from './gameObject.js';

class Skydome extends GameObject {

    pivot;
    texture;
    cloudTexture;
    size;
    dome;
    cloudcount = 1000;
    clouds;
    speed = 0.08;

    constructor(size) {
        super();
        this.texture = GAME.Globals.assets.find(x => x.name === 'skydome').contents;
        this.cloudTexture = GAME.Globals.assets.find(x => x.name === 'cloud').contents;
        this.size = size;
    }

    initialize() {
        super.initialize();

        this.pivot = new THREE.Group();

        const skygeometry = new THREE.SphereGeometry(this.size, 32, 32);
        const skymaterial = new THREE.MeshBasicMaterial( { map: this.texture, side:THREE.BackSide, depthWrite: false});
        this.dome = new THREE.Mesh( skygeometry, skymaterial );

        this.buildClouds();

        this.pivot.add(this.dome);
        this.pivot.add(this.clouds);

        GAME.Globals.scene.add(this.pivot);
    }

    buildClouds() {

        const geometry = new THREE.BufferGeometry();
        const vertices = [];
        const cloudscale = 1000;

        for ( let i = 0; i < this.cloudcount; i ++ ) {

            //const angle = Math.random() * Math.PI * 2;

            const x = (Math.random() * (this.size * 2.0)) - this.size;//Math.cos(angle) * this.size;
            const z = (Math.random() * (this.size * 2.0)) - this.size;//Math.sin(angle) * this.size;
            const y = this.calcCloudHeight(x, z);

            vertices.push( x, y, z );

        }

        geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );

        const material = new THREE.PointsMaterial( {
            size: cloudscale,
            sizeAttenuation: true,
            map: this.cloudTexture,
            //alphaTest: 0.5,
            opacity: 0.05,
            transparent: true,
            depthWrite: false 
        } );

        this.clouds = new THREE.Points( geometry, material );
    }

    calcCloudHeight( x, z ) {
        return Math.min(this.size - Math.max(Math.abs(x), Math.abs(z)), 600.0);
    }

    onGameTick( delta ) {

        const positions = this.clouds.geometry.attributes.position.array;
        for ( let i = 0; i < positions.length; i += 3 ) {            

            positions[i] = positions[i] += this.speed; //x
            if (positions[i] > this.size + 10) {
                positions[i] = -(this.size + 10);
            } 
            positions[i+1] = this.calcCloudHeight(positions[i], positions[i+2]); //y

        }
        this.clouds.geometry.attributes.position.needsUpdate = true;

        //this.clouds.rotation.y += 0.01;

    }

};

export { Skydome };