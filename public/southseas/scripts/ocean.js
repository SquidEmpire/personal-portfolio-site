import * as THREE from './vendor/three.core.js';
import * as GAME from './game.js';
import { GameObject } from './gameObject.js';
import { Wave } from './wave.js';

class Ocean extends GameObject {

    pivot;
    //object;
    distantObject;
    oceanSize;
    useShader = true;
    shaderMaterial;

    depthData;
    depthTexture;

    renderWaterDepth;
    _lastRenderWaterDepth;

    waves = [];

    fogmesh1;
    fogmesh2;
    fogmesh3;

    speed = 1;

    constructor( size ) {
        super();
        this.oceanSize = size;
        this.renderWaterDepth = false;
        this._lastRenderWaterDepth = this.renderWaterDepth;
    }

    initialize() {
        super.initialize();

        this.pivot = new THREE.Group();

        if (this.useShader) {

            const noiseTexture = GAME.Globals.assets.find(x => x.name === 'noise').contents;
            noiseTexture.wrapS = THREE.RepeatWrapping;
            noiseTexture.wrapT = THREE.RepeatWrapping;

            this.shaderUniforms = {
                colour: {type: 'vec3', value: new THREE.Color(0x000000)}, //todo
                time: {type: 'f', value: 0},
                noiseTexture: { type: "t", value: noiseTexture},
                depthTexture: { type: "t", value: null}, //set in UpdateDepth()
                geoSize: {type: 'f', value: this.oceanSize},
                dataSize: {type: 'f', value: 1}, //this can only be set once the island is available
                opacity: {type: 'f', value: 1.0}
            }
            this.shaderMaterial =  new THREE.ShaderMaterial({
                uniforms: this.shaderUniforms,
                fragmentShader: GAME.Globals.assets.find(x => x.name === 'ocean2Frag').contents,
                vertexShader: GAME.Globals.assets.find(x => x.name === 'ocean2Vert').contents,
                transparent: true, //required for depth plane fog things, but kills depth write for waves
                //side: THREE.DoubleSide
            });

            this.updateDepth();

        }


        const distantGeometry = new THREE.CircleGeometry( this.oceanSize, 32 );
        this.distantObject = new THREE.Mesh( distantGeometry, this.shaderMaterial );
        this.distantObject.rotation.x = -Math.PI / 2;
        this.pivot.add(this.distantObject);

        //when viewed at a very low angle, the mesh vanishes. No idea why. Material? fulstrumCulling = false didn't fix it
        //I think it has to do with the opacity/transparency on the first 2 fog materials below
        //depthWrite has an effect
        const fogmaterial = new THREE.MeshLambertMaterial({ 
            color: 0x061E49,
            transparent: true,
            depthWrite: false,
            opacity: 0.6
        });
        const fogplane = new THREE.CircleGeometry( this.oceanSize, 8 );
        this.fogmesh1 = new THREE.Mesh( fogplane, fogmaterial );
        this.fogmesh1.rotation.x = -Math.PI / 2;
        this.fogmesh1.position.y -= 0.5;
        this.pivot.add(this.fogmesh1);

        this.fogmesh2 = new THREE.Mesh( fogplane, fogmaterial );
        this.fogmesh2.rotation.x = -Math.PI / 2;
        this.fogmesh2.position.y -= 1.0;
        this.pivot.add(this.fogmesh2);

        const fogmaterial2 = new THREE.MeshLambertMaterial({ 
            color: 0x061E49
        });
        this.fogmesh3 = new THREE.Mesh( fogplane, fogmaterial2 );
        this.fogmesh3.rotation.x = -Math.PI / 2;
        this.fogmesh3.position.y -= 1.5;
        this.pivot.add(this.fogmesh3);

        this.fogmesh1.visible = false;
        this.fogmesh2.visible = false;
        this.fogmesh3.visible = false;

        this.fogmesh1.frustumCulled = false;
        this.fogmesh2.frustumCulled = false;
        this.fogmesh3.frustumCulled = false;
        
        this.pivot.position.y += GAME.Globals.gridScale * 0.5;

        GAME.Globals.scene.add( this.pivot );
    }


    updateDepth( shoreCubes = false ) {

        //can only work once an island exists!!
        if (!GAME.Globals.island) {
            let data = new Uint8Array( 4 );
            data[0] = 255;
            data[1] = 255;
            data[2] = 255;
            data[3] = 255;
            this.depthTexture = new THREE.DataTexture( data, 1, 1 );
            this.depthTexture.needsUpdate = true;

            this.shaderUniforms.dataSize.value = 1;
            this.shaderUniforms.depthTexture.value = this.depthTexture;
            return;
        }

        //lot of costly and slow (inefficient) calcs here, only call this function when absolutely necessary!

        this.depthData = new Uint8Array( 4 * (GAME.Globals.island.maxX * GAME.Globals.island.maxY) );

        let cubeCoords = false;
        let distVec1 = new THREE.Vector2();
        let distVec2 = new THREE.Vector2();
        let waveDestination = new THREE.Vector3();
        if (shoreCubes) {
            const origin = GAME.Globals.island.getWorldCentre();
            //shoreCubes is 2D array of x/y coord objects as used by island
            cubeCoords = [];
            for (let i = 0; i < shoreCubes.length; i++) {
                for (let j = 0; j < shoreCubes[i].length; j++) {
                    let cube = shoreCubes[i][j];
                    cubeCoords.push(cube);
                    waveDestination.set(
                        (cube.x - GAME.Globals.island.maxX/2) * GAME.Globals.gridScale / 2,
                        0,
                        (cube.y - GAME.Globals.island.maxY/2) * GAME.Globals.gridScale / 2
                    );
                    const tile = GAME.Globals.island.getTile(cube.x, cube.y, 0);
                    if (tile) {
                        //angle = angle to nearest tile
                        //const closest = GAME.Globals.island.getNearestTileOnLayer(tile);
                        //if (closest) {
                            //TODO: angle is angle to center of island (world origin)
                            const normalisedX = tile.pivot.position.x - origin.x; //closest.position.x
                            const normalisedZ = tile.pivot.position.z - origin.z; //closest.position.z
                            const angle = Math.atan2(normalisedX, normalisedZ);
                            const newWave = new Wave( waveDestination, angle );
                            this.waves.push(newWave);
                            newWave.initialize( this.pivot );
                        //}          
                    }
                }
            }
        }

        let shortestDistance = 1000000000;
        //fill in the data texture
        let index = 0;
        for ( let i = 0; i < GAME.Globals.island.maxX; i++ ) {
            for ( let j = 0; j < GAME.Globals.island.maxY; j++ ) {
                
                const stride = index * 4;

                if (shoreCubes) {
                    //use the distance from shore as the depth data

                    shortestDistance = 1000000000;

                    distVec1.set(i, j); //current grid coords we're looking at
                    for (let k = 0; k < cubeCoords.length; k++ ) {
                        distVec2.set(cubeCoords[k].x, cubeCoords[k].y ); //gird coords of the shore tile we're checking
                        const dist = distVec1.distanceToSquared(distVec2); //slow but it's a one-off operation
                        if (dist < shortestDistance) {
                            shortestDistance = dist;
                        }
                    }

                    //distance is in cell units, not metres (fyi)
                    if (shortestDistance > 255) {
                        shortestDistance = 255;
                    }

                    this.depthData[ stride ] = shortestDistance; //r
                    this.depthData[ stride + 1 ] = shortestDistance; //g
                    this.depthData[ stride + 2 ] = shortestDistance; //b
                    this.depthData[ stride + 3 ] = 255; //a

                } else {
                    //no depth data, just use max distance
                    this.depthData[ stride ] = 255;
                    this.depthData[ stride + 1 ] = 255;
                    this.depthData[ stride + 2 ] = 255;
                    this.depthData[ stride + 3 ] = 255;
                }

                index++;
            }

        }

        //the actual ocean size is a lot bigger than the depth data size
        this.depthTexture = new THREE.DataTexture( this.depthData, GAME.Globals.island.maxX, GAME.Globals.island.maxY );
        this.depthTexture.needsUpdate = true;

        this.shaderUniforms.depthTexture.value = this.depthTexture;
        this.shaderUniforms.dataSize.value = GAME.Globals.island.maxX;

    }

    destroyWaves() {
        for (let i = 0; i < this.waves.length; i++) {
            this.pivot.remove(this.waves[i].pivot);
            this.waves[i].destroy();
        }
        this.waves = [];
    }


    destroy() {
        super.destroy();
        this.destroyWaves();
        if (this.useShader) {
            this.shaderMaterial.dispose();
        }        
    }

    onGameTick(delta) {
        if (this.useShader) {
            this.shaderUniforms.time.value += delta * this.speed;
            this.shaderMaterial.uniformsNeedUpdate = true;
        }

        if (this.renderWaterDepth !== this._lastRenderWaterDepth) {

            //turn on and off using the water depth objects when we're near or far from the shore for mem and to avoid z-index rubbish
            if (this.renderWaterDepth) {
                this.fogmesh1.visible = true;
                this.fogmesh2.visible = true;
                this.fogmesh3.visible = true;
                this.shaderUniforms.opacity.value = 0.8;
            } else {
                this.fogmesh1.visible = false;
                this.fogmesh2.visible = false;
                this.fogmesh3.visible = false;
                this.shaderUniforms.opacity.value = 1.0;
            }

            this._lastRenderWaterDepth = this.renderWaterDepth;
        }

        const disp = Math.sin(this.shaderUniforms.time.value) * 0.001;
        this.pivot.position.y += disp;
    }

}

export { Ocean };