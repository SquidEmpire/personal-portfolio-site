import * as GAME from './game.js';
import { GameObject } from './gameObject.js';


class InventoryController extends GameObject {

    items; //array
    MAX_SLOTS = 9;
    
    constructor()
    {
        super();
        this.items = [];
        for (let i = 0; i < this.MAX_SLOTS; i++) {
            this.items[i] = {item: null, count: null};
        }
    }

    addItems(item, countToAdd) {
        const exisitingItemCount = this.getItemCount(item);
        const newCount = exisitingItemCount + countToAdd;
        const nextFreeSlotIndex = this.getNextFreeSlotIndex();
        if (exisitingItemCount) {

            const index = this.getItemSlotIndex(item);
            this.items[index].count = newCount;

            this.updateUi();

        }else if (nextFreeSlotIndex > -1) {

            this.items[nextFreeSlotIndex] = {item: item, count: newCount};

            this.updateUi();

        } else {
            console.warn(`attempted to add ${countToAdd} ${item.name} to inventory but inventory already full`);
        }
    }

    removeItems(item, countToRemove) {
        const exisitingItemCount = this.getItemCount(item);
        const newCount = exisitingItemCount - countToRemove;
        if (exisitingItemCount && exisitingItemCount > countToRemove)
        {
            const index = this.getItemSlotIndex(item);

            if (newCount <= 0) {
                //delete the item entirely
                this.items[index] = {item: null, count: null};
            } else {
                this.items[index].count = newCount;
            }

            this.updateUi();
        } else {
            console.warn(`attempted to remove ${countToAdd} ${item.name} from inventory (invalid)`);
        }
    }

    getItemCount(item) {
        for (let i = 0; i < this.MAX_SLOTS; i++) {
            if (this.items[i].item && this.items[i].item.name === item.name) {
                return this.items[i].count;
            }
        }
        return 0;
    }

    getItemSlotIndex(item) {
        for (let i = 0; i < this.MAX_SLOTS; i++) {
            if (this.items[i].item && this.items[i].item.name === item.name) {
                return i;
            }
        }
        return -1;
    }

    getNextFreeSlotIndex() {
        for (let i = 0; i < this.MAX_SLOTS; i++) {
            if (this.items[i].item === null) {
                return i;
            }
        }
        return -1;
    }

    initialize() {
        
    }

    updateUi() {
        for (let i = 0; i < this.MAX_SLOTS; i++) {
            GAME.Globals.Ui.setInventorySlot(i, this.items[i].item, this.items[i].count);
        }
    }

}

export { InventoryController }