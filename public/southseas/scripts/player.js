import * as THREE from './vendor/three.core.js';
import * as GAME from './game.js';
import { GameObject } from './gameObject.js';

class Player extends GameObject {    
    // global
    controls;

    //internal
    body;
    head;
    //TODO: pivot is at about "eye" level = e.g. the neck between head and body... it should be at the feet?
    pivot;
    _downRaycaster;
    _motionRaycaster;
    _motionRaycaster2;
    _interactionRaycaster;

    _motionRaycasterDebugArrow1;
    _motionRaycasterDebugArrow2;

    _positionTemp = new THREE.Vector3();
    _directionTemp = new THREE.Quaternion();
    

    //state
    moveForward = false;
    moveBackward = false;
    moveLeft = false;
    moveRight = false;
    canJump = false;
    mousedown = false;
    crouched = false;
    swimming = false;
    
    velocity = new THREE.Vector3();
    direction = new THREE.Vector3();    

    item;

    //settings
    mass = 20;
    bodyHeight = 1.5;
    headSize = 0.3;
    baseHeight = this.bodyHeight + this.headSize;
    crouchHeight = this.baseHeight * 0.6;
    height = this.baseHeight;
    jumpSpeed = 20;
    movementSpeed = 60;
    swimSpeed = 20; 
    slopeClimbFactor = 0.5;
    offsetFromCamera = new THREE.Vector3(0, 0, 0);
    terminalVelocity = -300;
    interactionDistance = 1.6;

    constructor( controls ) {

        super();
        this.pivot = new THREE.Group();
        this.controls = controls;

        document.addEventListener( 'keydown', this.onKeyDown.bind(this), false );
        document.addEventListener( 'keyup', this.onKeyUp.bind(this), false );

        document.addEventListener( 'mousedown', this.onMouseDown.bind(this), false );
        document.addEventListener( 'mouseup', this.onMouseUp.bind(this), false );

        this._downRaycaster = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3(), 0 );
        this._motionRaycaster = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3(), 0, 0.5 );
        this._motionRaycaster2 = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3(), 0, 0.5 );
        this._interactionRaycaster = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3(), 0, this.interactionDistance );

        this._downRaycaster.firstHitOnly = true;
        this._motionRaycaster.firstHitOnly = true;
        this._motionRaycaster2.firstHitOnly = true;
        this._interactionRaycaster.firstHitOnly = true;

        let bodyGeometry = new THREE.BoxGeometry( this.bodyHeight*0.42, this.bodyHeight, this.bodyHeight*0.42*0.5 );
        let headGeometry = new THREE.BoxGeometry( this.headSize, this.headSize, this.headSize );
        let bodyMaterial = new THREE.MeshStandardMaterial({emissive: 0xffaa00});
        let headMaterial = new THREE.MeshStandardMaterial({emissive: 0xff6600});
    
        this.body = new THREE.Mesh( bodyGeometry, bodyMaterial );
        this.head = new THREE.Mesh( headGeometry, headMaterial );

        this.body.castShadow = true;
        this.head.castShadow = true;

        //controls change angle on mouse move which can happen many times per render so we need to reposition every time the controls change
        this.controls.addEventListener( 'change', this.repositionPlayer.bind(this) );

    }

    initialize() {

        this.body.add( this.head );
        this.head.position.set( 0, this.bodyHeight/2 + this.headSize/2, 0 );

        this.pivot.add( this.body );
        this.body.position.copy( this.offsetFromCamera );
        this.body.position.y -= this.bodyHeight / 2;

        this._motionRaycasterDebugArrow1 = new THREE.ArrowHelper( this._motionRaycaster.ray.direction, this._motionRaycaster.ray.origin, 100, 0xffaaaa ); //salmon
        this._motionRaycasterDebugArrow2 = new THREE.ArrowHelper( this._motionRaycaster.ray.direction, this._motionRaycaster2.ray.origin, 100, 0xaaffaa ); //green
        //GAME.Globals.scene.add(this._motionRaycasterDebugArrow1);
        //GAME.Globals.scene.add(this._motionRaycasterDebugArrow2);

        this._interactionRaycasterDebugArrow1 = new THREE.ArrowHelper( this._interactionRaycaster.ray.direction, this._interactionRaycaster.ray.origin, this.interactionDistance, 0xff00aa ); //purple
        //GAME.Globals.scene.add(this._interactionRaycasterDebugArrow1);

        GAME.Globals.scene.add( this.pivot );
        super.initialize();

    }

    interact() {
        //check there's something in front of us we can interact with
        this._interactionRaycaster.setFromCamera( new THREE.Vector2(), this.controls.getObject() );
        let interactionIntersections = this._interactionRaycaster.intersectObjects( GAME.Globals.interactableObjects );

        this._interactionRaycasterDebugArrow1.position.copy( this._interactionRaycaster.ray.origin );
        this._interactionRaycasterDebugArrow1.setDirection( this._interactionRaycaster.ray.direction );

        if (interactionIntersections.length) {
            if (interactionIntersections[0].object.userData.onInteract) {
                //interactionIntersections[0] contains data on the intersection that onInteract needs
                interactionIntersections[0].object.userData.onInteract(interactionIntersections[0]);
            }
        }  
    }

    onKeyDown ( event ) {

        switch ( event.code ) {

            case "ArrowUp":
            case "KeyW":
                this.moveForward = true;
                break;

            case "ArrowLeft":
            case "KeyA":
                this.moveLeft = true;
                break;

            case "ArrowDown":
            case "KeyS":
                this.moveBackward = true;
                break;

            case "ArrowRight":
            case "KeyD":
                this.moveRight = true;
                break;

            case "Space":
                if ( this.canJump === true ) this.velocity.y += this.jumpSpeed;
                this.canJump = false;
                break;

            case "KeyE":
                this.interact();
                break;

            case "KeyC":
                this.onCrouch();
                break;

        }

    };

    onKeyUp ( event ) {

        switch ( event.code ) {

            case "ArrowUp":
            case "KeyW":
                this.moveForward = false;
                break;

            case "ArrowLeft":
            case "KeyA":
                this.moveLeft = false;
                break;

            case "ArrowDown":
            case "KeyS":
                this.moveBackward = false;
                break;

            case "ArrowRight":
            case "KeyD":
                this.moveRight = false;
                break;

            case "KeyC":
                this.onUnCrouch();
                break;

        }
    }

    onMouseDown () {
        this.mousedown = true;
    }

    onMouseUp () {
        this.mousedown = false;
    }

    onCrouch() {
        this.height = this.crouchHeight;
        this.crouched = true;
    }

    onUnCrouch() {
        this.height = this.baseHeight;
        this.crouched = false;
    }

    onGameTickAlways (delta) {
        if ( this.controls.isLocked === true && GAME.Globals.paused === false )
         {
            //check if using our current Y velocity whether in this tick we are on a solid object and set our velocity accordingly            
            let nextVerticalPosition = this.calculateNextVerticalPosition(delta);

            this.velocity.x -= this.velocity.x * 10.0 * delta;
            this.velocity.z -= this.velocity.z * 10.0 * delta;            

            this.direction.z = Number( this.moveForward ) - Number( this.moveBackward );
            this.direction.x = Number( this.moveRight ) - Number( this.moveLeft );
            this.direction.normalize(); // this ensures consistent movements in all directions          
            
            //set our velocity
            let speed = this.swimming ? this.swimSpeed : this.movementSpeed;
            if ( this.moveForward || this.moveBackward ) this.velocity.z -= this.direction.z * speed * delta;
            if ( this.moveLeft || this.moveRight ) this.velocity.x -= this.direction.x * speed * delta;

            //before we move check the area is free
            this.performMotionRaycasts();            

            //apply movement
            this.controls.moveRight( - this.velocity.x * delta );
            this.controls.moveForward( - this.velocity.z * delta );
            this.controls.getObject().position.y = nextVerticalPosition; 

            //after calculating our position, reposition anything on us
            this.repositionPlayer();
        }
    }

    calculateNextVerticalPosition(delta) {
        
        let bodyWorldPosition = new THREE.Vector3();
        this.body.updateMatrixWorld();
        let nextYPosition = this.controls.getObject().position.y + ( this.velocity.y * delta );

        this.body.getWorldPosition(bodyWorldPosition);
        this._downRaycaster.ray.origin.copy( bodyWorldPosition );
        this._downRaycaster.ray.origin.y += this.height;

        let raycastDirectionVector = new THREE.Vector3(0, -1, 0);

        this._downRaycaster.ray.direction.copy(raycastDirectionVector);
        let downwardIntersections = this._downRaycaster.intersectObjects( GAME.Globals.solidObjects );

        let onObject = false;
        this.swimming = false;
           
        for (var i = 0; i < downwardIntersections.length; i++) {
            if (downwardIntersections[i].point.y + this.height + 0.1 >= nextYPosition) {
                this.velocity.y = Math.max( 0, this.velocity.y );
                nextYPosition = downwardIntersections[i].point.y + this.height + 0.1;
                this.canJump = true;
                onObject = true;
                break;
            }    
        }

        if ( onObject === false ) {

            this.velocity.y = Math.max(this.terminalVelocity, this.velocity.y - (GAME.Globals.gravity * this.mass * delta));

            //swim in the ocean
            //bit jank at the moment because it only applies when the water height is at peak during the wave motion
            const boyancyPoint = GAME.Globals.ocean.pivot.position.y + this.height * 0.2;
            if ( nextYPosition <  boyancyPoint) {

                this.velocity.y = 0;
                nextYPosition = boyancyPoint;
                this.swimming = true;

            }

            //stop us going below 0
            if ( this.controls.getObject().position.y < this.height ) {

                this.velocity.y = 0;
                this.controls.getObject().position.y = this.height;
                this.canJump = true;

            }
        }

        return nextYPosition;
    }

    performMotionRaycasts() {
        let bodyWorldPosition = new THREE.Vector3();
        this.pivot.getWorldPosition(bodyWorldPosition);
        
        this._motionRaycaster.ray.origin.copy( bodyWorldPosition );
        this._motionRaycaster.ray.origin.y -= this.height;
        //motion raycaster2 is placed above the base raycaster. If we detect a collision in caster 1 but not caster 2 the obstruction is very short and can be "slid" over
        this._motionRaycaster2.ray.origin.copy( bodyWorldPosition );
        this._motionRaycaster2.ray.origin.y -= this.height - this.slopeClimbFactor;

        let raycastDirectionVector1 = new THREE.Vector3( this.direction.x, 0, 0 );
        raycastDirectionVector1.applyQuaternion( this.pivot.quaternion );
        raycastDirectionVector1.normalize();
        this._motionRaycaster.ray.direction.copy(raycastDirectionVector1);
        this._motionRaycaster2.ray.direction.copy(raycastDirectionVector1);    
        let movementXIntersections = this._motionRaycaster.intersectObjects( GAME.Globals.solidObjects );
        let movementXIntersections2 = this._motionRaycaster2.intersectObjects( GAME.Globals.solidObjects );

        let raycastDirectionVector2 = new THREE.Vector3( 0, 0, -this.direction.z );
        raycastDirectionVector2.applyQuaternion( this.pivot.quaternion );
        raycastDirectionVector2.normalize();
        this._motionRaycaster.ray.direction.copy(raycastDirectionVector2);
        this._motionRaycaster2.ray.direction.copy(raycastDirectionVector2);
        let movementZIntersections = this._motionRaycaster.intersectObjects( GAME.Globals.solidObjects );
        let movementZIntersections2 = this._motionRaycaster2.intersectObjects( GAME.Globals.solidObjects );

        if ( movementXIntersections.length > 0 ) {
            if ( movementXIntersections2.length === 0 ) {
                //this.velocity.y = this.jumpSpeed; //slide
                this.controls.getObject().position.y += this.slopeClimbFactor;
            } else {
                this.velocity.x = 0;
            }
        };
        if ( movementZIntersections.length > 0 ) {  
            if ( movementZIntersections2.length === 0 ) {
                //this.velocity.y = this.jumpSpeed; //slide
                this.controls.getObject().position.y += this.slopeClimbFactor;
            } else {
                this.velocity.z = 0;
            }

            this._motionRaycasterDebugArrow1.position.copy( this._motionRaycaster.ray.origin );
            this._motionRaycasterDebugArrow1.setDirection( raycastDirectionVector2 );

            this._motionRaycasterDebugArrow2.position.copy( this._motionRaycaster2.ray.origin );
            this._motionRaycasterDebugArrow2.setDirection( raycastDirectionVector2 );
        } 

        

    }

    repositionPlayer () {
       
        this._positionTemp.copy(this.controls.getObject().position);
        this._directionTemp.copy(this.controls.getObject().quaternion);

        this.pivot.position.copy( this._positionTemp);
        this.pivot.quaternion.copy(this._directionTemp);
        this.pivot.quaternion.x = 0;
        this.pivot.quaternion.z = 0;
        this.pivot.quaternion.normalize();

        this.head.quaternion.copy(this._directionTemp);
        this.head.quaternion.y = 0;
        this.head.quaternion.z = 0;
        this.head.quaternion.normalize();

    }

};

export { Player };