import * as THREE from '../vendor/three.core.js';
import * as GAME from '../game.js';
import { Tile, Face } from './tile.js';

class OuterCorner extends Tile {

    hasGrass = true;

    constructor( gridX, gridY, layer ) {
        super( gridX, gridY, layer );
        
        this.pickAssets();

        this.type = "outerCorner";
        this.object = this.fullAsset.contents.clone();

        this.topFaceGeo = this.topAsset.contents.geometry.clone();        
        this.xPosFaceGeo = this.sideAsset1.contents.geometry.clone();
        this.yNegFaceGeo = this.sideAsset2.contents.geometry.clone();

        this.xPosFaceGeo.rotateY(-Math.PI / 2);
        this.yNegFaceGeo.rotateY(0);

        this.slopesPosX = true;
        this.slopesPosY = true;

        this.xPosFace = new Face(false, true, true, true);
        this.xNegFace = new Face(false, false, false, false);
        this.yPosFace = new Face(false, false, false, false);
        this.yNegFace = new Face(true, false, true, true);
    }

    pickAssets() {
        this.fullAsset = GAME.Globals.assets.find(x => x.name === 'outerCorner');
        this.topAsset = GAME.Globals.assets.find(x => x.name === 'outerCornerTop');
        this.sideAsset1 = GAME.Globals.assets.find(x => x.name === 'rampSide1');
        this.sideAsset2 = GAME.Globals.assets.find(x => x.name === 'rampSide2');
    }

    initialize( position ) {
        super.initialize( position );
    }

};

export { OuterCorner };