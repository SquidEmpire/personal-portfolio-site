import * as GAME from '../game.js';
import { OuterCorner } from './outerCorner.js';

class SandOuterCorner extends OuterCorner {

    hasGrass = false;

    constructor( gridX, gridY, layer ) {
        super( gridX, gridY, layer );
    }

    pickAssets() {
        this.fullAsset = GAME.Globals.assets.find(x => x.name === 'outerCorner');
        this.topAsset = GAME.Globals.assets.find(x => x.name === 'sandOuterCornerTop');
        this.sideAsset1 = GAME.Globals.assets.find(x => x.name === 'sandRampSide1');
        this.sideAsset2 = GAME.Globals.assets.find(x => x.name === 'sandRampSide2');
    }

};

export { SandOuterCorner };