import * as THREE from '../vendor/three.core.js';
import * as GAME from '../game.js';
import { Tile } from './tile.js';

class RiverbedTile extends Tile {

    facingX = false;
    facingY = false;

    waterObject;
    hasWaterfall = false;
    waterfallObject;
    waterObjectRotation = 0;
    waterfallObjectRotation = 0;

    waterGeometry;
    waterfallGeometry;

    reverseWaterFlow = false;
    reverseWaterFlowFix = false; //mega hack

    constructor( gridX, gridY, layer ) {
        super( gridX, gridY, layer );
        this.waterfallAsset = GAME.Globals.assets.find(x => x.name === 'riverwaterWaterfall');
        this.waterfallObject = this.waterfallAsset.contents.clone();
    }

    initialize( position, facingX = false, facingY = false ) {
        super.initialize( position );
        this.facingX = facingX;
        this.facingY = facingY;
    }

    serialise() {
        let data = "";
        data += `{
            "pos":[${this.gridX},${this.gridY},${this.layer}],
            "airgaps":[${this.lastHasAirgapXPos},${this.lastHasAirgapXNeg},${this.lastHasAirgapYPos},${this.lastHasAirgapYNeg}],
            "type":"${this.type}",
            "rot":${this.rotations},
            "hasWaterfall":${this.hasWaterfall},
            "reverseWaterFlow":${this.reverseWaterFlow},
            "reverseWaterFlowFix":${this.reverseWaterFlowFix}
        }`
        return data.trim();
    }

    setWater(addWaterfall = false) {

        this.waterGeometry = this.waterObject.geometry.clone();
        this.waterfallGeometry = this.waterfallObject.geometry.clone();

        this.waterObject.position.copy( this.pivot.position );
        this.waterfallObject.position.copy( this.pivot.position );
        
        if (this.reverseWaterFlow) {
            this.waterObject.scale.x = -1;

            //no idea what this is about. Must be to do with corner tile offsets. Probably breaks straights if we ever try to flip them
            if (this.reverseWaterFlowFix) {
                this.waterObjectRotation = this.rotation - Math.PI / 2;
            } else {
                this.waterObjectRotation = this.rotation + Math.PI / 2;
            }
            
            this.waterfallObjectRotation = this.waterObjectRotation;
            

            this.waterObject.rotateY( this.waterObjectRotation ); 
            this.waterfallObject.rotateY( this.waterfallObjectRotation );

            if (this.type === "riverbedCorner") { //this function should be overwritten in the subclass instead?
                if (this.reverseWaterFlowFix) {
                    this.waterfallObjectRotation -= Math.PI / 2;
                    this.waterfallObject.rotateY( -Math.PI / 2 ); //corners are offset compared to straights
                } else {
                    this.waterfallObjectRotation += Math.PI / 2;
                    this.waterfallObject.rotateY( Math.PI / 2 );
                }  
            }
        } else {
            this.waterObjectRotation = this.rotation;
            this.waterfallObjectRotation = this.waterObjectRotation;

            this.waterObject.rotateY( this.waterObjectRotation );
            this.waterfallObject.rotateY( this.waterfallObjectRotation );

            if (this.type === "riverbedCorner") {
                this.waterfallObjectRotation -= Math.PI / 2;
                this.waterfallObject.rotateY( -Math.PI / 2 );
            }
        }
        
        if (addWaterfall) {
            this.hasWaterfall = true;
        }

        //should the unused river water object materials be deleted manually? Do they take up memory?
    }

    getWaterGeometries() {
        let geometries = [];

        const matrix = new THREE.Matrix4();
        
        matrix.makeRotationY( this.waterObjectRotation );
        this.waterGeometry.applyMatrix4( matrix );

        matrix.identity();

        if (this.waterObject.scale.x === -1) {
            
            matrix.identity();

            matrix.makeScale( -1, 1, 1);
            this.waterGeometry.applyMatrix4( matrix );

            //flip normals (winding order)
            //https://stackoverflow.com/a/54496265
            const index = this.waterGeometry.index.array;
            for (let i = 0, il = index.length / 3; i < il; i++) {
                let x = index[i * 3];
                index[i * 3] = index[i * 3 + 2];
                index[i * 3 + 2] = x;
            }
            this.waterGeometry.index.needsUpdate = true;
        }

        matrix.identity();
        matrix.makeTranslation( this.waterObject.position.x, this.waterObject.position.y, this.waterObject.position.z );
        this.waterGeometry.applyMatrix4( matrix );
        
        geometries.push(this.waterGeometry);

        if (this.hasWaterfall) {

            matrix.identity();
            matrix.makeRotationY( this.waterfallObjectRotation );
            this.waterfallGeometry.applyMatrix4( matrix );

            matrix.identity();
            matrix.makeTranslation( this.waterfallObject.position.x, this.waterfallObject.position.y, this.waterfallObject.position.z );
            this.waterfallGeometry.applyMatrix4( matrix );

            geometries.push(this.waterfallGeometry);

        }

        return geometries;
    }


};

export { RiverbedTile };