import * as GAME from '../game.js';
import { Ramp } from './ramp.js';

class SandRamp extends Ramp {

    hasGrass = false;

    constructor( gridX, gridY, layer ) {
        super( gridX, gridY, layer );
    }

    pickAssets() {
        this.fullAsset = GAME.Globals.assets.find(x => x.name === 'ramp');
        this.topAsset = GAME.Globals.assets.find(x => x.name === 'sandRampTop');
        this.sideAsset1 = GAME.Globals.assets.find(x => x.name === 'sandRampSide1');
        this.sideAsset2 = GAME.Globals.assets.find(x => x.name === 'sandRampSide2');
        this.backSideAsset = GAME.Globals.assets.find(x => x.name === 'cubeSideCoast');
    }

};

export { SandRamp };