import * as THREE from '../vendor/three.core.js';
import * as GAME from '../game.js';
import { Tile, Face } from './tile.js';

class Ramp extends Tile {

    hasGrass = true;

    constructor( gridX, gridY, layer ) {
        super( gridX, gridY, layer );

        this.pickAssets();

        this.type = "ramp";
        this.object = this.fullAsset.contents.clone();

        this.topFaceGeo = this.topAsset.contents.geometry.clone();
        this.xNegFaceGeo = this.sideAsset1.contents.geometry.clone();
        this.xPosFaceGeo = this.sideAsset2.contents.geometry.clone();
        this.yNegFaceGeo = this.backSideAsset.contents.geometry.clone();

        this.xNegFaceGeo.name = "og-xneg-side";
        this.xPosFaceGeo.name = "og-xpos-side";
        this.yNegFaceGeo.name = "og-ypos-back";

        this.yNegFaceGeo.rotateY(Math.PI);

        this.slopesPosY = true;

        this.xPosFace = new Face(false, true, true, true);
        this.xNegFace = new Face(true, false, true, true);
        this.yPosFace = new Face(false, false, false, false);
        this.yNegFace = new Face(true, true, true, true);
    }

    pickAssets() {
        this.fullAsset = GAME.Globals.assets.find(x => x.name === 'ramp');
        this.topAsset = GAME.Globals.assets.find(x => x.name === 'rampTop');
        this.sideAsset1 = GAME.Globals.assets.find(x => x.name === 'rampSide1');
        this.sideAsset2 = GAME.Globals.assets.find(x => x.name === 'rampSide2');
        this.backSideAsset = GAME.Globals.assets.find(x => x.name === 'cubeSide');
    }

    initialize( position ) {
        super.initialize( position );
    }

};

export { Ramp };