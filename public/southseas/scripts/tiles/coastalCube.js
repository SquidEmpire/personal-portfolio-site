import * as GAME from '../game.js';
import { Cube } from './cube.js';

class CoastalCube extends Cube {

    hasGrass = true;

    constructor( gridX, gridY, layer ) {
        super( gridX, gridY, layer );
    }

    pickAssets() {
        const topassetVar = `cubeTopVar${GAME.Globals.sRand.getRandomInt(1, 4)}`;
        this.topAsset = GAME.Globals.assets.find(x => x.name === topassetVar);
        this.sideAsset = GAME.Globals.assets.find(x => x.name === 'cubeSideCoast');
    }

};

export { CoastalCube };