import * as THREE from '../vendor/three.core.js';
import * as GAME from '../game.js';
import { Face } from './tile.js';
import { RiverbedTile } from './riverbedTile.js';

class RiverbedRamp extends RiverbedTile {

    constructor( gridX, gridY, layer ) {
        super( gridX, gridY, layer );
        this.fullAsset = GAME.Globals.assets.find(x => x.name === 'ramp');
        this.topAsset = GAME.Globals.assets.find(x => x.name === "riverbedRamp");
        this.sideAsset1 = GAME.Globals.assets.find(x => x.name === 'rampSide1');
        this.sideAsset2 = GAME.Globals.assets.find(x => x.name === 'rampSide2');
        this.backSideAsset = GAME.Globals.assets.find(x => x.name === 'riverbedSide');
        this.waterAsset = GAME.Globals.assets.find(x => x.name === 'riverwaterRamp');

        this.type = "riverbedRamp";
        this.object = this.fullAsset.contents.clone();
        this.waterObject = this.waterAsset.contents.clone();
        
        this.topFaceGeo = this.topAsset.contents.geometry.clone();
        this.yNegFaceGeo = this.backSideAsset.contents.geometry.clone();

        this.yNegFaceGeo.rotateY(Math.PI);

        this.slopesPosY = true;

        this.xPosFace = new Face(false, true, true, true);
        this.xNegFace = new Face(true, false, true, true);
        this.yPosFace = new Face(false, false, false, false);
        this.yNegFace = new Face(false, false, true, true); //(not working adjacent to perpendicular slopes??)
    }

    initialize( position, facingX = true, facingY = false ) {
        super.initialize( position, facingX, facingY );

        if (this.layer < 2) {
            this.sideAsset1 = GAME.Globals.assets.find(x => x.name === 'rampCoastSide1');
            this.sideAsset2 = GAME.Globals.assets.find(x => x.name === 'rampCoastSide2');
        }

        this.xNegFaceGeo = this.sideAsset1.contents.geometry.clone();
        this.xPosFaceGeo = this.sideAsset2.contents.geometry.clone();
    }

};

export { RiverbedRamp };