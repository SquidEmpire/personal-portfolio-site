import * as GAME from '../game.js';
import { Ramp } from './ramp.js';

class CoastalRamp extends Ramp {

    hasGrass = true;

    constructor( gridX, gridY, layer ) {
        super( gridX, gridY, layer );
    }

    pickAssets() {
        this.fullAsset = GAME.Globals.assets.find(x => x.name === 'ramp');
        this.topAsset = GAME.Globals.assets.find(x => x.name === 'rampTop');
        this.sideAsset1 = GAME.Globals.assets.find(x => x.name === 'rampCoastSide1');
        this.sideAsset2 = GAME.Globals.assets.find(x => x.name === 'rampCoastSide2');
        this.backSideAsset = GAME.Globals.assets.find(x => x.name === 'cubeSideCoast');
    }

};

export { CoastalRamp };