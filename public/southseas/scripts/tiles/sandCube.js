import * as GAME from '../game.js';
import { Cube } from './cube.js';

class SandCube extends Cube {

    hasGrass = false;

    constructor( gridX, gridY, layer ) {
        super( gridX, gridY, layer );
    }

    pickAssets() {
        this.topAsset = GAME.Globals.assets.find(x => x.name === 'sandCubeTop');
        this.sideAsset = GAME.Globals.assets.find(x => x.name === 'sandCubeSide');
    }

};

export { SandCube };