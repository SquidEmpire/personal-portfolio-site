import * as THREE from '../vendor/three.core.js';
import * as GAME from '../game.js';
import { Face } from './tile.js';
import { RiverbedTile } from './riverbedTile.js';

class RiverbedCorner extends RiverbedTile {

    constructor( gridX, gridY, layer ) {
        super( gridX, gridY, layer );
        this.fullAsset = GAME.Globals.assets.find(x => x.name === 'cube');
        this.topAsset = GAME.Globals.assets.find(x => x.name === "riverbedCorner");
        this.sideAsset = GAME.Globals.assets.find(x => x.name === 'cubeSideTop');
        this.sideAsset2 = GAME.Globals.assets.find(x => x.name === 'riverbedSide');
        this.waterAsset = GAME.Globals.assets.find(x => x.name === 'riverwaterCorner');
        
        this.type = "riverbedCorner";
        this.object = this.fullAsset.contents.clone();
        this.waterObject = this.waterAsset.contents.clone();

        this.xPosFace = new Face(true, true, true, true);
        this.xNegFace = new Face(false, false, true, true); //hack
        this.yPosFace = new Face(true, true, true, true); 
        this.yNegFace = new Face(false, false, true, true); //hack
    }

    initialize( position, facingX = true, facingY = false ) {
        super.initialize( position, facingX, facingY );

        if (this.layer < 2) {
            this.sideAsset = GAME.Globals.assets.find(x => x.name === 'cubeSideCoast');
            this.sideAsset2 = GAME.Globals.assets.find(x => x.name === 'riverbedSideCoast');
        }

        this.topFaceGeo = this.topAsset.contents.geometry.clone();
        this.xPosFaceGeo = this.sideAsset.contents.geometry.clone();
        this.xNegFaceGeo = this.sideAsset2.contents.geometry.clone();
        this.yPosFaceGeo = this.sideAsset.contents.geometry.clone();
        this.yNegFaceGeo = this.sideAsset2.contents.geometry.clone();

        this.xPosFaceGeo.rotateY(Math.PI / 2);
        this.xNegFaceGeo.rotateY(-Math.PI / 2);
        this.yPosFaceGeo.rotateY(0);
        this.yNegFaceGeo.rotateY(Math.PI);
    }
};

export { RiverbedCorner };