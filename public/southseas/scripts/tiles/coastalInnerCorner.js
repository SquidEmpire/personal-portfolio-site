import * as GAME from '../game.js';
import { InnerCorner } from './innerCorner.js';

class CoastalInnerCorner extends InnerCorner {

    hasGrass = true;

    constructor( gridX, gridY, layer ) {
        super( gridX, gridY, layer );
    }

    pickAssets() {
        this.fullAsset = GAME.Globals.assets.find(x => x.name === 'innerCorner');
        this.topAsset = GAME.Globals.assets.find(x => x.name === 'innerCornerTop');
        this.sideAsset1 = GAME.Globals.assets.find(x => x.name === 'rampCoastSide1');
        this.sideAsset2 = GAME.Globals.assets.find(x => x.name === 'rampCoastSide2');
        this.backSideAsset = GAME.Globals.assets.find(x => x.name === 'cubeSideCoast');
    }

};

export { CoastalInnerCorner };