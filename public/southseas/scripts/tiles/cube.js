import * as THREE from '../vendor/three.core.js';
import * as GAME from '../game.js';
import { Tile } from './tile.js';
import { Pinetree } from '../resources/pinetree.js';
import { OliveTree } from '../resources/olivetree.js';
import { Bush } from '../resources/bush.js';
import { RoseBush } from '../resources/roseBush.js';
import { LavenderBush } from '../resources/lavenderBush.js';
import { RosemaryBush } from '../resources/rosemaryBush.js';
import { GoldenrodFlowers } from '../resources/goldenrodFlowers.js';
import { CornflowersFlowers } from '../resources/cornFlowers.js';
import { BasilPlant } from '../resources/basilPlant.js';
import { MintPlant } from '../resources/mintPlant.js';
import { SagePlant } from '../resources/sagePlant.js';
import { ThymePlant } from '../resources/thymePlant.js';
import { Pinetree2 } from '../resources/pinetree2.js';

class Cube extends Tile {

    hasGrass = true;
    tree = null;
    bushes = [];
    plants = [];
    flowers = [];

    constructor( gridX, gridY, layer ) {
        super( gridX, gridY, layer );
        this.fullAsset = GAME.Globals.assets.find(x => x.name === 'cube');
        
        this.type = "cube";
        this.object = this.fullAsset.contents.clone();
    }

    initialize( position ) {
        super.initialize( position );
    }

    destroy() {
        if (this.tree) {
            this.tree.destroy();
        }
        //todo... maybe? If cubes ever need to be destroyed individually.
        //At the moment all flowers, bushes, and plants are destroyed when their instance managers are reset during entire island destruction
        /*
        for (let i = 0; i < this.flowers.length; i++) {
            this.flowers[i].destroy();
        }
        */
        super.destroy();
    }

    serialise() {
        let data = "";
        data += `{
            "pos":[${this.gridX},${this.gridY},${this.layer}],
            "airgaps":[${this.lastHasAirgapXPos | 0},${this.lastHasAirgapXNeg | 0},${this.lastHasAirgapYPos | 0},${this.lastHasAirgapYNeg | 0}],
            "type":"${this.type}",
            "rot":${this.rotations},
            `
        if (this.tree) {
            data += `"tree":${this.tree.serialise()},`;
        }

        data += `"plants":[`;
        for (let i = 0; i < this.plants.length; i++) {
            data += this.plants[i].serialise() + ",";
        }
        if (data.endsWith(",")) {
            data = data.substring(0, data.length - 1);
        }
        data += "],"
        data += `"bushes":[`;
        for (let i = 0; i < this.bushes.length; i++) {
            data += this.bushes[i].serialise() + ",";
        }
        if (data.endsWith(",")) {
            data = data.substring(0, data.length - 1);
        }
        data += "],"
        data += `"flowers":[`;
        for (let i = 0; i < this.flowers.length; i++) {
            data += this.flowers[i].serialise() + ",";
        }
        if (data.endsWith(",")) {
            data = data.substring(0, data.length - 1);
        }
        data += "]";
        data += "}";
        return data.trim();
    }

    pickAssets(hasAirgapAbove) {
        const topassetVar = `cubeTopVar${GAME.Globals.sRand.getRandomInt(1, 4)}`;
        this.topAsset = GAME.Globals.assets.find(x => x.name === topassetVar);

        if (hasAirgapAbove) {
            this.sideAsset = GAME.Globals.assets.find(x => x.name === 'cubeSideTop');
        } else {
            this.sideAsset = GAME.Globals.assets.find(x => x.name === 'cubeSideMiddle');
        }
    }

    setSideGeometies( hasAirgapAbove ) {

        this.pickAssets(hasAirgapAbove);

        this.topFaceGeo = this.topAsset.contents.geometry.clone();
        this.xPosFaceGeo = this.sideAsset.contents.geometry.clone();
        this.xNegFaceGeo = this.sideAsset.contents.geometry.clone();
        this.yPosFaceGeo = this.sideAsset.contents.geometry.clone();
        this.yNegFaceGeo = this.sideAsset.contents.geometry.clone(); 

        const topRotation = GAME.Globals.sRand.getRandomCubeRotation();
        //TODO looks weird with current texture
        //this.topFaceGeo.rotateY(topRotation);

        this.xPosFaceGeo.rotateY(Math.PI / 2);
        this.xNegFaceGeo.rotateY(-Math.PI / 2);
        this.yPosFaceGeo.rotateY(0);
        this.yNegFaceGeo.rotateY(Math.PI);
    }

    getMinimalFacesGeometries( hasAirgapAbove, hasAirgapXPos, hasAirgapXNeg, hasAirgapYPos, hasAirgapYNeg ) {

        this.setSideGeometies( hasAirgapAbove );
        return super.getMinimalFacesGeometries(hasAirgapAbove, hasAirgapXPos, hasAirgapXNeg, hasAirgapYPos, hasAirgapYNeg);
        
    }

    generateAndAddTree() {
        const rando = GAME.Globals.sRand.random();
        let tree;
        let xScale = 1;
        let yScale = 1;
        let zScale = 1;

        if (rando > 0.7) {
            tree = new OliveTree();
            yScale = GAME.Globals.sRand.getRandom(1, 1.3, 3);
        } else {
            const rando2 = GAME.Globals.sRand.random();
            if (rando < 0.4) {
                tree = new Pinetree2();
                xScale = GAME.Globals.sRand.getRandom(0.5, 1.5, 3);
                yScale = GAME.Globals.sRand.getRandom(0.6, 1.6, 3);
                zScale = xScale;
            } else {
                tree = new Pinetree();
                xScale = GAME.Globals.sRand.getRandom(0.5, 1.5, 3);
                yScale = GAME.Globals.sRand.getRandom(0.6, 1.6, 3);
                zScale = xScale;
            }       
        }

        let xOffset = GAME.Globals.sRand.getRandom(-1, 1, 3);
        let yOffset = GAME.Globals.gridScale + GAME.Globals.sRand.getRandom(-0.2, 0.1, 3);
        let zOffset = GAME.Globals.sRand.getRandom(-1, 1, 3);
        
        let rotation = GAME.Globals.sRand.getRandom(0, 6.283185307179586, 3);

        this.addTree(tree, xOffset, yOffset, zOffset, xScale, yScale, zScale, rotation);
    }

    addTree(tree, xOffset = 0, yOffset = 0, zOffset = 0, xScale = 1, yScale = 1, zScale = 1, rotation = 0) {
        this.tree = tree;
        this.tree.initialize(  this, xOffset, yOffset, zOffset, xScale, yScale, zScale, rotation );
    }

    addSurfaceFeatures() {

        this.addBush().setGlobalInstance();
        this.addBush().setGlobalInstance();
        this.addBush().setGlobalInstance();

        this.addFlowers().setGlobalInstance();
        this.addFlowers().setGlobalInstance();
        this.addFlowers().setGlobalInstance();

        this.addPlant().setGlobalInstance();
        this.addPlant().setGlobalInstance();
        this.addPlant().setGlobalInstance();
    }

    addBush() {
        let bush;
        const rand = GAME.Globals.sRand.random();
        if (rand > 0.9) {
            bush = new RoseBush();
        } else if (rand > 0.7) {
            bush = new LavenderBush();
        } else if (rand > 0.6) {
            bush = new RosemaryBush();
        } else {
            bush = new Bush();
        }
        
        bush.initialize( this );
        this.bushes.push(bush);
        return bush;
    }

    addPlant() {
        let plant;
        const rand = GAME.Globals.sRand.random();
        if (rand > 0.9) {
            plant = new ThymePlant();
        } else if (rand > 0.7) {
            plant = new SagePlant();
        } else if (rand > 0.6) {
            plant = new MintPlant();
        } else {
            plant = new BasilPlant();
        }
        
        plant.initialize( this );
        this.plants.push(plant);
        return plant;
    }

    addFlowers() {
        let flowers;
        const rand = GAME.Globals.sRand.random();
        if (rand > 0.6) {
            flowers = new CornflowersFlowers();
        } else {
            flowers = new GoldenrodFlowers();
        }
        
        flowers.initialize( this );
        this.flowers.push(flowers);
        return flowers;
    }

};

export { Cube };