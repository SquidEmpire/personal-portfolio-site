import * as GAME from '../game.js';
import { InnerCorner } from './innerCorner.js';

class SandInnerCorner extends InnerCorner {

    hasGrass = false;

    constructor( gridX, gridY, layer ) {
        super( gridX, gridY, layer );
    }

    pickAssets() {
        this.fullAsset = GAME.Globals.assets.find(x => x.name === 'innerCorner');
        this.topAsset = GAME.Globals.assets.find(x => x.name === 'sandInnerCornerTop');
        this.sideAsset1 = GAME.Globals.assets.find(x => x.name === 'sandRampSide1');
        this.sideAsset2 = GAME.Globals.assets.find(x => x.name === 'sandRampSide2');
        this.backSideAsset = GAME.Globals.assets.find(x => x.name === 'sandCubeSide');
    }

};

export { SandInnerCorner };