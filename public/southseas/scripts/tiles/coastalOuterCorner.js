import * as GAME from '../game.js';
import { OuterCorner } from './outerCorner.js';

class CoastalOuterCorner extends OuterCorner {

    hasGrass = true;

    constructor( gridX, gridY, layer ) {
        super( gridX, gridY, layer );
    }

    pickAssets() {
        this.fullAsset = GAME.Globals.assets.find(x => x.name === 'outerCorner');
        this.topAsset = GAME.Globals.assets.find(x => x.name === 'outerCornerTop');
        this.sideAsset1 = GAME.Globals.assets.find(x => x.name === 'rampCoastSide1');
        this.sideAsset2 = GAME.Globals.assets.find(x => x.name === 'rampCoastSide2');
    }

};

export { CoastalOuterCorner };