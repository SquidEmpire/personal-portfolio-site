import * as THREE from '../vendor/three.core.js';
import * as GAME from '../game.js';
import { GrassInstance } from '../grassInstance.js';


//"struct" used to compare edges of faces for culling shared faces
class Face {
    /*
        c1 - c2
        |     |
        c3 - c4
    */
    //bool
    c1;
    c2;
    c3;
    c4;
    cornerCount;

    constructor(c1, c2, c3, c4) {
        this.c1 = c1;
        this.c2 = c2;
        this.c3 = c3;
        this.c4 = c4;
        this.cornerCount = this.c1 + this.c2 + this.c3 + this.c4;
    }

    equals(face) {
        return (this.c1 === face.c1) && (this.c2 === face.c2) && (this.c3 === face.c3) && (this.c4 === face.c4);
    }

    hasLessEdgesThan(face) {
        return (this.cornerCount < face.cornerCount);
    }

    hasSameCountEdgesAs(face) {
        return (this.cornerCount === face.cornerCount);
    }

    equalsMirrored(face) {
        const mirroredC1 = this.c2;
        const mirroredC2 = this.c1;
        const mirroredC3 = this.c4;
        const mirroredC4 = this.c3;

        return (mirroredC1 === face.c1) && (mirroredC2 === face.c2) && (mirroredC3 === face.c3) && (mirroredC4 === face.c4);
    }
}

class Tile {

    asset;
    texture;
    object; //TODO: vertex (attribute position) precision seems to be what's causing the gaps between tiles?
    pivot;
    rotation = 0; //three js messes with the rotation so we store our own
    rotations = 0;

    gridX;
    gridY;
    layer;

    width;
    height;

    hasGrass = false;
    grass;

    purged = false;
    merged = false;
    markedForRiver = false;

    forceNoAirgapXPos = false;
    forceNoAirgapXNeg = false;
    forceNoAirgapYPos = false;
    forceNoAirgapYNeg = false;

    lastHasAirgapAbove;
    lastHasAirgapXPos;
    lastHasAirgapXNeg;
    lastHasAirgapYPos;
    lastHasAirgapYNeg;
    
    topFaceGeo;
    //put these in face class?
    xPosFaceGeo;
    xNegFaceGeo;
    yPosFaceGeo;
    yNegFaceGeo;

    xPosFace;
    xNegFace;
    yPosFace;
    yNegFace;

    //I don't think these should go in the Face class. Feels like more of a tile-level value
    slopesPosX = false;
    slopesNegX = false;
    slopesPosY = false;
    slopesNegY = false;

    type;

    constructor( gridX, gridY, layer ) {
        this.asset = GAME.Globals.assets.find(x => x.name === 'cube');
        this.width = 1.5;
        this.height = 1.5;
        this.halfWidth = this.width/2;
        this.halfHeight = this.height/2;
        this.type = "default";
        this.pivot = new THREE.Group();
        this.object = this.asset.contents.clone();
        
        this.gridX = gridX;
        this.gridY = gridY;
        this.layer = layer;

        this.xPosFace = new Face(true, true, true, true);
        this.xNegFace = new Face(true, true, true, true);
        this.yPosFace = new Face(true, true, true, true);
        this.yNegFace = new Face(true, true, true, true);

        //"default face" seems to be ypos (e.g. rotate 180 for yneg)        
    }

    initialize( position ) {
        this.object.position.set( 0, 0, 0 );
        this.pivot.add( this.object );
        this.pivot.position.copy( position );
    }
    
    destroy() {
        if (this.hasGrass && this.grass) {
            this.grass.destroy();
        }
        this.object.geometry.dispose();
        if (this.topFaceGeo) {this.topFaceGeo.dispose();}
        if (this.xPosFaceGeo) {this.xPosFaceGeo.dispose();}
        if (this.xNegFaceGeo) {this.xNegFaceGeo.dispose();}
        if (this.yPosFaceGeo) {this.yPosFaceGeo.dispose();}
        if (this.yNegFaceGeo) {this.yNegFaceGeo.dispose();}
        this.object.material.dispose();
    }

    //return a json string with the minimal information needed to re-create this tile later
    serialise() {
        return `{
            "pos":[${this.gridX},${this.gridY},${this.layer}],
            "airgaps":[${this.lastHasAirgapXPos},${this.lastHasAirgapXNeg},${this.lastHasAirgapYPos},${this.lastHasAirgapYNeg}],
            "type":"${this.type}",
            "rot":${this.rotations}
        }`.trim();
    }

    //only allow rotation in 90 degree blocks so that we can track which face is facing which direction
    rotate() {
        const angle = Math.PI / 2;
        this.pivot.rotateY(angle);
        this.rotation += angle;

        //shuffle the faces as we rotate so that we know which ones to cull later
        //rotation direction is counter clockwise ↺ 
        /*
             ypos
        ↙   [] ^ []   ↖
       xpos <  [] > xneg
        ↘   [] v []   ↗
             yneg
        */

        const temp = this.xPosFaceGeo;
        this.xPosFaceGeo = this.yPosFaceGeo;
        this.yPosFaceGeo = this.xNegFaceGeo;
        this.xNegFaceGeo = this.yNegFaceGeo;
        this.yNegFaceGeo = temp;       

        const temp2 = this.slopesPosX;
        this.slopesPosX = this.slopesPosY;
        this.slopesPosY = this.slopesNegX;
        this.slopesNegX = this.slopesNegY;
        this.slopesNegY = temp2;

        const temp3 = this.xPosFace;
        this.xPosFace = this.yPosFace;
        this.yPosFace = this.xNegFace;
        this.xNegFace = this.yNegFace;
        this.yNegFace = temp3;

        this.rotations++; //should wrap this?

    }

    pickAssets() {
        
    }

    getMinimalFacesGeometries( hasAirgapAbove, hasAirgapXPos, hasAirgapXNeg, hasAirgapYPos, hasAirgapYNeg ) {

        let geometries = [];

        const matrix = new THREE.Matrix4();

        matrix.makeRotationY( this.rotation );

        if (this.topFaceGeo) {this.topFaceGeo.applyMatrix4( matrix )};
        if (this.xPosFaceGeo) {this.xPosFaceGeo.applyMatrix4( matrix )};
        if (this.xNegFaceGeo) {this.xNegFaceGeo.applyMatrix4( matrix )};
        if (this.yPosFaceGeo) {this.yPosFaceGeo.applyMatrix4( matrix )};
        if (this.yNegFaceGeo) {this.yNegFaceGeo.applyMatrix4( matrix )};

        matrix.identity();
        matrix.makeTranslation( this.pivot.position.x, this.pivot.position.y, this.pivot.position.z );

        if (this.topFaceGeo) {this.topFaceGeo.applyMatrix4( matrix )};
        if (this.xPosFaceGeo) {this.xPosFaceGeo.applyMatrix4( matrix )};
        if (this.xNegFaceGeo) {this.xNegFaceGeo.applyMatrix4( matrix )};
        if (this.yPosFaceGeo) {this.yPosFaceGeo.applyMatrix4( matrix )};
        if (this.yNegFaceGeo) {this.yNegFaceGeo.applyMatrix4( matrix )};

        if (hasAirgapAbove && this.topFaceGeo) {
            this.topFaceGeo.name = this.type + "TopFaceGeo";
            geometries.push( this.topFaceGeo );
        } 
        if (!this.forceNoAirgapXPos && hasAirgapXPos && this.xPosFaceGeo) {
            this.xPosFaceGeo.name = this.type + "XPosFaceGeo";
            geometries.push( this.xPosFaceGeo );
        }
        if (!this.forceNoAirgapXNeg && hasAirgapXNeg && this.xNegFaceGeo) {
            this.xNegFaceGeo.name = this.type + "XNegFaceGeo";
            geometries.push( this.xNegFaceGeo );
        }
        if (!this.forceNoAirgapYPos && hasAirgapYPos && this.yPosFaceGeo) {
            this.yPosFaceGeo.name = this.type + "YPosFaceGeo";
            geometries.push( this.yPosFaceGeo );
        }
        if (!this.forceNoAirgapYNeg && hasAirgapYNeg && this.yNegFaceGeo) {
            this.yNegFaceGeo.name = this.type + "YNegFaceGeo";
            geometries.push( this.yNegFaceGeo );
        }      

        this.lastHasAirgapAbove = hasAirgapAbove;
        this.lastHasAirgapXPos = hasAirgapXPos;
        this.lastHasAirgapXNeg = hasAirgapXNeg;
        this.lastHasAirgapYPos = hasAirgapYPos;
        this.lastHasAirgapYNeg = hasAirgapYNeg;

        return geometries;
    }

    initializeSurfaceFeatures()
    {
        if (this.hasGrass) {
            if (this.lastHasAirgapAbove) {
                this.grass = new GrassInstance();
                this.grass.initialize(this);
            }
        }
    }

};

export { Tile, Face };