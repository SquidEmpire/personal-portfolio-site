import { InstanceController } from './instanceController.js';
import { SurfaceFeature } from './surfaceFeature.js';
import * as GAME from '../game.js';
import { Item } from '../Item.js';

class CornflowersInstanceController extends InstanceController {
    constructor() {
        super();

        this.model = GAME.Globals.assets.find(x => x.name === "cornflowersFlowers").contents.clone();
        this.name = "cornflowersInstances";
    }
}

class CornflowersFlowers extends SurfaceFeature {

    instanceType = "cornflowers";

    constructor(x = null, y= null, z = null, rotation = null) {
        super(x, y, z, rotation);
        this.controller = GAME.Globals.instanceControllers.cornflowers;
        this.item = new CornflowerItem();
        this.itemCount = 5;
    }

};

class CornflowerItem extends Item {
    name = "cornflower";
    imagesrc = "cornflower.png"
}

export { CornflowersFlowers, CornflowersInstanceController, CornflowerItem };