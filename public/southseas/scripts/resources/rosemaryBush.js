import { InstanceController } from './instanceController.js';
import { SurfaceFeature } from './surfaceFeature.js';
import * as GAME from '../game.js';
import { Item } from '../Item.js';

class RosemaryInstanceController extends InstanceController {
    constructor() {
        super();

        this.model = GAME.Globals.assets.find(x => x.name === "rosemaryBush").contents.clone();
        this.name = "rosemaryInstances";
    }
}

class RosemaryBush extends SurfaceFeature {

    instanceType = "rosemary";

    constructor(x = null, y= null, z = null, rotation = null) {
        super(x, y, z, rotation);
        this.controller = GAME.Globals.instanceControllers.rosemary;
        this.item = new RosemaryItem();
        this.itemCount = 5;
    }

};

class RosemaryItem extends Item {
    name = "rosemary";
    imagesrc = "rosemary.png"
}

export { RosemaryBush, RosemaryInstanceController, RosemaryItem };