import * as THREE from '../vendor/three.core.js';
import * as GAME from '../game.js';
import { Tree } from './tree.js';

class Pinetree extends Tree {

    positionInSolidObectsArray;

    constructor() {
        super();      
    }

    initialize( tile, xOffset, yOffset, zOffset, xScale, yScale, zScale, rotation ) {
        super.initialize( tile, xOffset, yOffset, zOffset, xScale, yScale, zScale, rotation );

        this.type = "PineTree";
        
        const treeset = GAME.Globals.assets.find(x => x.name === 'pinetree').contents.clone();

        this.leaves = treeset.children.find(e => e.name.includes("leaves"));
        this.trunk = treeset.children.find(e => e.name.includes("trunk"));

        this.leaves.material = this.leaves.material.clone();
        this.modifyMaterial(this.leaves.material); 
        this.trunk.material = this.leaves.material; //trunk and leaves are the same material
        
        treeset.position.set( 0, 0, 0 );

        this.trunk.castShadow = true;
        this.leaves.castShadow = true;

        this.pivot.add(treeset);
        this.positionInSolidObectsArray = GAME.Globals.solidObjects.push( this.trunk ) - 1;
    }

    destroy() {
        GAME.Globals.solidObjects.splice(this.positionInSolidObectsArray, 1);
        super.destroy();
    }

};

export { Pinetree };