import { InstanceController } from './instanceController.js';
import { SurfaceFeature } from './surfaceFeature.js';
import * as GAME from '../game.js';
import { Item } from '../Item.js';

class MintInstanceController extends InstanceController {
    constructor() {
        super();

        this.model = GAME.Globals.assets.find(x => x.name === "mintPlant").contents.clone();
        this.name = "mintInstances";
    }
}

class MintPlant extends SurfaceFeature {

    instanceType = "mint";

    constructor(x = null, y= null, z = null, rotation = null) {
        super(x, y, z, rotation);
        this.controller = GAME.Globals.instanceControllers.mint;
        this.item = new MintItem();
        this.itemCount = 5;
    }

};

class MintItem extends Item {
    name = "mint";
    imagesrc = "mint.png"
}

export { MintPlant, MintInstanceController, MintItem };