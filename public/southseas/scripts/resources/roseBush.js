import { InstanceController } from './instanceController.js';
import { SurfaceFeature } from './surfaceFeature.js';
import * as GAME from '../game.js';
import { Item } from '../Item.js';

class RoseInstanceController extends InstanceController {
    constructor() {
        super();

        this.model = GAME.Globals.assets.find(x => x.name === "roseBush").contents.clone();
        this.name = "roseInstances";
        this.controller = GAME.Globals.instanceControllers.rose;
    }
}

class RoseBush extends SurfaceFeature {

    instanceType = "rose";

    constructor(x = null, y= null, z = null, rotation = null) {
        super(x, y, z, rotation);
        this.controller = GAME.Globals.instanceControllers.rose;
        this.item = new RoseItem();
        this.itemCount = 5;
    }

};

class RoseItem extends Item {
    name = "rose";
    imagesrc = "rose.png"
}

export { RoseBush, RoseInstanceController, RoseItem };