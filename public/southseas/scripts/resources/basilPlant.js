import { InstanceController } from './instanceController.js';
import { SurfaceFeature } from './surfaceFeature.js';
import * as GAME from '../game.js';
import { Item } from '../Item.js';

class BasilInstanceController extends InstanceController {
    constructor() {
        super();

        this.model = GAME.Globals.assets.find(x => x.name === "basilPlant").contents.clone();
        this.name = "basilInstances";
    }
}

class BasilPlant extends SurfaceFeature {

    instanceType = "basil";

    constructor(x = null, y= null, z = null, rotation = null) {
        super(x, y, z, rotation);
        this.controller = GAME.Globals.instanceControllers.basil;
        this.item = new BasilItem();
        this.itemCount = 5;
    }

};

class BasilItem extends Item {
    name = "basil";
    imagesrc = "basil.png"
}

export { BasilPlant, BasilInstanceController, BasilItem };