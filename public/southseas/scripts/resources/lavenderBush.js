import { InstanceController } from './instanceController.js';
import { SurfaceFeature } from './surfaceFeature.js';
import * as GAME from '../game.js';
import { Item } from '../Item.js';

class LavenderInstanceController extends InstanceController {
    constructor() {
        super();

        this.model = GAME.Globals.assets.find(x => x.name === "lavenderBush").contents.clone();
        this.name = "lavenderInstances";
    }
}

class LavenderBush extends SurfaceFeature {

    instanceType = "lavender";

    constructor(x = null, y= null, z = null, rotation = null) {
        super(x, y, z, rotation);
        this.controller = GAME.Globals.instanceControllers.lavender;
        this.item = new LavenderItem();
        this.itemCount = 5;
    }

};

class LavenderItem extends Item {
    name = "lavender";
    imagesrc = "lavender.png"
}

export { LavenderBush, LavenderInstanceController, LavenderItem };