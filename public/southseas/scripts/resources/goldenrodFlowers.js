import { InstanceController } from './instanceController.js';
import { SurfaceFeature } from './surfaceFeature.js';
import * as GAME from '../game.js';
import { Item } from '../Item.js';

class GoldenrodInstanceController extends InstanceController {
    constructor() {
        super();

        this.model = GAME.Globals.assets.find(x => x.name === "goldenrodFlowers").contents.clone();
        this.name = "goldenrodInstances";
    }
}

class GoldenrodFlowers extends SurfaceFeature {

    instanceType = "goldenrod";

    constructor(x = null, y= null, z = null, rotation = null) {
        super(x, y, z, rotation);
        this.controller = GAME.Globals.instanceControllers.goldenrod;
        this.item = new GoldenrodItem();
        this.itemCount = 5;
    }

};

class GoldenrodItem extends Item {
    name = "goldenrod";
    imagesrc = "goldenrod.png"
}

export { GoldenrodFlowers, GoldenrodInstanceController, GoldenrodItem };