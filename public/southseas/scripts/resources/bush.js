import { InstanceController } from './instanceController.js';
import * as GAME from '../game.js';
import { SurfaceFeature } from './surfaceFeature.js';

class BushInstanceController extends InstanceController {
    constructor() {
        super();

        this.model = GAME.Globals.assets.find(x => x.name === "bush").contents.clone();
        this.name = "bushInstances";
    }
}

class Bush extends SurfaceFeature {

    instanceType = "bush";

    constructor(x = null, y= null, z = null, rotation = null) {
        super(x, y, z, rotation);
        this.controller = GAME.Globals.instanceControllers.bush;
    }

    onInteract()
    {
        //overwrite the default behaviour to do nothing; you can't harvest bushes
    }

    getIsGrown() {
        //overwrite the default behaviour to always return true; you can't harvest bushes
        return true;
    }

};

export { Bush, BushInstanceController };