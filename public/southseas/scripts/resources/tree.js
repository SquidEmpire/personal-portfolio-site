import * as THREE from '../vendor/three.core.js';
import * as GAME from '../game.js';
import { Noise, PlantMotion, SmoothNoise } from '../../shaders/fragments.js';
import { GameObject } from '../gameObject.js';

class Tree extends GameObject {

    pivot;
    trunk;
    leaves;

    xOffset;
    yOffset;
    zOffset;
    xScale;
    yScale;
    zScale;
    rotation;

    shaderUniforms;
    shaderMaterial;

    type;

    parentTile;
    speed = 1;

    constructor() {
        super();
        this.pivot = new THREE.Group();       
    }

    //inject shader code to modify the exisiting shader instead of making one from scratch (mainly for lighting)
    
    modifyMaterial( material ) {
        const rando = GAME.Globals.sRand.random() * 1000;
        material.onBeforeCompile = function ( shader ) {
            //this is an async function! there can be no calls to the game's random number gen inside the callback!!
            shader.uniforms.time = { value: rando };

            //prefix custom uniforms
            shader.vertexShader = 'uniform float time;\n' + shader.vertexShader;
            //inject code before the main
            shader.vertexShader = shader.vertexShader.replace(
                'void main() {',
                [
                    Noise,
                    SmoothNoise,
                    'void main() {'
                ].join( '\n' )
            );
            //inject code into main
            shader.vertexShader = shader.vertexShader.replace(
                '#include <begin_vertex>',

                PlantMotion
            ); 

            material.userData.shader = shader;

        };

        return material;
    }

    initialize( tile, xOffset, yOffset, zOffset, xScale, yScale, zScale, rotation ) {
        super.initialize();

        this.type = "Tree";
        
        this.parentTile = tile;
        this.pivot.position.copy(this.parentTile.pivot.position);

        this.xOffset = xOffset;
        this.yOffset = yOffset;
        this.zOffset = zOffset;
        this.xScale = xScale;
        this.yScale = yScale;
        this.zScale = zScale;
        this.rotation = rotation;

        //this.modifyMaterial(this.leaves.material);
        //this.trunk.material = this.leaves.material;        
        
        this.pivot.position.y += this.yOffset;
        this.pivot.position.x += this.xOffset;
        this.pivot.position.z += this.zOffset;

        this.pivot.scale.x = this.xScale;
        this.pivot.scale.y = this.yScale;
        this.pivot.scale.z = this.zScale;
        this.pivot.rotateY( this.rotation );

        //this.pivot.add(treeset);
        GAME.Globals.island.pivot.add( this.pivot );
        //tile.pivot.add(this.pivot); //swap this with above for testing direct on cubes
        //GAME.Globals.solidObjects.push( this.trunk ); //TODO: make sure we remove this when deleting this object
    }

    destroy() {
        GAME.Globals.island.pivot.remove( this.pivot );
        this.pivot.clear();
    }

    serialise() {
        return `{"type":"${this.type}", "offset":[${this.xOffset},${this.yOffset},${this.zOffset}], "scale":[${this.xScale},${this.yScale},${this.zScale}], "rot":${this.rotation}}`.trim();
    }

    onGameTick( delta ) {
        let shader;
        if (this.leaves) {
            shader = this.leaves.material.userData.shader;
           
        } else if(this.trunk) {
            shader = this.trunk.material.userData.shader;
        }
         //this is not set until the shader is compiled
        if (shader) {
            shader.uniforms.time.value += delta * this.speed;
            shader.uniforms.uniformsNeedUpdate = true;
        }
    }

};

export { Tree };