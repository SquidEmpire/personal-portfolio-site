import * as THREE from '../vendor/three.core.js';
import * as GAME from '../game.js';
import { GameObject } from '../gameObject.js';
import { Noise, PlantMotion, SmoothNoise } from '../../shaders/fragments.js';

class InstanceController extends GameObject {


    name;

    lastIndex;
    instanceCount;
    instances;
    instancesObjects; //an array of the game object (e.g. minPlant objects) managed by this controller (order matches instance ID order from this.index)

    model;

    speed = 2;
    

    constructor() {
        super();

        this.lastIndex = 0;
        this.model = GAME.Globals.assets.find(x => x.name === "bush").contents.clone();
        this.name = "instances";
        this.instancesObjects = [];

    }

    //inject shader code to modify the exisiting shader
    modifyMaterial( material ) {
        const rando = GAME.Globals.sRand.random() * 1000;
        material.onBeforeCompile = function ( shader ) {
            //this is an async function! there can be no calls to the game's random number gen inside the callback!!
            shader.uniforms.time = { value: rando };

            //prefix custom uniforms
            shader.vertexShader =
                'uniform float time;\n' +
                'attribute float vOffsets;\n' +
                shader.vertexShader;

            //inject code before the main
            shader.vertexShader = shader.vertexShader.replace(
                'void main() {',
                [
                    Noise,
                    SmoothNoise,
                    'void main() {'
                ].join( '\n' )
            );
            //inject code into main
            shader.vertexShader = shader.vertexShader.replace(
                '#include <begin_vertex>',
                PlantMotion
            ); 
            //uv offset stuff
            shader.vertexShader = shader.vertexShader.replace(
                '#include <uv_vertex>',
                [
            
                '#ifdef USE_MAP',
                'vMapUv = ( mapTransform * vec3( MAP_UV, 1 ) ).xy;',
                'vMapUv.y += vOffsets;', //vec2(0.0, 0.125);',
                '#endif',
                
                ].join( '\n' )
            );

            material.userData.shader = shader;

        };

        return material;
    }


    //TODO: some sort of handling if an index out of bounds is requested? 
    getNextInstanceIndex() {
        let index = this.lastIndex;
        this.lastIndex++;
        return index;
    }

    setMatrixAt( instanceIndex, matrix ) {
        this.instances.setMatrixAt( instanceIndex, matrix );
    }



    initialize( instanceCount ) {

        super.initialize();

        this.instanceCount = instanceCount;

        //these V offsets are used to move the UV around to show the state of the object (harvested, grown)
        let vOffsets = new Float32Array(this.instanceCount);
        for (let i = 0; i < this.instanceCount; ++i) {
            vOffsets[i] = 0;
        }


        let material = this.modifyMaterial(this.model.material);
        this.model.geometry.setAttribute("vOffsets", new THREE.InstancedBufferAttribute(vOffsets, 1));

        this.instances = new THREE.InstancedMesh( this.model.geometry, material, this.instanceCount );
        this.instances.frustumCulled = true;
        this.instances.userData.onInteract = this.onInteract.bind(this);
        this.instances.name = this.name;



        GAME.Globals.island.pivot.add(this.instances);
        GAME.Globals.interactableObjects.push(this.instances);

    }

    destroy() {
        GAME.Globals.island.pivot.remove(this.instances);
    }

    updateMatricies() {
        this.instances.instanceMatrix.needsUpdate = true;
        this.instances.computeBoundingSphere();
    }

    getInstaceObject(id) {
        return this.instancesObjects[id];
    }

    onInteract(intersectionData) {
        this.getInstaceObject(intersectionData.instanceId).onInteract(intersectionData);
    }

    setInstanceTextureHarvestedState(id, isHarvested) {
        const vOffsets = this.instances.geometry.attributes.vOffsets;       

        if (isHarvested) {
            //harvested
            vOffsets.setX(id, 0.125); // +1/8 (next texture down in the 8-tile-high atlas)
        } else {
            //regrown
            vOffsets.setX(id, 0);
        }
        vOffsets.needsUpdate = true;
        
    }

    onGameTick( delta ) {

        let shader = this.instances.material.userData.shader;
        //this is not set until the shader is compiled
        if (shader) {
            shader.uniforms.time.value += delta * this.speed;
            shader.uniforms.uniformsNeedUpdate = true;
        }

    }

}

export { InstanceController };