import { InstanceController } from './instanceController.js';
import { SurfaceFeature } from './surfaceFeature.js';
import * as GAME from '../game.js';
import { Item } from '../Item.js';

class SageInstanceController extends InstanceController {
    constructor() {
        super();

        this.model = GAME.Globals.assets.find(x => x.name === "sagePlant").contents.clone();
        this.name = "sageInstances";
    }
}

class SagePlant extends SurfaceFeature {

    instanceType = "sage";

    constructor(x = null, y= null, z = null, rotation = null) {
        super(x, y, z, rotation);
        this.controller = GAME.Globals.instanceControllers.sage;
        this.item = new SageItem();
        this.itemCount = 5;
    }

};

class SageItem extends Item {
    name = "sage";
    imagesrc = "sage.png"
}

export { SagePlant, SageInstanceController, SageItem };