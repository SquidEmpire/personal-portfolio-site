import * as THREE from '../vendor/three.core.js';
import * as GAME from '../game.js';
import { GameObject } from '../gameObject.js';

//this class contains the data for the surface feature: the 3d mesh/object is handled by the various InstanceControllers
class SurfaceFeature extends GameObject {

    parentTile;    
    matrix;
    xOffset;
    yOffset;
    zOffset;
    rotation;
    instanceIndex;
    instanceType = "none";
    controller; //the instance controller (set by children)

    item; //the item this feature gives when harvested
    itemCount; //the number of items to give
    timeToGrow; //in seconds
    currentGrowth; //0 - 1

    constructor(x = null, y= null, z = null, rotation = null) {
        super();

        this.xOffset = x === null ? GAME.Globals.sRand.getRandom(-1, 1, 3) : x;
        this.yOffset = y === null ? GAME.Globals.gridScale : y;
        this.zOffset = z === null ? GAME.Globals.sRand.getRandom(-1, 1, 3) : z;

        this.rotation = rotation === null ? GAME.Globals.sRand.getRandom(0, 6.283185307179586, 3) : rotation;

        this.timeToGrow = 10;
        this.currentGrowth = this.timeToGrow;
    }

    initialize( tile ) {
        super.initialize();
        
        this.parentTile = tile;
        
        //todo: don't create objects
        let dummy = new THREE.Object3D();
        let pos = new THREE.Vector3();
        pos.copy(this.parentTile.pivot.position);

        pos.y += this.yOffset;
        pos.x += this.xOffset;
        pos.z += this.zOffset;

        dummy.position.set( pos.x, pos.y, pos.z );
        dummy.rotateY( this.rotation );
        dummy.updateMatrix();

        this.matrix = dummy.matrix.clone();
        dummy = null;
    }

    setGlobalInstance() {
        this.instanceIndex = this.controller.getNextInstanceIndex( this.instanceType );
        this.controller.setMatrixAt( this.instanceIndex, this.matrix );
        this.controller.instancesObjects.push(this);
        
        this.controller.setInstanceTextureHarvestedState(this.instanceIndex, !this.getIsGrown());
    }

    serialise() {
        return `{"type":"${this.instanceType}", "offset":[${this.xOffset},${this.yOffset},${this.zOffset}], "rot":${this.rotation}, "grow":${Math.round(this.currentGrowth)}}`.trim();
    }

    getIsGrown() {
        return this.currentGrowth === this.timeToGrow;
    }

    onInteract(intersectionData) {
        this.harvest();
    }

    harvest() {
        if (this.getIsGrown()) {
            if (this.item && this.itemCount) {
                GAME.Globals.inventory.addItems(this.item, this.itemCount);
            }
            this.currentGrowth = 0;
            this.controller.setInstanceTextureHarvestedState(this.instanceIndex, true);
        }
    }

    restore() {
        if (!this.getIsGrown()) {
            this.currentGrowth = this.timeToGrow;
            this.controller.setInstanceTextureHarvestedState(this.instanceIndex, false);
        }
    }

    onGameTick(delta) {
        if (this.currentGrowth > this.timeToGrow) {
            this.restore();
        } else if (this.currentGrowth < this.timeToGrow) {
            this.currentGrowth += delta;
        }
    }

};

export { SurfaceFeature };