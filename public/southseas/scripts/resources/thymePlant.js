import { InstanceController } from './instanceController.js';
import { SurfaceFeature } from './surfaceFeature.js';
import * as GAME from '../game.js';
import { Item } from '../Item.js';

class ThymeInstanceController extends InstanceController {
    constructor() {
        super();

        this.model = GAME.Globals.assets.find(x => x.name === "thymePlant").contents.clone();
        this.name = "thymeInstances";
    }
}

class ThymePlant extends SurfaceFeature {

    instanceType = "thyme";

    constructor(x = null, y= null, z = null, rotation = null) {
        super(x, y, z, rotation);
        this.controller = GAME.Globals.instanceControllers.thyme;
        this.item = new ThymeItem();
        this.itemCount = 5;
    }

};

class ThymeItem extends Item {
    name = "thyme";
    imagesrc = "thyme.png"
}

export { ThymePlant, ThymeInstanceController, ThymeItem };