import * as THREE from './vendor/three.core.js';
import { GLTFLoader } from './vendor/GLTFLoader.js';

class Loader {

    texturepath;

    loadingManager;
    textureLoader;
    cubeTextureLoader;
    objectLoader;
    audioLoader;
    fileLoader;

    loaderTextElement;

    constructor( displayElement ) {
        this.loadingManager = new THREE.LoadingManager();

        this.textureLoader = new THREE.TextureLoader(this.loadingManager);
        this.cubeTextureLoader = new THREE.CubeTextureLoader(this.loadingManager);
        this.objectLoader = new GLTFLoader(this.loadingManager);
        this.audioLoader = new THREE.AudioLoader(this.loadingManager);
        this.fileLoader = new THREE.FileLoader(this.loadingManager);

        this.loadingManager.onStart = this.onLoadStart.bind(this);
        this.loadingManager.onLoad = this.onLoad.bind(this);
        this.loadingManager.onProgress = this.onLoadProgress.bind(this);
        this.loadingManager.onError = this.onLoadError.bind(this);

        this.loaderTextElement = displayElement;
    }

    onLoadStart(url, itemsLoaded, itemsTotal) {
        console.log( 'Started loading file: ' + url + '.\nLoaded ' + itemsLoaded + ' of ' + itemsTotal + ' files.' );
    }

    onLoad() {
        console.log( 'Loading complete!');    
    };

    onLoadProgress( url, itemsLoaded, itemsTotal ) {
        let displayFilename = url;
        if (displayFilename.startsWith("data:")) {
            displayFilename = "Encoded data";
        }
        if (this.loaderTextElement) {
            this.loaderTextElement.innerHTML = `<p>${displayFilename}</p><p>${itemsLoaded}/${itemsTotal}</p>`;
        }   
    };
    
    onLoadError( url ) {
        console.error( 'There was an error loading ' + url );
    };

    async loadAssets(assets) {
        let assetPromises = [];

        assets.forEach(asset => {

            switch (asset.type) {

                case THREE.Texture:
                assetPromises.push(new Promise(function(resolve, reject) {
                    this.textureLoader.load(asset.path, (textureContent) => {
                        textureContent.magFilter = THREE.NearestFilter;
                        textureContent.minFilter = THREE.LinearMipMapLinearFilter;
                        textureContent.colorSpace = THREE.SRGBColorSpace;
                        asset.setContents(textureContent);
                        resolve();
                    }, undefined, (e) => {
                        reject(e);
                    });
                }.bind(this)));
                break;

                case "model_texture":
                    //special case of THREE.Texture for materials to be used in GLTF models, requires texture flipping to be disabled and other changes
                    assetPromises.push(new Promise(function(resolve, reject) {
                        this.textureLoader.load(asset.path, (textureContent) => {
                            textureContent.magFilter = THREE.NearestFilter;
                            textureContent.minFilter = THREE.LinearMipMapLinearFilter;
                            textureContent.colorSpace = THREE.SRGBColorSpace;

                            textureContent.minFilter = THREE.NearestMipmapNearestFilter;
                            textureContent.wrapS = THREE.RepeatWrapping;
                            textureContent.wrapT = THREE.RepeatWrapping;
                            textureContent.flipY = false;
                            
                            asset.setContents(textureContent);
                            resolve();
                        }, undefined, (e) => {
                            reject(e);
                        });
                    }.bind(this)));
                    break;

                case THREE.CubeTexture:
                assetPromises.push(new Promise(function(resolve, reject) {
                    this.cubeTextureLoader.load(asset.path, (textureContent) => {
                        textureContent.magFilter = THREE.NearestFilter;
                        textureContent.minFilter = THREE.LinearMipMapLinearFilter;
                        asset.setContents(textureContent);
                        resolve();
                    }, undefined, (e) => {
                        reject(e);
                    });
                }.bind(this)));
                break;

                case "GLTF":
                assetPromises.push(new Promise(function(resolve, reject) {
                    this.objectLoader.load(asset.path, (objectContent) => {
                        asset.setContents( objectContent.scene.children[0] );
                        resolve();
                    }, undefined, (e) => {
                        reject(e);
                    });
                }.bind(this)));
                break;

                case THREE.Audio:
                assetPromises.push(new Promise(function(resolve, reject) {
                    this.audioLoader.load( asset.path, ( buffer ) => {
                        asset.setContents(buffer);
                        resolve();
                    }, undefined, (e) => {
                        reject(e);
                    });
                }.bind(this)));
                break;

                default: //generic file (including shader files)
                assetPromises.push(new Promise(function(resolve, reject) {
                    this.fileLoader.load( asset.path, ( buffer ) => {
                        asset.setContents(buffer);
                        resolve();
                    }, undefined, (e) => {
                        reject(e);
                    });
                }.bind(this)));
                break;

            }

        });

        return Promise.all(assetPromises);
    }

};

export { Loader };