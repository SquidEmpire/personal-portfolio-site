import * as THREE from './vendor/three.core.js';
import * as GAME from './game.js';
import { GameObject } from './gameObject.js';

class Wave extends GameObject {

    //waves have a start point and direction, and animate from their direction into their start point
    pivot; //pivot is also the end pos
    object;
    startObj; //this should just be a vec3 pos but for the love of god I couldn't get it to work

    speed = 0.1;
    size = GAME.Globals.gridScale * 4;
    distance = 50;
    index;

    _animationProgress; //0 - 1

    constructor(target, direction) {
        super();
        this.pivot = new THREE.Group();
        this.texture = GAME.Globals.assets.find(x => x.name === 'waveTexture').contents;
        this.texture.minFilter = THREE.NearestMipmapLinearFilter  ; //shared texture
        this.direction = direction;
        this.pivot.position.copy(target);
        this.pivot.position.y += 0.05; //for z-fighting lol

        this.startObj = new THREE.Object3D();
        this.startObj.position.copy(this.pivot.position);
        this.startObj.rotateY(this.direction);
        this.startObj.translateZ(this.distance);
       
    }

    initialize( pivot ) {
        super.initialize();

        //TODO should share material between all waves for performance
        const geometry = new THREE.PlaneGeometry(this.size, this.size, 1, 1);
        const material = new THREE.MeshLambertMaterial({
            map: this.texture,
            //color: 0xff0000,
            depthWrite: false,
            transparent: true
        });
        this.object = new THREE.Mesh( geometry, material );
        this.object.rotateX ( -Math.PI / 2 );
        this.object.rotateZ ( this.direction - Math.PI / 2 );
        this.object.position.copy(this.startObj.position);
        this.object.renderOrder = 1;

        //test thing for visualising the origin ;)
        const g2 = new THREE.BoxGeometry( GAME.Globals.gridScale, GAME.Globals.gridScale, GAME.Globals.gridScale );
        const m2 = new THREE.Mesh(g2, material);
        m2.position.copy(this.startObj.position);
        m2.rotation.copy(this.startObj.rotation);
        //this.pivot.add(m2);

        this.pivot.add(this.startObj);
        this.pivot.add(this.object);

        //start off on a random pos of the animation
        this._animationProgress = GAME.Globals.sRand.random();
        
        pivot.add(  this.pivot );
    }

    destroy () {
        this.object.material.dispose();
        this.object.geometry.dispose();
        this.pivot.clear();
    }

    onGameTick( delta ) {
        
        this._animationProgress += (1 - this._animationProgress + 0.1) * this.speed * delta;
        
        if ( this._animationProgress >= 1 && this.object.material.opacity === 0 ) {
            this.object.position.copy( this.startObj.position );
            this._animationProgress = this._animationProgress % 1;
        }
        this.object.position.lerpVectors( this.startObj.position, this.pivot.position, this._animationProgress );
        let opacity = Math.min(this._animationProgress * 2, 1);
        if (this._animationProgress > 0.9) {
            opacity = 1 - (this._animationProgress * 10 - 9);
            opacity = Math.max(opacity, 0);          
        }
        
        this.object.material.opacity = opacity;
    }

};

export { Wave };