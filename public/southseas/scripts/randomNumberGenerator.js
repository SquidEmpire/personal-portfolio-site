//https://stackoverflow.com/a/47593316
class RandomNumberGenerator {

    seed;
    rand;

    constructor(seed = "apples") {
        this.seed = this.cyrb128(seed);
        this.rand = this.mulberry32(this.seed[0]);
    }

    cyrb128(str) {
        let h1 = 1779033703, h2 = 3144134277,
            h3 = 1013904242, h4 = 2773480762;
        for (let i = 0, k; i < str.length; i++) {
            k = str.charCodeAt(i);
            h1 = h2 ^ Math.imul(h1 ^ k, 597399067);
            h2 = h3 ^ Math.imul(h2 ^ k, 2869860233);
            h3 = h4 ^ Math.imul(h3 ^ k, 951274213);
            h4 = h1 ^ Math.imul(h4 ^ k, 2716044179);
        }
        h1 = Math.imul(h3 ^ (h1 >>> 18), 597399067);
        h2 = Math.imul(h4 ^ (h2 >>> 22), 2869860233);
        h3 = Math.imul(h1 ^ (h3 >>> 17), 951274213);
        h4 = Math.imul(h2 ^ (h4 >>> 19), 2716044179);
        return [(h1^h2^h3^h4)>>>0, (h2^h1)>>>0, (h3^h1)>>>0, (h4^h1)>>>0];
    }

    mulberry32(a) {
        return function() {
          var t = a += 0x6D2B79F5;
          t = Math.imul(t ^ t >>> 15, t | 1);
          t ^= t + Math.imul(t ^ t >>> 7, t | 61);
          return ((t ^ t >>> 14) >>> 0) / 4294967296;
        }
    }

    random() {
        return this.rand();
    }

    //https://stackoverflow.com/a/29494612
    toFixedNumber(num, digits, base = 10){
        const pow = Math.pow(base ?? 10, digits);
        return Math.round(num*pow) / pow;
    }

    getRandom(min = 0, max = 1, decimalPoints = -1) {
        let n = (this.random()  * (max - min)) + min;
        if (decimalPoints >= 0) {
            n = this.toFixedNumber(n, decimalPoints);
        }
        return n;
    }

    getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.round(this.random() * (max - min)) + min;
    }

    getRandomValueFromArray(array) {
        return array[~~(this.random() * array.length)];
    }

    getRandomCubeRotation() {
        switch (this.getRandomInt(1, 4)) 
        {
            case 1:
                return 0;
            case 2:
                return Math.PI / 2;
            case 3:
                return Math.PI;
            case 4:
                return - Math.PI / 2;
        }
    }

    getRandomBool() {
        return this.random() > 0.5;
    }

}

export { RandomNumberGenerator }