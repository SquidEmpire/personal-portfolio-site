import * as THREE from './vendor/three.core.js';
import * as GAME from './game.js';
import * as BufferGeometryUtils from './vendor/BufferGeometryUtils.js';
import { RiverbedStraight } from './tiles/riverbedStraight.js';
import { RiverbedEnd } from './tiles/riverbedEnd.js';
import { RiverbedCorner } from './tiles/riverbedCorner.js';
import { RiverbedRamp } from './tiles/riverbedRamp.js';
import { GameObject } from './gameObject.js';

class River extends GameObject {

    colour; //different river colours via shader? 🤔
    speed = 0.05; //different speeds for rivers?

    //rivers have an array of tiles they follow, starting at 0 on the terminus and ending at <length> at the source
    //rivers are backwards because by generating from the outside in we can guarentee they end up in the ocean
    tiles = []; //this holds the original tiles we replace
    riverTiles = []; //this holds the new tiles this river is made of
    lastTileIndex = 0;
    merged = false;
    plotted = false;
    replaced = false;

    lastX;
    lastY;
    lastLayer;

    lastDirectionX;
    lastDirectionY;

    minLength = 3;
    maxLength;

    mergedWater;

    constructor( ) {
        super();        
    }

    prepareForGeneration(gridX, gridY, layer) {
        this.lastX = gridX;
        this.lastY = gridY;
        this.lastLayer = layer;
        this.maxLength = GAME.Globals.sRand.getRandomInt(3, 20);
    }

    initialize() {

        const waterTexture = GAME.Globals.assets.find(x => x.name === 'riverTexture').contents;
        waterTexture.wrapS = THREE.RepeatWrapping; //horizontal wrapping only

        this.material = new THREE.MeshBasicMaterial({
            color: 0xffffff,
            transparent: true,
            opacity: 0.9,
            map: waterTexture
        });
        const rando = GAME.Globals.sRand.random() * 1000;
        this.material.onBeforeCompile = function ( shader ) {

            shader.uniforms.time = { value: (rando) };
            //prefix custom uniforms
            shader.vertexShader = 'uniform float time;\n' + shader.vertexShader;
            //inject code into the fragment to move the water over time
            shader.vertexShader = shader.vertexShader.replace(
                '#include <uv_vertex>',
                `
                #include <uv_vertex>
                vMapUv.x = vMapUv.x + time;
                vMapUv.x = vMapUv.x + time;
                `
            );
            this.material.userData.shader = shader;
        }.bind(this);
        super.initialize();
    }
    
    destroy() {
        if (this.mergedWater) {
            this.mergedWater.geometry.dispose();
        }
        for (let i = 0; i < this.tiles.length; i++) {
            this.tiles[i].markedForRiver = false;
        }
        for (let i = 0; i < this.riverTiles.length; i++) {
            this.riverTiles[i].destroy(); //no idea what this will do if the river exists in geometry lol
        }
        this.material.dispose();
        super.destroy();
    }

    //return a json string with the minimal information needed to re-create this river later
    serialise() {
        let data = `{"tiles":[`;
        for (let i = 0; i < this.riverTiles.length; i++) {
            //for clarity it might be better to use tiles[] instead of river tiles here? They should have the same coords
            data += `{"pos":[${this.riverTiles[i].gridX},${this.riverTiles[i].gridY},${this.riverTiles[i].layer}]},`;
        }
        if (data.endsWith(",")) {
            data = data.substring(0, data.length - 1);
        }
        data += "]}"
        return data.trim();
    }

    onGameTick(delta) {
        let shader = this.material.userData.shader;
        //this is not set until the shader is compiled
        if (shader) {
            shader.uniforms.time.value += delta * this.speed;
            shader.uniforms.uniformsNeedUpdate = true;
        }
    }

    plotNextTile( island ) {

        if (this.tiles.length === this.maxLength) {
            this.plotted = true;
            return false;
        }

        //find an adjacent tile we can move the river to
        const xpN = island.getPosXNeighbour( this.lastX, this.lastY, this.lastLayer );
        const xnN = island.getNegXNeighbour( this.lastX, this.lastY, this.lastLayer );
        const ypN = island.getPosYNeighbour( this.lastX, this.lastY, this.lastLayer );
        const ynN = island.getNegYNeighbour( this.lastX, this.lastY, this.lastLayer );

        const xpUN = island.getPosXNeighbour( this.lastX, this.lastY, this.lastLayer + 1 );
        const xnUN = island.getNegXNeighbour( this.lastX, this.lastY, this.lastLayer + 1 );
        const ypUN = island.getPosYNeighbour( this.lastX, this.lastY, this.lastLayer + 1 );
        const ynUN = island.getNegYNeighbour( this.lastX, this.lastY, this.lastLayer + 1 );

        const xPosValid = xpN && !xpN.markedForRiver && xpUN === null && xpN.type === "cube";
        const xNegValid = xnN && !xnN.markedForRiver && xnUN === null && xnN.type === "cube";
        const yPosValid = ypN && !ypN.markedForRiver && ypUN === null && ypN.type === "cube";
        const yNegValid = ynN && !ynN.markedForRiver && ynUN === null && ynN.type === "cube";

        //also check for ramps going up
        const xPosUValid = xpUN && !xpUN.markedForRiver && xpUN.type === "ramp";
        const xNegUValid = xnUN && !xnUN.markedForRiver && xnUN.type === "ramp";
        const yPosUValid = ypUN && !ypUN.markedForRiver && ypUN.type === "ramp";
        const yNegUValid = ynUN && !ynUN.markedForRiver && ynUN.type === "ramp";
      
        let validTiles = [];
        if (xPosValid) {validTiles.push(xpN)};
        if (xNegValid) {validTiles.push(xnN)};
        if (yPosValid) {validTiles.push(ypN)};
        if (yNegValid) {validTiles.push(ynN)};
        if (xPosUValid) {validTiles.push(xpUN)};
        if (xNegUValid) {validTiles.push(xnUN)};
        if (yPosUValid) {validTiles.push(ypUN)};
        if (yNegUValid) {validTiles.push(ynUN)};

        if (validTiles.length === 0) {
            this.plotted = true;
            return false;
        } 

        let nextTile;

        if (this.tiles.length === 0) {

            //the first tile should just be added immediately
            const tile = island.getTile( this.lastX, this.lastY, this.lastLayer );
            if (!tile) {
                this.plotted = true;
                return false;
            }
            nextTile = tile;
        
        } else if (this.tiles.length <= 2) {

            //TODO: this is opaque and also doesn't work very well. Think of something smarter!
            //near the shore, always take the direction that takes us closer to the island center
            const islandCenterX = island.halfMaxX;
            const islandCenterY = island.halfMaxY;
            const xDistToCenter = this.lastX - islandCenterX;
            const yDistToCenter = this.lastY - islandCenterY;
            
            if (Math.abs(xDistToCenter) > Math.abs(yDistToCenter)) {
                //prefer to take the X direction
                if (xDistToCenter > 0) {
                    if (xNegValid) { nextTile = xnN; }
                    else if (xNegUValid) { nextTile = xnUN; }
                    else { nextTile = GAME.Globals.sRand.getRandomValueFromArray(validTiles); }
                } else {
                    if (xPosValid) { nextTile = xpN; }
                    else if (xPosUValid) { nextTile = xpUN; }
                    else { nextTile = GAME.Globals.sRand.getRandomValueFromArray(validTiles); }
                }
            } else {
                //prefer to take the Y direction
                if (yDistToCenter > 0) {
                    if (yNegValid) { nextTile = ynN; }
                    else if (yNegUValid) { nextTile = ynUN; }
                    else { nextTile = GAME.Globals.sRand.getRandomValueFromArray(validTiles); }
                } else {
                    if (yPosValid) { nextTile = ypN; }
                    else if (yPosUValid) { nextTile = ypUN; }
                    else { nextTile = GAME.Globals.sRand.getRandomValueFromArray(validTiles); }
                }
            }

        } else if (GAME.Globals.sRand.random() > 0.5) {

            //now that we're away from the shore, the river tries to move in the same direction as it's been going
            const doubleBackTile = this.tiles[this.tiles.length - 2];

            if        (this.lastX > doubleBackTile.gridX && xPosValid) {
                nextTile = xpN;
            } else if (this.lastX < doubleBackTile.gridX && xNegValid) {
                nextTile = xnN;
            } else if (this.lastY > doubleBackTile.gridY && yPosValid) {
                nextTile = ypN;
            } else if (this.lastY < doubleBackTile.gridY && yNegValid) {
                nextTile = ynN;
            } else if (this.lastX > doubleBackTile.gridX && xPosUValid) {
                nextTile = xpUN;
            } else if (this.lastX < doubleBackTile.gridX && xNegUValid) {
                nextTile = xnUN;
            } else if (this.lastY > doubleBackTile.gridY && yPosUValid) {
                nextTile = ypUN;
            } else if (this.lastY < doubleBackTile.gridY && yNegUValid) {
                nextTile = ynUN;
            } else {
                nextTile = GAME.Globals.sRand.getRandomValueFromArray(validTiles);
            }

        } else {

            nextTile = GAME.Globals.sRand.getRandomValueFromArray(validTiles);

        }       

        
        nextTile.markedForRiver = true;

        this.tiles.push( nextTile );

        this.lastX = nextTile.gridX;
        this.lastY = nextTile.gridY;
        this.lastLayer = nextTile.layer;

        return true;

    }

    setNextTile( island ) {

        if (this.lastTileIndex < this.tiles.length) {

            const previousTile = this.lastTileIndex > 0 ? this.tiles[this.lastTileIndex - 1] : null;
            const tile = this.tiles[this.lastTileIndex];
            const nextTile = this.lastTileIndex < (this.tiles.length - 1) ? this.tiles[this.lastTileIndex + 1] : null;
            
            let goingXPos;
            let goingXNeg;
            let goingYPos;
            let goingYNeg;

            let goingUp;

            if (!previousTile) {
                goingXPos = nextTile.gridX > tile.gridX;
                goingXNeg = nextTile.gridX < tile.gridX;
                goingYPos = nextTile.gridY > tile.gridY;
                goingYNeg = nextTile.gridY < tile.gridY;
                goingUp = false;

                //if this is the first tile, make sure the tile "in front" is empty, so the river terminates on the sea (null tile)
                if (goingXPos && tile.gridX < island.maxX - 1 && tile.gridX > 0) { island.removeTileAndAbove( tile.gridX - 1, tile.gridY, 0 ); } //first tile is always on layer 0
                if (goingXNeg && tile.gridX < island.maxX - 1 && tile.gridX > 0) { island.removeTileAndAbove( tile.gridX + 1, tile.gridY, 0 ); }
                if (goingYPos && tile.gridY < island.maxY - 1 && tile.gridY > 0) { island.removeTileAndAbove( tile.gridX, tile.gridY - 1, 0 ); }
                if (goingYNeg && tile.gridY < island.maxY - 1 && tile.gridY > 0) { island.removeTileAndAbove( tile.gridX, tile.gridY + 1, 0 ); }


            } else if (!nextTile) {
                goingXPos = tile.gridX > previousTile.gridX;
                goingXNeg = tile.gridX < previousTile.gridX;
                goingYPos = tile.gridY > previousTile.gridY;
                goingYNeg = tile.gridY < previousTile.gridY;
                goingUp = tile.layer > previousTile.layer;
            } else {
                goingXPos = nextTile.gridX > previousTile.gridX;
                goingXNeg = nextTile.gridX < previousTile.gridX;
                goingYPos = nextTile.gridY > previousTile.gridY;
                goingYNeg = nextTile.gridY < previousTile.gridY;
                goingUp = tile.layer > previousTile.layer;
            }

            let newTile;
            switch (true) {
                
                //corners
                case goingYPos && goingXPos:
                    //corner 1
                    newTile = island.replaceTile( tile.gridX, tile.gridY, tile.layer, RiverbedCorner );      
                    if (nextTile.gridY > tile.gridY) {
                        newTile.rotate();
                        newTile.reverseWaterFlow = true;
                    } else {
                        newTile.rotate();
                        newTile.rotate();
                        newTile.rotate();
                    }
                    break;

                case goingYPos && goingXNeg:
                    //corner 2
                    newTile = island.replaceTile( tile.gridX, tile.gridY, tile.layer, RiverbedCorner );
                    if (nextTile.gridY > tile.gridY) {
                        newTile.rotate();
                        newTile.rotate();
                    } else {
                        newTile.reverseWaterFlow = true;
                        newTile.reverseWaterFlowFix = true;
                    }
                    break;

                case goingYNeg && goingXPos:
                    //corner 3
                    newTile = island.replaceTile( tile.gridX, tile.gridY, tile.layer, RiverbedCorner );
                    if (nextTile.gridY < tile.gridY) {

                    } else {
                        newTile.rotate();
                        newTile.rotate();  
                        newTile.reverseWaterFlow = true;
                        newTile.reverseWaterFlowFix = true;
                    }
                    break;
                case goingYNeg && goingXNeg:
                    //corner 4
                    newTile = island.replaceTile( tile.gridX, tile.gridY, tile.layer, RiverbedCorner );
                    if (nextTile.gridY < tile.gridY) {
                        newTile.rotate();
                        newTile.rotate();
                        newTile.rotate();
                        newTile.reverseWaterFlow = true;
                    } else {
                        newTile.rotate();
                    }
                    break;
                
                //straights
                case goingYPos && !(goingXPos || goingXNeg):
                    //ypos straight
                    if (goingUp) {
                        newTile = island.replaceTile( tile.gridX, tile.gridY, tile.layer, RiverbedRamp );

                        //this nonsense is to deal with the way riverbed ramps connect to diagonal (down) riverbed tiles, the tile below the ramp has to be forced to not 
                        //create a face against the riverbed the ramp is connecting to from above
                        let tilebelow = island.getTileBelow( tile.gridX, tile.gridY, tile.layer );
                        if (tilebelow) {
                            tilebelow.forceNoAirgapYNeg = true;
                            tilebelow.purged = false;
                        }
                    } else {
                        newTile = island.replaceTile( tile.gridX, tile.gridY, tile.layer, RiverbedStraight );
                    }
                    newTile.rotate();
                    newTile.rotate();
                    break;
                case goingYNeg && !(goingXPos || goingXNeg):
                    //yneg straight
                    if (goingUp) {
                        newTile = island.replaceTile( tile.gridX, tile.gridY, tile.layer, RiverbedRamp );

                        let tilebelow = island.getTileBelow( tile.gridX, tile.gridY, tile.layer );
                        if (tilebelow) {
                            tilebelow.forceNoAirgapYPos = true;
                            tilebelow.purged = false;
                        }
                    } else {
                        newTile = island.replaceTile( tile.gridX, tile.gridY, tile.layer, RiverbedStraight );
                    }
                    
                    break;

                case goingXPos && !(goingYPos || goingYNeg):
                    //xpos straight
                    if (goingUp) {
                        newTile = island.replaceTile( tile.gridX, tile.gridY, tile.layer, RiverbedRamp );

                        let tilebelow = island.getTileBelow( tile.gridX, tile.gridY, tile.layer );
                        if (tilebelow) {
                            tilebelow.forceNoAirgapXNeg = true;
                            tilebelow.purged = false;
                        }
                    } else {
                        newTile = island.replaceTile( tile.gridX, tile.gridY, tile.layer, RiverbedStraight );
                    }
                    
                    newTile.rotate();
                    newTile.rotate();
                    newTile.rotate();
                    break;
                case goingXNeg && !(goingYPos || goingYNeg):
                default:
                    //xneg straight
                    if (goingUp) {
                        newTile = island.replaceTile( tile.gridX, tile.gridY, tile.layer, RiverbedRamp );

                        let tilebelow = island.getTileBelow( tile.gridX, tile.gridY, tile.layer );
                        if (tilebelow) {
                            tilebelow.forceNoAirgapXPos = true;
                            tilebelow.purged = false;
                        }
                    } else {
                        newTile = island.replaceTile( tile.gridX, tile.gridY, tile.layer, RiverbedStraight );
                    }
                    newTile.rotate();
                    break;

            }

            let addWaterfall = false;
            if ((goingUp && newTile.type !== "riverbedRamp") || this.lastTileIndex === 0) {
                addWaterfall = true;
            }

            newTile.setWater(addWaterfall);
            
            this.lastTileIndex++;

            this.lastX = tile.gridX;
            this.lasyY = tile.gridY;
            this.lastLayer = tile.layer;

            this.riverTiles.push(newTile);

            return false;
        } else {
            //the river is done
            //the last tile will always be either a ramp or straight
            const lastTile = this.riverTiles[this.lastTileIndex - 1];
            const lastTileRotataions = lastTile.rotations;
            let newTile;
            let addWaterfall = false;

            if (lastTile.type === "riverbedRamp") {
                newTile = island.replaceTile( lastTile.gridX, lastTile.gridY, lastTile.layer, RiverbedEnd );
                //the tile below this tile will have been set up to have a forced no airgap to deal with the ramp
                const below = island.getTileBelow( lastTile.gridX, lastTile.gridY, lastTile.layer );
                below.forceNoAirgapXPos = false;
                below.forceNoAirgapXNeg = false;
                below.forceNoAirgapYPos = false;
                below.forceNoAirgapYNeg = false;

                addWaterfall = true;

            } else {
                newTile = island.replaceTile( lastTile.gridX, lastTile.gridY, lastTile.layer, RiverbedEnd );
            }

            this.riverTiles.splice(this.riverTiles.length - 1, 1);
            this.riverTiles.push(newTile);

            for (let i = 0; i < lastTileRotataions; i++) {
                newTile.rotate();
            }

            newTile.setWater(addWaterfall);

            //TODO: make sure the tile the last river tile was pointing to hasn't been purged (so that the backface can be added correctly)
                        
            this.replaced = true;
            return true;
        }        
    }

    mergeWaters() {
        let geometries = [];
        for (let i = 0; i < this.riverTiles.length; i++) {
            const newGeos = this.riverTiles[i].getWaterGeometries();
            geometries = geometries.concat(newGeos);
        }

        let mergedWaterGeometry = BufferGeometryUtils.mergeBufferGeometries(geometries, false);   
        mergedWaterGeometry = BufferGeometryUtils.mergeVertices(mergedWaterGeometry);
        mergedWaterGeometry.computeBoundingSphere();
        this.mergedWater = new THREE.Mesh(mergedWaterGeometry, this.material);//new THREE.MeshBasicMaterial( {color: 0xffffff} ));

        this.merged = true;
    }

};

export { River };