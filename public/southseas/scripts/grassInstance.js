import * as THREE from './vendor/three.core.js';
import * as GAME from './game.js';
import { GameObject } from './gameObject.js';

class GrassInstance extends GameObject {

    cullingBox;
    instancedMesh;
    shaderUniforms;
    shaderMaterial;
    speed = 1;
    raycaster;

    constructor() {
        super();

        this.raycaster = new THREE.Raycaster();

        //this.baseObject = GAME.Globals.assets.find(x => x.name === 'grassBlade').contents;

        // const halftile = GAME.Globals.gridScale / 2;
        // this.cullingBox = new THREE.Box3(
        //     new THREE.Vector3(
        //         tile.pivot.position.x - halftile,
        //         tile.pivot.position.y + GAME.Globals.gridScale,
        //         tile.pivot.position.z - halftile
        //         ),
        //     new THREE.Vector3(
        //         tile.pivot.position.x + halftile, 
        //         tile.pivot.position.y + GAME.Globals.gridScale * 2,
        //         tile.pivot.position.z + halftile
        //         )
        // );

        this.shaderUniforms = {
            time: {type: 'f', value: 0}
        }

        this.shaderMaterial = new THREE.ShaderMaterial({
            uniforms: this.shaderUniforms,
            fragmentShader: GAME.Globals.assets.find(x => x.name === 'grassFrag').contents,
            vertexShader: GAME.Globals.assets.find(x => x.name === 'grassVert').contents,
            //lights: true,
            side: THREE.DoubleSide
        });

    }

    initialize(tile) {
        super.initialize();

        const instanceNumber = 200;
        const dummy = new THREE.Object3D();

        const grassWidth = 0.06;
        const grassHeight = 0.4;
        const geometry = new THREE.PlaneGeometry( grassWidth, grassHeight, 1, 4 );
        geometry.translate( 0, grassHeight * 0.5, 0 ); // move grass blade geometry lowest point at 0.

        this.instancedMesh = new THREE.InstancedMesh( geometry, this.shaderMaterial, instanceNumber );
        this.instancedMesh.cullingBox = this.cullingBox; //SUS
        //this.instancedMesh.onBeforeRender = this.onBeforeRender;

        this.instancedMesh.position.copy(tile.pivot.position);
        this.instancedMesh.position.y += GAME.Globals.gridScale;

        const raycastPos = new THREE.Vector3();
        const raycastDir = new THREE.Vector3(0, -1, 0);
        for ( let i=0 ; i < instanceNumber ; i++ ) {

            const x = GAME.Globals.sRand.getRandom(-1.4, 1.4);
            let y = 0;
            const z = GAME.Globals.sRand.getRandom(-1.4, 1.4);

            //raycast from x/y down to find the intersect on the tile for the z
            //lazy and slow but w/e, it's a one-off operation
            tile.pivot.getWorldPosition( raycastPos );
            //shoudln't have to do this ;_;
            const islandCentre = GAME.Globals.island.getWorldCentre();
            raycastPos.x -= islandCentre.x;
            raycastPos.z -= islandCentre.z;

            raycastPos.x += x;
            raycastPos.y += GAME.Globals.gridScale * 2;
            raycastPos.z += z;
            
            this.raycaster.set( raycastPos, raycastDir );

            const intersects = this.raycaster.intersectObject( GAME.Globals.island.mergedIsland );
            if (intersects.length) {
                y = intersects[0].point.y - (tile.layer * GAME.Globals.gridScale) - GAME.Globals.gridScale;
            }

            dummy.position.set( x, y, z );

            dummy.scale.setScalar( 0.5 + GAME.Globals.sRand.random() * 0.5 );
            dummy.rotation.y = GAME.Globals.sRand.random() * Math.PI;
            dummy.updateMatrix();
            this.instancedMesh.setMatrixAt( i, dummy.matrix );

        }

        //I don't know if doing this helps, but I really dislike raycaster objects hanging around
        this.raycaster = null;

        //manual bounding sphere for fulstrum culling
        //https://discourse.threejs.org/t/how-to-do-frustum-culling-with-instancedmesh/22633
        //https://github.com/mrdoob/three.js/pull/21507
        this.instancedMesh.frustumCulled = true;
        this.instancedMesh.geometry.boundingSphere = new THREE.Sphere( new THREE.Vector3(), GAME.Globals.gridScale ); //this is overdoing it, but why not

        this.instancedMesh.receiveShadow = true; //TODO lol

        GAME.Globals.island.pivot.add( this.instancedMesh );
    }

    destroy() {
        GAME.Globals.island.pivot.remove(this.instancedMesh);
        this.instancedMesh.geometry.dispose();
        this.instancedMesh.material.dispose();
    }

    //https://jsfiddle.net/prisoner849/n1emstwd/

    //https://discourse.threejs.org/t/how-to-do-frustum-culling-with-instancedmesh/22633/6
    //should be possible

    //this function runs on the InstancedMesh, NOT this class
    // onBeforeRender(renderer, scene, camera, geometry, material, group) {
    //     const frustum = new THREE.Frustum().setFromProjectionMatrix( 
    //         new THREE.Matrix4().multiplyMatrices( camera.projectionMatrix, camera.matrixWorldInverse ) 
    //     );

    //     this.visible = frustum.intersectsBox(this.cullingBox);
    // }

    onGameTick(delta) {
        this.shaderUniforms.time.value += delta * this.speed;
        this.shaderMaterial.uniformsNeedUpdate = true;
    }

}

export {GrassInstance};