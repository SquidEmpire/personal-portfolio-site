import * as THREE from './vendor/three.core.js';
import * as THREEGL from './vendor/three.module.js';
import WebGL from './vendor/WebGL.js';
import WebGPU from './vendor/WebGPU.js';
import * as BufferGeometryUtils from './vendor/BufferGeometryUtils.js';
import * as OrbitControls from './vendor/OrbitControls.js';
import Stats from './vendor/stats.module.js';
import { computeBoundsTree, disposeBoundsTree, acceleratedRaycast } from './vendor/three.mesh.bvh.module.js';

import  { RandomNumberGenerator } from './randomNumberGenerator.js';
import { PointerLockControls } from './vendor/PointerLockControls.js';
import { Skydome } from './skydome.js';
import { Asset } from './asset.js';
import { Loader } from './loader.js';
import { Island } from './island.js';
import { UiController } from './UiController.js';
import { Ocean } from './ocean.js';
import { Wave } from './wave.js';
import { Player } from './player.js';
import { Cube } from './tiles/cube.js'
import { Ramp } from './tiles/ramp.js'
import { RoseInstanceController } from './resources/roseBush.js';
import { BushInstanceController } from './resources/bush.js';
import { LavenderInstanceController } from './resources/lavenderBush.js';
import { RosemaryInstanceController } from './resources/rosemaryBush.js';
import { BasilInstanceController } from './resources/basilPlant.js';
import { MintInstanceController } from './resources/mintPlant.js';
import { SageInstanceController } from './resources/sagePlant.js';
import { ThymeInstanceController } from './resources/thymePlant.js';
import { GoldenrodInstanceController } from './resources/goldenrodFlowers.js';
import { CornflowersInstanceController } from './resources/cornFlowers.js';
import { InventoryController } from './inventory.js';
import { OuterCorner } from './tiles/outerCorner.js';

var camera, renderer, controls, stats;
var thirdPersonControls, firstPersonControls;

var loader;

var lastTickTime = performance.now();

var origin = new THREE.Vector3( 0, 0, 0 );

var waitingForNextFrame = [];

var Globals = {
    
    sRand: null, //the seeded random number generator to use for all random numbers
    seed: "",

    gameSpeed: 200, //ticks per second
    solidObjects: [], //in order to raycast on this three.js needs a normal array
    interactableObjects: [],
    gridScale: 3, //size of a tile in metres
    gameObjects: new Map(),
    scene: null,
    firstPerson: false,
    firstPersonInitialised: false,
    player: null,
    paused: false,
    assets: [],
    Ui: null,
    inventory: null,

    gravity: 5.0,

    island: null,
    ocean: null,
    sun: null,

    instanceControllers: 
    {
        bush: null,
        rose: null,
        lavender: null,
        rosemary: null,

        basil: null,
        mint: null,
        sage: null,
        thyme: null,

        goldenrod: null,
        cornflowers: null
    }
}

function buildAssetList() {

    //textures
    Globals.assets.push(new Asset('tilesTexture', './models/tiles3.png', "model_texture"));
    Globals.assets.push(new Asset('tilesNormalsTexture', './models/tilesNormals3.png', "model_texture"));

    Globals.assets.push(new Asset('skydome', './sprites/skymap/grad_sd.png', THREE.Texture));
    Globals.assets.push(new Asset('cloud', './sprites/skymap/doobcloud.png', THREE.Texture));
    Globals.assets.push(new Asset('waveTexture', './sprites/wave.png', THREE.Texture));
    Globals.assets.push(new Asset('riverTexture', 'models/river.png', THREE.Texture));
    Globals.assets.push(new Asset('noise', './sprites/noise.png', THREE.Texture));

    //shaders
    Globals.assets.push(new Asset('ocean2Frag', 'shaders/ocean2.frag'));
    Globals.assets.push(new Asset('ocean2Vert', 'shaders/ocean2.vert'));
    Globals.assets.push(new Asset('grassFrag', 'shaders/grass.frag'));
    Globals.assets.push(new Asset('grassVert', 'shaders/grass.vert'));

    //models
    Globals.assets.push(new Asset('cube', 'models/cube.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('ramp', 'models/ramp.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('innerCorner', 'models/innerCorner.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('outerCorner', 'models/outerCorner.glb', "GLTF", "tile"));

    Globals.assets.push(new Asset('cubeTop', 'models/cubeTop.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('cubeTopVar1', 'models/cubeTopVar1.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('cubeTopVar2', 'models/cubeTopVar2.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('cubeTopVar3', 'models/cubeTopVar3.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('cubeTopVar4', 'models/cubeTopVar4.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('cubeSideTop', 'models/cubeSideTop.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('cubeSideMiddle', 'models/cubeSideMiddle.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('cubeSideCoast', 'models/cubeSideCoast.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('rampCoastSide1', 'models/coastRampSide1.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('rampCoastSide2', 'models/coastRampSide2.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('cubeSide', 'models/cubeSide.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('rampSide1', 'models/rampSide1.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('rampSide2', 'models/rampSide2.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('rampTop', 'models/rampTop.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('innerCornerTop', 'models/innerCornerTop.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('outerCornerTop', 'models/outerCornerTop.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('riverbedCorner', 'models/riverbedCorner2.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('riverbedRamp', 'models/riverbedRamp.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('riverbedSide', 'models/riverbedSide.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('riverbedSideCoast', 'models/riverbedSideCoast.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('riverbedStraight', 'models/riverbedStraight.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('riverbedEnd', 'models/riverbedEnd.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('riverwaterStraight', 'models/riverwaterStraight.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('riverwaterCorner', 'models/riverwaterCorner.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('riverwaterRamp', 'models/riverwaterRamp.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('riverwaterWaterfall', 'models/riverwaterWaterfall.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('sandCubeTop', 'models/sandCubeTop.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('sandCubeSide', 'models/sandSide.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('sandInnerCornerTop', 'models/sandInnerCornerTop.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('sandOuterCornerTop', 'models/sandOuterCornerTop.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('sandRampSide1', 'models/sandRampSide1.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('sandRampSide2', 'models/sandRampSide2.glb', "GLTF", "tile"));
    Globals.assets.push(new Asset('sandRampTop', 'models/sandRampTop.glb', "GLTF", "tile"));

    Globals.assets.push(new Asset('pinetree', 'models/foliage/pinetree.glb', "GLTF"));
    Globals.assets.push(new Asset('pinetree2', 'models/foliage/pinetree2.glb', "GLTF"));
    Globals.assets.push(new Asset('olivetree', 'models/foliage/oliveTree.glb', "GLTF"));
    Globals.assets.push(new Asset('grassBlade', 'models/foliage/grassBlade.glb', "GLTF"));
    Globals.assets.push(new Asset('basilPlant', 'models/foliage/basilPlant.glb', "GLTF"));
    Globals.assets.push(new Asset('mintPlant', 'models/foliage/mintPlant.glb', "GLTF"));
    Globals.assets.push(new Asset('sagePlant', 'models/foliage/sagePlant.glb', "GLTF"));
    Globals.assets.push(new Asset('thymePlant', 'models/foliage/thymePlant.glb', "GLTF"));
    Globals.assets.push(new Asset('cornflowersFlowers', 'models/foliage/cornflowersFlowers.glb', "GLTF"));
    Globals.assets.push(new Asset('goldenrodFlowers', 'models/foliage/goldenrodFlowers.glb', "GLTF"));
    Globals.assets.push(new Asset('rosemaryBush', 'models/foliage/rosemaryBush.glb', "GLTF"));
    Globals.assets.push(new Asset('lavenderBush', 'models/foliage/lavenderBush.glb', "GLTF"));
    Globals.assets.push(new Asset('roseBush', 'models/foliage/roseBush.glb', "GLTF"));
    Globals.assets.push(new Asset('bush', 'models/foliage/bush.glb', "GLTF"));

}

init();

async function init() {

    Globals.Ui = new UiController();
    Globals.Ui.initialize();

    //check we have WEBgl / GPU acceleration enabled
    const webGL = WebGL.isWebGLAvailable();
    const webGPU = WebGPU.isAvailable();

    if (!webGL && !webGPU) {
        const warning = WebGL.getWebGLErrorMessage();
        console.warn(warning.textContent);
        window.alert("Neither WebGL or WebGPU are available on your browser! The app won't run. Sorry!");
        return;
    } else if (!webGPU) {
        const warning = WebGL.getWebGLErrorMessage();
        console.warn(warning.textContent);
        window.alert("Your browser doesn't have webGPU enabled. Performance may suffer...");
    }

    // Add the bvh extension functions 
    //https://github.com/gkjohnson/three-mesh-bvh
    THREE.BufferGeometry.prototype.computeBoundsTree = computeBoundsTree;
    THREE.BufferGeometry.prototype.disposeBoundsTree = disposeBoundsTree;
    THREE.Mesh.prototype.raycast = acceleratedRaycast;

    //setup the random number generator and seed
    Globals.seed = Date.now();
    Globals.Ui.setSeedInput(Globals.seed);

    Globals.Ui.setSizeInput("small");

    //build our asset list
    buildAssetList();

    //load
    let loaderTextElement = document.getElementById( 'loader-description' );
    loader = new Loader(loaderTextElement);
    await loader.loadAssets(Globals.assets);

    //tiles don't come with texture image information, add this back in
    const tileTexture = Globals.assets.find(x => x.name === 'tilesTexture');
    const tileNormalsTexture = Globals.assets.find(x => x.name === 'tilesNormalsTexture');
    tileNormalsTexture.contents.colorSpace = ""; //not sure why this is needed, but without it normals are borked
    for (let i = 0; i < Globals.assets.length; i++) {
        if (Globals.assets[i].tag === "tile") {

            Globals.assets[i].contents.material.map = tileTexture.contents;
            Globals.assets[i].contents.material.normalMap = tileNormalsTexture.contents;

        }
    }
   
    


    //start the game ticks
    setInterval(tick.bind(this), 1000/Globals.gameSpeed);

    Globals.paused = false;

    camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.01, 20000 );

    Globals.scene = new THREE.Scene();
    Globals.scene.background = new THREE.Color( 0x96C1E1 );

    const light = new THREE.AmbientLight( 0x909090, 4 );
    Globals.scene.add( light ); 
    Globals.sun = new THREE.DirectionalLight( 0xffffff, 6 );
    Globals.sun.position.set( 75, 300, -75 );
    Globals.sun.castShadow = false;
    Globals.sun.shadow.mapSize.width = 1024;
    Globals.sun.shadow.mapSize.height = 1024;
    Globals.sun.shadow.camera.near = 100;
    Globals.sun.shadow.camera.far = 350;
    Globals.sun.shadow.camera.left = -64;
    Globals.sun.shadow.camera.bottom = -64;
    Globals.sun.shadow.camera.top = 64;
    Globals.sun.shadow.camera.right = 64;
    Globals.scene.add( Globals.sun );
    const helper = new THREE.CameraHelper( Globals.sun.shadow.camera );
    //Globals.scene.add( helper );

    renderer = new THREEGL.WebGLRenderer( { antialias: false } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight, false );
    renderer.outputColorSpace = THREE.SRGBColorSpace;
    renderer.shadowMap.enabled = true;
	renderer.shadowMap.type = THREE.PCFShadowMap;

    renderer.shadowCameraNear = 100;
    renderer.shadowCameraFar = 15000;

    document.body.appendChild( renderer.domElement );

    stats = new Stats();
    const uiElement = document.getElementById("ui");
	uiElement.appendChild( stats.dom );
    stats.dom.id = "stats";
    stats.dom.style = "";

    window.addEventListener( 'resize', onWindowResize, false );

    camera.lookAt( 0, 0, 0 );
    Globals.scene.add(camera);

    //add the inventory controller
    Globals.inventory = new InventoryController();
    Globals.inventory.initialize();

    thirdPersonControls = new OrbitControls.OrbitControls( camera, renderer.domElement );
    thirdPersonControls.listenToKeyEvents( window );
    thirdPersonControls.maxDistance = 1000;
    thirdPersonControls.minDistance = 1;
    thirdPersonControls.zoomSpeed = 0.5;
    camera.position.set( 50, 50, 50 );

    thirdPersonControls.enableDamping = true;
    thirdPersonControls.dampingFactor = 0.05;
    thirdPersonControls.update();

    controls = thirdPersonControls;

    firstPersonControls = new PointerLockControls( camera, document.body );
    let blocker = document.getElementById( 'blocker' );
    let instructions = document.getElementById( 'instructions' );

    Globals.scene.add( firstPersonControls.getObject() );
    let player = new Player( firstPersonControls );
    player.initialize();
    Globals.player = player;

    document.body.addEventListener( 'mousedown', () => {if (!Globals.paused && Globals.firstPerson) player.onMouseDown()} );
    document.body.addEventListener( 'mouseup', () => {if (!Globals.paused && Globals.firstPerson) player.onMouseUp()} );

    instructions.addEventListener( 'click', (e) => {

        if (Globals.firstPerson) {
            e.preventDefault();
            e.stopPropagation();
            firstPersonControls.lock();
        }       

    }, false );

    firstPersonControls.addEventListener( 'lock', () => {

        if (Globals.firstPerson) {
            instructions.style.display = 'none';
            blocker.style.display = 'none';
            Globals.paused = false;
            Globals.Ui.hideTopControls();
        }

    } );

    firstPersonControls.addEventListener( 'unlock', () => {

        if (Globals.firstPerson) {
            blocker.style.display = 'block';
            instructions.style.display = '';
            Globals.paused = true;
            Globals.Ui.showTopControls();
        }

    } );

    firstPersonControls.addEventListener( 'change', onPosChange.bind(this) );
    thirdPersonControls.addEventListener( 'change', onPosChange.bind(this) );


    //run a single game tick to make sure everthing is all good before we render for the first time
    tick();
    Globals.paused = false;

    animate();

    const loaderElement = document.getElementById( 'loader' );
    loaderElement.style.display = 'none';
    const blockerElement = document.getElementById( 'blocker' );
    blockerElement.style.display = 'none';

    //add sky
    const sceneSize = 1500 * Globals.gridScale;
    let skydome = new Skydome(sceneSize);
    skydome.initialize();

    //add ocean
    Globals.ocean = new Ocean( sceneSize );
    Globals.ocean.initialize();
    Globals.ocean.pivot.position.y += 0.1; //avoid z fighting and make the water a little off being perfectly gridded

    BuildInstanceControllers();







    //test objects


    const fullAsset = Globals.assets.find(x => x.name === 'cube');
    const topAsset = Globals.assets.find(x => x.name === 'cubeTop');
    const sideAsset = Globals.assets.find(x => x.name === 'cubeSide');

    const object = fullAsset.contents.clone();

    const matrix = new THREE.Matrix4();
    matrix.makeTranslation( 10, 1, 5.3 );
    
    const geo1 = topAsset.contents.geometry.clone();
    const geo2 = sideAsset.contents.geometry.clone().rotateY(Math.PI / 2);
    const geo3 = sideAsset.contents.geometry.clone().rotateY(-Math.PI / 2);
    const geo4 = sideAsset.contents.geometry.clone().rotateY(0);
    const geo5 = sideAsset.contents.geometry.clone().rotateY(Math.PI);

    geo1.applyMatrix4( matrix );
    geo2.applyMatrix4( matrix );
    geo3.applyMatrix4( matrix );
    geo4.applyMatrix4( matrix );
    geo5.applyMatrix4( matrix );

    matrix.makeRotationZ( Math.PI/2 );

    geo1.applyMatrix4( matrix );
    geo2.applyMatrix4( matrix );
    geo3.applyMatrix4( matrix );
    geo4.applyMatrix4( matrix );
    geo5.applyMatrix4( matrix );

    const geometry = BufferGeometryUtils.mergeBufferGeometries( [geo1, geo2, geo3, geo4, geo5] );
    geometry.computeBoundingSphere();
    //const material = new THREE.MeshBasicMaterial({color: 0xffffff});
    const mesh = new THREE.Mesh( geometry, object.material );
    //Globals.scene.add( mesh );


    const rtopAsset = Globals.assets.find(x => x.name === 'rampTop');
    const rsideAsset1 = Globals.assets.find(x => x.name === 'rampSide1');
    const rsideAsset2 = Globals.assets.find(x => x.name === 'rampSide2');
    const rbackSideAsset = Globals.assets.find(x => x.name === 'cubeSide');

    const rtopFaceGeo = rtopAsset.contents.geometry.clone();
    const rxPosFaceGeo = rsideAsset1.contents.geometry.clone();
    const rxNegFaceGeo = rsideAsset2.contents.geometry.clone();
    const ryPosFaceGeo = rbackSideAsset.contents.geometry.clone();

    ryPosFaceGeo.rotateY(Math.PI);

    const matrix2 = new THREE.Matrix4();

    matrix2.makeRotationY( Math.PI );

    rtopFaceGeo.applyMatrix4( matrix2 );
    rxPosFaceGeo.applyMatrix4( matrix2 );
    rxNegFaceGeo.applyMatrix4( matrix2 );
    ryPosFaceGeo.applyMatrix4( matrix2 );

    matrix2.identity();
    matrix2.makeTranslation( 5, 1, 1.5 );

    rtopFaceGeo.applyMatrix4( matrix2 );
    rxPosFaceGeo.applyMatrix4( matrix2 );
    rxNegFaceGeo.applyMatrix4( matrix2 );
    ryPosFaceGeo.applyMatrix4( matrix2 );

    const geometry2 = BufferGeometryUtils.mergeBufferGeometries( [rtopFaceGeo, rxPosFaceGeo, rxNegFaceGeo, ryPosFaceGeo] );
    geometry2.computeBoundingSphere();
    const mesh2 = new THREE.Mesh( geometry2, object.material );
    //Globals.scene.add( mesh2 );




    const riverTop = Globals.assets.find(x => x.name === 'riverbedCorner');
    const riverSide1 = Globals.assets.find(x => x.name === 'cubeSideTop');
    const riverSide2 = Globals.assets.find(x => x.name === 'riverbedSide');

    const riverTopGeo = riverTop.contents.geometry.clone();
    const riverSide1Geo = riverSide1.contents.geometry.clone();
    const riverSide2Geo = riverSide1.contents.geometry.clone();
    const riverSide3Geo = riverSide2.contents.geometry.clone();
    const riverSide4Geo = riverSide2.contents.geometry.clone();

    riverTopGeo.rotateY(Math.PI);
    riverSide1Geo.rotateY(Math.PI);
    riverSide2Geo.rotateY(-Math.PI / 2);
    riverSide3Geo.rotateY(0);
    riverSide4Geo.rotateY(Math.PI / 2);

    const matrix3 = new THREE.Matrix4();

    matrix3.makeRotationY( Math.PI );

    riverTopGeo.applyMatrix4( matrix3 );
    riverSide1Geo.applyMatrix4( matrix3 );
    riverSide2Geo.applyMatrix4( matrix3 );
    riverSide3Geo.applyMatrix4( matrix3 );
    riverSide4Geo.applyMatrix4( matrix3 );

    const geometry3 = BufferGeometryUtils.mergeBufferGeometries( [riverTopGeo, riverSide1Geo, riverSide2Geo, riverSide3Geo, riverSide4Geo] );
    geometry3.computeBoundingSphere();
    const mesh3 = new THREE.Mesh( geometry3, object.material );
    //Globals.scene.add( mesh3 );



    // const v1 = Globals.ocean.pivot.position.clone();
    // Globals.sRand = new RandomNumberGenerator("test only");
    // const testwave = new Wave(v1, 0);
    // //testwave.speed = 0.8;
    // testwave.initialize(Globals.ocean.pivot);
    // testwave._animationProgress = 0;


    // const cgeo = new THREE.BoxGeometry( 1, 1, 1 ); 
    // const cmat = new THREE.MeshPhongMaterial( {color: 0x00ff00} ); 
    // const cube = new THREE.Mesh( cgeo, cmat ); 
    // cube.position.set(0, 4, 0);
    // Globals.scene.add( cube );

    // const testCube = new Cube( 0, 0, 0);
    // testCube.initialize( new THREE.Vector3(0, 0, 0) );
    // Globals.scene.add(testCube.pivot);
    //testCube.generateAndAddTree();



    //direction arrows
    const origin = new THREE.Vector3( 0, 20, 0 );
    const length = 10;
    const xhex = 0xff00ff;
    const yhex = 0xffff00;

    const xdir = new THREE.Vector3( 1, 0, 0 );
    xdir.normalize();

    const ydir = new THREE.Vector3( 0, 0, 1 );
    ydir.normalize();
    
    const xarrowHelper = new THREE.ArrowHelper( xdir, origin, length, xhex );
    Globals.scene.add( xarrowHelper );

    const yarrowHelper = new THREE.ArrowHelper( ydir, origin, length, yhex );
    Globals.scene.add( yarrowHelper );

}

function GenerateIsland( fast ) {

    InitialiseIsland();

    Globals.firstPersonInitialised = false;
    Globals.seed = Globals.Ui.getSeedFromInput();
    Globals.sRand = new RandomNumberGenerator(Globals.seed);
    Globals.Ui.setAndLockSeedInput(Globals.seed);
    Globals.Ui.lockSizeInput();
    Globals.island.generateIsland( fast );
}

function InitialiseIsland() {

    const size = Globals.Ui.getSizeFromInput();

    Globals.island = new Island();
    Globals.island.initialize( size );

    const islandCentre = Globals.island.getWorldCentre();
    Globals.island.pivot.position.x = -islandCentre.x;
    Globals.island.pivot.position.z = -islandCentre.z;
}

function ClearIsland() {
    Globals.firstPersonInitialised = false;
    SwitchToThirdPerson();
    if (Globals.island) {
        Globals.island.clearIsland();
    }
    Globals.Ui.disableFirstPersonButton();
    Globals.Ui.enableGenerationButtons();
    Globals.Ui.disableClearButton();
    Globals.Ui.unlockSeedInput();
    Globals.Ui.unlockSizeInput();
    Globals.Ui.disableSaveButton();
    Globals.Ui.setStageText("idle");
}

function BuildInstanceControllers() {
    Globals.instanceControllers.bush = new BushInstanceController();
    Globals.instanceControllers.rose = new RoseInstanceController();
    Globals.instanceControllers.lavender = new LavenderInstanceController();
    Globals.instanceControllers.rosemary = new RosemaryInstanceController();

    Globals.instanceControllers.basil = new BasilInstanceController();
    Globals.instanceControllers.mint = new MintInstanceController();
    Globals.instanceControllers.sage = new SageInstanceController();
    Globals.instanceControllers.thyme = new ThymeInstanceController();

    Globals.instanceControllers.goldenrod = new GoldenrodInstanceController();
    Globals.instanceControllers.cornflowers = new CornflowersInstanceController();
}

//TODO make async
//https://stackoverflow.com/a/65939108
function Save() {
    //const decyledData = JSON.decycle(Globals.island.theGrid);
    

    //serialisation has to be done manually in a few steps:
    let data = { island: null };

    //first the island has to be saved
    const islandDataString = Globals.island.serialiseIsland();
    data.island = JSON.parse(islandDataString);

    //after the island is saved, other things like player position can be saved......

    //all these bits are added together to make the final file which is saved out

    const blob = new Blob([JSON.stringify(data)], { type: "text/json" });

    const link = document.createElement("a");
    link.download = "save.json";
    link.href = window.URL.createObjectURL(blob);
    link.dataset.downloadurl = ["text/json", link.download, link.href].join(":");

    const evt = new MouseEvent("click", {
        view: window,
        bubbles: true,
        cancelable: true,
    });

    link.dispatchEvent(evt);
    link.remove();
}

//TODO make async
function Load(file) {
    if (file)
    {
        const reader = new FileReader();
        reader.addEventListener("loadend", () => {
            let result = reader.result;
            result = JSON.parse(result);
            //result = JSON.retrocycle(result);
            console.log("Loaded file");

            ClearIsland();
            InitialiseIsland();

            Globals.island.deserialiseAndLoadIsland(result.island);
            Globals.Ui.clearLoadInput();
        });
        reader.readAsText(file);
    }
}

function SwitchToFirstPerson() {

    if (!Globals.firstPerson) {

        if (!Globals.firstPersonInitialised) {
            firstPersonControls.getObject().position.x = 0;
            firstPersonControls.getObject().position.y = 20;    
            firstPersonControls.getObject().position.z = 0;
            firstPersonControls.getObject().lookAt( new THREE.Vector3(0, 20, 0) );
            Globals.firstPersonInitialised = true;
        } else {
            firstPersonControls.getObject().position.copy(Globals.player.pivot.position);
            firstPersonControls.getObject().quaternion.copy(Globals.player.pivot.quaternion);
        }

        let blocker = document.getElementById( 'blocker' );
        let instructions = document.getElementById( 'instructions' );
        blocker.style.display = 'block';
        instructions.style.removeProperty('display');
        
        firstPersonControls.enabled = true;
        thirdPersonControls.enabled = false;
        Globals.Ui.disableFirstPersonButton();
        Globals.Ui.enableThirdPersonButton();

        Globals.firstPerson = true;

        Globals.Ui.showInventory();
    }
    
}

function SwitchToThirdPerson() {

    if (Globals.firstPerson) {

        let blocker = document.getElementById( 'blocker' );
        let instructions = document.getElementById( 'instructions' );
        instructions.style.display = 'none';
        blocker.style.display = 'none';
        Globals.paused = false;
        
        firstPersonControls.enabled = false;
        thirdPersonControls.enabled = true;        
        Globals.Ui.enableFirstPersonButton();
        Globals.Ui.disableThirdPersonButton();

        camera.position.set( 50, 50, 50 );
        thirdPersonControls.update();

        Globals.firstPerson = false;

        Globals.Ui.hideInventory();
    }

}

function onPosChange() {
    //this will be fired all the time...
    let pos;
    if (Globals.firstPerson) {
        pos = firstPersonControls.getObject().position;
    } else {
        pos = thirdPersonControls.object.position;
    }
    if (pos.y < 10) {
        Globals.ocean.renderWaterDepth = true;
    } else {
        Globals.ocean.renderWaterDepth = false;
    }
}

function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight, false );

}

function tick() {

    const time = performance.now();
    const delta = ( time - lastTickTime ) / 1000;

    Globals.gameObjects.forEach(object => {
        object.onGameTickAlways(delta);
    });

    lastTickTime = time;
}

function animate() {

    stats.update();
    if (!Globals.firstPerson) {
        controls.update();
    }
    render();

    requestAnimationFrame( animate );

}

function render() {
    if (waitingForNextFrame.length > 0) {
        for (let i = 0; i < waitingForNextFrame.length; i++) {
            waitingForNextFrame[i].resolve();
        }
        waitingForNextFrame = [];
    }
    renderer.render( Globals.scene, camera );
}

function AwaitNextFrame() {
    let promise = new Promise((resolve, reject) => {
        waitingForNextFrame.push({resolve: resolve, reject: reject});
    });
    return promise;
}


export { Globals, AwaitNextFrame, SwitchToFirstPerson, SwitchToThirdPerson, GenerateIsland, ClearIsland, Save, Load, BuildInstanceControllers };