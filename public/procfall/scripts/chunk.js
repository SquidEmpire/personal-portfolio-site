import * as GAME from './game.js';
import { Tree } from './tree.js';
import { perlin2Noise } from './vendor/noise.js';
import * as THREE from './vendor/three.module.js';

class Chunk {

    pivot;
    plane;
    size = 0;
    offsetX = 0;
    offsetY = 0;

    trees = [];
    maxTrees = 200;

    _geometry;
    _xOffset;
    _zOffset;

    constructor(size, offsetX, offsetY) {
        //TODO injest size here so it can be set relative to global max render view size
        this.pivot = new THREE.Group();
        this.size = size;
        this.offsetX = offsetX;
        this.offsetY = offsetY;

        for (let i = 0; i < this.maxTrees; i++) {
            const tree = new Tree(this);
            this.trees[i] = tree;
            tree.initialise(Math.round(Math.random() * 6) + 1);
        }
    }

    updateTerrain() {

        let vertices = this._geometry.attributes.position.array;

        this._xOffset = this.offsetX * this.size;
        this._zOffset = this.offsetY * this.size;

        const treev = new THREE.Vector3();
        let treeI = 0;

        for (let i = 1; i < vertices.length; i += 3) {
            const h = perlin2Noise( vertices[i - 1] + this._xOffset, vertices[i + 1] + this._zOffset );
            vertices[i] = h * 6;

            if (h % 10) {
                treev.set(vertices[i - 1], vertices[i], vertices[i + 1]);
                this.trees[treeI % this.maxTrees].place(treev);
                treeI++;
            }
        }

        this._geometry.attributes.position.needsUpdate = true;
        this._geometry.computeVertexNormals();

        

    }

    initialise() {
        const halfsize = this.size * 0.08;
        this._geometry = new THREE.PlaneGeometry( this.size, this.size, halfsize - 1, halfsize - 1 );
        this._geometry.rotateX(- Math.PI/2);
        
        this.updateTerrain();

        const texture = GAME.Globals.assets.find(x => x.name === 'snow').contents;
        texture.wrapS = THREE.RepeatWrapping;
        texture.wrapT = THREE.RepeatWrapping;
        texture.repeat.set(this.size * 0.2, this.size * 0.2);

        const material = new THREE.MeshStandardMaterial( {map: texture, side: THREE.FrontSide} );
        this.plane = new THREE.Mesh( this._geometry, material );
        this.plane.castShadow = true;
        this.plane.receiveShadow = true;

        this.pivot.add(this.plane);
        this.pivot.position.x = this._xOffset;
        this.pivot.position.z = this._zOffset;

        GAME.Globals.scene.add( this.pivot );
        GAME.Globals.solidObjects.push(this.plane);
    }

    move(offsetX, offsetY) {
        this.offsetX = offsetX;
        this.offsetY = offsetY;

        this.updateTerrain();

        this.pivot.position.x = this._xOffset;
        this.pivot.position.z = this._zOffset;
    }

    distanceToPlayer()
    {
        //TODO use distance to squared
        //the pivot is at the center
        return this.pivot.position.distanceTo(GAME.Globals.player.pivot.position);
    }

}

export {Chunk}