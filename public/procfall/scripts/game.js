import * as THREE from './vendor/three.module.js';
import Stats from './vendor/stats.module.js';
import { PointerLockControls } from './vendor/PointerLockControls.js';
import { Asset } from './asset.js';
import { Loader } from './loader.js';
import { Player } from './player.js';
import { Chunk } from './chunk.js';

var loader, camera, renderer, controls, stats, crank;

var lastTickTime = performance.now();

const MAX_DIST = 1000;
const CHUNK_SIZE = 200;

var Globals = {
    gameSpeed: 200, //ticks per second
    solidObjects: [], //in order to raycast on this three.js needs a normal array
    gameObjects: new Map(),
    scene: null,
    sky: null,
    chunks: [],
    player: null,
    playerChunkOffset: {x: 0, y: 0},
    paused: false,
    assets: [],

    gravity: 5.0,
}

function buildAssetList() {

    //textures
    Globals.assets.push(new Asset('sky', './sprites/skybox/31.png', THREE.Texture));
    Globals.assets.push(new Asset('snow', './sprites/1-0.png', THREE.Texture));
    Globals.assets.push(new Asset('tree1', './sprites/trees/2-0.png', THREE.Texture));
    Globals.assets.push(new Asset('tree2', './sprites/trees/5-0.png', THREE.Texture));
    Globals.assets.push(new Asset('tree3', './sprites/trees/11-0.png', THREE.Texture));
    Globals.assets.push(new Asset('tree4', './sprites/trees/13-0.png', THREE.Texture));
    Globals.assets.push(new Asset('tree5', './sprites/trees/15-0.png', THREE.Texture));
    Globals.assets.push(new Asset('tree6', './sprites/trees/24-0.png', THREE.Texture));
    Globals.assets.push(new Asset('tree7', './sprites/trees/25-0.png', THREE.Texture));

}

init();

async function init() {

    //start the game ticks
    setInterval(tick.bind(this), 1000/Globals.gameSpeed);

    Globals.paused = true;

    //build our asset list
    buildAssetList();

    //load
    let loaderTextElement = document.getElementById( 'loader-description' );
    loader = new Loader(loaderTextElement);
    await loader.loadAssets(Globals.assets);


    //scene setup

    Globals.scene = new THREE.Scene();
    Globals.scene.background = new THREE.Color( 0xA8A9AD );
    Globals.scene.fog = new THREE.Fog( 0xA8A9AD, 10, CHUNK_SIZE );

    //let skybox = Globals.assets.find(x => x.name === 'skybox').contents;
    //Globals.scene.background = skybox;


    //player setup
    
    camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.01, MAX_DIST );
    camera.lookAt( 0, 0, 0 );
    Globals.scene.add(camera);

    controls = new PointerLockControls( camera, document.body );

    let blocker = document.getElementById( 'blocker' );
    let instructions = document.getElementById( 'instructions' );
    blocker.style.display = 'block';
    instructions.style.removeProperty('display');

    instructions.addEventListener( 'click', (e) => {

        e.preventDefault();
        e.stopPropagation();
        controls.lock();

    }, false );

    controls.addEventListener( 'lock', () => {

        instructions.style.display = 'none';
        blocker.style.display = 'none';
        Globals.paused = false;

    } );

    controls.addEventListener( 'unlock', () => {

        blocker.style.display = 'block';
        instructions.style.display = '';
        Globals.paused = true;

    } );   

    Globals.scene.add( controls.getObject() );

    let player = new Player( controls );
    player.initialize();
    Globals.player = player;

    document.body.addEventListener( 'mousedown', () => {if (!Globals.paused) player.onMouseDown()} );
    document.body.addEventListener( 'mouseup', () => {if (!Globals.paused) player.onMouseUp()} );
    
    //move the player
    controls.getObject().position.x = 0;
    controls.getObject().position.y = 6.8;
    controls.getObject().position.z = 0;
    controls.getObject().lookAt( new THREE.Vector3(0, 6.8, 0) );


    //rest of scene setup

    // const m = new THREE.MeshStandardMaterial( {
    //     color: (Math.random()*0xFFFFFF<<0),
    // } );
    // const height = Math.random() * 2 + 0.5;
    // const g = new THREE.BoxGeometry( 2, height, 2 );

    // const object = new THREE.Mesh( g, m );
    // object.castShadow = true;
    // object.receiveShadow = true;
    // Globals.scene.add(object);


    const light = new THREE.AmbientLight( 0x909090, 5 );
    Globals.scene.add( light ); 
    const directionalLight = new THREE.DirectionalLight( 0xffffff, 2 );
    Globals.scene.add( directionalLight );
    directionalLight.target.position.x = -10;
    Globals.scene.add( directionalLight.target );

    renderer = new THREE.WebGLRenderer( { antialias: false } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight, false );
    renderer.outputColorSpace = THREE.SRGBColorSpace;
    renderer.shadowMap.enabled = true;
	renderer.shadowMap.type = THREE.PCFSoftShadowMap;

    renderer.shadowCameraNear = 100;
    renderer.shadowCameraFar = 15000;

    document.body.appendChild( renderer.domElement );

    stats = new Stats();
    const topControls = document.getElementById("top-controls");
	topControls.appendChild( stats.dom );
    stats.dom.id = "stats";
    stats.dom.style = "";

    window.addEventListener( 'resize', onWindowResize, false );

    animate();

    const loaderElement = document.getElementById( 'loader' );
    loaderElement.style.display = 'none';
    //const blockerElement = document.getElementById( 'blocker' );
    //blockerElement.style.display = 'none';


    //add sky
    let skyGeo = new THREE.SphereGeometry(MAX_DIST, 25, 25);
    let skytexture = Globals.assets.find(x => x.name === 'sky').contents;
    let material = new THREE.MeshBasicMaterial({ 
        map: skytexture,
        fog: false
    });
    Globals.sky = new THREE.Mesh(skyGeo, material);
    Globals.sky.material.side = THREE.BackSide;
    Globals.scene.add(Globals.sky);
    
    //add 9 chunks around the player
    Globals.playerChunkOffset.x = 0;
    Globals.playerChunkOffset.y = 0;
    // const chunk = new Chunk(CHUNK_SIZE, 0,0);
    //         chunk.initialise();
    //         Globals.chunks[0] = chunk;
    let k = 0;
    for (let i = -1; i < 2; i++) {
        for (let j = -1; j < 2; j++) {
            const chunk = new Chunk(CHUNK_SIZE, i, j);
            chunk.initialise();
            Globals.chunks[k] = chunk;
            k++;
        }
    }    

    render();
}



function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight, false );

}


function posToChunkspaceOffset(pos) {
    const x = Math.round(pos.x / CHUNK_SIZE);
    const y = Math.round(pos.z / CHUNK_SIZE);
    return {x: x, y: y};
}


function tick() {

    const time = performance.now();
    const delta = ( time - lastTickTime ) / 1000;

    Globals.gameObjects.forEach(object => {
        object.onGameTickAlways(delta);
    });

    if(Globals.sky) {
        Globals.sky.position.copy(Globals.player.pivot.position);
    }

    //temp
    if (!Globals.paused) {
        //check for whether we need to move a chunk
        if (Globals.player) {
            const newOffset = posToChunkspaceOffset(Globals.player.pivot.position);
            if (Globals.playerChunkOffset.x !== newOffset.x || Globals.playerChunkOffset.y !== newOffset.y) {
                Globals.playerChunkOffset = newOffset;
            }
        }
        

        for (let i = 0; i < Globals.chunks.length; i++) {
            const d = Globals.chunks[i].distanceToPlayer();
            if (d > CHUNK_SIZE * 2) {
                //move this chunk to the nearest open spot

                //get all empty spots
                const occupiedSpots = [];
                const spots = [];
                for (let x = -1; x < 2; x++) {
                    for (let y = -1; y < 2; y++) {
                        const offsetx = Globals.playerChunkOffset.x + x;
                        const offsety = Globals.playerChunkOffset.y + y;

                        for (let j = 0; j < Globals.chunks.length; j++) {
                            if (Globals.chunks[j].offsetX == offsetx &&
                                Globals.chunks[j].offsetY == offsety) {

                                occupiedSpots.push({x: offsetx, y: offsety});
                            }
                        }
                        spots.push({x: offsetx, y: offsety});
                    }
                }
                const emptySpots = spots.filter((spot) => {
                    for (let i = 0; i < occupiedSpots.length; i++) {
                        if (occupiedSpots[i].x == spot.x && occupiedSpots[i].y == spot.y) {
                            return false;
                        }
                    }
                    return true;
                });

                //find the nearest empty spot
                let closestSpot = null;
                for (let i = 0; i < emptySpots.length; i++) {
                    if (closestSpot == null || TEMPdistanceTo(emptySpots[i], Globals.playerChunkOffset) < TEMPdistanceTo(closestSpot, Globals.playerChunkOffset)) {
                        closestSpot = emptySpots[i];
                    }
                }

                //move the chunk
                if (closestSpot !== null) {
                    Globals.chunks[i].move(closestSpot.x, closestSpot.y);
                } else {
                    console.warn("Failed to move chunk!");
                }
                

                //only do one at a time
                break;
            }
        }
    }

    lastTickTime = time;
}

function TEMPdistanceTo( t1, t2 ) {
    const dx = t1.x - t2.x, dy = t1.y - t2.y;
    return /*Math.sqrt*/(dx * dx + dy * dy);
}

function animate() {

    stats.update();
    render();
    requestAnimationFrame( animate );

}

function render() {    
    renderer.render( Globals.scene, camera );
}

export { Globals };