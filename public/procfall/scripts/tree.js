import * as GAME from './game.js';
import * as THREE from './vendor/three.module.js';

class Tree {

    pivot;
    parent;
    size = 5;

    constructor( parent ) {
        this.pivot = new THREE.Group();
        this.parent = parent;
    }

    initialise( type ) {

        const texture = GAME.Globals.assets.find(x => x.name === `tree${type}`).contents;
        const material = new THREE.SpriteMaterial( { map: texture } );

        const sprite = new THREE.Sprite( material );
        sprite.scale.set( this.size, this.size, 1 );
        this.pivot.add(sprite);

        this.parent.pivot.add( this.pivot );
    }

    place ( pos ) {
        this.pivot.position.copy(pos);
        this.pivot.position.y += this.size * 0.5;
    }

}

export {Tree}