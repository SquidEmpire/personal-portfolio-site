/**
	 * @author Mugen87 / https://github.com/Mugen87
	 * @author Niklas Knaack / http://niklasknaack.de
	 *
	 * Creates a tube which extrudes along a 3d spline.
	 *
	 */

 import * as THREE from './three.module.js';

 function TentacleBufferGeometry( path, tubularSegments, radiusStart, radiusEnd, radialSegments, closed ) {

    THREE.BufferGeometry.call( this );

    this.type = 'TentacleBufferGeometry';

    this.parameters = {
        path: path,
        tubularSegments: tubularSegments,
        radiusStart: radiusStart,
        radiusEnd: radiusEnd,
        radialSegments: radialSegments,
        closed: closed
    };

    tubularSegments = tubularSegments || 64;
    radiusStart = radiusStart || 1;
    radiusEnd = radiusEnd || 1;
    radialSegments = radialSegments || 8;
    closed = closed || false;

    var frames = path.computeFrenetFrames( tubularSegments, closed );

    // expose internals

    this.tangents = frames.tangents;
    this.normals = frames.normals;
    this.binormals = frames.binormals;

    // helper variables

    var radiusMin = Math.min( radiusStart, radiusEnd );
    var radiusDiff = Math.abs( radiusStart - radiusEnd );

    var vertex = new THREE.Vector3();
    var normal = new THREE.Vector3();
    var uv = new THREE.Vector2();

    var i, j;

    // buffer

    var vertices = [];
    var normals = [];
    var uvs = [];
    var indices = [];

    // create buffer data

    generateBufferData();

    // build geometry

    this.setIndex( ( indices.length > 65535 ? new THREE.Uint32BufferAttribute( indices, 1 ) : new THREE.Uint16BufferAttribute( indices, 1 ) ) );
    this.setAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );
    this.setAttribute( 'normal', new THREE.Float32BufferAttribute( normals, 3 ) );
    this.setAttribute( 'uv', new THREE.Float32BufferAttribute( uvs, 2 ) );

    // functions

    function generateBufferData() {

        for ( i = 0; i < tubularSegments; i ++ ) {

            generateSegment( i );

        }

        // if the geometry is not closed, generate the last row of vertices and normals
        // at the regular position on the given path
        //
        // if the geometry is closed, duplicate the first row of vertices and normals (uvs will differ)

        generateSegment( ( closed === false ) ? tubularSegments : 0 );

        // uvs are generated in a separate function.
        // this makes it easy compute correct values for closed geometries

        generateUVs();

        // finally create faces

        generateIndices();

    }

    function generateSegment( i ) {

        // calc radius

        var radius;// = i * ( Math.abs( radiusStart - radiusEnd ) / tubularSegments );

        if ( radiusStart === radiusEnd ) {

            radius = radiusStart;

        } else {

            radius = ( tubularSegments - i ) * radiusDiff / tubularSegments + radiusMin; 

        }

        var radius = radiusDiff ? ( tubularSegments - i ) * radiusDiff / tubularSegments + radiusMin : radiusStart;

        // we use getPointAt to sample evenly distributed points from the given path

        var P = path.getPointAt( i / tubularSegments );

        // retrieve corresponding normal and binormal

        var N = frames.normals[ i ];
        var B = frames.binormals[ i ];

        // generate normals and vertices for the current segment

        for ( j = 0; j <= radialSegments; j ++ ) {

            var v = j / radialSegments * Math.PI * 2;

            var sin =   Math.sin( v );
            var cos = - Math.cos( v );

            // normal

            normal.x = ( cos * N.x + sin * B.x );
            normal.y = ( cos * N.y + sin * B.y );
            normal.z = ( cos * N.z + sin * B.z );
            normal.normalize();

            normals.push( normal.x, normal.y, normal.z );

            // vertex

            vertex.x = P.x + radius * normal.x;
            vertex.y = P.y + radius * normal.y;
            vertex.z = P.z + radius * normal.z;

            vertices.push( vertex.x, vertex.y, vertex.z );

        }

    }

    function generateIndices() {

        for ( j = 1; j <= tubularSegments; j ++ ) {

            for ( i = 1; i <= radialSegments; i ++ ) {

                var a = ( radialSegments + 1 ) * ( j - 1 ) + ( i - 1 );
                var b = ( radialSegments + 1 ) * j + ( i - 1 );
                var c = ( radialSegments + 1 ) * j + i;
                var d = ( radialSegments + 1 ) * ( j - 1 ) + i;

                // faces

                indices.push( a, b, d );
                indices.push( b, c, d );

            }

        }

    }

    function generateUVs() {

        for ( i = 0; i <= tubularSegments; i ++ ) {

            for ( j = 0; j <= radialSegments; j ++ ) {

                uv.x = i / tubularSegments;
                uv.y = j / radialSegments;

                uvs.push( uv.x, uv.y );

            }

        }

    }

}

TentacleBufferGeometry.prototype = Object.create( THREE.BufferGeometry.prototype );
TentacleBufferGeometry.prototype.constructor = TentacleBufferGeometry;

/**
 * @author oosmoxiecode / https://github.com/oosmoxiecode
 * @author WestLangley / https://github.com/WestLangley
 * @author zz85 / https://github.com/zz85
 * @author miningold / https://github.com/miningold
 * @author jonobr1 / https://github.com/jonobr1
 * @author Niklas Knaack / http://niklasknaack.de
 *
 * Creates a tube which extrudes along a 3d spline.
 */

function TentacleGeometry( path, tubularSegments, radiusStart, radiusEnd, radialSegments, closed ) {

    THREE.Geometry.call( this );

    this.type = 'TentacleGeometry';

    this.parameters = {
        path: path,
        tubularSegments: tubularSegments,
        radiusStart: radiusStart,
        radiusEnd: radiusEnd,
        radialSegments: radialSegments,
        closed: closed
    };

    var bufferGeometry = new TentacleBufferGeometry( path, tubularSegments, radiusStart, radiusEnd, radialSegments, closed );

    // expose internals

    this.tangents = bufferGeometry.tangents;
    this.normals = bufferGeometry.normals;
    this.binormals = bufferGeometry.binormals;

    // create geometry

    this.fromBufferGeometry( bufferGeometry );
    this.mergeVertices();

}

TentacleGeometry.prototype = Object.create( THREE.Geometry.prototype );
TentacleGeometry.prototype.constructor = TentacleGeometry;

export { TentacleBufferGeometry, TentacleGeometry };