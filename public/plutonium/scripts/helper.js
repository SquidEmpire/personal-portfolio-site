//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
var getRandomInt = function(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.round(Math.random() * (max - min)) + min;
}

var getRandom = function(min, max) {
    return (Math.random() * (max - min)) + min;
}

var getRandomBool = function() {
    return Math.random() > 0.5;
}

//https://stackoverflow.com/a/2117523
var getUUID = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

export { getRandom, getRandomInt, getRandomBool, getUUID };