import * as THREE from './vendor/three.module.js';
import * as GAME from './game.js';

class Skybox {

    constructor() {
        this.cubeTexture = GAME.Globals.assets.find(x => x.name === 'skybox');
    }

    initialize() {
        let texture = this.cubeTexture.contents;
        GAME.Globals.scene.background = texture;        
    }

};

export { Skybox };