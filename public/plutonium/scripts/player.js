import * as THREE from './vendor/three.module.js';
import * as GAME from './game.js';
import * as HELPER from './helper.js';
import { GameObject } from './gameObject.js';
import { Pistol } from './pistol.js';

class Player extends GameObject {    
    // global
    controls;

    //internal
    body;
    head;
    _pivot;
    _downRaycaster;
    _motionRaycaster;
    _motionRaycaster2;
    _interactionRaycaster;
    
    _motionRaycasterDebugArrow1;
    _motionRaycasterDebugArrow2;

    //state
    moveForward = false;
    moveBackward = false;
    moveLeft = false;
    moveRight = false;
    canJump = false;
    torchOn = false;
    mousedown = false;
    
    velocity = new THREE.Vector3();
    direction = new THREE.Vector3();    

    item;
    torch;

    //settings
    mass = 100;
    height = 10;
    headSize = 3;
    jumpSpeed = 100;
    slopeClimbFactor = 1;
    offsetFromCamera = new THREE.Vector3(0, 0, GAME.Globals.debug ? -15 : 0);
    terminalVelocity = -300;

    TEMP_lighthousePosition = new THREE.Vector3(-20, 76, 20);

    constructor( controls ) {

        super();

        this.pivot = new THREE.Group();

        this.controls = controls;

        document.addEventListener( 'keydown', this.onKeyDown.bind(this), false );
        document.addEventListener( 'keyup', this.onKeyUp.bind(this), false );

        document.addEventListener( 'mousedown', this.onMouseDown.bind(this), false );
        document.addEventListener( 'mouseup', this.onMouseUp.bind(this), false );

        this._downRaycaster = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3(), 0 );
        this._motionRaycaster = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3(), 0, 8 );
        this._motionRaycaster2 = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3(), 0, 8 );
        this._interactionRaycaster = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3(), 0, this.height );

        let bodyGeometry = new THREE.BoxGeometry( 4, this.height*2, 1 );
        let headGeometry = new THREE.BoxGeometry( this.headSize, this.headSize, this.headSize );
        let bodyMaterial = new THREE.MeshStandardMaterial({emissive: 0xffaa00});
        let headMaterial = new THREE.MeshStandardMaterial({emissive: 0xff6600});
    
        this.body = new THREE.Mesh( bodyGeometry, bodyMaterial );
        this.head = new THREE.Mesh( headGeometry, headMaterial );

        //controls change angle on mouse move which can happen many times per render so we need to reposition every time the controls change
        this.controls.addEventListener( 'change', this.repositionPlayer.bind(this) );

    }

    initialize() {
        this.torch = new THREE.SpotLight( 0xFFE6C9 );
        this.torch.castShadow = true;

        this.torch.shadow.mapSize.width = 512;
        this.torch.shadow.mapSize.height = 512;

        this.torch.castShadow = true;
        this.torch.shadow.mapSize.width = 1024;
        this.torch.shadow.mapSize.height = 1024;
        this.torch.shadow.camera.near = 1;
        this.torch.shadow.camera.far = 200;
        this.torch.shadow.radius = 1;

        this.torch.penumbra = 0.1;
        this.torch.decay = 2;
        this.torch.angle = 0.6;
        this.torch.distance = 100;

        this.head.add(this.torch);
        this.torch.position.set(0, 0, 0);
        this.head.add(this.torch.target);
        this.torch.target.position.set(0, -1, -10);

        this.torch.visible = this.torchOn;

        if (GAME.Globals.debug) {
            let lightHelper = new THREE.SpotLightHelper( this.torch );
            GAME.Globals.scene.add( lightHelper );

            this._motionRaycasterDebugArrow1 = new THREE.ArrowHelper( this._motionRaycaster.ray.direction, this._motionRaycaster.ray.origin, 100, 0xffaaaa );
            this._motionRaycasterDebugArrow2 = new THREE.ArrowHelper( this._motionRaycaster.ray.direction, this._motionRaycaster.ray.origin, 100, 0xaaffaa );
            GAME.Globals.scene.add(this._motionRaycasterDebugArrow1);
            GAME.Globals.scene.add(this._motionRaycasterDebugArrow2);
        }

        //equip a pistol
        this.item = new Pistol( this.controls );
        this.item.initialize();

        this.body.add( this.head );
        this.head.position.set( 0, this.height + this.headSize/2, 0 );

        this.pivot.add( this.body );
        this.body.position.copy( this.offsetFromCamera );
        this.body.position.y -= this.height;

        GAME.Globals.scene.add( this.pivot );
        super.initialize();
    }

    async arm() {

    }

    interact() {
        //check there's something in front of us we can interact with
        this._interactionRaycaster.setFromCamera( new THREE.Vector2(), this.controls.getObject() );
        let interactionIntersections = this._interactionRaycaster.intersectObjects( GAME.Globals.interactableObjects );

        if (interactionIntersections.length && interactionIntersections[0].object.userData.onInteract) {
            interactionIntersections[0].object.userData.onInteract();
        }        
    }

    toggleTorch() {
        this.torchOn = !this.torchOn;
        this.torch.visible = this.torchOn;
    }

    onKeyDown ( event ) {

        switch ( event.keyCode ) {

            case 38: // up
            case 87: // w
                this.moveForward = true;
                break;

            case 37: // left
            case 65: // a
                this.moveLeft = true;
                break;

            case 40: // down
            case 83: // s
                this.moveBackward = true;
                break;

            case 39: // right
            case 68: // d
                this.moveRight = true;
                break;

            case 32: // space
                if ( this.canJump === true ) this.velocity.y += this.jumpSpeed;
                this.canJump = false;
                break;

            case 69: //e
                this.interact();

            case 70: //e
                this.toggleTorch();

        }

    };

    onKeyUp ( event ) {

        switch ( event.keyCode ) {

            case 38: // up
            case 87: // w
                this.moveForward = false;
                break;

            case 37: // left
            case 65: // a
                this.moveLeft = false;
                break;

            case 40: // down
            case 83: // s
                this.moveBackward = false;
                break;

            case 39: // right
            case 68: // d
                this.moveRight = false;
                break;

        }
    }

    onMouseDown () {
        if (!GAME.Globals.paused) {
            if (this.item) {
                this.item.startUsing();
            }
            this.mousedown = true;
        }
    }

    onMouseUp () {
        if (this.item) {
            this.item.stopUsing();
        }
        this.mousedown = false;
    }

    onGameTickAlways (delta) {
        if (HELPER.getRandomInt(0, 5) > 4 ) {
            this.torch.intensity = (1 - HELPER.getRandom(0, 0.2));
        }

        if ( this.controls.isLocked === true && GAME.Globals.paused === false )
         {
            //check if using our current Y velocity whether in this tick we are on a solid object and set our velocity accordingly            
            let nextVerticalPosition = this.calculateNextVerticalPosition(delta);

            this.velocity.x -= this.velocity.x * 10.0 * delta;
            this.velocity.z -= this.velocity.z * 10.0 * delta;            

            this.direction.z = Number( this.moveForward ) - Number( this.moveBackward );
            this.direction.x = Number( this.moveRight ) - Number( this.moveLeft );
            this.direction.normalize(); // this ensures consistent movements in all directions          
            
            //set our velocity
            if ( this.moveForward || this.moveBackward ) this.velocity.z -= this.direction.z * 400.0 * delta;
            if ( this.moveLeft || this.moveRight ) this.velocity.x -= this.direction.x * 400.0 * delta;

            //before we move check the area is free
            this.performMotionRaycasts();            

            //apply movement
            this.controls.moveRight( - this.velocity.x * delta );
            this.controls.moveForward( - this.velocity.z * delta );
            this.controls.getObject().position.y = nextVerticalPosition; 

            //after calculating our position, reposition anything on us
            this.repositionPlayer();

            //TODO temp increase spooky based on prox to lighthouse
            GAME.Globals.spookyIndex = THREE.MathUtils.clamp((1 / this.controls.getObject().position.distanceTo(this.TEMP_lighthousePosition)) * 30000, 0, 100);
        }
    }

    calculateNextVerticalPosition(delta) {
        
        let bodyWorldPosition = new THREE.Vector3();
        this.body.updateMatrixWorld();
        let nextYPosition = this.controls.getObject().position.y + ( this.velocity.y * delta );

        this.body.getWorldPosition(bodyWorldPosition);
        this._downRaycaster.ray.origin.copy( bodyWorldPosition );
        this._downRaycaster.ray.origin.y += this.height;

        let raycastDirectionVector = new THREE.Vector3(0, -1, 0);

        this._downRaycaster.ray.direction.copy(raycastDirectionVector);
        let downwardIntersections = this._downRaycaster.intersectObjects( GAME.Globals.solidObjects );

        let onObject = false;
           
        for (var i = 0; i < downwardIntersections.length; i++) {
            if (downwardIntersections[i].point.y + this.height + 0.1 >= nextYPosition) {
                this.velocity.y = Math.max( 0, this.velocity.y );
                nextYPosition = downwardIntersections[i].point.y + this.height + 0.1;
                this.canJump = true;
                onObject = true;
                break;
            }    
        }

        if ( onObject === false ) {

            this.velocity.y = Math.max(this.terminalVelocity, this.velocity.y - (GAME.Globals.gravity * this.mass * delta));

            //stop us going below 0
            if ( this.controls.getObject().position.y < this.height ) {

                this.velocity.y = 0;
                this.controls.getObject().position.y = this.height;
                this.canJump = true;

            }
        }

        return nextYPosition;
    }

    performMotionRaycasts() {
        let bodyWorldPosition = new THREE.Vector3();
        this.body.getWorldPosition(bodyWorldPosition);
        
        this._motionRaycaster.ray.origin.copy( bodyWorldPosition );
        this._motionRaycaster.ray.origin.y += this.height;
        //motion raycaster2 is placed above the base raycaster. If we detect a collision in caster 1 but not caster 2 the obstruction is very short and can be "slid" over
        this._motionRaycaster2.ray.origin.copy( bodyWorldPosition );
        this._motionRaycaster2.ray.origin.y += this.height + this.slopeClimbFactor;

        let raycastDirectionVector1 = new THREE.Vector3( this.direction.x, 0, 0 );
        raycastDirectionVector1.applyQuaternion( this.pivot.quaternion );
        raycastDirectionVector1.normalize();
        this._motionRaycaster.ray.direction.copy(raycastDirectionVector1);
        this._motionRaycaster2.ray.direction.copy(raycastDirectionVector1);    
        let movementXIntersections = this._motionRaycaster.intersectObjects( GAME.Globals.solidObjects );
        let movementXIntersections2 = this._motionRaycaster2.intersectObjects( GAME.Globals.solidObjects );

        let raycastDirectionVector2 = new THREE.Vector3( 0, 0, -this.direction.z );
        raycastDirectionVector2.applyQuaternion( this.pivot.quaternion );
        raycastDirectionVector2.normalize();
        this._motionRaycaster.ray.direction.copy(raycastDirectionVector2);
        this._motionRaycaster2.ray.direction.copy(raycastDirectionVector2);
        let movementZIntersections = this._motionRaycaster.intersectObjects( GAME.Globals.solidObjects );
        let movementZIntersections2 = this._motionRaycaster2.intersectObjects( GAME.Globals.solidObjects );

        if ( movementXIntersections.length > 0 ) {
            this.velocity.x = 0
            if ( movementXIntersections2.length === 0 ) {
                this.velocity.y = 50; //slide
            }
        };
        if ( movementZIntersections.length > 0 ) {
            this.velocity.z = 0;
            if ( movementZIntersections2.length === 0 ) {
                this.velocity.y = 50; //slide
            }
        } 

        if (GAME.Globals.debug) {
            this._motionRaycasterDebugArrow1.position.copy( this._motionRaycaster.ray.origin );
            this._motionRaycasterDebugArrow1.setDirection( raycastDirectionVector1 );

            this._motionRaycasterDebugArrow2.position.copy( this._motionRaycaster.ray.origin );
            this._motionRaycasterDebugArrow2.setDirection( raycastDirectionVector2 );
        }
    
    }

    repositionPlayer () {
        let position = new THREE.Vector3();
        let direction = new THREE.Quaternion();
        position.copy(this.controls.getObject().position);
        direction.copy(this.controls.getObject().quaternion);

        this.pivot.position.copy(position);
        this.pivot.quaternion.copy(direction);
        this.pivot.quaternion.x = 0;
        this.pivot.quaternion.z = 0;
        this.pivot.quaternion.normalize();

        this.head.quaternion.copy(direction);
        this.head.quaternion.y = 0;
        this.head.quaternion.z = 0;
        this.head.quaternion.normalize();

        if (this.item) {
            this.item.repositionToView(position, direction);
        }
    }

};

export { Player };