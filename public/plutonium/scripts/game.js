import * as THREE from './vendor/three.module.js';
import * as HELPER from './helper.js';

import { PointerLockControls } from './vendor/PointerLockControls.js';
import { Skybox } from './skybox.js';
import { Player } from './player.js';
import { Asset } from './asset.js';
import { Loader } from './loader.js';
import { Lighthouse } from './lighthouse.js';
import { Region } from './region.js';
import { Tentacle } from './tentacle.js';
import { Planet } from './planet.js';

import { EffectComposer } from './vendor/postprocessing/EffectComposer.js';
import { RenderPass } from './vendor/postprocessing/RenderPass.js';
import { ShaderPass } from './vendor/postprocessing/ShaderPass.js';
import { UnrealBloomPass } from './vendor/postprocessing/UnrealBloomPass.js';
import { RGBShiftShader } from './vendor/shaders/RGBShiftShader.js';
import { Obis } from './obis.js';

var camera, renderer, controls;

var listener, loader;

var lastTickTime = performance.now();

var Globals = {
    debug: false,
    gravity: 4, //9.8 = earth
    gameSpeed: 200, //ticks per second
    animatedObjects: new Map(),
    solidObjects: [], //in order to raycast on this three.js needs a normal array
    interactableObjects: [],
    gameObjects: new Map(),
    scene: null,
    player: null,
    regions: new Map(),
    currentRegion: null,
    paused: false,
    assets: [],

    //layer codes
    ENTIRE_SCENE: 0,
    BLOOM_SCENE: 1,

    spookyIndex: 0, //0 - 100
    maxObis: 10,
    numObis: 0
}

const bloomLayer = new THREE.Layers();
const darkMaterial = new THREE.MeshBasicMaterial( { color: 'black' } );
const materials = {};
bloomLayer.set( Globals.BLOOM_SCENE );
let finalComposer;
let bloomComposer;

function buildAssetList() {

    //models

    Globals.assets.push(new Asset('lighthouse', 'models/lighthouse.json', THREE.Object3D));
    Globals.assets.push(new Asset('beacon', 'models/beaconModel.json', THREE.Object3D));
    Globals.assets.push(new Asset('regiongate', 'models/regiongate.json', THREE.Object3D));

    //audio

    //Globals.assets.push(new Asset('impact_1', './sounds/impact/sand_impact_bullet1.mp3', THREE.Audio));
    

    //textures

    Globals.assets.push(new Asset('skybox', [
        './sprites/skymap/px.png',
        "./sprites/skymap/nx.png",
        './sprites/skymap/py.png',
        "./sprites/skymap/ny.png",
        './sprites/skymap/pz.png',
        "./sprites/skymap/nz.png",
    ], THREE.CubeTexture));

    Globals.assets.push(new Asset('dust', './sprites/dust.png', THREE.Texture));
    Globals.assets.push(new Asset('cloud', './sprites/cloud.png', THREE.Texture));
    Globals.assets.push(new Asset('moon', './maps/texture_crater.png', THREE.Texture));
    Globals.assets.push(new Asset('planet', './sprites/planet.png', THREE.Texture));
    Globals.assets.push(new Asset('planetEmissive', './sprites/planetEmissive.png', THREE.Texture));
    Globals.assets.push(new Asset('planetClouds', './sprites/planetClouds.png', THREE.Texture));
    Globals.assets.push(new Asset('tentacle', './sprites/tentacle.png', THREE.Texture));
    Globals.assets.push(new Asset('lensFlare', './sprites/lensflare0.png', THREE.Texture));
}

init();

async function init() {

    //build our asset list
    buildAssetList();

    //load
    let loaderTextElement = document.getElementById( 'loader-description' );
    loader = new Loader(loaderTextElement);
    await loader.loadAssets(Globals.assets);

    //start the game ticks
    setInterval(tick.bind(this), 1000/Globals.gameSpeed);

    Globals.paused = false;

    camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 20000 );

    Globals.scene = new THREE.Scene();
    Globals.scene.background = new THREE.Color( 0xffffff );
    //Globals.scene.fog = new THREE.FogExp2( 0x000000, 0.01 );

    var light = new THREE.AmbientLight( 0x222222 /*0x14191E*/ );
    Globals.scene.add( light ); 
    
    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight, false );
    renderer.shadowMap.enabled = true;
	renderer.shadowMap.type = THREE.PCFSoftShadowMap;

    renderer.shadowCameraNear = 100;
    renderer.shadowCameraFar = 15000;
    //renderer.toneMapping = THREE.ACESFilmicToneMapping;
    document.body.appendChild( renderer.domElement );


    ///bloom rendering stuff
    //used for the planet glow

    const renderScene = new RenderPass( Globals.scene, camera );
    const bloomPass = new UnrealBloomPass( new THREE.Vector2( window.innerWidth, window.innerHeight ), 0.9, 1.0, 0.0 ); //boosting radius above 1 has weird (cool) effects

    bloomComposer = new EffectComposer( renderer );
    bloomComposer.renderToScreen = false;
    bloomComposer.addPass( renderScene );
    bloomComposer.addPass( bloomPass );

    const finalPass = new ShaderPass(
        new THREE.ShaderMaterial( {
            uniforms: {
                baseTexture: { value: null },
                bloomTexture: { value: bloomComposer.renderTarget2.texture }
            },
            vertexShader: document.getElementById( 'vertexshader' ).textContent,
            fragmentShader: document.getElementById( 'fragmentshader' ).textContent,
            defines: {}
        } ), 'baseTexture'
    );
    finalPass.needsSwap = true;

    const abberation = new ShaderPass( RGBShiftShader );
    abberation.uniforms[ 'amount' ].value = 0.0015;

    finalComposer = new EffectComposer( renderer );
    finalComposer.addPass( renderScene );
    finalComposer.addPass( finalPass );
    finalComposer.addPass( abberation );

    ///end bloom stuff

    
    initSkybox();    
    initPlayer();
    await initRegion();

    window.addEventListener( 'resize', onWindowResize, false );

    //run a single game tick to make sure everthing is all good before we render for the first time
    tick();
    Globals.paused = true;

    animate();

    const loaderElement = document.getElementById( 'loader' );
    loaderElement.style.display = 'none';

    //const obis = new Obis();
    //obis.initialize( new THREE.Vector3(450, 100, 450) );

}

function initSkybox () {
    let skybox = new Skybox();
    skybox.initialize();
}

async function initRegion () {
    let region = new Region('./maps/heightmap_crater.png');
    await region.initialize();
    initPlanet();
    initLighthouse(new THREE.Vector3(-20, 76, 20), Math.PI/4);
    initTentacles(region, 24);
    Globals.regions.set(region.uuid, region);
    Globals.currentRegion = region;

    //add dust particles
    const geometry = new THREE.BufferGeometry();
    const vertices = [];
    const dustSprite = Globals.assets.find(x => x.name === 'dust').contents;

    const halfWidth = (region.width * region.sizeAmplifier) / 2 + 100;
    const halfHeight = (region.height * region.sizeAmplifier) / 2 + 100;
    for ( let i = 0; i < 1000; i ++ ) {
        const x = HELPER.getRandom(-halfWidth, halfWidth);
        const y = HELPER.getRandom(0, 1000);
        const z = HELPER.getRandom(-halfHeight, halfHeight);
        vertices.push( x, y, z );
    }
    geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );
    const material = new THREE.PointsMaterial( { size: 1, sizeAttenuation: true, map: dustSprite } );
    material.color.setHSL( 0.0, 0.0, 0.1 );
    const particles = new THREE.Points( geometry, material );
    Globals.scene.add( particles );
}

function initLighthouse (position, rotation) {
    let lighthouse = new Lighthouse();
    lighthouse.initialize(position);
    lighthouse.object.rotateY(rotation);
}

function initTentacles(region, number) {

    const origin = new THREE.Vector3(0, 0, 0);
    const regionsize = region.width * (region.sizeAmplifier - 1);
    const degreeBetween = 2 * Math.PI / number;
    for (let i = 0; i < number; i++) {
        const angle = degreeBetween * i;
        const x = regionsize * Math.cos(angle);
        const z = regionsize * Math.sin(angle);
        const position = new THREE.Vector3(x, -1500, z);
        const size = HELPER.getRandomInt(6, 15);

        let tentacle = new Tentacle(size);
        
        tentacle.initialize(position);
        tentacle.object.lookAt( origin );
        tentacle.object.rotateY( Math.PI / 2 );

    }

}

function initPlanet() {
    let planet = new Planet();
    const position = new THREE.Vector3(0, 5000, -10000);
    planet.initialize(position);
}

function initPlayer () {

    controls = new PointerLockControls( camera, document.body );

    let blocker = document.getElementById( 'blocker' );
    let instructions = document.getElementById( 'instructions' );

    instructions.style.removeProperty('display');

    instructions.addEventListener( 'click', (e) => {

        e.preventDefault();
        e.stopPropagation();
        controls.lock();

    }, false );

    controls.addEventListener( 'lock', () => {

        instructions.style.display = 'none';
        blocker.style.display = 'none';
        Globals.paused = false;

    } );

    controls.addEventListener( 'unlock', () => {

        blocker.style.display = 'block';
        instructions.style.display = '';
        Globals.paused = true;

    } );    

    Globals.scene.add( controls.getObject() ); 

    let player = new Player( controls );
    listener = new THREE.AudioListener();
    controls.getObject().add(listener);
    player.initialize();
    player.arm();
    Globals.player = player;   

    document.body.addEventListener( 'mousedown', () => {if (!Globals.paused) player.onMouseDown()} );
    document.body.addEventListener( 'mouseup', () => {if (!Globals.paused) player.onMouseUp()} );
    
    //move the player
    controls.getObject().position.y = 20; 
    controls.getObject().position.x = 500;
    controls.getObject().position.z = 500;
    controls.getObject().lookAt( new THREE.Vector3(0, 100, 0) );

}



function spawnObis() {
    const obis = new Obis();
    const position = Globals.currentRegion.getRandomPositionInRegion();
    position.y = 2000;
    obis.initialize( new THREE.Vector3(position.x, position.y, position.z) );
}

function onGameplaytick() {
    if (!Globals.paused && Globals.numObis < Globals.maxObis && Math.random() > 0.9999) {
       spawnObis();
    }
}


function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight, false );

    bloomComposer.setSize(  window.innerWidth, window.innerHeight );
    finalComposer.setSize(  window.innerWidth, window.innerHeight );

}

//called once every game tick
function tick() {
    const time = performance.now();
    const delta = ( time - lastTickTime ) / 1000;

    Globals.gameObjects.forEach(object => {
        object.onGameTickAlways(delta);
        onGameplaytick();
    });

    //bloomComposer.passes[1].radius = Math.max((Globals.spookyIndex / 5 - 10), 0) + 1;

    lastTickTime = time;
}

function animate() {

    requestAnimationFrame( animate );

    Globals.animatedObjects.forEach(object => {
        object.onAnimate();
    });

    render();

}

function render() {

    // render scene with bloom
    Globals.scene.traverse( darkenNonBloomed );
    bloomComposer.render();
    Globals.scene.traverse( restoreMaterial );

    // render the entire scene, then render bloom scene on top
    finalComposer.render();
    
    //renderer.render( Globals.scene, camera );

}

function darkenNonBloomed( obj ) {
    if ( obj.isMesh && bloomLayer.test( obj.layers ) === false ) {
        materials[ obj.uuid ] = obj.material;
        obj.material = darkMaterial;
    }
}

function restoreMaterial( obj ) {
    if ( materials[ obj.uuid ] ) {
        obj.material = materials[ obj.uuid ];
        delete materials[ obj.uuid ];
    }
}

function getListener() {
    return listener;
}

export { Globals, getListener };