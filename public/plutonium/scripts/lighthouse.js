import * as THREE from './vendor/three.module.js';
import * as GAME from './game.js';
import { GameObject } from './gameObject.js';

class Lighthouse extends GameObject {

    asset;
    object;
    position;

    light;
    lensFlareScale;
    lensFlareTexture;
    lensFlare;

    _spotlightSourcePos;
    _spotlightTargetPos;

    constructor() {
        super();
        this.asset = GAME.Globals.assets.find(x => x.name === 'lighthouse');
        this.lensFlareTexture = GAME.Globals.assets.find(x => x.name === 'lensFlare');
        this.object = this.asset.contents.clone();
        this.object.scale.set(6, 6, 6);
    }

    initialize( position ) {

        this.object.position.copy( position );

        this.light = new THREE.PointLight( 0xEDB68D, 2, 100 ); //light glow... pointless?

        this.spotLight = new THREE.SpotLight( 0xFFE6C9 );

        this.spotLight.castShadow = true;

        this.spotLight.shadow.mapSize.width = 1024;
        this.spotLight.shadow.mapSize.height = 1024;

        this.spotLight.shadow.camera.near = 500;
        this.spotLight.shadow.camera.far = 2000;
        this.spotLight.shadow.camera.fov = 30;

        this.spotLight.angle = Math.PI / 7;
        this.spotLight.decay = 0.5;
        this.spotLight.penumbra = 0.05;
        this.spotLight.distance = 1000;

        this.spotLight.target.position.set(100,-40,0);

        this.lensFlare = new THREE.Sprite( new THREE.SpriteMaterial( { map: this.lensFlareTexture.contents } ) );
        this._spotlightSourcePos = new THREE.Vector3();
        this._spotlightTargetPos = new THREE.Vector3();
    
        GAME.Globals.scene.add( this.object );
        this.object.children.forEach(child => {
            if (child.name === "Light") {

                child.add(this.light);
                child.add(this.spotLight);
                child.add(this.spotLight.target);
                child.add(this.lensFlare);

                // const baseGeometry = new THREE.SphereGeometry( 5, 32, 32 );
                // const baseMaterial = new THREE.MeshBasicMaterial();
                // this.sphere = new THREE.Mesh( baseGeometry, baseMaterial );

                // child.add(this.sphere);

            }
            GAME.Globals.solidObjects.push( child );
        });
   
        super.initialize();
    }

    onGameTick() {
        let date = Date.now() * 0.0001;
        this.spotLight.target.position.set(
            Math.cos(date) * 20,
            0,
            Math.sin(date) * 20
        );
        // this.sphere.position.set(
        //     Math.cos(date) * 20,
        //     0,
        //     Math.sin(date) * 20
        // );

        //calc the size of the lens flare (todo: this should be in a shader lol)
       
        this.spotLight.getWorldPosition(this._spotlightSourcePos);
        this.spotLight.target.getWorldPosition(this._spotlightTargetPos);
        const playerPos = GAME.Globals.player.pivot.position;
        //ignore the y dimension by setting all to use the same
        this._spotlightTargetPos.y = playerPos.y;
        this._spotlightSourcePos.y = playerPos.y;

        const distToTarget = this._spotlightTargetPos.distanceToSquared(playerPos);
        const distToSource = this._spotlightSourcePos.distanceToSquared(playerPos);
        const targetToPlayerAngle = this._spotlightTargetPos.angleTo(playerPos);

        let modifier = 0;
        if (distToSource > distToTarget) {
            modifier = 1/targetToPlayerAngle / 10;
            modifier = THREE.MathUtils.clamp(modifier, 0, 6);
        }

        //scale should be beteen 0 and approx 100
        this.lensFlareScale = (distToSource / 10000) * modifier;
        this.lensFlare.scale.set(this.lensFlareScale, this.lensFlareScale, this.lensFlareScale);
        
    }
    

};

export { Lighthouse };