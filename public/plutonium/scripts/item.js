import * as THREE from './vendor/three.module.js';
import * as GAME from './game.js';
import { GameObject } from './gameObject.js';

class Item extends GameObject {

    object;
    pivot;
    isBeingUsed;
    useDelay = 0; //seconds
    controls; //global controls
    _timeSinceLastUse = 0;

    constructor( controls ) {
        super();
        this.controls = controls;
    }

    startUsing() {
        this.isBeingUsed = true;
    }

    stopUsing() {
        this.isBeingUsed = false;
    }

    performAction() {
        this._timeSinceLastUse = 0;
    }

    initialize() {
        super.initialize();
    }

    repositionToView(position, direction) {

    }

    onGameTick (delta) {
        this._timeSinceLastUse += delta;
        if (this.isBeingUsed && this._timeSinceLastUse > this.useDelay) {
            this.performAction();
        }
    }
};

export { Item };