import * as THREE from './vendor/three.module.js';
import * as GAME from './game.js';
import * as HELPER from './helper.js';
import { Beacon } from './beaconScript.js'; //please see the file for why the name isn't "beacon.js"
import { RegionGate } from './regionGate.js';

class Region {

    uuid;
    isEndRegion;
    modifier;

    heightmapsrc;
    moonTextureAsset;
    heightmapData;

    group;
    object;
    width;
    height;
    sizeAmplifier;
    heightModifier;

    heightValues = [];

    entryPoint;
    exitPoint;

    constructor(heightmapsrc) {
        this.uuid = HELPER.getUUID();
        this.heightmapsrc = heightmapsrc;
        this.sizeAmplifier = 7;
        this.heightModifier = 0.5;
        this.moonTextureAsset = GAME.Globals.assets.find(x => x.name === 'moon');
    }

    async prepareImageData() {
        return new Promise(function(resolve, reject) {
            let image = new Image();
            image.src = this.heightmapsrc;
            image.onload = () => {
                this.width = image.width;
                this.height = image.height;
        
                let canvas = document.createElement('canvas');
                canvas.width = this.width;
                canvas.height = this.height;
                let context = canvas.getContext('2d');
        
                context.drawImage(image, 0, 0);
                this.heightmapData = context.getImageData(0, 0, this.width, this.height).data;
        
                resolve();
            }
        }.bind(this));
    }

    async initialize() {
        await this.prepareImageData();
        let geometry = new THREE.PlaneBufferGeometry(this.width * this.sizeAmplifier, this.height * this.sizeAmplifier, this.width - 1, this.height - 1);   
        geometry.castShadow = true;
        geometry.receiveShadow = true;

        var vertices = geometry.attributes.position.array;
        //here we go over the hightmap data and buffer geometry verticies and apply the heightmap data to the buffer's Z verticies
        //why Z and not Y? Because the plane geometry is built along the X and Y axis by default. We're using Z for the height for now and then flipping the geometry later
        //why start at j = 2 and increase by +3 each iteration? Because it is a long array of verticies which comes in triplets (x,y,z), and we're only interested in the z values
        //heightmapData contains all the rbga values (one by one) for the heightmap in an array, so in the same fashion we jump forward 4 points each loop
        let children = [];
        let exitRegionDoor;
        for (var i = 0, j = 2; i < this.heightmapData.length; i += 4, j += 3) {
            //we use the GREEN channel for height, and the other channels for other data
            const r = this.heightmapData[i];
            const g = this.heightmapData[i+1];
            const b = this.heightmapData[i+2];
            const height = g * this.heightModifier;

            vertices[j] = height;
            if (r !== 0 || b !== 0) {
                //special pixel
                let location = new THREE.Vector3(vertices[j - 2], vertices[j], -vertices[j - 1]);
                if (b === 0 & r === 255) {
                    //beacon
                    let beacon = new Beacon();
                    beacon.initialize(location);
                    beacon.object.rotateZ(HELPER.getRandom(-0.1, 0.1));
                    beacon.object.rotateX(HELPER.getRandom(-0.1, 0.1));
                    children.push(beacon.object);
                } else if (b === 255 & r === 255) {
                    //exit
                    exitRegionDoor = new RegionGate();
                    exitRegionDoor.initialize(location);
                    exitRegionDoor.object.rotateY(Math.PI/2);
                    this.exitPoint = location;
                    children.push(exitRegionDoor.object);
                } else if (b === 255 & r === 0) {
                    //entrance
                    this.entryPoint = location;
                }
            }

            this.heightValues.push(height);
        }

        this.group = new THREE.Group();
        //here's where we flip the geometry to be rightways up
        geometry.rotateX(- Math.PI/2);
        let material = new THREE.MeshStandardMaterial({color: 0xbbbbbb, map: this.moonTextureAsset.contents});
        material.side = THREE.DoubleSide;
        let floor = new THREE.Mesh(geometry, material);
        floor.name = "floor";
        GAME.Globals.solidObjects.push( floor );
        this.group.add(floor);

        for (var i = 0; i < children.length; i++) {
            this.group.add(children[i]);
        }
    
        GAME.Globals.scene.add( this.group );
        
        this.object = floor;
    }

    //returns a janky but cheap Y value for a given coordinate
    //good for low res Y position calculations when you want to avoid raycasting (always lol)
    getHeightValueForCoords(x, y) {
        const halfWidth = this.width/2;
        const halfHeight = this.height/2;
        x = Math.round(x / this.sizeAmplifier) + halfWidth;
        y = Math.round(y / this.sizeAmplifier) + halfHeight;

        const index = (y * this.height) + x;

        return this.heightValues[index]; //+ 10; //I straight up do not know where this +10 comes from
        //it's like the entire geometry has been shifted up by 10 at some point?
    }

    getRandomPositionInRegion() {
        const halfWidth = (this.width * this.sizeAmplifier) / 2;
        const halfHeight = (this.height * this.sizeAmplifier) / 2;
        const x = HELPER.getRandom(-halfWidth, halfWidth);
        const z = HELPER.getRandom(-halfHeight, halfHeight);
        const y = this.getHeightValueForCoords(x, z);
        return {x: x, y: y, z: z};
    }

};

export { Region };