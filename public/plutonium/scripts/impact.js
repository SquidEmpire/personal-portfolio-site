import * as THREE from './vendor/three.module.js';
import * as GAME from './game.js';
import { GameObject } from './gameObject.js';

class Impact extends GameObject {

    object;
    position;
    lifetime = 0.1;
    scale = new THREE.Vector3(2, 2, 2);

    constructor() {
        super();
    }

    initialize( position ) {

        const asset = GAME.Globals.assets.find(x => x.name === 'cloud');
        this.object = new THREE.Sprite( new THREE.SpriteMaterial( { map: asset.contents } ) );
        GAME.Globals.scene.add( this.object );
        this.object.position.copy( position ); 
        this.object.scale.copy(this.scale);
        super.initialize();
    }

    destroy() {
        this.object.geometry.dispose();
        this.object.material.dispose();
        GAME.Globals.scene.remove(this.object);
    }

    onGameTick(delta) {
        if (this.lifetime < 0) {
            this.destroy();
            return;
        }
        this.lifetime -= delta;
        
        this.scale.addScalar(0.1);
        this.object.scale.copy(this.scale);
    }
    

};

export { Impact };