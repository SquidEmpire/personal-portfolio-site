import * as THREE from './vendor/three.module.js';
import * as HELPER from './helper.js';
import * as GAME from './game.js';

import { mergeBufferGeometries } from './vendor/BufferGeometryUtils.js';
import { GameObject } from './gameObject.js';
import { TentacleBufferGeometry } from './vendor/TentacleGeometry.js';

class Obis extends GameObject {

    body;
    tentacles = [];
    object; //this it the body+tentacles
    pivot;
    flash; //this is a sprite of a lens flare for when we're first spawned
    target = new THREE.Vector3();

    mass = 50;

    velocity = new THREE.Vector3();
    terminalVelocity = -300;

    //state
    fallen = false;
    activated = false;
    hasMoved = false;
    chasingPlayer = false;

    _tentacleSegments = 4;
    _tentacleMinWidth = 0.08;
    _tentacleMaxWidth = 0.5;
    _tentacleRadialSegments = 5;

    //animation config   
    _currentFloatHeight;
    _flashDuration = 15;
    _flashScale = 150;
    _maxFloatHeight = 6.9;
    _minFloatHeight = 6.2;
    _floatDirection = 1;
    _tentacleMaxOffset = 0.5;

    constructor() {
        super();

        this._downRaycaster = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3(), 0 );

        //build our object
        this.pivot = new THREE.Group();
        this.object = new THREE.Group();

        const baseHeight = 6;
        const baseLowerWidth = 2.4;
        const baseUpperWidth = 2;
        const halfBaseLowerWidth = baseLowerWidth / 2;
        const halfBaseUpperWidth = baseUpperWidth / 2;

        //create the main base
        const basePts = [
            new THREE.Vector3(-halfBaseLowerWidth, 0, -halfBaseLowerWidth), //bottom
            new THREE.Vector3(-halfBaseLowerWidth, 0, halfBaseLowerWidth),
            new THREE.Vector3(halfBaseLowerWidth, 0, halfBaseLowerWidth),
            new THREE.Vector3(halfBaseLowerWidth, 0, -halfBaseLowerWidth),

            new THREE.Vector3(-halfBaseUpperWidth, baseHeight, -halfBaseUpperWidth), //top
            new THREE.Vector3(-halfBaseUpperWidth, baseHeight, halfBaseUpperWidth),
            new THREE.Vector3(halfBaseUpperWidth, baseHeight, halfBaseUpperWidth),
            new THREE.Vector3(halfBaseUpperWidth, baseHeight, -halfBaseUpperWidth)
        ];
        
        const baseGeo = new THREE.BufferGeometry().setFromPoints(basePts);
        baseGeo.setIndex([
            4, 3, 0, //side1
            3, 4, 7,
            
            4, 1, 5, //side2
            1, 4, 0,

            5, 2, 6, //side3
            2, 5, 1,

            2, 7, 6, //side4
            7, 2, 3,

            3, 1, 0, //bottom
            1, 3, 2
        ]);
        baseGeo.computeVertexNormals();

        //create the pyramidion
        const tipPts = [
            new THREE.Vector3(0, baseUpperWidth, 0), //the peak point
            new THREE.Vector3(-halfBaseUpperWidth, 0, -halfBaseUpperWidth),
            new THREE.Vector3(-halfBaseUpperWidth, 0, halfBaseUpperWidth),
            new THREE.Vector3(halfBaseUpperWidth, 0, halfBaseUpperWidth),
            new THREE.Vector3(halfBaseUpperWidth, 0, -halfBaseUpperWidth)
        ];
        
        const tipGeo = new THREE.BufferGeometry().setFromPoints(tipPts);
        tipGeo.setIndex([
            0, 1, 2,
            0, 2, 3, 
            0, 3, 4,
            0, 4, 1
        ]);
        tipGeo.computeVertexNormals();
        tipGeo.translate(0, baseHeight, 0);

        const finalGeometry = mergeBufferGeometries([baseGeo, tipGeo]);

        const material = new THREE.MeshBasicMaterial({color: 0x0D0F16});
        this.body = new THREE.Mesh(finalGeometry, material);

        this.object.add(this.body);

        //tentacles for the thing
        //const tentacleTextureAsset = GAME.Globals.assets.find(x => x.name === 'tentacle');
        const minBound = -halfBaseLowerWidth + this._tentacleMaxWidth;
        const maxBound = halfBaseLowerWidth - this._tentacleMaxWidth;

        for (let i = 0; i < 4; i++) {
            let pathPoints = [];
            pathPoints[0] = new THREE.Vector3(0, 0, 0);
            //generate points for each segment
            for (let j = 1; j < this._tentacleSegments; j++) {
                const x = 0;//pathPoints[j - 1].x += HELPER.getRandom(-1, 1);
                const y = -j * 2;
                const z = 0;//pathPoints[j - 1].z += HELPER.getRandom(-1, 1);
                pathPoints.push(new THREE.Vector3(x, y, z))
            }    
            const path = new THREE.CatmullRomCurve3(pathPoints);    
            const geometry = new TentacleBufferGeometry( path, this._tentacleSegments, this._tentacleMinWidth, this._tentacleMaxWidth, this._tentacleRadialSegments, false );
            const material = new THREE.MeshBasicMaterial({color: 0x0D0F16 });//map: tentacleTextureAsset.contents});
        
            this.tentacles[i] = new THREE.Mesh( geometry, material );
                        
            this.object.add(this.tentacles[i]);          
        }

        this.tentacles[0].position.set( minBound, 0.1, minBound );
        this.tentacles[0].rotation.y = -Math.PI/4; //this just makes them face inwards if using a texture map

        this.tentacles[1].position.set( maxBound, 0.1, minBound );
        this.tentacles[1].rotation.y = -3*Math.PI/4;

        this.tentacles[2].position.set( maxBound, 0.1, maxBound );
        this.tentacles[2].rotation.y = 3*Math.PI/4;

        this.tentacles[3].position.set( minBound, 0.1 ,maxBound );
        this.tentacles[3].rotation.y = Math.PI/4;


        //lens flare
        const lensFlareTexture = GAME.Globals.assets.find(x => x.name === 'lensFlare');
        this.lensFlare = new THREE.Sprite( new THREE.SpriteMaterial( { map: lensFlareTexture.contents, color: 0x00ffaa } ) );
        this.lensFlare.scale.set(this._flashScale, this._flashScale, this._flashScale);

        this.pivot.add(this.object);
        this.pivot.add(this.lensFlare);

    }

    initialize( position ) {

        this.pivot.position.copy( position );
        GAME.Globals.scene.add( this.pivot );

        for (let i = 0; i < this.tentacles.length; i++) {
            this.tentacles[i].visible = false;
        }

        this._currentFloatHeight = 0;
        this.velocity.y = this.terminalVelocity;

        GAME.Globals.numObis += 1;

        super.initialize();

    }

    destroy() {
        console.log("obis dead");
        GAME.Globals.numObis -= 1;
        this.body.geometry.dispose();
        this.body.material.dispose();
        this.tentacles[0].material.dispose();
        for (let i = 0; i < this.tentacles.length; i++) {
            this.tentacles[i].geometry.dispose();
        }
        GAME.Globals.scene.remove(this.pivot);
        super.destroy();
    }

    findNewTarget() {
        const newTarget = GAME.Globals.currentRegion.getRandomPositionInRegion();
        this.target.set(newTarget.x, newTarget.y, newTarget.z);
        this.pivot.lookAt(this.target);
    }

    onGameTick(delta) {
        //movement
        const skipRaycast = this.pivot.position.y > 100 || !this.hasMoved;
        const nextYPosition = this.calculateNextVerticalPosition(delta, skipRaycast);
        this.hasMoved = this.pivot.position.y !== nextYPosition;
        this.pivot.position.y = nextYPosition;

        if (this.activated) {

            if (this.chasingPlayer) {
                //charge!

            } else {
                //move towards whatever our target is               
                this.pivot.translateZ( 0.1 );
                if (this.pivot.position.distanceTo(this.target) < 1) {
                    this.findNewTarget();
                }
                this.hasMoved = true;
            }

        }

        if (this.fallen) {
            //animate!
            if (this._floatDirection === 1) {
                if (this._currentFloatHeight < this._maxFloatHeight) {
                    this._currentFloatHeight += Math.random() / 50;
                } else if ( !this.activated ) {
                    //the first time this obis reaches max height it is then considered "active" and can move, shoot, etc.
                    this.activated = true;
                    this._floatDirection = -1;
                    this.findNewTarget();
                }
            } else {
                if (this._currentFloatHeight > this._minFloatHeight) {
                    this._currentFloatHeight -= Math.random() / 50;
                } else {
                    this._floatDirection = 1;
                }
            }
            this.object.position.y = this._currentFloatHeight;

            for (let i = 0; i < this.tentacles.length; i++) {
                this.tentacles[i].visible = true;
                this.wiggleTentacle(this.tentacles[i]);
            }
            this.lensFlare.visible = false;
        } else {
            if (this._flashDuration > 0) {
                
                this._flashScale -= 10;
                this.lensFlare.scale.set(this._flashScale, this._flashScale, this._flashScale);
                this._flashDuration--;

            } else {
                this.lensFlare.visible = false;
            }
        }
        
    }

    calculateNextVerticalPosition(delta, skipRaycast) {
        
        let nextYPosition = this.pivot.position.y + ( this.velocity.y * delta );
        let onObject = false;

        if (!skipRaycast) {

            const groundYPosition = GAME.Globals.currentRegion.getHeightValueForCoords(this.pivot.position.x, this.pivot.position.z);

            if (groundYPosition >= nextYPosition) {
                this.velocity.y = Math.max( 0, this.velocity.y );
                nextYPosition = groundYPosition;
                onObject = true;
                this.fallen = true;
                return nextYPosition;
            }
    
            if ( onObject === false ) {
    
                this.velocity.y = Math.max(this.terminalVelocity, this.velocity.y - (GAME.Globals.gravity * this.mass * delta));
                //die if we go below 0
                if ( this.pivot.position.y < 0 ) {
                    this.velocity.y = 0;
                    this.pivot.position.y = 0;
                    this.destroy();
                }
            }

        }

        return nextYPosition;
    }

    wiggleTentacle(tentacle) {
        for (let i = 1; i < this._tentacleSegments; i++) {
            //these are the line segment points
            const previous = tentacle.geometry.parameters.path.points[i - 1];
            const current = tentacle.geometry.parameters.path.points[i];
            const x = THREE.MathUtils.clamp(HELPER.getRandom(current.x -0.1, current.x + 0.1), previous.x - this._tentacleMaxOffset, previous.x + this._tentacleMaxOffset);
            const y = THREE.MathUtils.clamp(HELPER.getRandom(current.y -0.1, current.y + 0.1), previous.y - this._tentacleMaxOffset * 4, previous.y - this._tentacleMaxOffset * 8);
            const z = THREE.MathUtils.clamp(HELPER.getRandom(current.z -0.1, current.z + 0.1), previous.z - this._tentacleMaxOffset, previous.z + this._tentacleMaxOffset);
        
            tentacle.geometry.parameters.path.points[i] = new THREE.Vector3(x, y, z);
        }
        tentacle.geometry.dispose();
        tentacle.geometry.copy(new TentacleBufferGeometry(tentacle.geometry.parameters.path, this._tentacleSegments, this._tentacleMinWidth, this._tentacleMaxWidth, this._tentacleRadialSegments, false));
        tentacle.geometry.needsUpdate = true;
    }
    

};

export { Obis };