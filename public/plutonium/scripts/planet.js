import * as THREE from './vendor/three.module.js';
import * as GAME from './game.js';
import * as HELPER from './helper.js';

import { GameObject } from './gameObject.js';
import { TWEEN } from './vendor/Tween.js';

class Planet extends GameObject {

    object;
    atmoSphere;
    glow;

    constructor() {
        super();

        const planetTextureAsset = GAME.Globals.assets.find(x => x.name === 'planet');
        const planetTextureEmissiveAsset = GAME.Globals.assets.find(x => x.name === 'planetEmissive');
        const planetTextureClouds = GAME.Globals.assets.find(x => x.name === 'planetClouds'); 

        let geometry = new THREE.SphereGeometry( 8000, 128, 64 );
        let material = new THREE.MeshStandardMaterial( {
            color: 0x0C20FF,
            emissive: 0xC0F0FF,
            emissiveIntensity: 0.4,
            emissiveMap: planetTextureEmissiveAsset.contents,
            map: planetTextureAsset.contents 
        } );
    
        this.object = new THREE.Mesh( geometry, material );
        this.object.layers.toggle( GAME.Globals.BLOOM_SCENE ); //using bloom to approximate the planet atmosphere lol

        let geometry2 = new THREE.SphereGeometry( 8100, 128, 64 );
        let material2 = new THREE.MeshStandardMaterial( {
            color: 0xffffff,
            alphaMap: planetTextureClouds.contents,
            transparent: true
        } );
        this.atmoSphere = new THREE.Mesh(geometry2, material2);
        this.atmoSphere.layers.toggle( GAME.Globals.BLOOM_SCENE );

        this.glow = new THREE.DirectionalLight( 0xC0F0FF, 0.5 );
        this.glow.castShadow = false;

        const d = 20000;
        this.glow.shadow.camera.left = - d;
        this.glow.shadow.camera.right = d;
        this.glow.shadow.camera.top = d;
        this.glow.shadow.camera.bottom = - d;

    }

    initialize( position ) {

        this.object.position.copy( position );
        this.atmoSphere.position.copy( position );
        this.glow.position.copy( position );
        
        GAME.Globals.scene.add( this.object );
        GAME.Globals.scene.add( this.atmoSphere );
        GAME.Globals.scene.add( this.glow ); 

        super.initialize();
    }

    onGameTick() {
        this.atmoSphere.rotation.y += 0.0005;
    }

}

export { Planet };