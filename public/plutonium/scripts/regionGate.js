import * as THREE from './vendor/three.module.js';
import * as GAME from './game.js';
import { GameObject } from './gameObject.js';
import { Door } from './door.js';
import { Region } from './region.js';
 
class RegionGate extends GameObject {

    asset;
    object;
    position;
    light;
    height;

    door1;
    door2;

    canload;

    boundingBox;

    constructor() {
        super();
        this.asset = GAME.Globals.assets.find(x => x.name === 'regiongate');
        this.object = this.asset.contents.clone();
        this.object.scale.set(4, 4, 4);
    }

    initialize( position ) {

        this.object.position.copy( position );
        this.canload = true;

        this.object.children.forEach(child => {
            if (child.name === "door1 pivot") {
                this.door1 = new Door(child);
                this.door1.initialize();
            } else if (child.name === "door2 pivot") {
                this.door2 = new Door(child);
                this.door2.initialize();          
            }
            GAME.Globals.solidObjects.push( child );
        });

        this.boundingBox = new THREE.Box3().setFromObject(this.object);
        if (GAME.Globals.debug) {
            let helper = new THREE.Box3Helper( this.boundingBox, 0xffff00 );
            GAME.Globals.scene.add( helper );
        }      
   
        super.initialize();
    }

    async loadNextRoom() {
        //load the next region and destroy the old one
        this.door1.lock();
        this.door2.lock();
        let region = new Region('./maps/map1.png');
        await region.initialize();
        region.group.position.x = this.object.position.x - ((region.width * region.sizeAmplifier)/2);
        let diff = region.entryPoint.z - this.object.position.z;
        region.group.position.z -= diff;
        GAME.Globals.regions.set(region.uuid, region);
        GAME.Globals.currentRegion = region;
        this.door2.unlock();
    }

    onGameTick() {
        if (this.canload && this.boundingBox.containsPoint(GAME.Globals.player.object.position) && this.door1._state === "closed" && this.door2._state === "closed") {
            this.loadNextRoom();
            this.canload = false;
        }
    }
    

};

export { RegionGate };