import * as GAME from './game.js';
import * as HELPER from './helper.js';

class GameObject {

    uuid;
    destroyed = false;

    constructor(  ) {
        this.uuid = HELPER.getUUID();
    }

    initialize() {
        GAME.Globals.gameObjects.set(this.uuid, this);
    }

    onGameTick(delta) {

    }

    destroy() {
        this.destroyed = true;
        GAME.Globals.gameObjects.delete(this.uuid);
    }

    onGameTickAlways(delta) {
        if (!this.destroyed && GAME.Globals.paused == false) {
            this.onGameTick(delta);
        }
    }

};

export { GameObject };