import * as THREE from './vendor/three.module.js';
import * as GAME from './game.js';
import * as HELPER from './helper.js';
import { Item } from './item.js'; 
import { Impact } from './impact.js';

class Pistol extends Item {

    useDelay = 0.1;
    accuracyOffset = 0.1;

    direction = new THREE.Vector3();
    directionOffset = new THREE.Vector3();
    raycaster = new THREE.Raycaster();

    constructor( controls ) {
        super( controls );
    }

    performAction() {
        super.performAction();
        console.log("pew");

        this.directionOffset.set(
            HELPER.getRandom(-this.accuracyOffset, this.accuracyOffset),
            HELPER.getRandom(-this.accuracyOffset, this.accuracyOffset),
            HELPER.getRandom(-this.accuracyOffset, this.accuracyOffset)
        );

        this.controls.getDirection( this.direction );
        this.direction.add( this.directionOffset );
        //mess around a little with the direction to make us not completely accurate
        this.raycaster.set( this.controls.getObject().position, this.direction );
        const intersections = this.raycaster.intersectObjects( GAME.Globals.solidObjects );
        const target = intersections[0];

        const impact = new Impact();
        impact.initialize( target.point );
        console.log(target);
    }
    

};

export { Pistol };