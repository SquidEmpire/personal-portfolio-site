import * as THREE from './vendor/three.module.js';
import * as GAME from './game.js';
import * as HELPER from './helper.js';

import { GameObject } from './gameObject.js';
import { TentacleBufferGeometry } from './vendor/TentacleGeometry.js';
import { TWEEN } from './vendor/Tween.js';

class Tentacle extends GameObject {

    object;
    position;
    size;
    path;
    tentacleTextureAsset;

    _targetYPosition;

    _segments = 5;
    _radiusStart = 10;
    _radiusEnd = 0.1;
    _radialSegments = 6;

    //animation settings
    _animationSpeed = 0.1;
    _animationZMin = -10;
    _animationZMax = 10;
    _animationXBaseMin = -45;
    _animationXBaseMax = 0;
    _animationYMin = 10;
    _animationYMax = 60;
    _animationVelocityX = 0;
    _animationVelocityY = 1.5;
    _animationVelocityZ = 0.5;

    constructor(size = 10) {
        super();

        this.tentacleTextureAsset = GAME.Globals.assets.find(x => x.name === 'tentacle');

        this.size = size;
        //scale settings
        this._radiusStart *= this.size;
        this._radiusEnd *= this.size;
        this._animationSpeed *= this.size;
        this._animationZMin *= this.size;
        this._animationZMax *= this.size;
        this._animationXMin = this.size * this._animationXBaseMin;
        this._animationXMax = this.size * this._animationXBaseMax;
        this._animationYMin *= this.size;
        this._animationYMax *= this.size;
        this._animationVelocityX *= this.size;
        this._animationVelocityY *= this._animationSpeed * (HELPER.getRandomBool() ? -1 : 0);
        this._animationVelocityZ *= this._animationSpeed * (HELPER.getRandomBool() ? -1 : 0);

        let pathPoints = [];
        //point 0 must be at root
        pathPoints[0] = new THREE.Vector3(0, 0, 0);
        //point 1 must be directly above the root to have the tentacle "rise" over a hill
        pathPoints[1] = new THREE.Vector3(0, 20 * this.size, 0);
        //generate points for each segment
        for (let i = 2; i < this._segments; i++) {
            const x = pathPoints[i - 1].x += this._animationXMin * Math.random();
            const y = pathPoints[i - 1].y += this._animationYMin * Math.random();
            const z = 0;
            pathPoints.push(new THREE.Vector3(x, y, z))
        }

        this.path = new THREE.CatmullRomCurve3(pathPoints);

        let geometry = new TentacleBufferGeometry( this.path, this._segments, this.__radiusEnd, this._radiusStart, this._radialSegments, false );
        let material = new THREE.MeshStandardMaterial({color: 0xbbbbbb, map: this.tentacleTextureAsset.contents});
    
        this.object = new THREE.Mesh( geometry, material );

        //TODO
        const baseGeometry = new THREE.SphereGeometry( 5 * this.size, 32, 32 );
        const baseMaterial = new THREE.MeshBasicMaterial();
        this.sphere = new THREE.Mesh( baseGeometry, baseMaterial );
    }

    initialize( position ) {

        this.object.position.copy( position );
        this.sphere.position.copy( position );
        
        GAME.Globals.scene.add( this.object );
        //GAME.Globals.scene.add( this.sphere );

        this._targetYPosition = this.object.position.y;
   
        super.initialize();
    }

    randomShiftWithinConstraints(value, velocity, min, max) {
        
        return value + velocity;
    }

    onGameTick(fdt) {

        this._targetYPosition = GAME.Globals.spookyIndex / 100 * (100 - -1500 + 1) + -1500;

        if (this.object.position.y !== this._targetYPosition) {
            this.object.position.y = THREE.MathUtils.lerp(this.object.position.y, this._targetYPosition, fdt * Math.random());
        }

        for (let i = 2; i < this._segments; i++) {
            const x = this.object.geometry.parameters.path.points[i].x + this._animationVelocityX;
            if (x > this._animationXMax || x < this._animationXMin) {
                this._animationVelocityX *= -1;
            }

            const y = this.object.geometry.parameters.path.points[i].y + this._animationVelocityY;
            if (y > this._animationYMax || y < this._animationYMin) {
                this._animationVelocityY *= -1;
            }

            const z = this.object.geometry.parameters.path.points[i].z + this._animationVelocityZ;
            if (z > this._animationZMax || z < this._animationZMin) {
                this._animationVelocityZ *= -1;
            }

            this.object.geometry.parameters.path.points[i] = new THREE.Vector3(x, y, z);
        }
        this.object.geometry.copy(new TentacleBufferGeometry(this.object.geometry.parameters.path, this._segments, this.__radiusEnd, this._radiusStart, this._radialSegments, false));
        this.object.geometry.needsUpdate = true;
    }
    

};

export { Tentacle };