import * as THREE from "./vendor/three.module.js";

class Loader {

    texturepath;

    loadingManager;
    textureLoader;
    audioLoader;

    loaderTextElement;

    constructor( displayElement ) {
        this.loadingManager = new THREE.LoadingManager();

        this.textureLoader = new THREE.TextureLoader(this.loadingManager);
        this.audioLoader = new THREE.AudioLoader(this.loadingManager);

        this.loadingManager.onStart = this.onLoadStart.bind(this);
        this.loadingManager.onLoad = this.onLoad.bind(this);
        this.loadingManager.onProgress = this.onLoadProgress.bind(this);
        this.loadingManager.onError = this.onLoadError.bind(this);

        this.loaderTextElement = displayElement;
    }

    onLoadStart(url, itemsLoaded, itemsTotal) {
        console.log( 'Started loading file: ' + url + '.\nLoaded ' + itemsLoaded + ' of ' + itemsTotal + ' files.' );
    }

    onLoad() {
        console.log( 'Loading complete!');    
        this.loaderTextElement.innerHTML = "";
    };

    onLoadProgress( url, itemsLoaded, itemsTotal ) {
        let displayFilename = url;
        if (displayFilename.startsWith("data:")) {
            displayFilename = "Encoded data";
        }
        if (this.loaderTextElement) {
            this.loaderTextElement.innerHTML = `<p>Loading ${displayFilename}</p><p>${itemsLoaded}/${itemsTotal}</p>`;
        }   
    };
    
    onLoadError( url ) {
        console.log( 'There was an error loading ' + url );
    };

    async loadAssets(assets) {
        let assetPromises = [];

        assets.forEach(asset => {

            switch (asset.type) {

                case THREE.Texture:
                    assetPromises.push(new Promise(function(resolve, reject) {
                        this.textureLoader.load(asset.path, (textureContent) => {
                            textureContent.magFilter = THREE.NearestFilter;
                            textureContent.minFilter = THREE.LinearMipMapLinearFilter;
                            asset.setContents(textureContent);
                            resolve();
                        }, undefined, (e) => {
                            reject(e);
                        });
                    }.bind(this)));
                    break;

                case "spritesheet":
                    assetPromises.push(new Promise(function(resolve, reject) {
                        this.textureLoader.load(asset.path, (textureContent) => {
                            textureContent.magFilter = THREE.NearestFilter;
                            textureContent.minFilter = THREE.LinearMipMapLinearFilter;
                            textureContent.wrapS = THREE.RepeatWrapping;
                            textureContent.wrapT = THREE.RepeatWrapping;

                            const columns = textureContent.image.width / asset.size
                            const rows = textureContent.image.width / asset.size;
                            const numberOfSprites = columns * rows;
                            textureContent.repeat.set( 1 / rows, 1 / columns );

                            textureContent.offset.x = 0;
                            textureContent.offset.y = 0;

                            asset.setContents(textureContent);
                            asset.rows = rows;
                            asset.columns = columns;
                            asset.numberOfSprites = numberOfSprites;
                            resolve();
                        }, undefined, (e) => {
                            reject(e);
                        });
                    }.bind(this)));
                    break;               

                case THREE.Audio:
                    assetPromises.push(new Promise(function(resolve, reject) {
                        this.audioLoader.load( asset.path, ( buffer ) => {
                            asset.setContents(buffer);
                            resolve();
                        }, undefined, (e) => {
                            reject(e);
                        });
                    }.bind(this)));
                    break;

                default:
                    console.error("Attempted to load unknown asset type", asset.type);
                    break;

            }

        });

        return Promise.all(assetPromises);
    }

    async waitForUserInput(element) {
        return new Promise((resolve, reject) => {
            element.addEventListener('click', resolve);
        });
    }

};

export { Loader };