import {Globals} from "../core.js";
import { GameObject } from "./gameObject.js";
import * as THREE from '../vendor/three.module.js';
import * as HELPER from '../helper.js';

export class RandomSpriteObject extends GameObject {

    _mesh;
    _index = 0;
    _spritesheetAsset;
    _originalUVs;

    constructor(xScale = 1, yScale = 1, spritesheetName = "placeholder") {
        super();
        this._spritesheetAsset = Globals.assets.find(x => x.name === spritesheetName);
        const material = new THREE.MeshBasicMaterial({ map: this._spritesheetAsset.contents, transparent: true });

        //using a plane instead of a sprite for culling and lighting
        const geometry = new THREE.PlaneGeometry(1, 1, 1);
        this._mesh = new THREE.Mesh( geometry, material );
        this._mesh.lookAt(Globals.camera.position);
        this.root.add(this._mesh);
        this.setScale(xScale, yScale);        
    }

    initialize( x = 0, y = 0, z = 0 ) {
        super.initialize(x, y, z);
        this._originalUVs = this._mesh.geometry.attributes.uv.clone();
        this.randomiseSprite();
        Globals.scene.add(this.root);
    }

    setScale(xScale, yScale) {
        this._mesh.scale.set( xScale, yScale, 1 );
    }

    randomiseSprite() {
        this._index = HELPER.getRandomInt(0, this._spritesheetAsset.numberOfSprites - 1);
        const nextUvCoords = this._spritesheetAsset.getUvCoordsForIndex(this._index);
        this.setUvFromCoords(nextUvCoords);
    }

    setUvFromCoords(coords) {
        for (let i = 0; i < this._originalUVs.count; i++) {

            let u = this._originalUVs.getX(i);
            let v = this._originalUVs.getY(i);

            u += coords.u;
            v += coords.v;

            this._mesh.geometry.attributes.uv.setXY(i, u, v);
        
        }
        this._mesh.geometry.attributes.uv.needsUpdate = true;
    }

}