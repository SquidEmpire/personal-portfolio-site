import {Globals} from "../core.js";
import { GameObject } from "./gameObject.js";
import * as THREE from '../vendor/three.module.js';

export class SceneObject extends GameObject {

    _mesh;

    constructor(xScale, yScale, material = null) {
        super();
        if (material === null) {
            const map = new THREE.TextureLoader().load( 'images/placeholder.png' );
            material = new THREE.MeshBasicMaterial({ map: map, transparent: true });
        }

        //using a plane instead of a sprite for culling and (future?) lighting
        const geometry = new THREE.PlaneGeometry(1, 1, 1);
        this._mesh = new THREE.Mesh( geometry, material );
        this._mesh.lookAt(Globals.camera.position);
        this.root.add(this._mesh);
        this.setScale(xScale, yScale);
    }

    initialize( x = 0, y = 0, z = 0 ) {
        super.initialize(x, y, z);
        Globals.scene.add(this.root);
    }

    destroy() {
        Globals.scene.remove(this.root);
        this._mesh.material.dispose();
        this._mesh.geometry.dispose();
        super.destroy();
    }

    setScale(xScale, yScale) {
        this._mesh.scale.set( xScale, yScale, 1 );
    }

    setMaterial(material) {
        this._mesh.material = material;
    }

    setUvFromCoords(coords) {
        for (let i = 0; i < this._mesh.geometry.attributes.uv.count; i++) {

            let u = this._mesh.geometry.attributes.uv.getX(i);
            let v = this._mesh.geometry.attributes.uv.getY(i);

            u += coords.u;
            v += coords.v;

            this._mesh.geometry.attributes.uv.setXY(i, u, v);
        
        }
        this._mesh.geometry.attributes.uv.needsUpdate = true;
    }

    moveTo(x, y, z) {
        this.root.position.setX(x);
        this.root.position.setY(y);
        this.root.position.setZ(z);
    }

}