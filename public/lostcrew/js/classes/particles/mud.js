import {Globals} from "../../core.js";
import * as HELPER from "../../helper.js";
import * as THREE from "../../vendor/three.module.js";
import { RandomSpriteObject } from "../randomSpriteObject.js";

export class Mud extends RandomSpriteObject {

    lifetime;
    remainingLifetime;

    xSpeed;
    zSpeed;
    gravity = 0.0008;

    xMin;
    xMax;
    zMin;
    zMax;

    constructor(xMin, xMax, zMin, zMax, lifeTime = 850) {
        super(2, 2, 'mudParticles');
        this.lifetime = lifeTime;
        this.xMin = xMin;
        this.xMax = xMax;
        this.zMin = zMin;
        this.zMax = zMax;
    }

    initialize( x = 0, y = 0, z = 0 ) {
        super.initialize(x, y, z);
        this.remainingLifetime = this.lifetime;
        //velocity will be dependant on the current speed
        this.xSpeed = HELPER.getRandom(0.01, 0.09) + Globals.speed * 0.03;
        this.zSpeed = -(HELPER.getRandom(0.01, 0.06) + Globals.speed * 0.01);
        //we want the y value to either be -1 (behind of tank) or 1 (in front of tank)
        let yValue = HELPER.getRandomBool() ? -1 : 1;
        this.root.position.set(HELPER.getRandom(this.xMin, this.xMax), yValue, HELPER.getRandom(this.zMin, this.zMax));
    }

    onLifetimeEnd() {
        if (Globals.speed == 0) {
            this._mesh.visible = false;
        } else {
            this._mesh.visible = true;
        }
        this.xSpeed = HELPER.getRandom(0.02, 0.1) + Globals.speed * 0.03;
        this.zSpeed = -(HELPER.getRandom(0.02, 0.08) + Globals.speed * 0.02);
        let yValue = HELPER.getRandomBool() ? -1 : 1;
        this.root.position.set(HELPER.getRandom(this.xMin, this.xMax), yValue, HELPER.getRandom(this.zMin, this.zMax));
        this.randomiseSprite();
        this.remainingLifetime = this.lifetime;
    }

    onGameTick(delta) {
        super.onGameTick(delta);
        if (this.remainingLifetime > 0) {
            const newX = this.root.position.x + this.xSpeed;
            const newZ = this.root.position.z + this.zSpeed;
            this.zSpeed += this.gravity;
            this.root.position.set(newX, this.root.position.y, newZ);
            this.remainingLifetime -= delta;
        } else {
            this.onLifetimeEnd();
        }        
    }

}