import {Globals} from "../../core.js";
import { AnimatedSpriteObject } from "../animatedSpriteObject.js";
import * as HELPER from '../../helper.js';

export class Dust extends AnimatedSpriteObject {

    initialMinX;
    initialMaxX;
    initialMinY;
    initialMaxY;
    initialMinZ;
    initialMaxZ;

    constructor() {
        super(5, 5, 50, false, 'dust');
    }

    initialize( minX, minY, minZ, maxX = null, maxY = null, maxZ = null) {
        maxX = maxX === null ? minX : maxX;
        maxY = maxY === null ? minY : maxY;
        maxZ = maxZ === null ? minZ : maxZ;

        super.initialize(minX, minY, minZ);

        this.initialMinX = minX;
        this.initialMinY = minY;
        this.initialMinZ = minZ;

        this.initialMaxX = maxX;
        this.initialMaxY = maxY;
        this.initialMaxZ = maxZ;

        this.resetPositionToRandomInRange();
    }

    resetPositionToRandomInRange() {
        const x = HELPER.getRandom(this.initialMinX, this.initialMaxX);
        const y = HELPER.getRandomInt(this.initialMinY, this.initialMaxY); //y values are int locked beacause y is our depth sorting layer
        const z = HELPER.getRandom(this.initialMinZ, this.initialMaxZ);

        this.root.position.set(x, y, z);
    }

    onAnimationEnd() {
        this.resetPositionToRandomInRange();
        if (Globals.speed == 0) {
            this._mesh.visible = false;
        } else {
            this._mesh.visible = true;
        }
    }

    onGameTick(delta) {
        super.onGameTick(delta);
        const newX =  this.root.position.x + (Math.random() * 0.03) + Globals.speed * 0.05;
        const newZ =  this.root.position.z - (Math.random() * 0.03);
        this.root.position.set(newX, this.root.position.y, newZ);
    }

}