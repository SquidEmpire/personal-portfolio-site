import {Globals} from "../../core.js";
import { AnimatedSpriteObject } from "./../animatedSpriteObject.js";

export class Smoke extends AnimatedSpriteObject {

    initialX;
    initialY;
    initialZ;

    constructor() {
        super(2, 2, 20, false, 'smoke1');
    }

    initialize( x = 0, y = 0, z = 0 ) {
        super.initialize(x, y, z);
        this.initialX = x;
        this.initialY = y;
        this.initialZ = z;
    }

    onAnimationEnd() {
        this.root.position.set(this.initialX, this.initialY, this.initialZ);
        if (Globals.speed == 0) {
            this._mesh.visible = false;
        } else {
            this._mesh.visible = true;
        }
    }

    onGameTick(delta) {
        super.onGameTick(delta);
        const newX =  this.root.position.x + (Math.random() * 0.03) + Globals.speed * 0.05;
        const newZ =  this.root.position.z - (Math.random() * 0.03);
        this.root.position.set(newX, this.initialY, newZ);
    }

}