import { Asset } from "./asset.js";

class SpritesheetAsset extends Asset {

    size;
    rows;
    columns;
    numberOfSprites;

    constructor( name, path, size ) {
        const type = "spritesheet";
        super( name, path, type );
        this.type = type;
        this.size = size;
    }

    clone() {
        let clone = new Asset(this.name, this.path, this.type);
        clone.setContents(this.contents.clone());
        return clone;
    }

    getUvCoordsForIndex( index ) {
        const u = index % this.columns;
        const v = Math.floor(index / this.columns);
        return {u: u, v: v};
    }

};

export { SpritesheetAsset };