export class InventoryItem {
    name;
    isFlammable;
    isPerishable;
    description;
    isLiquid;

    maxCountPerBin; //count for discrete, litres for liquids

    constructor(name, isLiquid, isFlammable, isPerishable, maxCountPerBin, description)
    {
        this.name = name;
        this.isFlammable = isFlammable;
        this.isPerishable = isPerishable;
        this.description = description;
        this.isLiquid = isLiquid;
        this.maxCountPerBin = maxCountPerBin;
    }
}