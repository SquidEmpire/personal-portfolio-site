import {Globals} from "../core.js";
import { AnimatedSpriteObject } from "./animatedSpriteObject.js";
import * as THREE from '../vendor/three.module.js';
import { Smoke } from "./particles/smoke.js";
import { Dust } from "./particles/dust.js";
import { Mud } from "./particles/mud.js";
import * as HELPER from '../helper.js';

export class Tank extends AnimatedSpriteObject {

    engineSmokes = [];
    dusts = [];
    muds = [];
    smokeCount = 10;
    dustCount = 10;
    mudCount = 0; //mud doesn't look that good, not worth
    smokeInitialisationIndex = 0;
    dustInitialisationIndex = 0;
    mudInitialisationIndex = 0;
    
    extBackItem1Plane;
    extBackItem2Plane;
    extFrontItem1Plane;
    extFrontItem2Plane;

    constructor() {
        super(20, 20, 10, true, 'tank');
    }

    initialize( x = 0, y = 0, z = 0 ) {
        super.initialize(x, y, z);
        this._mesh.position.set(0, 0, -7.3);

        for (let i = 0; i < this.smokeCount; i++) {
            const smoke = new Smoke();
            this.engineSmokes.push(smoke);
        }
        for (let i = 0; i < this.dustCount; i++) {
            const dust = new Dust();
            this.dusts.push(dust);
        }
        for (let i = 0; i < this.mudCount; i++) {
            const mud = new Mud(-4, 5, 1.8, 2.5);
            this.muds.push(mud);
        }

        let spritesheetAsset = Globals.assets.find(x => x.name === 'extBackBinItem1');
        let material = new THREE.MeshBasicMaterial({ map: spritesheetAsset.contents, transparent: true });
        let geometry = new THREE.PlaneGeometry(1, 1, 1);
        this.extBackItem1Plane  = new THREE.Mesh( geometry, material );
        this.extBackItem1Plane.lookAt(Globals.camera.position);
        this.extBackItem1Plane.scale.set( 20, 20, 1 );
        this.extBackItem1Plane.position.set(0, 0, -7.3);

        spritesheetAsset = Globals.assets.find(x => x.name === 'extBackBinItem2');
        material = new THREE.MeshBasicMaterial({ map: spritesheetAsset.contents, transparent: true });
        geometry = new THREE.PlaneGeometry(1, 1, 1);
        this.extBackItem2Plane = new THREE.Mesh( geometry, material );
        this.extBackItem2Plane.lookAt(Globals.camera.position);
        this.extBackItem2Plane.scale.set( 20, 20, 1 );
        this.extBackItem2Plane.position.set(0, 0, -7.3);

        spritesheetAsset = Globals.assets.find(x => x.name === 'extFrontBinItem1');
        material = new THREE.MeshBasicMaterial({ map: spritesheetAsset.contents, transparent: true });
        geometry = new THREE.PlaneGeometry(1, 1, 1);
        this.extFrontItem1Plane = new THREE.Mesh( geometry, material );
        this.extFrontItem1Plane.lookAt(Globals.camera.position);
        this.extFrontItem1Plane.scale.set( 20, 20, 1 );
        this.extFrontItem1Plane.position.set(0, 0, -7.3);

        spritesheetAsset = Globals.assets.find(x => x.name === 'extFrontBinItem2');
        material = new THREE.MeshBasicMaterial({ map: spritesheetAsset.contents, transparent: true });
        geometry = new THREE.PlaneGeometry(1, 1, 1);
        this.extFrontItem2Plane = new THREE.Mesh( geometry, material );
        this.extFrontItem2Plane.lookAt(Globals.camera.position);
        this.extFrontItem2Plane.scale.set( 20, 20, 1 );
        this.extFrontItem2Plane.position.set(0, 0, -7.3);

        this.root.add(this.extBackItem1Plane);
        this.root.add(this.extBackItem2Plane);
        this.root.add(this.extFrontItem1Plane);
        this.root.add(this.extFrontItem2Plane);
    }

    onGameTick(delta) {
        super.onGameTick(delta);
        this.root.position.set(Math.random() * Globals.speed * 0.05, 0, (Math.random() * Globals.speed) / 2 * 0.01);

        if (delta % 5 === 0)
        {
            if (this.smokeInitialisationIndex < this.smokeCount) {
                const smoke = this.engineSmokes[this.smokeInitialisationIndex];
                if (!smoke.isInitialised) {
                    smoke.initialize( HELPER.getRandom(6.2, 7), 0, HELPER.getRandom(-1.5, -0.8) );
                } 
                this.smokeInitialisationIndex++;
            }

            if (this.dustInitialisationIndex < this.dustCount) {
                const dust = this.dusts[this.dustInitialisationIndex];
                if (!dust.isInitialised) {
                    dust.initialize( -4, -1, 1.8, 5, 0, 2.5);
                }
                this.dustInitialisationIndex++;
            }

            if (this.mudInitialisationIndex < this.mudCount) {
                const mud = this.muds[this.mudInitialisationIndex];
                if (!mud.isInitialised) {
                    mud.initialize();
                }
                this.mudInitialisationIndex++;
            }
        }
    }

}