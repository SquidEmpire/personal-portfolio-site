import {Globals} from "../core.js";
import { GameObject } from "./gameObject.js";
import * as THREE from '../vendor/three.module.js';

export class AnimatedSpriteObject extends GameObject {

    _mesh;
    _index = 0;
    _spritesheetAsset;
    _ticksSinceLastAnimation = 0;
    _animationSpeed;
    _originalUVs;
    _useTankSpeed;

    constructor(xScale = 1, yScale = 1, animationSpeed = 10, useTankSpeed = false, spritesheetName = "placeholder") {
        super();
        this._spritesheetAsset = Globals.assets.find(x => x.name === spritesheetName);
        const material = new THREE.MeshBasicMaterial({ map: this._spritesheetAsset.contents, transparent: true });

        this._useTankSpeed = useTankSpeed;
        this._animationSpeed = animationSpeed;

        //using a plane instead of a sprite for culling and lighting
        const geometry = new THREE.PlaneGeometry(1, 1, 1);
        this._mesh = new THREE.Mesh( geometry, material );
        this._mesh.lookAt(Globals.camera.position);
        this.root.add(this._mesh);
        this.setScale(xScale, yScale);
    }

    initialize( x = 0, y = 0, z = 0 ) {
        super.initialize(x, y, z);
        this._originalUVs = this._mesh.geometry.attributes.uv.clone();
        Globals.scene.add(this.root);
    }

    setScale(xScale, yScale) {
        this._mesh.scale.set( xScale, yScale, 1 );
    }

    onAnimationEnd() {
        
    }

    onGameTick(delta) {
        const speedModifier = this._useTankSpeed ? Globals.speed : 1;
        if (this._ticksSinceLastAnimation * speedModifier > this._animationSpeed ) {
            if (this._index === this._spritesheetAsset.numberOfSprites - 1) {
                this.onAnimationEnd();
                this._index = 0;
            } else {
                this._index = this._index + 1;
            }

            const nextUvCoords = this._spritesheetAsset.getUvCoordsForIndex(this._index);
            this.setUvFromCoords(nextUvCoords);

            this._ticksSinceLastAnimation = 0;

        } else {
            this._ticksSinceLastAnimation += 1;
        }
    }

    setUvFromCoords(coords) {
        for (let i = 0; i < this._originalUVs.count; i++) {

            let u = this._originalUVs.getX(i);
            let v = this._originalUVs.getY(i);

            u += coords.u;
            v += coords.v;

            this._mesh.geometry.attributes.uv.setXY(i, u, v);
        
        }
        this._mesh.geometry.attributes.uv.needsUpdate = true;
    }

}