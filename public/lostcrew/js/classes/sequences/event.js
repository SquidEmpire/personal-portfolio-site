
//an event is a subcomponent of a sequence
//each event has a condition that upon fufillment concludes the event
export class Event {

    isCompleted = false;
    onComplete = () => {}; //arrow function for custom actions during complete
    conditions = [];
    repeatable = false; //shouldn't ever really be used for events but included to make sequence extension easier

    constructor() {
        
    }

    start() {
        this.isCompleted = false;
    }

    reset() {
        this.isCompleted = false;
    }

    complete() {
        this.onComplete();
        this.isCompleted = true;
    }

    step(delta) {

    }

    //events can be conditional
    isAvailable() {
        if (this.isCompleted && !this.repeatable) {
            return false;
        }
        let allConditionsMet = true;
        for (let i = 0; i < this.conditions.length; i++) {
            if (!this.conditions[i].getIsMet()) {
                allConditionsMet = false;
                break;
            }
        }
        return allConditionsMet;
    }

}