
//a condition is a subcomponent of a sequence
//each condition contains a check to determine whether it is met
export class Condition {

    constructor() {
        
    }

    getIsMet() {
        return true;
    }

}