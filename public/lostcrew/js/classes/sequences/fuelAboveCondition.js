import { Condition } from "./condition.js";
import { Globals } from "../../core.js";

export class FuelAboveCondition extends Condition {

    requiredMinFuel;

    constructor(requiredMinFuel) {
        super();
        this.requiredMinFuel = requiredMinFuel;
    }

    getIsMet() {
        return (Globals.tankController.getFuelLevel() >= this.requiredMinFuel);
    }

}