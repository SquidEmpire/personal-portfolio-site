import { Condition } from "./condition.js";
import { Globals } from "../../core.js";

export class DistanceSinceLastSequenceCondition extends Condition {

    requiredDistance;

    constructor(requiredDistance) {
        super();
        this.requiredDistance = requiredDistance;
    }

    getIsMet() {
        return (Globals.sequenceController.distanceSinceLastSequencePlayed >= this.requiredDistance);
    }

}