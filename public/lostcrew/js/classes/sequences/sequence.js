import { Event } from "./event.js";
import { Condition } from "./condition.js";


//a sequence is a series of events that must be completed in order to consider the sequence complete
//it behaves like an event itself
//sequences can have conditions as well which must be met to allow the sequence to be run
//each sequence must be self contained, i.e. sequences should not be designed to run in a paticular order
export class Sequence extends Event {

    repeatable = false; //whether a sequence can be run multiple times
    eventIndex = 0;
    events = []; //queue of events to run in order - note that events can also be used as 'effects' of e.g. a splash event
    conditions = []; //an array of conditions to be met before this sequence can be started
    currentEvent = null;

    constructor(...args) {
        super();
        //args should be an array of conditions and events
        for (let i = 0; i < args.length; i++) {
            const arg = args[i];
            if (arg instanceof Event) {
                this.events.push(arg);
            } else if (arg instanceof Condition) {
                this.conditions.push(arg);
            } else {
                console.error("Invalid (not event or condition) argument passed to sequence: ", arg);
            }
        }
    }

    start() {
        this.isCompleted = false;
        for (let i = 0; i < this.events.length; i++) {
            this.events[i].reset();
        }
        this.eventIndex = 0;
        this.startNextEvent();
    }

    //events may be conditional. If the next event's conditions are not met it is skipped
    startNextEvent() {
        if (this.events.length && this.eventIndex < this.events.length) {
            while (this.eventIndex < this.events.length) {
                this.currentEvent = this.events[this.eventIndex];
                if (this.currentEvent.isAvailable()) {
                    this.currentEvent.start();
                    return;
                }
                this.eventIndex++;
            }
        }
        //if there's no next (ready) event
        this.complete();
    }

    step(delta) {
        if (this.currentEvent.isCompleted) {
            this.startNextEvent();
        } else {
            this.currentEvent.step(delta);
        }
    }

}