import { Condition } from "./condition.js";
import { Globals } from "../../core.js";

export class SpeedCondition extends Condition {

    requiredMinSpeed;
    requiredMaxSpeed;

    constructor(requiredMinSpeed, requiredMaxSpeed) {
        super();
        this.requiredMinSpeed = requiredMinSpeed;
        this.requiredMaxSpeed = requiredMaxSpeed;
    }

    getIsMet() {
        return (Globals.speed >= this.requiredMinSpeed && Globals.speed <= this.requiredMaxSpeed);
    }

}