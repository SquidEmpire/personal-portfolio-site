import { Event } from "./event.js";
import { Globals } from "../../core.js";

//this event changes the tank's target speed
export class SpeedChangeEvent extends Event {

    targetSpeed;

    constructor(targetSpeed) {
        super();
        this.targetSpeed = targetSpeed;
    }

    step(delta) {
        Globals.targetSpeed = this.targetSpeed;
        this.complete();
    }

}