import { Condition } from "./condition.js";
import { Globals } from "../../core.js";

export class SpeedNotCondition extends Condition {

    speed;

    constructor(speed) {
        super();
        this.speed = speed;
    }

    getIsMet() {
        return (Globals.speed !== this.speed);
    }

}