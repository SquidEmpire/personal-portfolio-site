import { Event } from "./event.js";

//this event is completed after a certain time
export class TimedEvent extends Event {

    remainingTime = 0; //milliseconds
    initialTime = 0;

    constructor(duration) {
        super();
        this.initialTime = duration;
        this.remainingTime = duration; //milliseconds
    }

    reset() {
        super.reset();
        this.remainingTime = this.initialTime;
    }

    step(delta) {
        if (this.remainingTime > 0) {
            this.remainingTime -= delta;
        } else {
            this.remainingTime = 0;
            this.complete();
        }
    }

}