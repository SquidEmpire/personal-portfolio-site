import { Event } from "./event.js";

//this is a choice option for a splash event that can be clicked to resolve the event
export class SplashEventChoice {

    text;
    id; //the id is used by other events to know what option was chosen. It only needs to be unique per event (not globally)
    conditions = []; //splash choice events can have conditions that must be passed to allow this choice to be picked

    constructor(id, text, conditions = []) {
        this.id = id;
        this.text = text;
        this.conditions = conditions;
        if (!this.id || !this.text) {
            console.error("Bad value/s passed to splash event choice!", this.id, this.text);
        }
    }

    isAvailable() {
        let allConditionsMet = true;
        for (let i = 0; i < this.conditions.length; i++) {
            if (!this.conditions[i].getIsMet()) {
                allConditionsMet = false;
                break;
            }
        }
        return allConditionsMet;
    }

}