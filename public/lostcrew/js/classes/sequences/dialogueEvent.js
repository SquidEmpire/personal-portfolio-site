import {Globals} from "../../core.js";
import { TimedEvent } from "./timedEvent.js";
import * as HELPER from "../../helper.js";

//this event displays dialogue for a given character for a given time
export class DialogueEvent extends TimedEvent {

    character; //commander, driver, or gunner
    text; //either a string or an array of strings to randomly choose from

    constructor(character, text, duration = 5000) {
        super(duration); //5 seconds duration by default
        this.character = character;
        this.text = text;
    }

    start() {
        let text = "";
        if (this.text instanceof Array) {
            text = HELPER.getRandomValueFromArray(this.text);
        } else {
            text = this.text;
        }
        switch (this.character) {
            case "commander" :
                Globals.uiController.setCommanderSpeech(text);
                break;
            case "driver" :
                Globals.uiController.setDriverSpeech(text);
                break;
            case "gunner" :
                Globals.uiController.setGunnerSpeech(text);
                break;
            default: 
                console.error(`unidentified character for dialogue - "${this.character}"`);
                this.complete();
        }
    }

    complete() {
        super.complete();
        switch (this.character) {
            case "commander" :
                Globals.uiController.setCommanderSpeech("");
                break;
            case "driver" :
                Globals.uiController.setDriverSpeech("");
                break;
            case "gunner" :
                Globals.uiController.setGunnerSpeech("");
                break;
            default: 
                return; //no need to error again from start()
        }
    }

}