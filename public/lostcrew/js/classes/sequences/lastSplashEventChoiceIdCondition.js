import { Condition } from "./condition.js";
import { Globals } from "../../core.js";

export class LastSplashEventChoiceIdCondition extends Condition {

    requiredId;

    constructor(requiredId) {
        super();
        this.requiredId = requiredId;
    }

    getIsMet() {
        return (Globals.sequenceController.lastSplashEventChoiceId === this.requiredId);
    }

}