import { Globals } from "../../core.js";
import { Event } from "./event.js";
import { SplashEventChoice } from "./splashEventChoice.js";

//this is a complex event that displays a title, text, and optional image in a splash with an OK button
//if splashEventChoices are passed in, they are added as buttons
export class SplashEvent extends Event {

    title;
    text;
    image;
    choices;

    constructor(title, text, image = null, conditions = [], choices = []) {
        super();
        this.title = title;
        this.text = text;
        this.image = image;
        this.choices = choices;
        this.conditions = conditions;

        if (this.choices instanceof Array) {
            if (this.choices.length === 0) {
                this.choices.push(new SplashEventChoice(1, "Ok"));
            }
        } else {
            this.choices = [];
        }
    }

    start() {
        super.start();
        Globals.uiController.displaySplashEvent(this);
    }

    onChoiceMade(choice) {
        this.complete();
        Globals.sequenceController.lastSplashEventChoiceId = choice.id;
        Globals.uiController.hideSplashEvent();
    }

    reset() {
        super.reset();
    }
}