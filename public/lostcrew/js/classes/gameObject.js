import {Globals} from "../core.js";
import * as HELPER from "../helper.js";
import * as THREE from '../vendor/three.module.js';

class GameObject {

    uuid;
    isDestroyed = false;
    root;
    isInitialised = false;

    constructor(  ) {
        this.uuid = HELPER.getUUID();
        this.root = new THREE.Group();
        this.root.name= "root";
    }

    initialize(x = 0, y = 0, z = 0) {
        this.root.position.set(x, y, z);
        Globals.gameObjects.set(this.uuid, this);
        this.isInitialised = true;
    }

    destroy() {
        Globals.gameObjects.delete(this.uuid);
        this.isDestroyed = true;
    }

    onGameTick(delta) {

    }

    onGameTickAlways(delta) {
        if (Globals.paused == false) {
            this.onGameTick(delta);
        }
    }

};

export { GameObject };