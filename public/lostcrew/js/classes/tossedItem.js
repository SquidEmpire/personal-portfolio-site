import {Globals} from "../core.js";
import * as THREE from '../vendor/three.module.js';
import { SceneObject } from "./sceneObject.js";
import * as HELPER from '../helper.js';

export class TossedItem extends SceneObject {

    _velocity;
    _grav = 9.8;
    _rotataion;
    _fallenHeight;
    _lifetime;

    constructor(isLiquid = false) {
        const scale = 3;
        let map;
        if (isLiquid) {
            map = Globals.assets.find(x => x.name === 'barrel').contents;
        } else {
            map = Globals.assets.find(x => x.name === 'crate').contents;
        }
        const material = new THREE.MeshBasicMaterial({ map: map, transparent: true, depthWrite: false, depthTest: false });
        super(scale, scale, material);

        this._inAir = true;
        this._velocity = new THREE.Vector3(0.03, 0, -0.15);
        this._rotation = HELPER.getRandom(-0.06, 0.06);
        this._fallenHeight = HELPER.getRandom(3, 7);
    }

    initialize() {
        //start at the tank turret
        this._lifetime = 5000;
        super.initialize(1.5, 1, -5.5);
    }

    onGameTick(delta) {
        if (this._lifetime > 0) {
            if (this.root.position.z < this._fallenHeight) {
                this._velocity.z += 0.0004 * delta;
                this.root.rotateY(this._rotation);
            } else {
                //once this item has hit the ground it needs to move across like a terrain fragment
                this._velocity.z = 0;
                this._velocity.x = Globals.speed / 400 * delta;
            }
            this.root.position.add(this._velocity);
            this._lifetime--;
        } else {
            this.destroy();
        }        
    }
}