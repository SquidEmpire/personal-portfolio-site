import {Globals} from "./core.js";
import { GameObject } from "./classes/gameObject.js";
import * as THREE from "./vendor/three.module.js";
import * as HELPER from './helper.js';
import { Sequence } from "./classes/sequences/sequence.js";
import { dataSequences } from "./data/sequences.js";
import { repeatableDataSequences } from "./data/repeatableSequences.js";

//the sequence controller decides when to run a sequence from the queue it has
//it will decide to run sequences randomly after some given time gap
export class SequenceController extends GameObject {

    lastSplashEventChoiceId = 0; //this is the ONE evil thing I'm doing in this project
    //the way the multi choice splash events know what option was picked on the last splash event is via
    //this global level variable that remembers. It would be better to have each multi choice splash events
    //track the progress inside itself, but that would require a much more robust system. This is the quickfix

    sequences = [];
    currentSequence = null;
    pausedSequence = null;
    distanceSinceLastSequencePlayed = 0;
    timeSinceLastSequencePlayed = 0;
    //each tick a random number between 0 and timeSinceLastSequencePlayed is generated, if it's higher than this, start a new sequence
    //this means that there's a random time between sequences, but the longer the gap is the less likely it is to wait any longer
    chanceToStartSequence = 10000;

    constructor() {
        super();
    }

    initialize( x = 0, y = 0, z = 0 ) {
        super.initialize(x, y, z);
        
        //import the list of repeatable sequences
        for (var i = 0; i < repeatableDataSequences.length; i++) {
            repeatableDataSequences[i].repeatable = true;
            this.addSequence(repeatableDataSequences[i]);
        }

        //import the list of normal sequences
        for (var i = 0; i < dataSequences.length; i++) {
            this.addSequence(dataSequences[i]);
        }

        //randomly shuffle the sequences
        HELPER.shuffleArray(this.sequences);
        
    }

    addSequence(sequence) {
        if (sequence instanceof Sequence) {
            this.sequences.push(sequence);
        } else {
            console.error("Non-sequence passed as sequence to sequence controller: ", sequence);
        }
    }

    //run a sequence immediatly, bypassing the queue
    //if a sequence is already running, the running sequence is paused and resumed afterwards
    //TODO: pausing and resuming sequences is not good if the player spams the control buttons! Disabled for now
    runSequenceImmediately(sequence, forceRun = false) {
        if (sequence instanceof Sequence) {
            if (sequence.isAvailable() || forceRun) {
                if (this.currentSequence) {
                    //this.pausedSequence = this.currentSequence;
                    this.currentSequence.complete();
                }
                this.currentSequence = sequence;
                this.currentSequence.start();
            } else {
                //sequence to be force runned wasn't ready
                //console.warn("Attempted to immedietly run a sequence that wasn't available", sequence);
            } 
        } else {
            console.error("Non-sequence passed as sequence to sequence controller: ", sequence);
        }
    }

    //take the next valid event sequence from the queue and start it
    chooseNextSequence() {
        //if there's a paused sequence always use that first
        if (this.pausedSequence) {
            this.currentSequence = this.pausedSequence;
            this.pausedSequence = null;
            return true;
        }
        //randomly shuffle the sequences before choosing one
        HELPER.shuffleArray(this.sequences);
        for (var i = 0; i < this.sequences.length; i++) {
            const sequence = this.sequences[i];
            if (sequence.isAvailable()) {
                this.currentSequence = sequence;
                this.currentSequence.start();
                return true;
            }
        }
        //no valid sequences found!!!
        return false
    }



    onGameTick(delta) {
        if (this.currentSequence && !this.currentSequence.isCompleted) {
            this.currentSequence.step(delta);
        } else {
            if (HELPER.getRandom(0, this.timeSinceLastSequencePlayed) > this.chanceToStartSequence) {
                if (this.chooseNextSequence()) {
                    this.timeSinceLastSequencePlayed = 0;
                    this.distanceSinceLastSequencePlayed = 0;
                }
            }
            this.timeSinceLastSequencePlayed += delta;
            this.distanceSinceLastSequencePlayed += delta * Globals.speed;
        }
    }

}