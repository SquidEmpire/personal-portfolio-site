import {Globals} from "./core.js";
import { Tank } from "./classes/tank.js";
import { GameObject } from "./classes/gameObject.js";
import * as THREE from "./vendor/three.module.js";
import * as HELPER from './helper.js';

export class GameController extends GameObject {

    tank;

    remainingFuel = 68;
    fuelConsumptionModifier = 0.0001; //fuel consumption rate = speed * modifier / delta

    _timeSpentAccelerating = 0;
    _acceleration = 100000;

    sequences = []; //queue of sequences to play through

    constructor() {
        super();
    }

    initialize( x = 0, y = 0, z = 0 ) {
        super.initialize(x, y, z);
        this.tank = new Tank();
        this.tank.initialize();

        //start us off already moving at normal speed
        Globals.targetSpeed = 1;
        Globals.speed = 1;
    }

    onGameTick(delta) {

        //mess around with the camera if we're moving, why not
        if (Globals.speed > 0) {
            const xFuzz = HELPER.getRandom(-Globals.speed, Globals.speed) * 0.01;
            const yFuzz = HELPER.getRandom(-Globals.speed, Globals.speed) * 0.01;
            Globals.camera.position.set( xFuzz, Globals.camera.position.y, yFuzz );
        } else {
            Globals.camera.position.set( 0, 10, 0 );
        }

        //change speed if needed
        if (Globals.speed != Globals.targetSpeed) {

            if (Math.abs(Globals.targetSpeed - Globals.speed) < 0.1) {
                Globals.speed = Globals.targetSpeed;
                return;
            }

            const lerpFactor = this._timeSpentAccelerating / this._acceleration;
            THREE.MathUtils.clamp(lerpFactor, 0, 1);
            
            Globals.speed = THREE.MathUtils.lerp(Globals.speed, Globals.targetSpeed, lerpFactor);
            this._timeSpentAccelerating += delta;

        } else {
            this._timeSpentAccelerating = 0;
        }

        //consume some fuel
        if (this.remainingFuel > 0) {
            this.remainingFuel -= (Globals.speed * this.fuelConsumptionModifier);
        } else {
            Globals.targetSpeed = 0;
        }

    }

    getFuelLevel() {
        return this.remainingFuel;
    }

}