import {Globals} from "./core.js";
import * as THREE from './vendor/three.module.js';
import { GameObject } from "./classes/gameObject.js";
import { SceneObject } from "./classes/sceneObject.js";
import * as HELPER from './helper.js';

export class SceneController extends GameObject {

    sceneXBounds = {min: 0, max: 0};
    sceneWidth = 0;

    //scale for fragments
    terrainFragmentWidth = 5;
    objectScale = 5;

    //this is how much terrain to render before and after the center,
    //note that if the user has a massively long screen they might see off the end
    terrainRenderCount = 100;

    midgroundObjectCount = 1000;
    foregroundObjectCount = 10;

    groundplane;
    foregroundplane;

    foregroundObjects = []; //things that move in front of the scene
    terrainFragments = []; //the ground under the tank
    midgroundObjects = []; //things behind the scene
    lowMountainFragments = []; //distant foothills
    clouds = []; //clouds in the sky
    explosions = []; //horizon glows from distant explosions
    explosionColours = [new THREE.Color(0xFF2714), new THREE.Color(0xFF6030), new THREE.Color(0xFF4823)]; //valid colours for the explosion gradients

    scenerySpritesheetAsset;
    cloudSpritesheetAsset;

    finalFragmentPosition = this.terrainRenderCount * 0.5 * this.terrainFragmentWidth;
    firstFragmentPosition = -this.finalFragmentPosition;
    fragmentBoundary = this.finalFragmentPosition;

    //ash/rain particle system
    particles;

    constructor() {
        super();

        const groundmaterial = new THREE.MeshBasicMaterial({ color: 0x140E0A });
        const geometry = new THREE.PlaneGeometry(500, 30, 1);
        this.groundplane = new THREE.Mesh( geometry, groundmaterial );
        this.groundplane.lookAt(Globals.camera.position);
        this.groundplane.position.setY(-101);
        this.groundplane.position.setZ(10);
        Globals.scene.add(this.groundplane);

        this.foregroundplane = new THREE.Mesh( geometry, groundmaterial );
        this.foregroundplane.lookAt(Globals.camera.position);
        this.foregroundplane.position.setZ(22.5);
        Globals.scene.add(this.foregroundplane);

        //load  materials

        const mudmap = new THREE.TextureLoader().load( 'images/mud.png' );
        const mudMaterial = new THREE.MeshBasicMaterial({map: mudmap, transparent: true});
        const skylineSpritesheetAsset = Globals.assets.find(x => x.name === 'skyline');
        const hillsMaterial = new THREE.MeshBasicMaterial({map: skylineSpritesheetAsset.contents, transparent: true});

        this.scenerySpritesheetAsset = Globals.assets.find(x => x.name === 'scenery');
        const sceneryMaterial = new THREE.MeshBasicMaterial({map: this.scenerySpritesheetAsset.contents, transparent: true});

        this.cloudSpritesheetAsset = Globals.assets.find(x => x.name === 'cloud');
        const cloudMaterial = new THREE.MeshBasicMaterial({map: this.cloudSpritesheetAsset.contents, transparent: true});

        const gradientMap = Globals.assets.find(x => x.name === 'gradient');

        //populate the initial lists
        for (let i = 0; i < this.terrainRenderCount; i++) {
            const xPosition =  (i * this.terrainFragmentWidth) - (this.terrainRenderCount * 0.5 * this.terrainFragmentWidth);
            this.terrainFragments[i] = new SceneObject(this.terrainFragmentWidth, this.terrainFragmentWidth, mudMaterial);
            this.terrainFragments[i].initialize();
            this.terrainFragments[i].moveTo(xPosition, 0, this.terrainFragmentWidth);
        }

        for (let i = 0; i < this.terrainRenderCount; i++) {
            const xPosition =  (i * this.terrainFragmentWidth) - (this.terrainRenderCount * 0.5 * this.terrainFragmentWidth);
            this.lowMountainFragments[i] = new SceneObject(this.terrainFragmentWidth, this.terrainFragmentWidth, hillsMaterial);
            this.lowMountainFragments[i].initialize();
            this.lowMountainFragments[i].moveTo(xPosition, -60, -3);
            const randomUvCoords = skylineSpritesheetAsset.getUvCoordsForIndex(HELPER.getRandomInt(0, skylineSpritesheetAsset.numberOfSprites - 1));
            this.lowMountainFragments[i].setUvFromCoords(randomUvCoords);
        }

        for (let i = 0; i < 10; i++) {
            const xPosition = HELPER.getRandom(-50, 50);
            const yPosition = -400;
            const zPosition = -5;
            const colour = HELPER.getRandomValueFromArray(this.explosionColours);
            const newGradientMaterial = new THREE.MeshBasicMaterial({map: gradientMap.contents, transparent: true, color: colour});
            this.explosions[i] = new SceneObject(18, 4, newGradientMaterial);
            this.explosions[i].initialize();
            this.explosions[i].moveTo(xPosition, yPosition, zPosition);
        }

        //clouds
        for (let i = 0; i < this.midgroundObjectCount; i++) {

            const xPosition = HELPER.getRandom(this.firstFragmentPosition, this.finalFragmentPosition);
            const yPosition = HELPER.getRandom(-60, -400); //distance from camera
            const zPosition = (yPosition / 20) - 2; //distance up/down - used to help fake persective

            //fake perspective scale based on distance from camera (y position)
            const scale = (Math.abs(yPosition) / this.objectScale) / 15;

            this.clouds[i] = new SceneObject(scale, scale, cloudMaterial);
            this.clouds[i].initialize();
            this.clouds[i].moveTo(xPosition, yPosition, zPosition);
            const randomUvCoords = this.cloudSpritesheetAsset.getUvCoordsForIndex(HELPER.getRandomInt(0, this.cloudSpritesheetAsset.numberOfSprites - 1));
            this.clouds[i].setUvFromCoords(randomUvCoords);
        }

        //midground objects
        for (let i = 0; i < this.midgroundObjectCount; i++) {

            const xPosition = HELPER.getRandom(this.firstFragmentPosition, this.finalFragmentPosition);
            const yPosition = HELPER.getRandom(-5, -50); //distance from camera
            const zPosition = (yPosition + 10) / 8; //distance up/down - used to help fake persective

            //fake perspective scale based on distance from camera (y position)
            const scale = (this.objectScale / Math.abs(yPosition)) * 10;

            this.midgroundObjects[i] = new SceneObject(scale, scale, sceneryMaterial);
            this.midgroundObjects[i].initialize();
            this.midgroundObjects[i].moveTo(xPosition, yPosition, zPosition);
            const randomUvCoords = this.scenerySpritesheetAsset.getUvCoordsForIndex(HELPER.getRandomInt(0, this.scenerySpritesheetAsset.numberOfSprites - 1));
            this.midgroundObjects[i].setUvFromCoords(randomUvCoords);
        }

        //foreground objects
        for (let i = 0; i < this.foregroundObjectCount; i++) {

            const xPosition = HELPER.getRandom(this.firstFragmentPosition, this.finalFragmentPosition);
            const yPosition = HELPER.getRandom(2, 5); //distance from camera
            const zPosition = yPosition / 5 + (Math.random() * 5); //distance up/down - used to help fake persective
            const scale = (this.objectScale * 8);

            this.foregroundObjects[i] = new SceneObject(scale, scale, sceneryMaterial);
            this.foregroundObjects[i].initialize();
            this.foregroundObjects[i].moveTo(xPosition, yPosition, zPosition);
            const randomUvCoords = this.scenerySpritesheetAsset.getUvCoordsForIndex(HELPER.getRandomInt(0, this.scenerySpritesheetAsset.numberOfSprites - 1));
            this.midgroundObjects[i].setUvFromCoords(randomUvCoords);
        }



        // //build particle system
        // const particleGeometry = new THREE.BufferGeometry();
        // const particleVertices = [];

        // for ( let i = 0; i < 1000; i ++ ) {
        //     const x = (Math.random() * 200) - 100;
        //     const y = (Math.random() * 2) - 1;
        //     const z = (Math.random() * 200) - 100;
        //     particleVertices.push( x, y, z );
        // }
        // particleGeometry.setAttribute( 'position', new THREE.Float32BufferAttribute( particleVertices, 3 ) );

        // const particleMaterial = new THREE.PointsMaterial( { size: 2, color: 0xffffff, blending: THREE.AdditiveBlending, transparent: true } );

        // this.particles = new THREE.Points( particleGeometry, particleMaterial );
        // Globals.scene.add( this.particles );

    }

    initialize( x = 0, y = 0, z = 0 ) {
        super.initialize(x, y, z);
    }

    onGameTick(delta) {
        for (let i = 0; i < this.terrainFragments.length; i++) {

            let fragment = this.terrainFragments[i];
            let nextFragIndex = i+1 < this.terrainFragments.length ? i+1 : 0;
            let nextFrag = this.terrainFragments[nextFragIndex];

            //if this fragment has crossed the boundary move it to the start
            if (fragment.root.position.x >= this.fragmentBoundary) {
                fragment.moveTo(this.firstFragmentPosition, fragment.root.position.y, fragment.root.position.z);
                //if when we move it to the start there's a gap before the next fragment, remove the gap
                if (nextFrag.root.position.x - fragment.root.position.x !== this.terrainFragmentWidth) {
                    fragment.root.position.x = nextFrag.root.position.x - this.terrainFragmentWidth;
                }
            }
            //shuffle fragments
            fragment.moveTo(fragment.root.position.x + (Globals.speed / 400) * delta, fragment.root.position.y, fragment.root.position.z);

            //do the same for the other terrain fragments at the same time
            fragment = this.lowMountainFragments[i];
            nextFrag = this.lowMountainFragments[nextFragIndex];
            if (fragment.root.position.x >= this.fragmentBoundary) {
                fragment.moveTo(this.firstFragmentPosition, fragment.root.position.y, fragment.root.position.z);
                if (nextFrag.root.position.x - fragment.root.position.x !== this.terrainFragmentWidth) {
                    fragment.root.position.x = nextFrag.root.position.x - this.terrainFragmentWidth;
                }
            }
            fragment.moveTo(fragment.root.position.x + (Globals.speed / 40000) * delta, fragment.root.position.y, fragment.root.position.z);

            //shuffle particles
            //this.particles();

        }

        //move clouds
        for (let i = 0; i < this.clouds.length; i++) {
            
            let object = this.clouds[i];
            if (object.root.position.x >= this.fragmentBoundary) {
                //randomly move the object on wrap
                const xPosition = this.firstFragmentPosition;
                const yPosition = HELPER.getRandom(-60, -400);
                const zPosition = (yPosition / 20) - 2;
                const scale = (Math.abs(yPosition) / this.objectScale) / 15;

                this.clouds[i].setScale(scale, scale);
                this.clouds[i].moveTo(xPosition, yPosition, zPosition);
                const randomUvCoords = this.cloudSpritesheetAsset.getUvCoordsForIndex(HELPER.getRandomInt(0, this.cloudSpritesheetAsset.numberOfSprites - 1));
                this.clouds[i].setUvFromCoords(randomUvCoords);
            }
            object.moveTo(object.root.position.x + (object.root.position.y * 0.0000005) * -Globals.speed * delta + 0.0001, object.root.position.y, object.root.position.z);
        }

        //move midground objects
        for (let i = 0; i < this.midgroundObjects.length; i++) {
            
            let object = this.midgroundObjects[i];
            if (object.root.position.x >= this.fragmentBoundary) {
                //randomly move the object on wrap
                const xPosition = this.firstFragmentPosition;
                const yPosition = HELPER.getRandom(-5, -50);
                const zPosition = (yPosition + 10) / 8;
                const scale = (this.objectScale / Math.abs(yPosition)) * 10;

                this.midgroundObjects[i].setScale(scale, scale);
                this.midgroundObjects[i].moveTo(xPosition, yPosition, zPosition);
                const randomUvCoords = this.scenerySpritesheetAsset.getUvCoordsForIndex(HELPER.getRandomInt(0, this.scenerySpritesheetAsset.numberOfSprites - 1));
                this.midgroundObjects[i].setUvFromCoords(randomUvCoords);
            }
            object.moveTo(object.root.position.x + (1 / object.root.position.y / 400) * -Globals.speed * delta, object.root.position.y, object.root.position.z);
        }

        //move foreground objects
        for (let i = 0; i < this.foregroundObjects.length; i++) {
            
            let object = this.foregroundObjects[i];
            if (object.root.position.x >= this.fragmentBoundary) {
                //randomly move the object on wrap
                const xPosition = this.firstFragmentPosition;
                const yPosition = HELPER.getRandom(2, 5); //distance from camera
                const zPosition = yPosition / 5 + (Math.random() * 5); //distance up/down - used to help fake persective
                const scale = (this.objectScale * 8);

                this.foregroundObjects[i].setScale(scale, scale);
                this.foregroundObjects[i].moveTo(xPosition, yPosition, zPosition);
                const randomUvCoords = this.scenerySpritesheetAsset.getUvCoordsForIndex(HELPER.getRandomInt(0, this.scenerySpritesheetAsset.numberOfSprites - 1));
                this.foregroundObjects[i].setUvFromCoords(randomUvCoords);
            }
            object.moveTo(object.root.position.x + (object.root.position.y) * Globals.speed / 400 * delta, object.root.position.y, object.root.position.z);
        }

        //move or fade in/out explosions
        for (let i = 0; i < this.explosions.length; i++) {
            const explosion = this.explosions[i];
            if (explosion._mesh.material.opacity > 1) {
                explosion._mesh.material.opacity = 1;
            } else if (explosion._mesh.material.opacity > 0) {
                explosion._mesh.material.opacity += HELPER.getRandom(-0.05, 0.05);
            } else {
                const colour = HELPER.getRandomValueFromArray(this.explosionColours);
                explosion._mesh.material.color = colour;
                explosion._mesh.material.opacity = 0.01;
                const xPosition = HELPER.getRandom(-50, 50);
                const yPosition = -400;
                const zPosition = -5;
                explosion.moveTo(xPosition, yPosition, zPosition);
            }
        }

    }

    setSceneBounds(min, max) {
        this.sceneXBounds.min = min;
        this.sceneXBounds.max = max;
        this.sceneWidth = max - min;
    }

}