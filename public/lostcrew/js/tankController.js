import {Globals} from "./core.js";
import { Tank } from "./classes/tank.js";
import { GameObject } from "./classes/gameObject.js";
import * as THREE from "./vendor/three.module.js";
import * as HELPER from './helper.js';

export class TankController extends GameObject {

    tank;
    sound;

    fuelConsumptionModifier = 0.0002; //fuel consumption rate = speed * modifier / delta
    remainingRadiatorFluid = 71;
    fluidConsumptionModifier = 0.0001; //fluid consumption rate has to do with engine heat(?)

    _pendingFuelConsumption = 0; //only allow fuel consumption in whole integers but track floats

    _timeSpentAccelerating = 0;
    _acceleration = 100000;

    constructor() {
        super();
    }

    initialize( x = 0, y = 0, z = 0 ) {
        super.initialize(x, y, z);
        this.tank = new Tank();
        this.tank.initialize();

        //start us off already moving at normal speed
        Globals.targetSpeed = 1;
        Globals.speed = 1;

        //engine noise lol
        this.sound = new THREE.Audio( Globals.listener );
        const engineSoundAsset = Globals.assets.find(x => x.name === "tank-loop").contents;
        this.sound.setBuffer( engineSoundAsset );
        this.sound.setLoop( true );
        this.sound.setVolume( 0.1 );
        this.sound.play();

        //caching
        this.maxFuelPerBin = Globals.inventoryController.itemTypes.get("fuel").maxCountPerBin;

        this.updateTankAppearance();
    }

    onGameTick(delta) {

        //mess around with the camera if we're moving, why not
        if (Globals.speed > 0) {
            const xFuzz = HELPER.getRandom(-Globals.speed, Globals.speed) * 0.02;
            const yFuzz = HELPER.getRandom(-Globals.speed, Globals.speed) * 0.02;
            Globals.camera.position.set( xFuzz, Globals.camera.position.y, yFuzz );
        } else {
            Globals.camera.position.set( 0, 10, 0 );
        }

        //change speed if needed
        if (Globals.speed != Globals.targetSpeed) {

            if (Math.abs(Globals.targetSpeed - Globals.speed) < 0.1) {
                Globals.speed = Globals.targetSpeed;
                return;
            }

            const lerpFactor = this._timeSpentAccelerating / this._acceleration;
            THREE.MathUtils.clamp(lerpFactor, 0, 1);
            
            Globals.speed = THREE.MathUtils.lerp(Globals.speed, Globals.targetSpeed, lerpFactor);
            this._timeSpentAccelerating += delta;

            //detune
            //Modify pitch, measured in cents. +/- 100 is a semitone. +/- 1200 is an octave. Default is 0.
            this.sound.setDetune((Globals.speed - 1) * 100);
            //modify volume too
            this.sound.setVolume( Globals.speed * 0.1 ) + 0.05;

        } else {
            this._timeSpentAccelerating = 0;
        }

        //consume some fuel
        if (this.getFuelLevel() > 0) {
            const consumption = (Globals.speed * this.fuelConsumptionModifier);
            this._pendingFuelConsumption += consumption;
            const intConsumption = Math.floor(this._pendingFuelConsumption);
            if (intConsumption) {
                Globals.inventoryController.subtrackItemOfTypeFromBins("Fuel", intConsumption);
                if (Globals.uiController.inventoryIsVisible) {
                    Globals.uiController.updateInventoryUi();
                }
                this._pendingFuelConsumption = 0;
            }
        } else {
            Globals.targetSpeed = 0;
        }

    }

    updateTankAppearance() {
        let binContents = Globals.inventoryController.getBinContents(0, true);
        if (binContents.count !== 0) {
            this.tank.extFrontItem1Plane.visible = true;
            if (binContents.itemType.isLiquid) {
                this.tank.extFrontItem1Plane.material.map = Globals.assets.find(x => x.name === 'extFrontBinLiquid1').contents;
                this.tank.extFrontItem1Plane.material.map.needsUpdate = true;
            } else {
                this.tank.extFrontItem1Plane.material.map = Globals.assets.find(x => x.name === 'extFrontBinItem1').contents;
                this.tank.extFrontItem1Plane.material.map.needsUpdate = true;
            }
        } else {
            this.tank.extFrontItem1Plane.visible = false;
        }

        binContents = Globals.inventoryController.getBinContents(1, true);
        if (binContents.count !== 0) {
            this.tank.extFrontItem2Plane.visible = true;
            if (binContents.itemType.isLiquid) {
                this.tank.extFrontItem2Plane.material.map = Globals.assets.find(x => x.name === 'extFrontBinLiquid2').contents;
                this.tank.extFrontItem2Plane.material.map.needsUpdate = true;
            } else {
                this.tank.extFrontItem2Plane.material.map = Globals.assets.find(x => x.name === 'extFrontBinItem2').contents;
                this.tank.extFrontItem2Plane.material.map.needsUpdate = true;
            }
        } else {
            this.tank.extFrontItem2Plane.visible = false;
        }

        binContents = Globals.inventoryController.getBinContents(2, true);
        if (binContents.count !== 0) {
            this.tank.extBackItem1Plane.visible = true;
            if (binContents.itemType.isLiquid) {
                this.tank.extBackItem1Plane.material.map = Globals.assets.find(x => x.name === 'extBackBinLiquid1').contents;
                this.tank.extBackItem1Plane.material.map.needsUpdate = true;
            } else {
                this.tank.extBackItem1Plane.material.map = Globals.assets.find(x => x.name === 'extBackBinItem1').contents;
                this.tank.extBackItem1Plane.material.map.needsUpdate = true;
            }
        } else {
            this.tank.extBackItem1Plane.visible = false;
        }

        binContents = Globals.inventoryController.getBinContents(3, true);
        if (binContents.count !== 0) {
            this.tank.extBackItem2Plane.visible = true;
            if (binContents.itemType.isLiquid) {
                this.tank.extBackItem2Plane.material.map = Globals.assets.find(x => x.name === 'extBackBinLiquid2').contents;
                this.tank.extBackItem2Plane.material.map.needsUpdate = true;
            } else {
                this.tank.extBackItem2Plane.material.map = Globals.assets.find(x => x.name === 'extBackBinItem2').contents;
                this.tank.extBackItem2Plane.material.map.needsUpdate = true;
            }
        } else {
            this.tank.extBackItem2Plane.visible = false;
        }
    }

    getFuelLevel() {
        const fuelBins = Globals.inventoryController.getAllBinsContainingType("Fuel");
        let fuelLevel = 0;
        let maxFuelLevel = 0;
        for (let i = 0; i < fuelBins.length; i++) {
            fuelLevel += fuelBins[i].count;
            maxFuelLevel += this.maxFuelPerBin;
        }

        return (fuelLevel / maxFuelLevel) * 100;
    }

}