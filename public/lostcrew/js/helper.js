//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
var getRandomInt = function(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.round(Math.random() * (max - min)) + min;
}

var getRandom = function(min, max) {
    return (Math.random() * (max - min)) + min;
}

var getRandomBool = function() {
    return Math.random() > 0.5;
}

//https://stackoverflow.com/a/2117523
var getUUID = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

var getRandomValueFromArray = function(array) {
    return array[~~(Math.random() * array.length)];
}

https://stackoverflow.com/a/12646864
var shuffleArray = function(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}

export { getRandom, getRandomInt, getRandomBool, getUUID, getRandomValueFromArray, shuffleArray };