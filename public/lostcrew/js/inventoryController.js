import {Globals} from "./core.js";
import { GameObject } from "./classes/gameObject.js";
import * as THREE from "./vendor/three.module.js";
import * as HELPER from './helper.js';
import { InventoryItem } from "./classes/inventoryItem.js";
import { TossedItem } from "./classes/tossedItem.js";

class InventoryBinContents {
    count;
    timeToAccess;
    itemType;
    name;

    constructor(timeToAccess, itemType = null, count = 0, name ="") {
        this.count = count;
        this.timeToAccess = timeToAccess;
        this.itemType = itemType;
        this.name = name;
    }

    getCSSstring() {
        
        if (this.itemType) {

            //TODO: refresh when scale changes (by tracking every onResize event??)
            const scale = document.documentElement.clientWidth > 1024 ? 4 : 2;
            const width = 110 * scale;
            const height = 130 * scale;
            const binHeight = 13;
            const binWidth = 22;

            let x = 0;

            const fullness = this.count / this.itemType.maxCountPerBin;
            if (fullness === 0) {
                x = 0;
            } else if (fullness <= 0.4) {
                x = -1 * binWidth;
            } else if (fullness < 0.75) {
                x = -2 * binWidth;
            } else if (fullness < 1) {
                x = -3 * binWidth;
            } else {
                x = -4 * binWidth;
            }

            x -= 1; //border
            x *= scale;

            let y = 0;

            switch (this.itemType.name)
            {
                case "Fuel": y = -0 * binHeight; break;
                case "Water": y = -1 * binHeight; break;
                case "Shells": y = -2 * binHeight; break;
                case "Bullets": y = -3 * binHeight; break;
                case "Blankets": y = -4 * binHeight; break;
                case "Rations": y = -5 * binHeight; break;
                case "Spare parts": y = -6 * binHeight; break;
                case "Tools": y = -7 * binHeight; break;
                case "Oil": y = -8 * binHeight; break;
                case "Personal effects": y = -9 * binHeight; break;
            }
            y -= 1; //border
            y *= scale;

            return `
            background: url(./images/bins.png)  ${x}px ${y}px/${width}px ${height}px;
            image-rendering: pixelated;
            `;


        } else {

            //empty bin

            return "";

        }
    }
}

export class InventoryController extends GameObject {

    itemTypes;

    externalPassengers = [];
    turrentPassenger = null;
    internalPassengers = [];
    externalBins = [];
    internalBins = [];

    constructor() {
        super();
        this.itemTypes = new Map();
    }

    initialize( x = 0, y = 0, z = 0 ) {
        super.initialize(x, y, z);

        this.itemTypes.set("fuel", new InventoryItem("Fuel", true, true, false, 40, "Diesel fuel for the engine"));
        this.itemTypes.set("water", new InventoryItem("Water", true, false, false, 40, "Fresh water for both drinking and engine coolant"));
        this.itemTypes.set("shells", new InventoryItem("Shells", false, true, true, 4, "Explosive shells for the tank's cannon"));
        this.itemTypes.set("bullets", new InventoryItem("Bullets", false, true, true, 400, "Bullets for the tank's machine-guns"));
        this.itemTypes.set("blankets", new InventoryItem("Blankets", false, true, false, 4, "Fabric, rags, and blankets. Good for keeping warm"));
        this.itemTypes.set("rations", new InventoryItem("Rations", false, false, true, 10, "Food rations in cans"));
        this.itemTypes.set("spareParts", new InventoryItem("Spare parts", false, false, false, 20, "Spare engine and machine parts for repairs"));
        this.itemTypes.set("tools", new InventoryItem("Tools", false, true, false, 8, "Tools for repairs or road work"));
        this.itemTypes.set("oil", new InventoryItem("Oil", true, true, false, 40, "Engine lubrication oil"));
        this.itemTypes.set("personalItems", new InventoryItem("Personal effects", false, true, false, 4, "Sentimental or practical items owned by the crew"));


        this.externalBins[0] = new InventoryBinContents(10, this.itemTypes.get("tools"), 2, "External front storage");
        this.externalBins[1] = new InventoryBinContents(10, this.itemTypes.get("spareParts"), 12, "External front storage");
        this.externalBins[2] = new InventoryBinContents(10, this.itemTypes.get("blankets"), 1, "External turret storage");
        this.externalBins[3] = new InventoryBinContents(10, this.itemTypes.get("fuel"), 7, "External back storage");

        this.internalBins[0] = new InventoryBinContents(15, this.itemTypes.get("shells"), 3, "Turret bustle storage");
        this.internalBins[1] = new InventoryBinContents(10, this.itemTypes.get("personalItems"), 3, "Internal fore storage");
        this.internalBins[2] = new InventoryBinContents(10, this.itemTypes.get("bullets"), 250, "Internal fore storage");
        this.internalBins[3] = new InventoryBinContents(10, this.itemTypes.get("rations"), 6, "Internal fore storage");
        this.internalBins[4] = new InventoryBinContents(10, this.itemTypes.get("rations"), 0, "Internal fore storage");
        this.internalBins[5] = new InventoryBinContents(10, this.itemTypes.get("shells"), 1, "Internal storage");
        this.internalBins[6] = new InventoryBinContents(10, this.itemTypes.get("water"), 20, "Internal storage");
        this.internalBins[7] = new InventoryBinContents(10, this.itemTypes.get("water"), 40, "Internal storage");
        this.internalBins[8] = new InventoryBinContents(10, this.itemTypes.get("oil"), 20, "Internal storage");
        this.internalBins[9] = new InventoryBinContents(10, this.itemTypes.get("fuel"), 10, "Internal storage");
        this.internalBins[10] = new InventoryBinContents(10, this.itemTypes.get("fuel"), 40, "Internal storage");

        //bit rough to have to do this here
        Globals.uiController.selectedBinContents =  this.getBinContents(0, true);
    }

    onGameTick(delta) {

    }

    getAllBinsContainingType(type) {
        const internalBinsContainingType = this.internalBins.filter(e => e.itemType.name === type);
        const externalBinsContainingType = this.externalBins.filter(e => e.itemType.name === type);
        return internalBinsContainingType.concat(externalBinsContainingType);
    }

    /*
    attempts to subtract the value given from the bins available, returns the amount that was unable to be subtracted (0 if everything is all good)
    */
    subtrackItemOfTypeFromBins(type, amount) {
        const bins = this.getAllBinsContainingType(type);
        let amountLeftToSubtract = amount;
        if (bins.length) {
            let i = 0;
            while (amountLeftToSubtract > 0 && i < bins.length) {
                if (bins[i].count > 0) {
                    bins[i].count -= 1;
                    amountLeftToSubtract -= 1;
                } else {
                    i++;
                }
            }
        }
        return amountLeftToSubtract;
    }

    getBinContents(i, external = false) {
        if (external) {
            return this.externalBins[i];
        } else {
            return this.internalBins[i];
        }
    }

    getPassenger(i, external = false) {
        if (external) {
            return this.externalPassengers[i];
        } else {
            return this.internalPassengers[i];
        }
    }

    getTurretPassenger() {
        return this.turrentPassenger;
    }



    swapBins(index1, isExternal1, index2, isExternal2) {
        const arr1 = isExternal1 ? this.externalBins : this.internalBins;
        const arr2 = isExternal2 ? this.externalBins : this.internalBins;

        //only swap contents and item types

        const temp1c = arr1[index1].count;
        const temp1t = arr1[index1].itemType;
        const temp2c = arr2[index2].count;
        const temp2t = arr2[index2].itemType;
        
        arr1[index1].itemType = temp2t;
        arr1[index1].count = temp2c;
        arr2[index2].itemType = temp1t;
        arr2[index2].count = temp1c;

        Globals.tankController.updateTankAppearance();
    }

    mergeBins(index1, isExternal1, index2, isExternal2) {
        const arr1 = isExternal1 ? this.externalBins : this.internalBins;
        const arr2 = isExternal2 ? this.externalBins : this.internalBins;
        const bin1 = arr1[index1];
        const bin2 = arr2[index2];

        if (bin1.itemType === bin2.itemType) {
            const freeSpaceInBin1 = bin1.itemType.maxCountPerBin - bin1.count; 
            if (freeSpaceInBin1 > 0) {
                if (freeSpaceInBin1 < bin2.count) {
                    bin1.count += freeSpaceInBin1;
                    bin2.count -= freeSpaceInBin1;
                } else {
                    bin1.count += bin2.count;
                    bin2.count = 0;
                }
            }
        }

        Globals.tankController.updateTankAppearance();
    }

    assignBin(index, isExternal, itemName) {
        const arr = isExternal ? this.externalBins : this.internalBins;
        arr[index].itemType = this.itemTypes.get(itemName);
        arr[index].count = 0;

        Globals.tankController.updateTankAppearance();
    }

    dumpBin(index, isExternal) {
        const arr = isExternal ? this.externalBins : this.internalBins;
        const bin = arr[index];
        bin.count = 0;

        Globals.tankController.updateTankAppearance();

        const newTossedItem  = new TossedItem(arr[index].itemType.isLiquid);
        newTossedItem.initialize();
    }



}