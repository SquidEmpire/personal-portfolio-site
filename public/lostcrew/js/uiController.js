import {Globals} from "./core.js";
import { GameObject } from "./classes/gameObject.js";
import * as THREE from './vendor/three.module.js';
import * as HELPER from './helper.js';
import { Sequence } from "./classes/sequences/sequence.js";
import { DialogueEvent } from "./classes/sequences/dialogueEvent.js";
import { SpeedChangeEvent } from "./classes/sequences/speedChangeEvent.js";
import { SpeedNotCondition } from "./classes/sequences/speedNotCondition.js";

class SelectedBin {
    index;
    isExternal;
    isPassenger;
    uiElement;

    constructor(index, isExternal, isPassenger, uiElement)
    {
        this.index = index;
        this.isExternal = isExternal;
        this.isPassenger = isPassenger;
        this.uiElement = uiElement;
    }
}

export class UiController extends GameObject {

    //game title
    title;

    //options buttons
    optionsButton;
    optionsMenu;
    creditsButton;
    creditsSplash;
    creditsCloseButton;

    //control buttons
    journalButton;
    mapButton;
    inventoryButton;
    stopButton;
    goButton;
    goFastButton;

    //speech bubbles
    commanderBubble;
    driverBubble;
    gunnerBubble;

    //displays
    fuelGauge;
    fuelGaugeNeedle;

    //splashes
    splashJournal;
    journalCloseButton;
    splashMap;
    splashInventory;
    inventoryCloseButton;
    mapCloseButton;

    //event splash
    eventSplash;
    eventTitle;
    eventImage;
    eventText;
    eventChoices;
    
    inventoryBins;
    inventoryItemTitle;
    inventoryItemDescription;

    //inventory UI state
    inventoryIsVisible;
    selectedBins = [];
    preparedToDump;
    ableToSwap;
    ableToDump;
    ableToMerge;
    ableToAssign; //can only assign to empty slots
    preparedToAssign;

    //inventory UI controls
    inventorySwapButton;
    inventoryMergeButton;
    inventoryAssignButton;
    inventoryDumpButton;
    inventoryAssignmentButtons;

    constructor() {
        super();
    }

    initialize( x = 0, y = 0, z = 0 ) {
        super.initialize(x, y, z);

        //collect html elements
        this.title = document.getElementById("game-title");

        this.optionsButton = document.getElementById("options-button");
        this.optionsMenu = document.getElementById("options-menu");
        this.creditsButton = document.getElementById("options-credits");
        this.soundButton = document.getElementById("options-sound");

        this.creditsSplash = document.getElementById("splash-credits");
        this.creditsCloseButton = document.getElementById("credits-close");

        this.stopButton = document.getElementById("controls-stop");
        this.goButton = document.getElementById("controls-go");
        this.goFastButton = document.getElementById("controls-gofast");
        this.journalButton = document.getElementById("controls-journal");
        this.mapButton = document.getElementById("controls-map");
        this.inventoryButton = document.getElementById("controls-inventory");

        this.commanderBubble = document.getElementById("bubble-commander");
        this.driverBubble = document.getElementById("bubble-driver");
        this.gunnerBubble = document.getElementById("bubble-gunner");
        
        this.fuelGauge = document.getElementById("fuel-gauge");
        this.fuelGaugeNeedle = document.getElementById("fuel-needle");

        this.splashJournal = document.getElementById("splash-journal");
        this.journalCloseButton = document.getElementById("journal-close");
        this.splashMap = document.getElementById("splash-map");
        this.mapCloseButton = document.getElementById("map-close");
        this.splashInventory = document.getElementById("splash-inventory");
        this.inventoryCloseButton = document.getElementById("inventory-close");

        this.splashEvent = document.getElementById("splash-event");
        this.eventTitle = document.getElementById("event-title");
        this.eventText = document.getElementById("event-contents-text");
        this.eventImage = document.getElementById("event-contents-image");
        this.eventChoices = document.getElementById("event-contents-choices");

        //inventory slots
        this.inventoryBins = document.getElementById("inventory-bins"); 

        this.invExtPas0 = document.getElementById("inv-ext-pas-0");
        this.invExtPas1 = document.getElementById("inv-ext-pas-1");
        this.invExtPas2 = document.getElementById("inv-ext-pas-2");
        this.invTurPas0 = document.getElementById("inv-tur-pas-0");
        this.invIntPas0 = document.getElementById("inv-int-pas-0");
        this.invIntPas1 = document.getElementById("inv-int-pas-1");
        this.invIntPas2 = document.getElementById("inv-int-pas-2");
        this.invIntPas3 = document.getElementById("inv-int-pas-3");
        
        this.invExtBin0 = document.getElementById("inv-ext-bin-0");
        this.invExtBin1 = document.getElementById("inv-ext-bin-1");
        this.invExtBin2 = document.getElementById("inv-ext-bin-2");
        this.invExtBin3 = document.getElementById("inv-ext-bin-3");
        this.invIntBin0 = document.getElementById("inv-int-bin-0");
        this.invIntBin1 = document.getElementById("inv-int-bin-1");
        this.invIntBin2 = document.getElementById("inv-int-bin-2");
        this.invIntBin3 = document.getElementById("inv-int-bin-3");
        this.invIntBin4 = document.getElementById("inv-int-bin-4");
        this.invIntBin5 = document.getElementById("inv-int-bin-5");
        this.invIntBin6 = document.getElementById("inv-int-bin-6");
        this.invIntBin7 = document.getElementById("inv-int-bin-7");
        this.invIntBin8 = document.getElementById("inv-int-bin-8");
        this.invIntBin9 = document.getElementById("inv-int-bin-9");
        this.invIntBin10 = document.getElementById("inv-int-bin-10");

        //inventory assingment buttons
        this.assignFuel = document.getElementById("inventory-assign-fuel");
        this.assignWater = document.getElementById("inventory-assign-water");
        this.assignShells = document.getElementById("inventory-assign-shells");
        this.assignBullets = document.getElementById("inventory-assign-bullets");
        this.assignBlankets = document.getElementById("inventory-assign-blankets");
        this.assignRations = document.getElementById("inventory-assign-rations");
        this.assignParts = document.getElementById("inventory-assign-spare-parts");
        this.assignTools = document.getElementById("inventory-assign-tools");
        this.assignOil = document.getElementById("inventory-assign-oil");
        this.assignEffects = document.getElementById("inventory-assign-personal-effects");

        //inventory controls
        this.inventoryItemTitle = document.getElementById("inventory-item-title");
        this.inventoryItemDescription = document.getElementById("inventory-item-description");
        this.inventorySwapButton = document.getElementById("inventory-controls-swap");
        this.inventoryMergeButton = document.getElementById("inventory-controls-merge");
        this.inventoryAssignButton = document.getElementById("inventory-controls-assign");
        this.inventoryDumpButton = document.getElementById("inventory-controls-dump");
        this.inventoryAssignmentButtons = document.getElementById("inventory-assignment");

        //add events listeners
        this.optionsButton.addEventListener("click", this.onOptions.bind(this));
        this.creditsButton.addEventListener("click", this.onCreditsOpen.bind(this));
        this.soundButton.addEventListener("click", this.onSoundButtonClick.bind(this));
        this.creditsCloseButton.addEventListener("click", this.onCreditsClose.bind(this));

        this.stopButton.addEventListener("click", this.onStop.bind(this));
        this.goButton.addEventListener("click", this.onGo.bind(this));
        this.goFastButton.addEventListener("click", this.onGoFast.bind(this));

        this.journalButton.addEventListener("click", this.onJournal.bind(this));
        this.mapButton.addEventListener("click", this.onMap.bind(this));
        this.inventoryButton.addEventListener("click", this.onInventory.bind(this));

        this.journalCloseButton.addEventListener("click", this.onJournal.bind(this));
        this.mapCloseButton.addEventListener("click", this.onMap.bind(this));
        this.inventoryCloseButton.addEventListener("click", this.onInventory.bind(this));

        this.invExtPas0.addEventListener("click", () => {this.onExtPasClick(0)});
        this.invExtPas1.addEventListener("click", () => {this.onExtPasClick(1)});
        this.invExtPas2.addEventListener("click", () => {this.onExtPasClick(2)});
        this.invTurPas0.addEventListener("click", () => {this.onTurPasClick(0)});
        this.invIntPas0.addEventListener("click", () => {this.onIntPasClick(0)});
        this.invIntPas1.addEventListener("click", () => {this.onIntPasClick(1)});
        this.invIntPas2.addEventListener("click", () => {this.onIntPasClick(2)});
        this.invIntPas3.addEventListener("click", () => {this.onIntPasClick(3)});
        
        this.invExtBin0.addEventListener("click", () => {this.onExtInvClick(0)});
        this.invExtBin1.addEventListener("click", () => {this.onExtInvClick(1)});
        this.invExtBin2.addEventListener("click", () => {this.onExtInvClick(2)});
        this.invExtBin3.addEventListener("click", () => {this.onExtInvClick(3)});
        this.invIntBin0.addEventListener("click", () => {this.onIntInvClick(0)});
        this.invIntBin1.addEventListener("click", () => {this.onIntInvClick(1)});
        this.invIntBin2.addEventListener("click", () => {this.onIntInvClick(2)});
        this.invIntBin3.addEventListener("click", () => {this.onIntInvClick(3)});
        this.invIntBin4.addEventListener("click", () => {this.onIntInvClick(4)});
        this.invIntBin5.addEventListener("click", () => {this.onIntInvClick(5)});
        this.invIntBin6.addEventListener("click", () => {this.onIntInvClick(6)});
        this.invIntBin7.addEventListener("click", () => {this.onIntInvClick(7)});
        this.invIntBin8.addEventListener("click", () => {this.onIntInvClick(8)});
        this.invIntBin9.addEventListener("click", () => {this.onIntInvClick(9)});
        this.invIntBin10.addEventListener("click", () => {this.onIntInvClick(10)});

        this.inventorySwapButton.addEventListener("click", this.onSwap.bind(this));
        this.inventoryMergeButton.addEventListener("click", this.onMerge.bind(this));
        this.inventoryAssignButton.addEventListener("click", this.onAssign.bind(this));
        this.inventoryDumpButton.addEventListener("click", this.onDump.bind(this));

        this.assignFuel.addEventListener("click", () => {this.onAssignmentClicked("fuel")});
        this.assignWater.addEventListener("click", () => {this.onAssignmentClicked("water")});
        this.assignShells.addEventListener("click", () => {this.onAssignmentClicked("shells")});
        this.assignBullets.addEventListener("click", () => {this.onAssignmentClicked("bullets")});
        this.assignBlankets.addEventListener("click", () => {this.onAssignmentClicked("blankets")});
        this.assignRations.addEventListener("click", () => {this.onAssignmentClicked("rations")});
        this.assignParts.addEventListener("click", () => {this.onAssignmentClicked("spareParts")});
        this.assignTools.addEventListener("click", () => {this.onAssignmentClicked("tools")});
        this.assignOil.addEventListener("click", () => {this.onAssignmentClicked("oil")});
        this.assignEffects.addEventListener("click", () => {this.onAssignmentClicked("personalItems")});

        this.selectedBins.push(new SelectedBin(0, true, false, this.invExtBin0));
        this.preparedToDump = false;
        this.ableToSwap = false;
        this.ableToDump = false;
        this.ableToMerge = false;
        this.ableToAssign = false;
        this.preparedToAssign = false;

        //begin the title animation
        this.title.classList.add("animate");

        //also set the options menu buttons to their initial values
        this.soundButton.innerText = (Globals.volume === 0 ? "Sound off" : "Sound on");

        this.inventoryIsVisible = false;
    }

    onGameTick(delta) {
        //might be dom thrashing?
        if (Globals.speed) {
            const fuzz = HELPER.getRandom(-0.4, 0.4);
            const fuzzedFuelAsDegrees = THREE.MathUtils.clamp(((Globals.tankController.getFuelLevel() + fuzz) / 100) * 180, 0, 180);
            this.fuelGaugeNeedle.style.transform = (`translateX(5px) rotate(${fuzzedFuelAsDegrees}deg)`);
            this.fuelGauge.classList.remove("off");
        } else {
            this.fuelGauge.classList.add("off");
        }
    }

    onOptions() {
        this.optionsMenu.classList.toggle("hidden");
    }

    onSoundButtonClick() {
        let newVolume = 0;
        let newSoundButtonText = "Sound off";
        if (Globals.volume === 0) {
            newVolume = 1;
            newSoundButtonText = "Sound on"
        }
        this.soundButton.innerText = newSoundButtonText;
        Globals.volume = newVolume;
        Globals.listener.setMasterVolume(newVolume);
        localStorage.setItem('sound', newVolume);
    }

    onCreditsOpen() {
        this.creditsSplash.classList.remove("hidden");
        this.onOptions();
        Globals.paused = true;
    }

    onCreditsClose() {
        this.creditsSplash.classList.add("hidden");
        Globals.paused = false;
    }

    onJournal() {
        this.splashJournal.classList.toggle("hidden");
    }

    onMap() {
        this.splashMap.classList.toggle("hidden");
    }

    onInventory() {
        this.updateInventoryUi();
        this.splashInventory.classList.toggle("hidden");
        this.inventoryIsVisible = !this.splashInventory.classList.contains("hidden");
    }

    onStop() {
        const seq = new Sequence(
            new SpeedNotCondition(0),
            new DialogueEvent("commander", ["Halt", "Stop!", "Stop, quickly!"], 500),
            new SpeedChangeEvent(0),
            new DialogueEvent("driver", "Stopping!", 500),
        );
        Globals.sequenceController.runSequenceImmediately(seq);
    }

    onGo() {
        const seq = new Sequence(
            new SpeedNotCondition(1),
            new DialogueEvent("commander", ["Half speed", "Half speed please", "Driver, half speed", "Take us to half", "Half speed Enno"], 1000),
            new SpeedChangeEvent(1),
            new DialogueEvent("driver", ["Half speed", "Going to half", "Ok", "Half, rodger"], 1000),
        );
        Globals.sequenceController.runSequenceImmediately(seq);
    }

    onGoFast() {
        const seq = new Sequence(
            new SpeedNotCondition(3),
            new DialogueEvent("commander", ["Full speed!", "Faster!", "Full power!", "Driver, full speed!", "Full speed Enno!"], 1000),
            new SpeedChangeEvent(3),
            new DialogueEvent("driver", ["Giving it all", "Yes sir!", "Full throttle, rodger"], 1000),
        );
        Globals.sequenceController.runSequenceImmediately(seq);
    }

    setCommanderSpeech(text) {
        this.commanderBubble.innerText = text;
        if (text && text !== "") {            
            this.commanderBubble.classList.remove("hidden");
        } else {
            this.commanderBubble.classList.add("hidden");
        }        
    }

    setDriverSpeech(text) {
        this.driverBubble.innerText = text;
        if (text && text !== "") {            
            this.driverBubble.classList.remove("hidden");
        } else {
            this.driverBubble.classList.add("hidden");
        }
    }

    setGunnerSpeech(text) {
        this.gunnerBubble.innerText = text;
        if (text && text !== "") {            
            this.gunnerBubble.classList.remove("hidden");
        } else {
            this.gunnerBubble.classList.add("hidden");
        }
    }

    displaySplashEvent(splashEvent) {
        this.eventTitle.innerText = splashEvent.title;
        this.eventText.innerText = splashEvent.text;
        if (splashEvent.image) {
            this.eventImage.setAttribute('src', splashEvent.image);
            this.eventImage.classList.remove("hidden");
        } else {
            this.eventImage.classList.add("hidden");
        }
        while (this.eventChoices.firstChild && !this.eventChoices.firstChild.remove()); //quick clear the last elements in the html
        
        for (var i = 0; i < splashEvent.choices.length; i++) {
            const choice = splashEvent.choices[i];
            const isAvailable = choice.isAvailable();
            const choiceButton = document.createElement('button');
            choiceButton.innerText = choice.text;
            if (isAvailable) {
                choiceButton.onclick = () => {splashEvent.onChoiceMade(choice)};
            } else {
                choiceButton.classList.add("disabled");
            }   
            this.eventChoices.appendChild(choiceButton);
        }

        this.splashEvent.classList.remove("hidden");
    }

    hideSplashEvent() {
        this.splashEvent.classList.add("hidden");
        this.inventoryIsVisible = false;
    }











    /*
        Inventory UI below
    */



    updateInventoryUi() {
        this.invExtPas0;
        this.invExtPas1;
        this.invExtPas2;
        this.invTurPas0;
        this.invIntPas0;
        this.invIntPas1;
        this.invIntPas2;
        this.invIntPas3;
        
        this.invExtBin0.style = Globals.inventoryController.getBinContents(0, true).getCSSstring();
        this.invExtBin1.style = Globals.inventoryController.getBinContents(1, true).getCSSstring();
        this.invExtBin2.style = Globals.inventoryController.getBinContents(2, true).getCSSstring();
        this.invExtBin3.style = Globals.inventoryController.getBinContents(3, true).getCSSstring();
        this.invIntBin0.style = Globals.inventoryController.getBinContents(0, false).getCSSstring();
        this.invIntBin1.style = Globals.inventoryController.getBinContents(1, false).getCSSstring();
        this.invIntBin2.style = Globals.inventoryController.getBinContents(2, false).getCSSstring();
        this.invIntBin3.style = Globals.inventoryController.getBinContents(3, false).getCSSstring();
        this.invIntBin4.style = Globals.inventoryController.getBinContents(4, false).getCSSstring();
        this.invIntBin5.style = Globals.inventoryController.getBinContents(5, false).getCSSstring();
        this.invIntBin6.style = Globals.inventoryController.getBinContents(6, false).getCSSstring();
        this.invIntBin7.style = Globals.inventoryController.getBinContents(7, false).getCSSstring();
        this.invIntBin8.style = Globals.inventoryController.getBinContents(8, false).getCSSstring();
        this.invIntBin9.style = Globals.inventoryController.getBinContents(9, false).getCSSstring();
        this.invIntBin10.style = Globals.inventoryController.getBinContents(10, false).getCSSstring();

        this.updateSelectedInventoryBin();
        this.updatePreparedToDump();
        this.updateAbleToSwap();
        this.updateAbleToMerge();
        this.updateAbleToDump();
        this.updateAbleToAssign();
        this.updatePreparedToAssign();
    }

    updatePreparedToDump() {
        if (this.preparedToDump) {
            this.inventoryDumpButton.innerText = "Are you sure?";
            this.inventoryDumpButton.classList.add("prepared");
        } else {
            this.inventoryDumpButton.innerText = "Dump";
            this.inventoryDumpButton.classList.remove("prepared");
        }
    }

    updatePreparedToAssign() {
        if (this.preparedToAssign) {
            this.inventoryAssignButton.disabled = true;
            this.inventoryAssignmentButtons.classList.remove("hidden");
        } else {
            this.inventoryAssignButton.disabled = false;
            this.inventoryAssignmentButtons.classList.add("hidden");
        }
    }

    updateAbleToSwap() {
        if (this.selectedBins.length == 2) {
            if (this.selectedBins[0].isPassenger && this.selectedBins[1].isPassenger) {

                this.ableToSwap = true;
                this.inventorySwapButton.disabled = false;

            } else if (!this.selectedBins[0].isPassenger && !this.selectedBins[1].isPassenger) {
                this.ableToSwap = true;
                this.inventorySwapButton.disabled = false;
            }
        } else {
            this.ableToSwap = false;
            this.inventorySwapButton.disabled = true;
        }
    }

    updateAbleToDump() {
        const binContents = Globals.inventoryController.getBinContents(this.selectedBins[0].index, this.selectedBins[0].isExternal);
            if (binContents.count != 0) {
                this.ableToDump = true;
                this.inventoryDumpButton.disabled = false;
            } else {
                this.ableToDump = false;
                this.inventoryDumpButton.disabled = true;
            }
    }

    updateAbleToMerge() {
        if (this.selectedBins.length == 2) {
            if (this.selectedBins[0].isPassenger || this.selectedBins[1].isPassenger) {

                this.ableToMerge = false;
                this.inventoryMergeButton.disabled = true;

            } else if (!this.selectedBins[0].isPassenger && !this.selectedBins[1].isPassenger) {
                const bin1Contents = Globals.inventoryController.getBinContents(this.selectedBins[0].index, this.selectedBins[0].isExternal);
                const bin2Contents = Globals.inventoryController.getBinContents(this.selectedBins[1].index, this.selectedBins[1].isExternal);
                if (bin1Contents.itemType === bin2Contents.itemType) {
                    this.ableToMerge = true;
                    this.inventoryMergeButton.disabled = false;
                } else {
                    this.ableToMerge = false;
                    this.inventoryMergeButton.disabled = true;
                }
            }
        } else {
            this.ableToMerge = false;
            this.inventoryMergeButton.disabled = true;
        }
    }

    updateAbleToAssign() {
        if (this.selectedBins[0].isPassenger) {

            this.ableToAssign = false;
            this.inventoryAssignButton.disabled = true;

        } else {
            const binContents = Globals.inventoryController.getBinContents(this.selectedBins[0].index, this.selectedBins[0].isExternal);
            if (binContents.count === 0) {
                this.ableToAssign = true;
                this.inventoryAssignButton.disabled = false;
            } else {
                this.ableToAssign = false;
                this.inventoryAssignButton.disabled = true;
            }
        }
    }

    updateSelectedInventoryBin() {
        for (let i = 0; i < this.inventoryBins.children.length; i++) {
            this.inventoryBins.children[i].classList.remove("selected-primary");
            this.inventoryBins.children[i].classList.remove("selected-secondary");
        }

        if (this.selectedBins.length) {
            const selectedBin = this.selectedBins[0];
            const selectedBinContents = Globals.inventoryController.getBinContents(selectedBin.index, selectedBin.isExternal);
            const L = selectedBinContents.itemType.isLiquid ? "L" : "";
            this.inventoryItemTitle.innerText = `${selectedBinContents.name} - ${selectedBinContents.itemType.name}`;
            this.inventoryItemDescription.innerText = `${selectedBinContents.itemType.description} - ${selectedBinContents.count}${L} / ${selectedBinContents.itemType.maxCountPerBin}${L}`;
        } else {
            this.inventoryItemTitle.innerText = "";
            this.inventoryItemDescription.innerText = "";
        }

        this.selectedBins[0].uiElement.classList.add("selected-primary");
        if (this.selectedBins.length > 1) {
            for (let i = 1; i < this.selectedBins.length; i++) {
                this.selectedBins[i].uiElement.classList.add("selected-secondary");
            }
        }    
    }

    onExtPasClick(i) {
        let selectedBin;
        switch (i) {
            case 0: selectedBin = this.invExtPas0; break;
            case 1: selectedBin = this.invExtPas1; break;
            case 2: selectedBin = this.invExtPas2; break;
        }
        this.addOrRemoveSelectedUiBin(selectedBin, true, true, 0);
    }
    onTurPasClick(i) {
        this.addOrRemoveSelectedUiBin(this.invTurPas0, true, false, 0);
    }
    onIntPasClick(i) {
        let selectedBin;
        switch (i) {
            case 0: selectedBin = this.invIntPas0; break;
            case 1: selectedBin = this.invIntPas1; break;
            case 2: selectedBin = this.invIntPas2; break;
            case 3: selectedBin = this.invIntPas2; break;
        }
        this.addOrRemoveSelectedUiBin(selectedBin, true, false, 0);
    }

    onExtInvClick(i) {
        let selectedBin;
        switch (i) {
            case 0: selectedBin = this.invExtBin0; break;
            case 1: selectedBin = this.invExtBin1; break;
            case 2: selectedBin = this.invExtBin2; break;
            case 3: selectedBin = this.invExtBin3; break;
        }
        this.addOrRemoveSelectedUiBin(selectedBin, false, true, i);
    }
    onIntInvClick(i) {
        let selectedBin;
        switch (i) {
            case 0: selectedBin = this.invIntBin0; break;
            case 1: selectedBin = this.invIntBin1; break;
            case 2: selectedBin = this.invIntBin2; break;
            case 3: selectedBin = this.invIntBin3; break;
            case 4: selectedBin = this.invIntBin4; break;
            case 5: selectedBin = this.invIntBin5; break;
            case 6: selectedBin = this.invIntBin6; break;
            case 7: selectedBin = this.invIntBin7; break;
            case 8: selectedBin = this.invIntBin8; break;
            case 9: selectedBin = this.invIntBin9; break;
            case 10: selectedBin = this.invIntBin10; break;
        }
        this.addOrRemoveSelectedUiBin(selectedBin, false, false, i);
    }

    addOrRemoveSelectedUiBin(selectedUiBin, isPassenger, isExternal, index) {
        const exisitingIndex = this.selectedBins.findIndex(e => e.uiElement === selectedUiBin);
        if (exisitingIndex !== -1) {
            if (this.selectedBins.length > 1) {
                if (exisitingIndex === 0) {
                    this.selectedBins.splice(1, 1);
                } else {
                    this.selectedBins.splice(0, 1);
                }
            }
        } else {
            if (this.selectedBins.length < 2) {
                this.selectedBins.unshift(
                    new SelectedBin(index, isExternal, isPassenger, selectedUiBin)
                );
            } else {
                this.selectedBins.splice(1, 1);
                this.selectedBins.unshift(
                    new SelectedBin(index, isExternal, isPassenger, selectedUiBin)
                );
            }
        }

        this.preparedToDump = false;
        this.preparedToAssign = false;
        this.updatePreparedToDump();
        this.updateAbleToDump();
        this.updateAbleToSwap();
        this.updateAbleToMerge();
        this.updateAbleToAssign();
        this.updateSelectedInventoryBin();
    }

    onSwap() {
        if (this.selectedBins.length == 2) {
            if (this.selectedBins[0].isPassenger && this.selectedBins[1].isPassenger) {

            } else if (!this.selectedBins[0].isPassenger && !this.selectedBins[1].isPassenger) {
                Globals.inventoryController.swapBins(
                    this.selectedBins[0].index,
                    this.selectedBins[0].isExternal,
                    this.selectedBins[1].index,
                    this.selectedBins[1].isExternal
                );
            }
        }
        this.preparedToDump = false;
        this.preparedToAssign = false;
        this.updateInventoryUi();
    }

    onMerge() {
        if (this.selectedBins.length == 2) {
            if (this.selectedBins[0].isPassenger || this.selectedBins[1].isPassenger) {
                //can't merge passengers!
            } else if (!this.selectedBins[0].isPassenger && !this.selectedBins[1].isPassenger) {
                Globals.inventoryController.mergeBins(
                    this.selectedBins[0].index,
                    this.selectedBins[0].isExternal,
                    this.selectedBins[1].index,
                    this.selectedBins[1].isExternal
                );
            }
        }
        this.preparedToDump = false;
        this.preparedToAssign = false;
        this.updateInventoryUi();
    }

    onAssign() {
        this.selectedBins.splice(1, 1); 
        this.preparedToAssign = true;
        this.preparedToDump = false;
        this.updateInventoryUi();
    }

    onAssignmentClicked(type) {
        Globals.inventoryController.assignBin( this.selectedBins[0].index, this.selectedBins[0].isExternal, type );
        this.preparedToDump = false;
        this.preparedToAssign = false;
        this.updateInventoryUi();
    }

    onDump() {
        this.selectedBins.splice(1, 1); //only allow one bin to be dumped at once
        if (this.preparedToDump) {
            if (this.selectedBins[0].isPassenger) {

            } else {
                Globals.inventoryController.dumpBin(
                    this.selectedBins[0].index,
                    this.selectedBins[0].isExternal
                );
                this.preparedToDump = false;
            }
        } else {
            this.preparedToDump = true;
        }
        
        this.preparedToAssign = false;
        this.updateInventoryUi();
    }

}