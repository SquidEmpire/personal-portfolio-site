import { Sequence } from "../classes/sequences/sequence.js";
import { DialogueEvent } from "../classes/sequences/dialogueEvent.js";
import { TimedEvent } from "../classes/sequences/timedEvent.js";
import { SplashEvent } from "../classes/sequences/splashEvent.js";
import { SplashEventChoice } from "../classes/sequences/splashEventChoice.js";
import { LastSplashEventChoiceIdCondition } from "../classes/sequences/lastSplashEventChoiceIdCondition.js";
import { SpeedCondition } from "../classes/sequences/speedCondition.js";

let sequences = [];

// sequences.push(new Sequence(
//     new SplashEvent("Test splash event", "Please pick an option from the following list:", null, [], [
//         new SplashEventChoice(1, "Option one (only when stopped)", [new SpeedCondition(0, 0)]),
//         new SplashEventChoice(2, "Option two"),
//         new SplashEventChoice(3, "Option three"),
//     ]),
//     new SplashEvent("Picked 1", "thanks for picking option 1", null, [
//         new LastSplashEventChoiceIdCondition(1),
//     ]),
//     new SplashEvent("Picked 2", "thanks for picking option 2", null, [
//         new LastSplashEventChoiceIdCondition(2),
//     ]),
//     new SplashEvent("Picked 3", "thanks for picking option 3", null, [
//         new LastSplashEventChoiceIdCondition(3),
//     ]),
//     new SplashEvent("End slide", "That concludes our business"),
// ));

/*
sequences.push(new Sequence(
    new DialogueEvent("commander", "We must be at least 600km east of our next waypoint", 3000),
    new DialogueEvent("gunner", "right", 1000),
    new DialogueEvent("gunner", "ok", 2000),
    new TimedEvent(3000),
    new DialogueEvent("commander", ["We must", "We must be"], 1500),
));

sequences.push(new Sequence(
    new DialogueEvent("driver", "Where exactly are we, Harri?", 3000),
    new DialogueEvent("commander", "Certainly... still within Muraka", 3000),
    new TimedEvent(1000),
    new DialogueEvent("gunner", "How can that be?", 2000),
    new DialogueEvent("gunner", "We should have reached Oonurme days ago", 2000),
    new TimedEvent(4000),
    new DialogueEvent("gunner", "Maybe it's gone", 3000),
    new TimedEvent(2000),
    new DialogueEvent("gunner", "Maybe Oonurme's gone", 3000),
    new TimedEvent(3500),
    new DialogueEvent("driver", "Harri?", 2000),
    new DialogueEvent("commander", "We continue.", 3000),
    new TimedEvent(3000),
    new DialogueEvent("gunner", "Muraka's gone. Look at it...", 3000),
));

sequences.push(new Sequence(
    new DialogueEvent("gunner", "Look at this place", 2500),
    new TimedEvent(1000),
    new DialogueEvent("gunner", "It's changed so much", 2000),
    new TimedEvent(3000),
    new DialogueEvent("gunner", "So many dead trees...", 3000),
    new DialogueEvent("gunner", "Is this all there is now? Mud and dead trees?", 3000),
    new TimedEvent(4000),
    new DialogueEvent("driver", "We're here too", 3000),
));

sequences.push(new Sequence(
    new DialogueEvent("driver", "Do you suppose this is our territory?", 3000),
    new DialogueEvent("commander", "Absolutely.", 2000),
    new TimedEvent(4000),
    new DialogueEvent("commander", "We haven't seen any Prantsi for weeks", 3000),
    new TimedEvent(1000),
    new DialogueEvent("gunner", "We haven't seen anyone at all for weeks", 3000),
    new TimedEvent(4000),
    new DialogueEvent("commander", "We retook this area I'm sure", 3000),
    new TimedEvent(1000),
    new DialogueEvent("commander", "Muraka forest", 3000),
    new TimedEvent(2000),
    new DialogueEvent("commander", "The 8th must have come through here", 2500),
    new DialogueEvent("commander", "they're just ahead of us, undoubtedly", 3000),
    new TimedEvent(5000),
    new DialogueEvent("gunner", "My friend Edgar is in the 8th", 2000),
    new DialogueEvent("gunner", "We worked together for a while", 3000),
    new TimedEvent(2000),
    new DialogueEvent("commander", "Once we catch up with the 8th maybe you'll see him", 3000),
    new DialogueEvent("commander", "That's something to look forward to.", 3000),
    new TimedEvent(1000),
    new DialogueEvent("gunner", "It would be nice", 2000),
    new TimedEvent(4000),
    new DialogueEvent("driver", "What job did you and Edgar do?", 2500),
    new TimedEvent(1000),
    new DialogueEvent("gunner", "We were painters", 2000),
    new DialogueEvent("gunner", "We painted an entire barn blue ourselves", 2000),
    new TimedEvent(1000),
    new DialogueEvent("gunner", "Such a beautiful colour", 1500),
    new DialogueEvent("gunner", "You should have seen it", 2000),
    new TimedEvent(1500),
    new DialogueEvent("gunner", "But when the farmer saw it he was furious!", 2000),
    new DialogueEvent("gunner", "\"A blue barn! Whoever heard of such a thing!\"", 2000),
    new DialogueEvent("gunner", "We refused to repaint it red!", 2200),
    new DialogueEvent("gunner", "And, that was the end of our career as painters", 3000),
    new TimedEvent(2000),
    new DialogueEvent("driver", "What do you think of that Harri?", 2500),
    new TimedEvent(3500),
    new DialogueEvent("commander", "I think...", 1500),
    new DialogueEvent("commander", "I would have liked to see the barn", 3000),
    new DialogueEvent("commander", "A blue barn... very... hmm", 3500),
    new DialogueEvent("commander", "very... unusual", 3000),
));

sequences.push(new Sequence(
    new DialogueEvent("gunner", "Why did you enlist Enno?", 2500),
    new TimedEvent(2500),
    new DialogueEvent("driver", "I didn't. They conscripted me", 2000),
    new TimedEvent(2000),
    new DialogueEvent("gunner", "Oh.", 1500),
    new TimedEvent(3000),
    new DialogueEvent("driver", "What about you?", 2000),
    new TimedEvent(1000),
    new DialogueEvent("gunner", "I was conscripted too", 2500),
    new TimedEvent(1000),
    new DialogueEvent("gunner", "Straight into the tank-corp", 2500),
    new TimedEvent(4000),
    new DialogueEvent("driver", "Why did you think I enslisted?", 2000),
    new TimedEvent(1000),
    new DialogueEvent("gunner", "You just always seemed...", 2500),
    new DialogueEvent("gunner", "ah", 1000),
    new DialogueEvent("gunner", "forget it.", 1500),
    new TimedEvent(3000),
    new DialogueEvent("driver", "I didn't want to be here.", 2000),
    new TimedEvent(2000),
    new DialogueEvent("gunner", "Of course", 1000),
));
*/

export { sequences as dataSequences };