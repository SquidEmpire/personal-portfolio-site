import { Sequence } from "../classes/sequences/sequence.js";
import { DialogueEvent } from "../classes/sequences/dialogueEvent.js";
import { TimedEvent } from "../classes/sequences/timedEvent.js";
import { SpeedCondition } from "../classes/sequences/speedCondition.js";
import { FuelAboveCondition } from "../classes/sequences/fuelAboveCondition.js";

let sequences = [];

sequences.push(new Sequence(
    new SpeedCondition(0, 0),
    new FuelAboveCondition(0), //everything is NOT ok if we're out of fuel lol (special condition for that? repeatable? Or special quest?)
    new DialogueEvent("commander", ["Is everything ok Enno?", "Is the vehicle ok?", "Are we mobile?", "Does everything look ok Enno?"], 3000),
    new DialogueEvent("driver", ["Uh", "Checking...", "Hmm", "I'll check", "Hold on..."], 1500),
    new TimedEvent(2000),
    new DialogueEvent("driver", ["Yes", "I think so", "Mm-hmm"], 1500),
    new DialogueEvent("driver", ["Yes everything's working", "It's still working", "We're alright", "It'll hold", "No serious damage"], 2500),
));

export { sequences as repeatableDataSequences };