import * as THREE from "./vendor/three.module.js";
import { EffectComposer } from './vendor/postprocessing/EffectComposer.js';
import { RenderPass } from './vendor/postprocessing/RenderPass.js';
import { ShaderPass } from "./vendor/postprocessing/ShaderPass.js";
import { FilmPass } from './vendor/postprocessing/FilmPass.js';

import { FXAAShader } from './vendor/shaders/FXAAShader.js';
import { BleachBypassShader } from './vendor/shaders/BleachBypassShader.js';
import { GammaCorrectionShader } from './vendor/shaders/GammaCorrectionShader.js';
import { VignetteShader } from './vendor/shaders/VignetteShader.js';

import { TankController } from "./tankController.js";
import { Loader } from "./loader.js";
import { SceneController } from "./sceneController.js";
import { UiController } from "./uiController.js";
import { Asset } from "./classes/asset.js" 
import { SpritesheetAsset } from "./classes/spritesheetAsset.js";
import { SequenceController } from "./sequenceController.js";
import { InventoryController } from "./inventoryController.js";

export var Globals = {
    scene: null,
    camera: null,
    listener: null,
    
    tankController: null,
    sceneController: null,
    uiController: null,
    sequenceController: null,
    inventoryController: null,

    assets: [], //loaded assets

    paused: false,
    targetSpeed: 0,
    speed: 0,

    volume: 1,

    gameObjects: new Map(),
}

let renderer = null;
let composer = null;
let postprocessing = {};

const camFactor = 20;
const gameSpeed = 200; //ticks per second
let lastUpdate = performance.now();
let pausedBeforeVisibilityChange = false;
let volumeBeforeVisibilityChange = 1;

function buildAssetList() {

    //sprites
    Globals.assets.push(new SpritesheetAsset('skyline', 'images/skyline.png', 64));
    Globals.assets.push(new SpritesheetAsset('scenery', 'images/scenery.png', 64));
    Globals.assets.push(new SpritesheetAsset('cloud', 'images/cloud.png', 32));
    Globals.assets.push(new SpritesheetAsset('tank', 'images/tank.png', 256));
    Globals.assets.push(new SpritesheetAsset('extBackBinItem1', 'images/extBackBinItem1.png', 256));
    Globals.assets.push(new SpritesheetAsset('extBackBinItem2', 'images/extBackBinItem2.png', 256));
    Globals.assets.push(new SpritesheetAsset('extBackBinLiquid1', 'images/extBackBinLiquid1.png', 256));
    Globals.assets.push(new SpritesheetAsset('extBackBinLiquid2', 'images/extBackBinLiquid2.png', 256));
    Globals.assets.push(new SpritesheetAsset('extFrontBinItem1', 'images/extFrontBinItem1.png', 256));
    Globals.assets.push(new SpritesheetAsset('extFrontBinItem2', 'images/extFrontBinItem2.png', 256));
    Globals.assets.push(new SpritesheetAsset('extFrontBinLiquid1', 'images/extFrontBinLiquid1.png', 256));
    Globals.assets.push(new SpritesheetAsset('extFrontBinLiquid2', 'images/extFrontBinLiquid2.png', 256));
    Globals.assets.push(new SpritesheetAsset('smoke1', 'images/smoke1.png', 32));
    Globals.assets.push(new SpritesheetAsset('dust', 'images/dust.png', 32));
    Globals.assets.push(new SpritesheetAsset('mudParticles', 'images/mudParticles.png', 8));
    Globals.assets.push(new SpritesheetAsset('gradient', 'images/gradient.png', 128));
    Globals.assets.push(new SpritesheetAsset('crate', 'images/crate.png', 32));
    Globals.assets.push(new SpritesheetAsset('barrel', 'images/barrel.png', 32));

    //sounds
    Globals.assets.push(new Asset("tank-loop", 'sounds/tank-1-mono-loop.ogg', THREE.Audio));
    //Globals.assets.push(new Asset("tank-loop", 'sounds/half-track1-2-mono-loop.ogg', THREE.Audio));
}


async function init() {

    //build our asset list
    buildAssetList();

    //load the assets
    const loaderElement = document.getElementById( 'loader' );
    const loaderTextElement = document.getElementById( 'loader-description' );
    const loaderStartElement = document.getElementById('loader-start');
    const loader = new Loader(loaderTextElement);
    await loader.loadAssets(Globals.assets);

    //wait for user input - we have to have this to because we can't play audio without 'user intent'
    loaderStartElement.classList.remove("hidden");
    //TODO don't forget this
    //await loader.waitForUserInput(loaderStartElement);

    loaderElement.classList.add("fade");

    Globals.scene = new THREE.Scene();

    const aspect = window.innerWidth / window.innerHeight;
    Globals.camera = new THREE.OrthographicCamera( - camFactor * aspect, camFactor * aspect, camFactor, - camFactor, 1, 1000 );
    Globals.camera.position.set( 0, 10, 0 );
    Globals.camera.lookAt( 0, 0, 0 );
    Globals.scene.add( Globals.camera );

    Globals.listener = new THREE.AudioListener();
    Globals.camera.add( Globals.listener );
    //set the listener to use the localstorage sound value (if there is one)
    const soundValue = localStorage.getItem('sound');
    if (soundValue !== null) {
        Globals.volume = 0;
        volumeBeforeVisibilityChange = Globals.volume;
        Globals.listener.setMasterVolume(parseInt(soundValue));
    } else {
        Globals.volume = 1;
        volumeBeforeVisibilityChange = Globals.volume;
        Globals.listener.setMasterVolume(1);
        localStorage.setItem('sound', 1);
    }

    renderer = new THREE.WebGLRenderer( { antialias: false, alpha: true } );
    renderer.shadowMap.enabled = false;
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.domElement.id = "game-canvas";
    document.body.appendChild( renderer.domElement );

    window.addEventListener( 'resize', onWindowResize.bind(this), false );
    document.addEventListener( 'visibilitychange', onPageVisibilityChange.bind(this), false );

    Globals.sceneController = new SceneController();
    Globals.sceneController.setSceneBounds(Globals.camera.left, Globals.camera.right);
    Globals.sceneController.initialize();

    Globals.uiController = new UiController();
    Globals.uiController.initialize();

    Globals.sequenceController = new SequenceController();
    Globals.sequenceController.initialize();

    Globals.inventoryController = new InventoryController();
    Globals.inventoryController.initialize();

    Globals.tankController = new TankController();
    Globals.tankController.initialize();

    initPostprocessing();
    renderer.autoClear = false;

    //start the game ticks
    lastUpdate = performance.now();
    setInterval(tick.bind(this), 1000/gameSpeed);
    render();

}


function initPostprocessing() {

    const height = window.innerHeight;
    const width = window.innerWidth;

    const renderPass = new RenderPass( Globals.scene, Globals.camera );

    const bleachPass = new ShaderPass( BleachBypassShader );
    const gammaCorrectionPass = new ShaderPass( GammaCorrectionShader );
    //const effectVignette = new ShaderPass( shaderVignette );
    const filmPass = new FilmPass( 0.2, 0.1, 648, false );
    const vignettePass = new ShaderPass( VignetteShader );

    //seems like massive overkill to use this AA shader when all I need is a transparent background...
    const fxaaPass = new ShaderPass( FXAAShader );
    fxaaPass.uniforms.resolution.value.set( 1 / width, 1 / height );
    fxaaPass.material.transparent = true;

    const parameters = { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter, format: THREE.RGBAFormat, stencilBuffer: true };
    const renderTarget = new THREE.WebGLRenderTarget( width, height, parameters );

    composer = new EffectComposer( renderer, renderTarget );
    composer.addPass( renderPass );
    //composer.addPass( gammaCorrectionPass );
    //composer.addPass( bleachPass );
    composer.addPass( filmPass );
    composer.addPass( vignettePass );
    composer.addPass( fxaaPass );

    postprocessing.composer = composer;
    //postprocessing.gammaCorrectionPass = gammaCorrectionPass;
    //postprocessing.bleachPass = bleachPass;
    postprocessing.filmPass = filmPass;
    postprocessing.vignettePass = vignettePass;
    postprocessing.fxaa = fxaaPass;

}

function tick() {
    const now = performance.now();
    const delta = now - lastUpdate;
    lastUpdate = now;

    Globals.gameObjects.forEach(object => {
        object.onGameTickAlways(delta);
    });
    
}

function render() {
    //renderer.render( Globals.scene, Globals.camera );
    postprocessing.composer.render( 0.1 );
    requestAnimationFrame( render );
}

function onWindowResize() {
    const aspect = window.innerWidth / window.innerHeight;
    renderer.setSize( window.innerWidth, window.innerHeight );
    Globals.camera.left = - camFactor * aspect;
    Globals.camera.right = camFactor * aspect;
    Globals.camera.top = camFactor;
    Globals.camera.bottom = -camFactor;
    Globals.camera.updateProjectionMatrix();
    Globals.sceneController.setSceneBounds(Globals.camera.left, Globals.camera.right);
}

function onPageVisibilityChange() {
    if (document.visibilityState === 'visible') {
        Globals.paused = pausedBeforeVisibilityChange;
        Globals.volume = volumeBeforeVisibilityChange;
    } else {
        pausedBeforeVisibilityChange = Globals.paused;
        volumeBeforeVisibilityChange = Globals.volume;
        Globals.volume = 0;
        Globals.paused = true;
    }
    Globals.listener.setMasterVolume(Globals.volume);
}

init();
