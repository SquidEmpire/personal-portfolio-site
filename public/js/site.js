var globals = {
    router: undefined,
    indexContent: undefined,
    artbox: undefined,
}

class Router {
    //properties not supported yet...
    //hash;
    constructor() {
        this.hash = undefined;
        this.onHashChange();
        window.addEventListener('hashchange', this.onHashChange.bind(this));
    }
    onHashChange() {
        this.hash = window.location.hash.substring(1);
        this.hideAllMainContent();
        this.displayMainContent(this.hash);
    }
    hideAllMainContent() {
        let mainContents = document.getElementById('mainContent').children;
        let mainNavs = document.getElementById('mainNav').children;
        for (let content of mainContents) {
            content.classList.add('js-hidden');
        }
        //also remove the active class from any header links
        for (let navs of mainNavs) {
            navs.classList.remove("active");
        }
    }
    displayMainContent(contentName) {
        let targetContent = document.getElementById(`content-${contentName}`);
        let targetLink = document.getElementById(`link-${contentName}`);
        if (targetContent) {
            targetContent.classList.remove('js-hidden');
            targetLink.classList.add("active");
            if (contentName === "audio") {
                loadSoundcloudIframes();
                loadBandcampIframes();
            }
        } else {
            globals.indexContent.classList.remove('js-hidden');
        }
    }
}

class Artwork {
    constructor(title, link, date, description, image) {
        this.title = title,
        this.link = link,
        this.date = date;
        this.description = description;
        this.image = image;
    }
}

class Artbox {
    //properties not supported yet...
    /*
    element;
    imageElement;
    imageLinkElement;
    titleElement;
    titleElementLink;
    descriptionElement;
    nextButtonElement;
    previousButtonElement;
    counterElement;

    artworkList;
    artIndex;
    totalArtworks;
    navigationDisabled;
    */
    constructor(elementId) {
        this.element = document.getElementById("artbox");
        this.imageElement = document.getElementById("artbox-image");
        this.imageLinkElement = document.getElementById("artbox-image-link");
        this.titleElement = document.getElementById("artbox-title");
        this.nextButtonElement = document.getElementById("artbox-next-button");
        this.previousButtonElement = document.getElementById("artbox-previous-button");
        this.counterElement = document.getElementById("artbox-counter");
        this.titleElementLink = this.titleElement.querySelector("a");
        //this.descriptionElement = document.getElementById("artbox-description");
        this.artworkList = [];
        this.artIndex = 0;
        this.totalArtworks = 0;
        this.navigationDisabled = true;

        this.nextButtonElement.addEventListener("click", this.onNextButtonClick.bind(this));
        this.previousButtonElement.addEventListener("click", this.onPreviousButtonClick.bind(this));
    }
    onNextButtonClick(e) {
        this.showNextArtwork();
    }
    onPreviousButtonClick(e) {
        this.showPreviousArtwork();
    }
    showCurrentArtwork() {
        this.showArtwork(this.artworkList[this.artIndex]);
    }
    showNextArtwork() {
        if (this.artIndex < this.totalArtworks - 1) {
            this.artIndex++;            
        } else {
            this.artIndex = 0;
        }
        this.showArtwork(this.artworkList[this.artIndex]);
    }
    showPreviousArtwork() {
        if (this.artIndex > 0) {
            this.artIndex--; 
        } else {
            this.artIndex = this.totalArtworks - 1;
        }        
        this.showArtwork(this.artworkList[this.artIndex]);
    }
    updateNavigation() {
        if (this.totalArtworks && this.navigationDisabled) {
             this.nextButtonElement.removeAttribute("disabled");
             this.previousButtonElement.removeAttribute("disabled");
             this.navigationDisabled = false;
        }
        this.counterElement.innerHTML = `${this.artIndex + 1}/${this.totalArtworks}`;
    }
    showArtwork(artwork) {
        this.imageElement.src = artwork.image;
        this.imageLinkElement.href = artwork.image;
        this.titleElementLink.innerHTML = artwork.title;
        this.titleElementLink.href = artwork.link;
        //TODO: description requires some parseing, there's all sorts of special characters coming through - should be done serverside
        //this.descriptionElement.innerHTML = artwork.description;
        this.updateNavigation();
    }
    async updateArtworkList() {
        let rawRSS = await this.fetchArtworks();
        this.artworkList = this.parseArtworks(rawRSS);
        this.totalArtworks = this.artworkList.length;
        this.updateNavigation();
    }
    parseArtworks(rawRSS) {
        let artworkList = [];
        let Parse = new DOMParser();
        let responseXML = Parse.parseFromString(rawRSS, "application/xml");
        let items = responseXML.getElementsByTagName("item");
        for (let item of items) {
            const title = item.querySelector('title').innerHTML;
            const link = item.querySelector('link').innerHTML;
            const date = item.querySelector('pubDate').innerHTML;
            const description = item.getElementsByTagName('media:description')[0].innerHTML;
            const image = item.getElementsByTagName('media:content')[0].getAttribute('url');
            let artwork = new Artwork(title, link, date, description, image);
            artworkList.push(artwork);
        }
        return artworkList;
    }
    async fetchArtworks() {
        return new Promise((resolve, reject) => {
            let request = new XMLHttpRequest();
            request.open('GET', "https://backend.deviantart.com/rss.xml?q=gallery:squidempire/62592859");
            request.onload = () => request.status === 200 ? resolve(request.response) : reject(Error(request.statusText));
            request.onerror = (e) => reject(Error(`Network Error: ${e}`));
            request.send();
        });
    }
}

async function setupArtbox() {
    globals.artbox = new Artbox();
    await globals.artbox.updateArtworkList();
    globals.artbox.showCurrentArtwork();
}

function loadSoundcloudIframes() {
    let soundCloudIframeContainer = document.getElementById("soundcloudIframes");
    let iframes = soundCloudIframeContainer.children;
    for (let i = 0; i < iframes.length; i++) {
        let iframe = iframes[i];
        iframe.src = iframe.dataset.src;
    }
}

function loadBandcampIframes() {
    let bandcampIframeContainer = document.getElementById("bandcampIframes");
    let iframes = bandcampIframeContainer.children;
    for (let i = 0; i < iframes.length; i++) {
        let iframe = iframes[i];
        iframe.src = iframe.dataset.src;
    }
}

function init() {
    globals.indexContent = document.getElementById(`content-index`);
    globals.router = new Router();
    window.location.hash = '#projects';
    initSmoke(); //from smoke.js
    setupArtbox();
}