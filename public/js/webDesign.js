'use: strict'

window.onload = function () { restyleAll(); };//setInterval(restyleAll, 5000); };

this.currentDupeElement = null;
this.duplicates = 0;

//we're going to set all the css values to something random every 5 seconds lol

function restyleAll() {
	var allElements = document.getElementsByTagName("*");
	for (var i=0, max=allElements.length; i<max; i++) {
		restyleElement(allElements[i]);
	}	
	restyleElement(allElements[0]);
}

function restyleElement(theElement) {
	var max=theElement.childNodes.length
	if (theElement.childNodes.length > 0) {
		for (var i=0; i < max; i++) {
			restyleElement(theElement.childNodes[i]);
		}
	}
	
	//console.log(theElement);
	if (theElement instanceof Element) {
		var styleString = '';
		styleString = "background-color: "+getRandomRGBA()+" !important; ";
		styleString = styleString + "color: "+getRandomRGB()+" !important; ";
		styleString = styleString + "width: "+(Math.floor((Math.random() * 99))+1)+"% !important; ";
		styleString = styleString + "height: "+(Math.floor((Math.random() * 99))+1)+"% !important; ";
		//styleString = styleString + "font-size: "+(Math.floor((Math.random() * 20))+90)+"% !important; ";
		
		styleString = styleString + "left: "+(Math.floor((Math.random() * 99))+1 - 20)+"% !important; ";
		styleString = styleString + "top: "+(Math.floor((Math.random() * 99))+1 - 20)+"% !important; ";
		
		if (Math.random() >= 0.5) {
			styleString = styleString + "postion: absolute; ";
		} else {
			styleString = styleString + "position: relative; ";
		}		
		
		if(theElement.tagName=="BODY") {
			styleString = styleString + "overflow:hidden; ";
		}
		
		theElement.setAttribute("style", styleString);	
		
		//maybe set this element to be duplicated?
		if (this.duplicates < 1000) {
			if (Math.random() >= 0.60) {
				this.currentDupeElement = theElement;
			}
			//duplicate the element into us
			if ((this.currentDupeElement != null) && (!this.currentDupeElement.contains(theElement))) {
				var newElement = this.currentDupeElement.cloneNode();
				theElement.appendChild(newElement);
				this.duplicates++;
			}
		}
		
	}	
}

function getRandomRGB() {
	var randomRGB = "rgb("+
	Math.floor((Math.random() * 255))+","+
	Math.floor((Math.random() * 255))+","+
	Math.floor((Math.random() * 255))+")";
	return randomRGB;	
}

function getRandomRGBA() {
	var randomRGB = "rgba("+
	Math.floor((Math.random() * 255))+","+
	Math.floor((Math.random() * 255))+","+
	Math.floor((Math.random() * 255))+","+
	(Math.random())+")";
	return randomRGB;	
}
