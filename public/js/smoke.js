var buttonCollection = [];

function onClick(e) {
    const smokeToMakePerSide = 4;
    let element = e.target;
    //right smonk
    let delay = 0;
    let direction = "right"
    for (let i = 0; i < smokeToMakePerSide*2; i++) {
        let randomClass = Math.floor(Math.random() * Math.floor(3)) + 1;
        if (i === smokeToMakePerSide) {
            //swap sides
            direction = "left"
            delay = 0;
        }
        let className = `smoke ${direction}${randomClass}`;
        makeSmoke(element, className, delay);
        delay += 0.05;
    }
}

function makeSmoke(element, className, delay) {
    let newSmoke = document.createElement('img');			
    newSmoke.className = className;
    newSmoke.src = './images/smoke.png'; //should look into loading this ONCE only (don't rely on cache)
    newSmoke.style.animationDelay = `${delay}s`;
    element.append(newSmoke);
    
    newSmoke.addEventListener("animationend", () => {
        newSmoke.style.opacity = 0;
        element.removeChild(newSmoke);
    });
}

function initSmoke() {
    buttonCollection = document.getElementsByClassName('smoke-button');
    for (let i = 0; i < buttonCollection.length; i++) {
        buttonCollection[i].addEventListener("click", onClick);
    }    
}